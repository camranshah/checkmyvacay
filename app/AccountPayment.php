<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Illuminate\Database\Eloquent\Model;

class AccountPayment extends Model
{
    use ChangeDateFormat;

    public $fillable = ['transaction_id', 'payment_through', 'promo_code_id', 'amount', 'user_id', 'start_date', 'end_date'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->with('profile');
    }

    public function promo_code(){
        return $this->belongsTo('App\PromoCode', 'promo_code_id')->withGlobalScope('not_expired');
    }
}
