<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ChangeDateFormat;

class ActivityLog extends Model
{
    use ChangeDateFormat;

    public $table = 'activity_log';

    public $fillable = ['user_id', 'title'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
