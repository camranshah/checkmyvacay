<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AircraftType extends Model
{
    protected $table = "aircraft_types";
    protected $fillable = ['aircraft_type', 'equipment_name'];
}
