<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Malhal\Geographical\Geographical;

class Airport extends Model
{
    const LATITUDE  = 'lat';
    const LONGITUDE = 'lon';

    use SoftDeletes, Geographical;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('sort', function (Builder $builder) {
            $builder->orderBy('sort', 'DESC');
        });
    }

    public $table = 'airports';

    public $fillable = ["icao", "iata", "name", "city", "state", "country", "elevation", "lat", "lon", "tz", "type", "sub_type"];
}
