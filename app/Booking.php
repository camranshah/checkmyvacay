<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use ChangeDateFormat;

    public $table = 'bookings';

    public $fillable = [
        'trip_id',
        'booking_id',
        'gds_pnr',
        'booking_pnr',
        'date_of_issue',
        'departure_datetime',
        'arrival_datetime',
        'fare',
        'discount',
        'promo_code_id',
        'flash_sale_id',
        'currency_code',
        'detail',
        'user_id',
        'no_of_traveller',
        'status'
    ];

    public $appends = ['flight_number', 'flight_details', 'new_departure_datetime', 'new_arrival_datetime'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function payment()
    {
        return $this->hasOne('App\BookingPayment', 'booking_id');
    }

    public function getDetailAttribute($value)
    {
        return unserialize($value);
    }

    public function getFlightNumberAttribute()
    {
        $flightNumber = '';

        if (isset($this->detail['AirReservation']['OriginDestinationOptions']['OriginDestinationOption']['FlightSegment'])) {
            $array = $this->detail['AirReservation']['OriginDestinationOptions']['OriginDestinationOption']['FlightSegment'];
        }

        if (isset($array[0]['@attributes']['FlightNumber'])) {
            $flightNumber = $array[0]['OperatingAirline']['@attributes']['Code'] . $array[0]['@attributes']['FlightNumber'];
        } elseif (isset($array['@attributes']['FlightNumber'])) {
            $flightNumber = $array['OperatingAirline']['@attributes']['Code'] . $array['@attributes']['FlightNumber'];
        }

        return $flightNumber;
    }

    public function getDepartureDatetimeAttribute($date)
    {
        if ($date):
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('m/d/Y H:i:s');
        endif;
    }

    public function getArrivalDatetimeAttribute($date)
    {
        if ($date):
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('m/d/Y H:i:s');
        endif;
    }

    public function getFlightDetailsAttribute()
    {
        $data = [];

	// dump($this->detail['AirReservation']['OriginDestinationOptions']['OriginDestinationOption']);

        $data['bookingId'] = $this->detail['AirReservation']['BookingReferenceID']['@attributes']['ID'];
        $data['bookingStatus'] = $this->detail['AirReservation']['BookingReferenceID']['@attributes']['BookingStatus'];

	if($this->detail['AirReservation']['OriginDestinationOptions']['OriginDestinationOption'])
        $data['totalStop'] = count($this->detail['AirReservation']['OriginDestinationOptions']['OriginDestinationOption'] ?? []);
	else
	    $data['totalStop'] = 0;

        $data['currencyCode'] = $this->detail['AirReservation']['ItinTotalFare']['TotalFare']['@attributes']['CurrencyCode'];
        $data['discount'] = $this->discount;

        $originDestinationOption = $this->detail['AirReservation']['OriginDestinationOptions']['OriginDestinationOption'];

        if (isset($originDestinationOption[0]['FlightSegment']['DepartureAirport']['@attributes']['LocationCode'])) {
            $data['originDepartureLocationCode'] = $originDestinationOption[0]['FlightSegment']['DepartureAirport']['@attributes']['LocationCode'];
        } elseif (isset($originDestinationOption[0]['FlightSegment'][0]['DepartureAirport']['@attributes']['LocationCode'])) {
            $data['originDepartureLocationCode'] = @$originDestinationOption[count($originDestinationOption)-1]['FlightSegment'][count($originDestinationOption[0]['FlightSegment'])-1]['DepartureAirport']['@attributes']['LocationCode'];
        } elseif (isset($originDestinationOption['FlightSegment'][0]['DepartureAirport']['@attributes']['LocationCode'])) {
            $data['originDepartureLocationCode'] = $originDestinationOption['FlightSegment'][0]['DepartureAirport']['@attributes']['LocationCode'];
        } else {
            $data['originDepartureLocationCode']  = $originDestinationOption['FlightSegment']['DepartureAirport']['@attributes']['LocationCode'];
        }

        if (isset($originDestinationOption[0]['FlightSegment']['@attributes']['DepartureDateTime'])) {
            $data['originDepartureDateTime'] = $originDestinationOption[0]['FlightSegment']['@attributes']['DepartureDateTime'];
        } elseif (isset($originDestinationOption[0]['FlightSegment'][0]['@attributes']['DepartureDateTime'])) {
            $data['originDepartureDateTime'] = $originDestinationOption[0]['FlightSegment'][0]['@attributes']['DepartureDateTime'];
        } elseif (isset($originDestinationOption['FlightSegment'][0]['@attributes']['DepartureDateTime'])) {
            $data['originDepartureDateTime'] = $originDestinationOption['FlightSegment'][0]['@attributes']['DepartureDateTime'];
        } else {
            $data['originDepartureDateTime'] = $originDestinationOption['FlightSegment']['@attributes']['DepartureDateTime'];
        }

        if (isset($originDestinationOption[$data['totalStop'] - 1]['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode'])) {
            $data['originArrivalLocationCode'] = $originDestinationOption[$data['totalStop'] - 1]['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode'];
        } elseif (isset($originDestinationOption[count($originDestinationOption ?? []) -1]['FlightSegment'][0]['ArrivalAirport']['@attributes']['LocationCode'])) {
            $data['originArrivalLocationCode'] = $originDestinationOption[count($originDestinationOption ?? []) -1]['FlightSegment'][0]['ArrivalAirport']['@attributes']['LocationCode'];
        } elseif (isset($originDestinationOption['FlightSegment'][$data['totalStop'] - 1]['ArrivalAirport']['@attributes']['LocationCode'])) {
            $data['originArrivalLocationCode'] = $originDestinationOption['FlightSegment'][$data['totalStop'] - 1]['ArrivalAirport']['@attributes']['LocationCode'];
        } else {
            $data['originArrivalLocationCode'] = $originDestinationOption['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode'];
        }

        if (isset($originDestinationOption[$data['totalStop'] - 1]['FlightSegment']['@attributes']['ArrivalDateTime'])) {
            $data['originArrivalDateTime'] = $originDestinationOption[$data['totalStop'] - 1]['FlightSegment']['@attributes']['ArrivalDateTime'];
        } elseif (isset($originDestinationOption[$data['totalStop'] - 1]['FlightSegment'][$data['totalStop'] - 1]['@attributes']['ArrivalDateTime'])) {
            $data['originArrivalDateTime'] = $originDestinationOption[$data['totalStop'] - 1]['FlightSegment'][$data['totalStop'] - 1]['@attributes']['ArrivalDateTime'];
        } elseif (isset($originDestinationOption['FlightSegment'][$data['totalStop'] - 1]['@attributes']['ArrivalDateTime'])) {
            $data['originArrivalDateTime'] = $originDestinationOption['FlightSegment'][$data['totalStop'] - 1]['@attributes']['ArrivalDateTime'];
        } else {
            $data['originArrivalDateTime'] = $originDestinationOption['FlightSegment']['@attributes']['ArrivalDateTime'];
        }

        if (isset($originDestinationOption[0]['FlightSegment']['OperatingAirline']['@attributes']['Code'])) {
            $data['originDepartureAirlineCode'] = $originDestinationOption[0]['FlightSegment']['OperatingAirline']['@attributes']['Code'];
        } elseif (isset($originDestinationOption[0]['FlightSegment'][0]['OperatingAirline']['@attributes']['Code'])) {
            $data['originDepartureAirlineCode'] = $originDestinationOption[0]['FlightSegment'][0]['OperatingAirline']['@attributes']['Code'];
        } elseif (isset($originDestinationOption['FlightSegment'][0]['OperatingAirline']['@attributes']['Code'])) {
            $data['originDepartureAirlineCode'] = $originDestinationOption['FlightSegment'][0]['OperatingAirline']['@attributes']['Code'];
        } else {
            $data['originDepartureAirlineCode'] = $originDestinationOption['FlightSegment']['OperatingAirline']['@attributes']['Code'];
        }

        if (isset($originDestinationOption[0]['FlightSegment']['@attributes']['FlightNumber'])) {
            $data['originDepartureFlightNo'] = $originDestinationOption[0]['FlightSegment']['@attributes']['FlightNumber'];
        } elseif (isset($originDestinationOption[0]['FlightSegment'][0]['@attributes']['FlightNumber'])) {
            $data['originDepartureFlightNo'] = $originDestinationOption[0]['FlightSegment'][0]['@attributes']['FlightNumber'];
        } elseif (isset($originDestinationOption['FlightSegment'][0]['@attributes']['FlightNumber'])) {
            $data['originDepartureFlightNo'] = $originDestinationOption['FlightSegment'][0]['@attributes']['FlightNumber'];
        } else {
            $data['originDepartureFlightNo'] = $originDestinationOption['FlightSegment']['@attributes']['FlightNumber'];
        }

        $data['totalFlightAmount'] = ($this->fare - $this->discount);

        $airTravelers = $this->detail['AirReservation']['TravelerInfo']['AirTraveler'];
        $data['airTravelers'] = [];

        if (isset($airTravelers[0])) {
            foreach ($airTravelers as $info) {
                $data['airTravelers'][] = [
                    'name' => $info['PersonName']['NamePrefix'] . '. ' . $info['PersonName']['GivenName'] . ' ' . $info['PersonName']['Surname'],
                    'travelerId' => $info['PersonName']['TravelerId'],
                    'ticketDocumentNo' => $info['TicketDocument']['@attributes']['TicketDocumentNbr'],
                    'ticketDocumentType' => $info['TicketDocument']['@attributes']['Type'],
                    'ticketDocumentDateOfIssue' => $info['TicketDocument']['@attributes']['DateOfIssue']
                ];
            }
        } else {
            $data['airTravelers'][] = [
                'name' => $airTravelers['PersonName']['NamePrefix'] . '. ' . $airTravelers['PersonName']['GivenName'] . ' ' . $airTravelers['PersonName']['Surname'],
                'travelerId' => $airTravelers['PersonName']['TravelerId'],
                'ticketDocumentNo' => $airTravelers['TicketDocument']['@attributes']['TicketDocumentNbr'],
                'ticketDocumentType' => $airTravelers['TicketDocument']['@attributes']['Type'],
                'ticketDocumentDateOfIssue' => $airTravelers['TicketDocument']['@attributes']['DateOfIssue']
            ];
        }

        $data['segments'] = [];
        if (isset($originDestinationOption[0]['FlightSegment'][0])) {
            foreach ($originDestinationOption as $key):
                foreach ($key['FlightSegment'] as $info):
                    $data['segments'][] = [
                        'operatingAirlineCode' => @$info['OperatingAirline']['@attributes']['Code'],
                        'flightNo' => @$info['@attributes']['FlightNumber'],
                        'baggageAllowance' => @$info['BaggageAllowance'],
                        'air_equip_type' => @$info['Equipment']['@attributes']['AirEquipType'],
                        'air_equip_name'=> @getAirEquipmentName($info['Equipment']['@attributes']['AirEquipType'])->equipment_name,
                        'departureAirportLocationCode' => @$info['DepartureAirport']['@attributes']['LocationCode'],
                        'departureDateTime' => @$info['@attributes']['DepartureDateTime'],
                        'duration' => @convertToHoursMins($info['@attributes']['Duration']),
                        'arrivalAirportLocationCode' => @$info['ArrivalAirport']['@attributes']['LocationCode'],
                        'arrivalDateTime' => @$info['@attributes']['ArrivalDateTime'],
                    ];
                endforeach;
            endforeach;

        } else if (isset($originDestinationOption[0])) {
            foreach ($originDestinationOption as $info):
                $data['segments'][] = [
                    'operatingAirlineCode' => @$info['FlightSegment']['OperatingAirline']['@attributes']['Code'],
                    'flightNo' => @$info['FlightSegment']['@attributes']['FlightNumber'],
                    'baggageAllowance' => @$info['FlightSegment']['BaggageAllowance'],
                    'air_equip_type' => @$info['FlightSegment']['Equipment']['@attributes']['AirEquipType'],
                    'air_equip_name'=> @getAirEquipmentName($info['FlightSegment']['Equipment']['@attributes']['AirEquipType'])->equipment_name,
                    'departureAirportLocationCode' => @$info['FlightSegment']['DepartureAirport']['@attributes']['LocationCode'],
                    'departureDateTime' => @$info['FlightSegment']['@attributes']['DepartureDateTime'],
                    'duration' => @convertToHoursMins($info['FlightSegment']['@attributes']['Duration']),
                    'arrivalAirportLocationCode' => @$info['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode'],
                    'arrivalDateTime' => @$info['FlightSegment']['@attributes']['ArrivalDateTime'],
                ];
            endforeach;
        } else if (isset($originDestinationOption['FlightSegment'][0])) {

            foreach ($originDestinationOption['FlightSegment'] as $info):
                $data['segments'][] = [
                    'operatingAirlineCode' => @$info['OperatingAirline']['@attributes']['Code'],
                    'flightNo' => @$info['@attributes']['FlightNumber'],
                    'baggageAllowance' => @$info['BaggageAllowance'],
                    'air_equip_type' => @$info['Equipment']['@attributes']['AirEquipType'],
                    'air_equip_name'=> @getAirEquipmentName($info['Equipment']['@attributes']['AirEquipType'])->equipment_name,
                    'departureAirportLocationCode' => $info['DepartureAirport']['@attributes']['LocationCode'],
                    'departureDateTime' => $info['@attributes']['DepartureDateTime'],
                    'duration' => @convertToHoursMins(@$info['@attributes']['Duration']),
                    'arrivalAirportLocationCode' => @$info['ArrivalAirport']['@attributes']['LocationCode'],
                    'arrivalDateTime' => @$info['@attributes']['ArrivalDateTime'],
                ];
            endforeach;
        } else {
            $data['segments'][] = [
                'operatingAirlineCode' => @$originDestinationOption['FlightSegment']['OperatingAirline']['@attributes']['Code'],
                'flightNo' => @$originDestinationOption['FlightSegment']['@attributes']['FlightNumber'],
                'baggageAllowance' => @$originDestinationOption['FlightSegment']['BaggageAllowance'],
                'air_equip_type' => @$originDestinationOption['FlightSegment']['Equipment']['@attributes']['AirEquipType'],
                'air_equip_name'=> @getAirEquipmentName($originDestinationOption['FlightSegment']['Equipment']['@attributes']['AirEquipType'])->equipment_name,
                'departureAirportLocationCode' => @$originDestinationOption['FlightSegment']['DepartureAirport']['@attributes']['LocationCode'],
                'departureDateTime' => @$originDestinationOption['FlightSegment']['@attributes']['DepartureDateTime'],
                'duration' => convertToHoursMins($originDestinationOption['FlightSegment']['@attributes']['Duration']),
                'arrivalAirportLocationCode' => @$originDestinationOption['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode'],
                'arrivalDateTime' => @$originDestinationOption['FlightSegment']['@attributes']['ArrivalDateTime'],
            ];
        }

        $paxTypeFares = $this->detail['AirReservation']['TPA_Extensions']['PaxTypeFare'];

        $data['amountDetails'] = [];

        if (isset($paxTypeFares[0])) {
            foreach ($paxTypeFares as $info):
                $data['amountDetails'][] = [
                    'qty' => $info['Pax']['@attributes']['Quantity'],
                    'type' => $info['Pax']['@attributes']['Type'],
                    'currencyCode' => $info['BaseFare']['@attributes']['CurrencyCode'],
                    'baseFareAmount' => $info['BaseFare']['@attributes']['Amount'],
                    'totalTaxAmount' => $info['TotalTax']['@attributes']['Amount'],
                    'totalFareAmount' => $info['TotalFare']['@attributes']['Amount']

                ];
            endforeach;
        } else {
            $data['amountDetails'][] = [
                'qty' => $paxTypeFares['Pax']['@attributes']['Quantity'],
                'type' => $paxTypeFares['Pax']['@attributes']['Type'],
                'currencyCode' => $paxTypeFares['BaseFare']['@attributes']['CurrencyCode'],
                'baseFareAmount' => $paxTypeFares['BaseFare']['@attributes']['Amount'],
                'totalTaxAmount' => $paxTypeFares['TotalTax']['@attributes']['Amount'],
                'totalFareAmount' => $paxTypeFares['TotalFare']['@attributes']['Amount']

            ];
        }

        $data['airportDepartureTop'] = getAirportDetails($data['originDepartureLocationCode']);
        $data['airportArrivalTop'] = getAirportDetails($data['originArrivalLocationCode']);
        return $data;
    }

    public function getNewDepartureDatetimeAttribute()
    {
        return @$this->flight_details['originDepartureDateTime'];
    }

    public function getNewArrivalDatetimeAttribute()
    {
        return @$this->flight_details['originArrivalDateTime'];
    }

    public function session(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(\App\FlightSession::class, 'booking_id');
    }
}
