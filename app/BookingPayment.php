<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Illuminate\Database\Eloquent\Model;

class BookingPayment extends Model
{
    use ChangeDateFormat;

    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }
}
