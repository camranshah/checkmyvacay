<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientApiLog extends Model
{
    public $table = 'client_api_logs';
}
