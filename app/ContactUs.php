<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactUs extends Model
{
    use SoftDeletes, ChangeDateFormat;

    public $table = 'contact_us';

    public $fillable = ['name', 'email', 'subject', 'message'];
}
