<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Notification;
use App\User;

class NotificationEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $title;
    public $description;
    public $type;
    public $typeId;
    public $url;
    public $notification;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $title, $description, $type, $typeId, $url, $push = false)
    {

        $this->user = User::find($userId);

        $this->title = $title;
        $this->description = $description;
        $this->type = $type;
        $this->typeId = $typeId;
        $this->url = $url;

        $this->notification = new Notification();
        $this->notification->user_id = $userId;
        $this->notification->title = $title;
        $this->notification->description = $description;
        $this->notification->type = $type;
        $this->notification->type_id = $typeId;
        $this->notification->url = $url;
        $this->notification->is_read = 0;
        $this->notification->save();

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('notification.'.$this->user->id);
    }

    public function broadcastWith(){

        $Notifications = Notification::whereUserId($this->user->id)->defaultTake()->get();
        $NotificationUnreadCount = Notification::whereUserId($this->user->id)->unread()->count();

        if($this->user->user_role->role_id == 1){

            $data['GlobalData']['Notifications'] = $Notifications;
            $data['GlobalData']['NotificationUnreadCount'] = $NotificationUnreadCount;

            $status = 'success';

            if ($data['GlobalData']['NotificationUnreadCount'] == 0) {
                $status = 'fail';
            }

            return ['view' => view('includes.notifications', $data)->render(), 'status' => $status];

        }else{
            return ['notification_details' => $this->notification, 'unread_count' => $NotificationUnreadCount];
        }
    }


}
