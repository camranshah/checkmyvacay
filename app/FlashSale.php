<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class FlashSale extends Model
{
    use SoftDeletes, ChangeDateFormat;

    protected $with = ['locations', 'intervals'];

    public $fillable = ['title', 'image','expiry_date', 'url', 'discount_type', 'discount_value', 'tour_details', 'what_to_expect', 'departure_details', 'status', 'type'];

    public $appends = ['remaining_time'];

    protected $dates = ['expiry_date'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('not_expired', function (Builder $builder) {
//            $builder->where('expiry_date', '>', date('Y-m-d'));
        });

    }

    public function getImageAttribute($value)
    {
        return str_replace('/api','', URL::to(Storage::url('public/images/' . $value)));
    }

    public function locations()
    {
        return $this->hasMany('App\FlashSaleLocation', 'flash_sale_id');
    }

    public function intervals()
    {
        return $this->hasMany('App\FlashSaleInterval', 'flash_sale_id');
    }

    public function setStatusAttribute($value)
    {
        switch ($value):
            case 'active':
                $bool = 1;
                break;
            case 'inactive':
                $bool = 0;
                break;
            default:
                $bool = 1;
                break;
        endswitch;

        $this->attributes['status'] = $bool;
    }

    public function setExpiryDateAttribute($value)
    {
        $this->attributes['expiry_date'] = date('Y-m-d', strtotime($value));
    }

//    public function getExpiryDateAttribute($date)
//    {
//        return @Carbon::createFromFormat('Y-m-d', $date)->format('m/d/Y');
//    }

    public function getRemainingTimeAttribute(){
        $expDate = Carbon::parse($this->expiry_date);
        return Carbon::now()->diffAsCarbonInterval($expDate);
    }



}
