<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class FlashSaleInterval extends Model
{
    use SoftDeletes, ChangeDateFormat;

    public $fillable = ['flash_sale_id', 'departure_date_1', 'departure_date_2', 'arrival_date_1', 'arrival_date_2'];

    public function setDepartureDate1Attribute($value)
    {
        $this->attributes['departure_date_1'] = date('Y-m-d', strtotime($value));
    }

    public function setDepartureDate2Attribute($value)
    {
        $this->attributes['departure_date_2'] = date('Y-m-d', strtotime($value));
    }

    public function setArrivalDate1Attribute($value)
    {
        $this->attributes['arrival_date_1'] = date('Y-m-d', strtotime($value));
    }

    public function setArrivalDate2Attribute($value)
    {
        $this->attributes['arrival_date_2'] = date('Y-m-d', strtotime($value));
    }

}
