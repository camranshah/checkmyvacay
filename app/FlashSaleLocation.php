<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FlashSaleLocation extends Model
{
    use SoftDeletes;

    protected $with = ['airport'];

    public $fillable = ['flash_sale_id', 'airport_id', 'type'];

    public function airport(){
        return $this->belongsTo('App\Airport', 'airport_id');
    }
}
