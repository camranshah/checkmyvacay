<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Booking;

class FlightSession extends Model
{
    protected $fillable = [
        'session_id',
        'search_params',
        'search_response',
        'provision_params',
        'provision_response',
        'booking_params',
        'booking_response',
        'booking_id',
    ];

    protected $casts = [
        'search_params' => 'json',
        'search_response' => 'json',
        'provision_params' => 'json',
        'provision_response' => 'json',
        'booking_params' => 'json',
        'booking_response' => 'json',
    ];

    public function booking(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Booking::class);
    }
}
