<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FrequentFlyer extends Model
{
    use SoftDeletes, ChangeDateFormat;

    public $table = 'frequent_flyers';

    public $fillable = [
        'user_id',
        'traveller_type',
        'name_prefix',
        'name',
        'surname',
        'phone',
        'doc_issue_location',
        'doc_id',
        'gender',
        'nationality',
        'date_of_birth',
        'expiry_date',
        'is_default',
        'extra_attribute_1',
        'extra_attribute_2',
        'extra_attribute_3',
        'extra_attribute_4',
        'extra_attribute_5'
    ];

    public $hidden = ['extra_attribute_1', 'extra_attribute_2', 'extra_attribute_3', 'extra_attribute_4', 'extra_attribute_5'];

    protected $dates = ['expiry_date', 'date_of_birth'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getDateOfBirthAttribute($date)
    {
        return date('m/d/Y', strtotime($date));
    }

    public function getExpiryDateAttribute($date)
    {
        if ($date):
            return date('m/d/Y', strtotime($date));
        endif;

        return "";
    }

    public function setDateOfBirthAttribute($date)
    {
        $this->attributes['date_of_birth'] = date('Y-m-d', strtotime($date));
    }

    public function setExpiryDateAttribute($date)
    {
        $this->attributes['expiry_date'] = date('Y-m-d', strtotime($date));
    }


}
