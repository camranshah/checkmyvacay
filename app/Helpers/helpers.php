<?php

function random_strings($length_of_string)
{

    // String of all alphanumeric character
    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    // Shufle the $str_result and returns substring
    // of specified length
    return substr(str_shuffle($str_result),
        0, $length_of_string);
}

function getNotifications()
{
    $notification['all'] = \App\Notification::whereReciever('admin')->orderBy('id', 'DESC')->get();
    $notification['new'] = \App\Notification::whereRecieverAndStatus('admin', '1')->count();
    return $notification;
}

function convertToHoursMins($time, $format = '%02dh %02dm')
{
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

function get_passenger_text($flyer, $count = 1)
{
    if($flyer === 'ADT'){
        return "Adult $count - 12 years and older";
    }
    if($flyer === 'CHD'){
        return "Child $count - 2-11 years old";
    }
    if($flyer === 'INF'){
        return "Infant $count - Under 2 years";
    }
    return '';
}

function getAirlineName($iata)
{
    $airline = \App\Airlines::select('name')->where('iata', $iata)->first();

    if($airline){
        return $airline;
    }

    return optional();
    return collect([]);
}

function getAirportDetails($iata)
{
    $airport = \App\Airport::where('iata', $iata)->first();

    if($airport){
        return $airport;
    }

    return collect([]);
}

function getRandom4Digit()
{
    $digits = 4;
    return rand(pow(10, $digits - 1), pow(10, $digits) - 1);
}

function getAirEquipmentName($aircraftType)
{
    $aircraftType = \App\AircraftType::whereAircraftType($aircraftType)->first();

    if($aircraftType){
        return $aircraftType;
    }

    return [];
}


function minutes_to_minutes_and_hours($minutes){
    $d = floor ($minutes / 1440);
    $h = floor (($minutes - $d * 1440) / 60);
    $m = $minutes - ($d * 1440) - ($h * 60);

    $days = $d > 0? "$d days,": '';
    return "$days $h hrs, $m mins";
}