<?php

namespace App\Http\Controllers;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\Login;
use App\Soap\Response\LoginResponse;

class SoapController extends Controller
{
    protected $soapWrapper;

    /**
     * SoapController constructor.
     *
     * @param SoapWrapper $soapWrapper
     */
    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
    }

    /**
     * Use the SoapWrapper
     */
    public function show()
    {
        $this->soapWrapper->add('Login', function ($service) {
            $service
                ->wsdl('https://cmf.provesio.com/axis2/services/ProvesioGateway.ProvesioGatewayHttpSoap11Endpoint/')
                ->trace(true)
                ->classmap([
                    Login::class,
                    LoginResponse::class,
                ]);
        });

//        // Without classmap
//        $response = $this->soapWrapper->call('Login.GetLoginResponse', [
//            'CurrencyFrom' => 'USD',
//            'CurrencyTo'   => 'EUR',
//            'RateDate'     => '2014-06-05',
//            'Amount'       => '1000',
//        ]);
//
//        var_dump($response);

        // With classmap
        $response = $this->soapWrapper->call('Login.GetLoginResponse', [
            new Login('otatest@test.com', 'otatest@123', 'VA00049')
        ]);

        var_dump($response);
        exit;
    }
}
