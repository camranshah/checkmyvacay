<?php

namespace App\Http\Controllers\admin;

use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{
    public function view(Request $request,$id = 0){
        if($id == 0){
            $bookings = Booking::with(['user','payment'])->paginate(10);
            return view('booking.view')->with(['bookings' => $bookings]);
        }else{
            $booking = Booking::with('user')->whereId($id)->first();
            return view('booking.single')->with(['booking' => $booking]);
        }
    }
}
