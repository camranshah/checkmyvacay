<?php

namespace App\Http\Controllers\admin;

use App\Commission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommissionController extends Controller
{
    public function commission(Request $request){
        $commissions = Commission::orderBy('id','DESC')->get();
        $currentCommission = Commission::whereStatus(1)->first();

        if(empty($currentCommission)){
            $currentCommission = 0;
        }else{
            $currentCommission = $currentCommission->percentage;
        }

        if($request->method() == "POST"){
            $validator = Validator::make($request->all(), [
                'percentage'  => 'required|between:1,100',
            ],[
                'password.required'     => 'Please provide password',
                'password.min'          => 'Password must be 8 characters long',
                'password.confirmed'    => 'Password mismatch',
            ]);


            if ($validator->fails()) {
                return back()
                    ->withErrors($validator,'create_commission')
                    ->withInput();
            }

            if($request->percentage == $currentCommission){
                return back()->with(['type' => 'danger','msg' => 'You haven\'t change the commission percentage']);
            }
            Commission::whereStatus(1)->update(['status' => '0']);

            $commission = new Commission;
            $commission->percentage = $request->percentage;
            if($commission->save()){
                return back()->with(['type' => 'success','msg' => 'Commission updated successfully']);
            }else{
                return back()->with(['type' => 'danger','msg' => 'Something went wrong']);
            }
        }

        return view('commission.view')->with(['commissions' => $commissions,'currentCommission' => $currentCommission]);
    }
}
