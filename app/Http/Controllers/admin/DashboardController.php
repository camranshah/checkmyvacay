<?php

namespace App\Http\Controllers\admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{
    public function index(Request $request){
        return view('dashboard.index');
    }

    public function logout(Request $request){
        Auth::logout();
        return Redirect::route('home');
    }

    public function changePassword(Request $request){
        if($request->method() == "POST"){
            $validator = Validator::make($request->all(), [
                'old_password'  => 'required|min:8',
                'password'      => 'required|confirmed|min:8',
            ],[
                'old_password.required' => 'Please provide current password',
                'old_password.min'      => 'Password must be 8 characters long',
                'password.required'     => 'Please provide new password',
                'password.min'          => 'Password must be 8 characters long',
                'password.confirmed'    => 'Password mismatch',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator,'change_password')
                    ->withInput();
            }
            $user = User::whereId(Auth::user()->id)->first();
            if(Hash::check($request->old_password,$user->password)){
                $user->password = Hash::make($request->password);
                if($user->save()){
                    return back()->with(['type' => 'success','msg' => 'Password changed successfully']);
                }else{
                    return back()->with(['type' => 'danger','msg' => 'Something went wrong']);
                }
            }else{
                return back()->with(['type' => 'danger','msg' => 'Invalid old password']);
            }
        }
        return view('setting.changepassword');
    }

    public function editProfile(Request $request){
        $user = User::whereId(Auth::user()->id)->first();
        if($request->method() == "POST"){
            $validator = Validator::make($request->all(), [
                'name'          => 'required',
                'email'         => 'required|unique:users,email,'.Auth::user()->id,
            ],[
                'name.required'     => 'Please provide name',
                'email.required'    => 'Please provide email',
                'email.unique'      => 'Email already registered',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator,'edit_profile')
                    ->withInput();
            }

            $user->name     = $request->name;
            $user->email    = $request->email;

            if($user->save()){
                return back()->with(['type' => 'success','msg' => 'Profile updated successfully']);
            }else{
                return back()->with(['type' => 'danger','msg' => 'Something went wrong']);
            }

        }
        return view('setting.edit-profile')->with(['user' => $user]);
    }
}
