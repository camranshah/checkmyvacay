<?php

namespace App\Http\Controllers\admin;

use App\ExchangeRate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ExchangeRateController extends Controller
{
    public function view(Request $request){
        $exchangeRates = ExchangeRate::get();
        return view('exchange-rate.view')->with(['exchangeRates' => $exchangeRates]);
    }

    public function add(Request $request){
        if($request->method() == "POST"){
            $validator = Validator::make($request->all(), [
                'currency_code'     => 'required|unique:exchange_rates',
                'exchange_rate'     => 'required',
            ],[
                'currency_code.required'    => 'Please provide currency code',
                'currency_code.unique'       => 'Currency code already inserted',
                'exchange_rate.required'      => 'Please provide exchange rate'
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator,'create_exchange_rate')
                    ->withInput();
            }
            $exchangeRate = new ExchangeRate;
            $exchangeRate->currency_code = $request->currency_code;
            $exchangeRate->exchange_rate = $request->exchange_rate;
            if($exchangeRate->save()){
                return back()->with(['type' => 'success','msg' => 'Exchange rate added successfully']);
            }else{
                return back()->with(['type' => 'danger','msg' => 'Something went wrong']);
            }
        }
        return view('exchange-rate.add');
    }

    public function update(Request $request,$id){
        $exchangeRate = ExchangeRate::whereId($id)->first();
        if($request->method() == "POST"){
            $validator = Validator::make($request->all(), [
                'currency_code'     => 'required|unique:exchange_rates,id,'.$id,
                'exchange_rate'     => 'required',
            ],[
                'currency_code.required'    => 'Please provide currency code',
                'currency_code.unique'       => 'Currency code already inserted',
                'exchange_rate.required'      => 'Please provide exchange rate'
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator,'create_exchange_rate')
                    ->withInput();
            }


            $exchangeRate->currency_code = $request->currency_code;
            $exchangeRate->exchange_rate = $request->exchange_rate;
            if($exchangeRate->save()){
                return back()->with(['type' => 'success','msg' => 'Exchange rate added successfully']);
            }else{
                return back()->with(['type' => 'danger','msg' => 'Something went wrong']);
            }
        }
        return view('exchange-rate.update')->with(['exchangeRate' => $exchangeRate]);
    }
}
