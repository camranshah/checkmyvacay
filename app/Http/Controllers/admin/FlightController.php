<?php

namespace App\Http\Controllers\admin;

use App\AccountPayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FlightController extends Controller
{
    public function accountPayments(Request $request){
        $accounts = AccountPayment::get()->groupBy('user_id');
        return view('payment.account')->with(['accounts' => $accounts]);
    }
}
