<?php

namespace App\Http\Controllers\admin;

use App\PasswordReset;
use App\User;
use App\UserRelationRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function login(Request $request){
        $this->isLogin();
        if($request->method() == "POST"){
            $validator = Validator::make($request->all(), [
                'email'     => 'required|email',
                'password'  => 'required|min:8',
            ],[
                'email.required'    => 'Please provide email',
                'email.email'       => 'Please provide valid email',
                'password.required' => 'Please provide password',
                'password.min'      => 'Password must be 8 characters long',
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator,'login')
                    ->withInput();
            }
            $user = User::whereEmail($request->email)->first();
            if(!empty($user)){
                $role = UserRelationRole::with('role')->whereUserId($user->id)->first();
                if($role->role['role'] == "admin"){
                    if(Hash::check($request->password,$user->password)){
                        $credentials = ['email' => $request->email,'password' => $request->password];
                        if(Auth::attempt($credentials)){
                            return Redirect::route('dashboard');
                        }
                    }else{
                        return back()->withInput($request->except('password'))->with(['type' => 'danger','msg' => 'Invalid Password']);
                    }
                }else{
                    return back()->withInput($request->except('password'))->with(['type' => 'danger','msg' => 'Only admin can login from this portal']);
                }
            }else{
                return back()->withInput($request->except('password'))->with(['type' => 'danger','msg' => 'Invalid Email']);
            }
        }
        return view('login');
    }

    public function forgetPassword(Request $request){
        $this->isLogin();
        if($request->method() == "POST"){
            $validator = Validator::make($request->all(), [
                'email'     => 'required|email|exists:users,email',
            ],[
                'email.required'    => 'Please provide email',
                'email.email'       => 'Please provide valid email',
                'email.exists'       => 'Email not found'
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator,'forget_password')
                    ->withInput();
            }

            $reset = PasswordReset::whereEmail($request->email)->first();
            if(empty($reset)){
                $token = random_strings(16);
                $reset = new PasswordReset;
                $reset->email = $request->email;
                $reset->token = $token;
                $reset->save();
            }else{
                $token = $reset->token;
            }

            if($reset->save()){
                $data = [
                    'link'          => route('recover.password',$token),
                    'from'          => 'cmv@admin.com',
                    'to'            => $request->email,
                    'subject'       => 'Password Recovery',
                    'sender_name'   => 'CMV',
                    'token'         => $token,
                ];

                Mail::send('email.forget-password', ['data' => $data], function ($message) use ($data) {
                    $message->from($data['from'], $data['sender_name']);
                    $message->to($data['to'])->subject($data['subject']);
                });

                return back()->withInput()->with(['type' => 'success','msg' => 'Password containg reset token sent on your email address']);
            }

        }
        return view('forget-password');
    }

    public function recoverPassword(Request $request, $token){
        $reset = PasswordReset::whereToken($token)->first();
        if(empty($reset)){
            return Redirect::route('forget.password')->withInput()->with(['type' => 'danger','msg' => 'Token not found, Please try again']);
        }else{
            if($request->method() == "POST"){
                $validator = Validator::make($request->all(), [
                    'password'  => 'required|confirmed|min:8',
                ],[
                    'password.required'     => 'Please provide password',
                    'password.min'          => 'Password must be 8 characters long',
                    'password.confirmed'    => 'Password mismatch',
                ]);


                if ($validator->fails()) {
                    return back()
                        ->withErrors($validator,'recover_password')
                        ->withInput();
                }

                $user = User::whereEmail($reset->email)->first();
                $user->password = Hash::make($request->password);
                if($user->save()){
                    PasswordReset::whereToken($token)->delete();
                    return Redirect::route('home')->with(['type' => 'success','msg' => 'Password reset successfully']);
                }else{
                    return back()->with(['type' => 'danger','msg' => 'Something went wrong']);
                }
            }
            return view('recover-password')->with(['token' => $token]);
        }
    }

    public function isLogin(){
        if(Auth::check()){
            return Redirect::route('dashboard')->send();
        }
    }
}
