<?php

namespace App\Http\Controllers\admin;

use App\AccountPayment;
use App\BookingPayment;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function accountPayments(Request $request){
        $accounts = AccountPayment::with('user')->get()->groupBy('user_id');
        return view('payment.account')->with(['accounts' => $accounts]);
    }

    public function bookingPayments(Request $request){
        $bookings = BookingPayment::with('booking')->orderBy('id','DESC')->get();
        return view('payment.booking')->with(['bookings' => $bookings]);
    }
}
