<?php

namespace App\Http\Controllers\admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function view(Request $request,$id = 0){
        $users = User::with('profile')
            ->join('user_relation_roles','user_relation_roles.user_id','=','users.id')
            ->where('user_relation_roles.role_id','2')
            ->get(['users.*']);
        return view('user.view')->with(['users' => $users]);
    }

    public function changeStatus($id){
        $user = User::whereId($id)->first();
        if($user->status == 0){
            $user->status = 1;
        }else{
            $user->status = 0;
        }

        if($user->save()){
            return back()->with(['type' => 'success','msg' => 'User status updated successfully']);
        }else{
            return back()->with(['type' => 'danger','msg' => 'Something went wrong']);
        }
    }
}
