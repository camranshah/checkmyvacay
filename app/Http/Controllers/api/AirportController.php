<?php

namespace App\Http\Controllers\api;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Airport;
use Illuminate\Support\Facades\DB;

class AirportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $airport = new Airport();
        $take = ($request->take) ? $request->take : 100;
        $skip = ($request->skip) ? $request->skip : 0;

        if (strlen($request->keyword) === 3) {
            $airport = $airport->where(function ($query) use ($request) {
                $query->Where('iata', 'like', '%' . $request->keyword . '%');
            });
        }elseif ($request->keyword) {
            $airport = $airport->where(function ($query) use ($request) {
                $query->where('city', 'like', '%' . $request->keyword . '%')
                    ->orWhere('country', 'like', '%' . $request->keyword . '%')
                    ->orWhere('name', 'like', '%' . $request->keyword . '%')
                    ->orWhere('icao', 'like', '%' . $request->keyword . '%');
            });
        }

        if ($request->city) {
            $airport = $airport->where('city', 'like', '%' . $request->city . '%');
        }
        if ($request->country) {
            $airport = $airport->where('country', 'like', '%' . $request->country . '%');
        }
        if ($request->name) {
            $airport = $airport->where('name', 'like', '%' . $request->name . '%');
        }
        if ($request->iata) {
            $airport = $airport->where('iata', 'like', '%' . $request->iata . '%');
        }
        if ($request->icao) {
            $airport = $airport->where('icao', 'like', '%' . $request->icao . '%');
        }

        if ($request->latitude && $request->longitude) {
            $airport = $airport->where('lat', '!=', null);
            $airport = $airport->where('lon', '!=', null);
            $airport = $airport->distance($request->latitude, $request->longitude);
            $airport = $airport->orderBy('distance', 'ASC');
        }

        if ($request->type) {
            $airport = $airport->where('type', $request->type);
        }

        if ($request->sub_type) {
            $airport = $airport->where('sub_type', $request->sub_type);
        }

        if($request->fly_now){
            $airport = $airport->where('fly_now', 'Y');
        }

        return $airport
                            ->where('iata', '!=', null)
                            ->orderBy('sort', 'DESC')
                            ->take($take)
                            ->skip($skip)
                            ->get();

//        DB::beginTransaction();
//        try {
//            $content = file_get_contents("https://raw.githubusercontent.com/mwgg/Airports/master/airports.json");
//
//            $data = json_decode($content, true);
//
//            foreach ($data as $key) {
//                $array["city"] = trim($key["city"]);
//                $array["country"] = trim($key["country"]);
//                $array["elevation"] = trim($key["elevation"]);
//                $array["iata"] = trim($key["iata"]);
//                $array["icao"] = trim($key["icao"]);
//                $array["lon"] = trim($key["lon"]);
//                $array["lat"] = trim($key["lat"]);
//                $array["name"] = trim($key["name"]);
//                $array["state"] = trim($key["state"]);
//                $array["tz"] = trim($key["tz"]);
//                Airport::updateOrCreate($array,$array);
//            }
//
//            DB::commit();
//        }catch (\Exception $ex){
//            DB::rollBack();
//            return $ex->getMessage();
//        }

    }

    public function index2(Request $request)
    {

        $take = ($request->take) ? $request->take : 100;
        $skip = ($request->skip) ? $request->skip : 0;


        Airport::query()
            ->when($request->keyword, function(Builder $builder) use ($request){
                $builder->where(function ($query) use ($request) {
                    $query->where('city', 'like', '%' . $request->keyword . '%')
                        ->orWhere('country', 'like', '%' . $request->keyword . '%')
                        ->orWhere('name', 'like', '%' . $request->keyword . '%')
                        ->orWhere('iata', 'like', '%' . $request->keyword . '%')
                        ->orWhere('icao', 'like', '%' . $request->keyword . '%');
                });
            })
            ->when($request->city, function(Builder $builder) use ($request){
                $builder->where('city', 'like', '%' . $request->city . '%');
            })
            ->when($request->country, function(Builder $builder) use ($request){
                $builder->where('country', 'like', '%' . $request->country . '%');
            })
            ->when($request->name, function(Builder $builder) use ($request){
                $builder->where('name', 'like', '%' . $request->name . '%');
            })
            ->when($request->iata, function(Builder $builder) use ($request){
                $builder->where('iata', 'like', '%' . $request->iata . '%');
            })
            ->when($request->icao, function(Builder $builder) use ($request){
                $builder->where('icao', 'like', '%' . $request->icao . '%');
            })
            ->when($request->latitude && $request->longitude, function(Builder $builder) use ($request){
                $builder->where('lat', '!=', null)
                    ->where('lon', '!=', null)
                    ->distance($request->latitude, $request->longitude)
                    ->orderBy('distance', 'ASC');
            })
            ->when($request->icao, function(Builder $builder) use ($request){
                $builder->where('icao', 'like', '%' . $request->icao . '%');
            });

        if ($request->type) {
            $airport = $airport->where('type', $request->type);
        }

        if ($request->sub_type) {
            $airport = $airport->where('sub_type', $request->sub_type);
        }

        if($request->fly_now){
            $airport = $airport->where('fly_now', 'Y');
        }

        return $airport
                            ->where('iata', '!=', null)
                            ->orderBy('sort', 'DESC')
                            ->take($take)
                            ->skip($skip)
                            ->get();

//        DB::beginTransaction();
//        try {
//            $content = file_get_contents("https://raw.githubusercontent.com/mwgg/Airports/master/airports.json");
//
//            $data = json_decode($content, true);
//
//            foreach ($data as $key) {
//                $array["city"] = trim($key["city"]);
//                $array["country"] = trim($key["country"]);
//                $array["elevation"] = trim($key["elevation"]);
//                $array["iata"] = trim($key["iata"]);
//                $array["icao"] = trim($key["icao"]);
//                $array["lon"] = trim($key["lon"]);
//                $array["lat"] = trim($key["lat"]);
//                $array["name"] = trim($key["name"]);
//                $array["state"] = trim($key["state"]);
//                $array["tz"] = trim($key["tz"]);
//                Airport::updateOrCreate($array,$array);
//            }
//
//            DB::commit();
//        }catch (\Exception $ex){
//            DB::rollBack();
//            return $ex->getMessage();
//        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
