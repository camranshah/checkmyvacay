<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Booking;
use Illuminate\Support\Facades\DB;
use App\FlightSession;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $take = 50;
        $skip = 0;
        $orderAs = 'desc';
        $orderBy = 'id';

        if ($request->take) {
            $take = $request->take;
        }

        if ($request->skip) {
            $skip = $request->skip;
        }

        if($request->order_as == 'asc' || $request->order_as == 'desc'){
            $orderAs = $request->order_as;
        }

        if($request->order_by){
            $orderBy = $request->order_by;
        }

        if($request->order_by === 'Booking Date'){
            $orderBy = 'id';
        }

        if($request->order_by === 'Departure Date'){
            $orderBy = 'departure_datetime';
        }

        $booking = new Booking();
        $booking = $booking->with(['user', 'payment']);
        $booking = $booking->whereUserId($user->id);

        if ($request->type) {
            switch ($request->type):
                case 'completed':
                    $booking->where('arrival_datetime', '<', date('Y-m-d H:i:00'));
                    break;
                case 'upcomming':
                    $booking->where('departure_datetime', '>', date('Y-m-d H:i:00'));
                    break;
                default:
                    break;
            endswitch;
        }

        if($request->booking_date){
            $booking->whereDate('created_at', date('Y-m-d',strtotime($request->booking_date)));
        }

        if($request->departure_date){
            $booking->whereDate('departure_datetime',  date('Y-m-d',strtotime($request->departure_date)));
        }

        $booking = $booking->orderBy($orderBy, $orderAs);
        $booking = $booking->take($take)->skip($skip);
        $booking = $booking->get();

        if ($booking->isEmpty()) {
            return response()->json(['message' => 'record not found'], 404);

        }

        return response()->json(['message' => 'success', 'bookings' => $booking], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = $request->user();

        $booking = new Booking();
        $booking = $booking->with(['user', 'payment']);
        $booking = $booking->whereId($id)->whereUserId($user->id);
        $booking = $booking->first();

        if (!$booking) {
            return response()->json(['message' => 'record not found'], 404);

        }

        return response()->json(['message' => 'success', 'booking' => $booking], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
