<?php

namespace App\Http\Controllers\api;

use App\ContactUs;
use App\Events\NotificationEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => ['required', 'string', 'max:50'],
                'email' => ['required', 'email', 'string', 'max:50'],
                'subject' => ['required'],
                'message' => ['required'],
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        DB::beginTransaction();

        try {

            $feedback = ContactUs::create([
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'message' => $request->message
            ]);

            /* Start Process To Send Notification To Admin */
            $title = $feedback->subject;
            $description = $feedback->message;
            $type = 'feedback';
            $typeId = $feedback->id;
            $url = 'feedback/'.$feedback->id;
            event(new NotificationEvent(1, $title, $description, $type, $typeId, $url));
            /* End Process To Send Notification To Admin */

            DB::commit();

            return response()->json(['message' => 'Thank you! we will get back to you soon.'], 200);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            DB::rollBack();

            return response()->json(['message' => $ex->getMessage()], 500);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
