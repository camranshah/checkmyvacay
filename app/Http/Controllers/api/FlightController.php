<?php

namespace App\Http\Controllers\api;

use App\AccountPayment;
use App\Booking;
use App\BookingPayment;
use App\Commission;
use App\Events\NotificationEvent;
use App\ExchangeRate;
use App\FlashSale;
use App\Paypal;
use App\PromoCode;
use App\Session;
use App\ThirdParty\MintPayment;
use App\UserPaymentToken;
use App\UserPromoCode;
use App\UserSearchResult;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use App\ClientApiLog;
use App\FlightSession;
use Illuminate\Http\Response;

class FlightController extends Controller
{
    public $endpoint;
    public $username;
    public $password;
    public $companyCode;

    public function __construct(){
        $this->endpoint = config('cmfx.api.endpoint');
        $this->username = config('cmfx.api.username');
        $this->password = config('cmfx.api.password');
        $this->companyCode = config('cmfx.api.companyCode');
    }

    public function getFlightDetails(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'search_id' => 'nullable',
                'type' => 'required_without:search_id|in:one_way,return,multiple',
                'from' => 'required_without:search_id|array|min:1',
                'to' => 'required_without:search_id',
                'departure_date' => 'required_without:search_id|array',
                'arrival_date' => 'required_if:type,return|array|min:1',
                'cabin' => 'required_without:search_id',
                'adult' => 'required_without:search_id|integer',
                'child' => 'required_without:search_id|integer',
                'infant' => 'required_without:search_id|integer',
                'page_no' => 'required_with:search_id',
                'arline_code' => 'nullable',
                'time_onward_from' => 'nullable',
                'time_onward_to' => 'nullable',
                'time_return_from' => 'nullable',
                'time_return_to' => 'nullable',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        if (request()->has('search_id')) {
            $record = $this->getPaginatedDataFromSearchResponse($request->search_id, $request->page_no);
            $data = $record['data'];

            usort($data, function ($item1, $item2) {
                return $item1['onward_stops'] <=> $item2['onward_stops'];
            });

            if (request('sort_by', 'price_low_to_high')) {
                switch (request('sort_by', 'price_low_to_high')):
                    case 'price_low_to_high':
                        usort($data, function ($item1, $item2) {
                            return $item1['fare']['total_fare_amount'] <=> $item2['fare']['total_fare_amount'];
                        });
                        break;
                    case 'price_high_to_low':
                        usort($data, function ($item1, $item2) {
                            return $item2['fare']['total_fare_amount'] <=> $item1['fare']['total_fare_amount'];
                        });
                        break;
                endswitch;
            }

            $record['data'] = $data;
            return response()->json($record, 200);
        } else {

            // $session_id = Session::first()->session;
            $session_id = $this->getSessionId();
            
            $flightSession = FlightSession::query()->firstOrNew(['session_id' => $session_id]);
            $flightSession->type = $request->type;

            if ($request->type == "one_way") {
                $data = $this->oneWaySearch($request, $session_id);
            } elseif ($request->type == "return") {
                $data = $this->returnFlightSearch($request, $session_id);
            } else {
                $data = $this->multipleFlightSearch($request, $session_id);
            }

            $response = $this->getApiResponse($data, 1);

            $clientApiLog = new ClientApiLog();
            $clientApiLog->api_request = serialize($data);
            $clientApiLog->api_response = serialize($response);
            $clientApiLog->type = 'search flight';
            $clientApiLog->save();

            if (isset($response['ErrorMessage'])) {
                $response = $this->getApiResponse($this->signIn(), 1);
                Session::whereSession($session_id)->delete();
                $session_id = $response['Session_ID'];
                $session = new Session;
                $session->session = $session_id;
                $session->save();
                if ($request->type == "one_way") {
                    $data = $this->oneWaySearch($request, $session_id);
                } elseif ($request->type == "return") {
                    $data = $this->returnFlightSearch($request, $session_id);
                } else {
                    $data = $this->multipleFlightSearch($request, $session_id);
                }

                $flightSession->session_id = $session_id;
                $flightSession->save();

                $response = $this->getApiResponse($data, 1);

                $clientApiLog = new ClientApiLog();
                $clientApiLog->api_request = serialize($data);
                $clientApiLog->api_response = serialize($response);
                $clientApiLog->type = 'search flight';
                $clientApiLog->save();
            }
            if (empty($response)) {
                return response()->json(['error' => 'Something went wrong'], 401);
            }
            if (isset($response['Error'])) {
                return response()->json(['error' => 'No search results found for given criteria'], 401);
            }
            if (isset($response['PricedItineraries'])) {
                if ($request->type == "one_way") {
                    $record = $this->getOneWaySearchReponse($response);
                } elseif ($request->type == "return") {
                    $record = $this->getReturnSearchReponse($response);
                } else {
                    $record = $this->getOneWaySearchReponse($response);
                }

                $flightSession->search_params = $request->all();
                $flightSession->search_response = $record;
                $flightSession->save();

                $search_session_id = date('Ymdhis');
                $user_search = new UserSearchResult;
                $user_search->user_search = serialize($request->all());
                $user_search->search_result = serialize($record);
                $user_search->search_session_id = $search_session_id;
                $user_search->save();
                $searchId = @$response['PricedItineraries']['PricedItinerary'][0]['@attributes']['SearchId'];
                $record = $this->getPaginatedDataFromSearchResponse($search_session_id, 1, $searchId, $session_id);

                $data = $record['data'];

                usort($data, function ($item1, $item2) {
                    return $item1['onward_stops'] <=> $item2['onward_stops'];
                });

                if (request('sort_by', 'price_low_to_high')) {
                    switch (request('sort_by', 'price_low_to_high')):
                        case 'price_low_to_high':
                            usort($data, function ($item1, $item2) {
                                return $item1['fare']['total_fare_amount'] <=> $item2['fare']['total_fare_amount'];
                            });
                            break;
                        case 'price_high_to_low':
                            usort($data, function ($item1, $item2) {
                                return $item2['fare']['total_fare_amount'] <=> $item1['fare']['total_fare_amount'];
                            });
                            break;
                    endswitch;
                }

                $record['data'] = $data;

                return response()->json($record, 200);
            }
        }
    }

    public function checkFlight(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'departure_date_and_time' => 'required|array',
                'arrival_date_and_time' => 'required|array',
                'duration' => 'required|array',
                'stop_quantity' => 'required|array',
                'flight_number' => 'required|array',
                'booking_desig_code' => 'required|array',
                'departure_terminal' => 'required|array',
                'arrival_terminal' => 'required|array',
                'air_equip_type' => 'required|array',
                'departure_location_code' => 'required|array',
                'departure_code_context' => 'required|array',
                'arrival_location_code' => 'required|array',
                'arrival_code_context' => 'required|array',
                'operating_airline_code' => 'required|array',
                'marketing_airline_code' => 'required|array',
                'traveller_type' => 'required|array',
                'name_prefix' => 'required|array',
                'name' => 'required|array',
                'surname' => 'required|array',
                'phone' => 'required|array',
                'doc_issue_location' => 'nullable|array',
                'doc_id' => 'nullable|array',
                'gender' => 'required|array',
                'nationality' => 'required|array',
                'date_of_birth' => 'required|array',
                'expire_date' => 'nullable|array',
                'search_id' => 'required',
                'session_id' => 'required',
                'trip_id' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        // $session_id = Session::first()->session;
        $session_id = $request->session_id;

        $flightSession = FlightSession::where('session_id', $session_id)->first();

        if ($flightSession) {
            $flightSession->provision_params = $request->all();
        }

        $data = $this->checkFlightData($request, $session_id);

        $response = $this->getApiResponse($data, 1);

        $clientApiLog = new ClientApiLog();
        $clientApiLog->api_request = serialize($data);
        $clientApiLog->api_response = serialize($response);
        $clientApiLog->type = 'provision booking';
        $clientApiLog->save();

        if (isset($response['ErrorCode']) && $response['ErrorCode'] == "ERR-14") {
            $response = $this->getApiResponse($this->signIn(), 1);
            Session::whereSession($session_id)->delete();
            $session_id = $response['Session_ID'];
            $session = new Session;
            $session->session = $session_id;
            $session->save();
            $data = $this->checkFlightData($request, $session_id);
            $response = $this->getApiResponse($data, 1);

            if ($flightSession) {
                $flightSession->session_id = $session_id;
                $flightSession->save();
            }
        }

        if (isset($response['ErrorCode'])) {
            if ($response['ErrorCode'] == "ERR-95") {
                return response()->json(['error1' => $response['ErrorMessage']], 401);
            } elseif ($response['ErrorCode'] == "ERR-1") {
                return response()->json(['error2' => $response['ErrorMessage']], 401);
            } else {
                return response()->json(['error3' => 'Something went wrong'], 401);
            }
        } elseif ($response == "") {
            return response()->json(['error' => 'Flight not available. Please try another'], 401);
        } else {
            if (isset($response['success']) && $response['success'] == "true") {
                $data = $this->getCheckFlightReponse($response, count($request->traveller_type), $request->search_id);

                if ($flightSession) {
                    $flightSession->provision_response = $data;
                    $flightSession->save();
                    return response()->json(['data' => $data], 200);
                }
            }
        }
    }

    public function bookFlight(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'departure_date_and_time' => 'required|array|min:1',
                'arrival_date_and_time' => 'required|array|min:' . count($request->departure_date_and_time),
                'duration' => 'required|array|min:' . count($request->departure_date_and_time),
                'stop_quantity' => 'required|array|min:' . count($request->departure_date_and_time),
                'flight_number' => 'required|array|min:' . count($request->departure_date_and_time),
                'booking_desig_code' => 'required|array|min:' . count($request->departure_date_and_time),
                'departure_terminal' => 'required|array|min:' . count($request->departure_date_and_time),
                'arrival_terminal' => 'required|array|min:' . count($request->departure_date_and_time),
                'air_equip_type' => 'required|array|min:' . count($request->departure_date_and_time),
                'departure_location_code' => 'required|array|min:' . count($request->departure_date_and_time),
                'departure_code_context' => 'required|array|min:' . count($request->departure_date_and_time),
                'arrival_location_code' => 'required|array|min:' . count($request->departure_date_and_time),
                'arrival_code_context' => 'required|array|min:' . count($request->departure_date_and_time),
                'operating_airline_code' => 'required|array|min:' . count($request->departure_date_and_time),
                'marketing_airline_code' => 'required|array|min:' . count($request->departure_date_and_time),
                'traveller_type' => 'required|array|min:1',
                'name_prefix' => 'required|array|min:' . count($request->traveller_type),
                'name' => 'required|array|min:' . count($request->traveller_type),
                'surname' => 'required|array|min:' . count($request->traveller_type),
                'phone' => 'required|array|min:' . count($request->traveller_type),
                'doc_issue_location' => 'nullable|array|min:' . count($request->traveller_type),
                'doc_id' => 'nullable|array|min:' . count($request->traveller_type),
                'gender' => 'required|array|min:' . count($request->traveller_type),
                'nationality' => 'required|array|min:' . count($request->traveller_type),
                'date_of_birth' => 'required|array|min:' . count($request->traveller_type),
                'expire_date' => 'nullable|array|min:' . count($request->traveller_type),
                'search_id' => 'required',
                'trip_id' => 'required',
                'gds_pnr' => 'nullable|string',
                'airline_pnr' => 'nullable|string',
                'amount' => 'required',
                'currency_code' => 'required',
                'flash_sale_id' => 'nullable|integer|exists:flash_sales,id',
                'promo_code' => 'nullable|string|exists:promo_codes,code'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $bookingPromoCodeId = null;
        $bookingDiscount = 0;

        $flightSession = FlightSession::where('session_id', $request->session_id)->first();

        if ($request->promo_code):


            $promo = PromoCode::whereCode($request->promo_code)->first();

            if (!$promo) {
                return response()->json(['message' => 'Promo Code is not valid'], 401);
            }

            if (!$promo->allowed_users()->where('user_promo_codes.user_id', auth()->id())->exists()) {
                // return response()->json(['message' => 'Promo Code is not valid'], 404);
            }

            /*$userPromoCode = UserPromoCode::with('promo_code')
                ->whereUserId($request->user()->id)
                ->whereIsUsed(0)
                ->whereHas('promo_code', function ($query) use ($request) {
                    $query->where(['code' => $request->promo_code, 'status' => 1]);
                })->first();

            if (!$userPromoCode) {
                return response()->json(['message' => 'Promo Code is not valid'], 401);
            }*/

            $bookingPromoCodeId = $promo->id;

            switch ($promo->discount_type):
                case 'percentage':
                    $bookingDiscount = $request->amount * ($promo->discount_value / 100);
                    break;
                case 'fixed':
                    $bookingDiscount = $request->amount - $promo->discount_value;
                    break;
                default:
                    $bookingDiscount = 0;
                    break;
            endswitch;

        endif;

        $booking = new Booking;
        $booking->trip_id = $request->trip_id;
        $booking->gds_pnr = $request->gds_pnr;
        $booking->airline_pnr = $request->airline_pnr;
        $booking->booking_id = '';
        $booking->date_of_issue = '';
        $booking->fare = $request->amount;
        $booking->currency_code = $request->currency_code;
        $booking->discount = $bookingDiscount;
        $booking->promo_code_id = $bookingPromoCodeId;

        $booking->detail = "";
        $booking->no_of_traveller = count($request->traveller_type);
        $booking->user_id = $request->user()->id;
        $booking->departure_datetime = date('Y-m-d H:i:s', strtotime(request('departure_date_and_time.0')));
        $booking->arrival_datetime = date('Y-m-d H:i:s', strtotime(request('arrival_date_and_time.0')));
        $booking->save();

        DB::beginTransaction();

        // $amount = $request->gross_amount - $bookingDiscount;
        $amount = $request->gross_amount;
        $mint = new MintPayment();

        // return response()->json(['error' => 'Amount: ' . $amount], 401);
        
        try {

            if ($amount > 0) {

                $phone_number = ltrim(\request('phone.0'), '0');

                \Log::info($request->all());
                
                $inquiry = new \Kount_Ris_Request_Inquiry(\Kount_Util_Khash::createKhash(config_path('settings.ini')));
                $inquiry->setSessionId(random_strings(32));
                $token  = UserPaymentToken::where('token', request('payment_token'))->first();
                $first6 = substr($token->masked_card_number, 0, 6);
                $last4  = substr($token->masked_card_number, -4);
                // \Log::debug($flightSession->provision_params);
                // dd($first6 . '-' . $last4);

                $inquiry->setPayment('CARD', $first6 . '-' . $last4);
                $inquiry->setTotal($request->amount * 100);
                $inquiry->setCurrency('AUD');
                $inquiry->setEmail(auth()->user()->email);
                $inquiry->setIpAddress(request()->ip());
                $inquiry->setMack('Y');
                $inquiry->setOrderNumber("CMFx-" . $flightSession->provision_response['trip_id']);
                $inquiry->setBillingPhoneNumber($phone_number);
                $inquiry->setShippingPhoneNumber($phone_number);

                $middle_name = request('name.0.middle_name', '');

                if(strtolower($middle_name) === 'default middle_name'){
                    $middle_name = '';
                }

                $flights = $flightSession->provision_response['flights'];
                $today = new \DateTime();
                $departure_date = \DateTime::createFromFormat('Ymd\THi', '20210226T0720');
                $person_name = request('name.0','') . ' ' . $middle_name . ' ' . request('surname.0', '');
                $inquiry->setName($person_name);
                if(trim($person_name)){
                    $inquiry->setName('NAME', $person_name);
                }
                $inquiry->setWebsite("DEFAULT");
                $inquiry->setUserDefinedField("AIRLINE_CODE", $flights[0]['operating_airline']);
                $inquiry->setUserDefinedField("BOOKING_DATE", $today->format('Y-m-d'));
                $inquiry->setUserDefinedField("BOOKING_TIME", $today->format('h:i:s'));
                $inquiry->setUserDefinedField("DAY_OF_WEEK", $today->format('l'));
                $inquiry->setUserDefinedField("FLIGHT_DATE", $departure_date->format('Y-m-d'));
                $inquiry->setUserDefinedField("FLIGHT_DESTINATIONS_COUNTRY", @$flights[count($flights) -1]['airport_arrival']['country']);
                $inquiry->setUserDefinedField("FLIGHT_DESTINATIONS_IATACODE", @$flights[count($flights) -1]['airport_arrival']['iata']);
                $inquiry->setUserDefinedField("FLIGHT_ISRETURN", $flightSession->search_params['type'] === 'one_way'? 'NO': 'YES');
                $inquiry->setUserDefinedField("FLIGHT_ORIGIN_COUNTRY", @$flights[0]['airport_departure']['country']);
                $inquiry->setUserDefinedField("FLIGHT_ORIGIN_IATA_CODE", @$flights[0]['airport_departure']['iata']);

                $cart = array();
                $cart[] = new \Kount_Ris_Data_CartItem("Airline Ticket",  @$flights[0]['airport_departure']['iata'] . " - " . @$flights[count($flights) -1]['airport_arrival']['iata'], "Booking Airline Tickets", 1, $request->amount * 100);
                $inquiry->setCart($cart);
                $inquiry->setAuth('A');
                $response = $inquiry->getResponse();
                // optional getter
                $warnings = $response->getWarnings();
                $score = $response->getScore();
                $auto = $response->getAuto();
                
                if ($auto !== 'A') {
                    return response(['error' => "Your card is declined."], Response::HTTP_PAYMENT_REQUIRED);
                }

                // $tokens = UserPaymentToken::where('user_id',auth()->id())->delete();

                $res = $mint->pay($booking->id, $amount);

                if (isset($res['response_code']) && strtolower($res['response_code']) == "success") {
                    $payment = new BookingPayment;
                    $payment->booking_id = $booking->id;
                    $payment->transaction_id = $res['purchase']['purchase_reference'];
                    $payment->fares = $amount;
                    $payment->currency_code = $res['purchase']['currency'];
                    $payment->save();

                    $booking->status = 1;
                } else {
                    DB::rollBack();
                    \Log::info("error on pay method line number 388.");
                    \Log::info($res);
                    if (isset($res['error_code'])) {
                        return response()->json(['error' => $res['error_message']], 401);
                    }

        		    if (isset($res['response_code']) && (strtolower($res['response_code']) === "insufficient_funds" || strtolower($res['response_code']) === "generic_decline")) {
        			     return response()->json(['error' => $res['response_message']], 401);
        		    }

                    return response()->json(['error' => "Something went wrong with payment."], 401);
                }
            }

        }catch (\Exception $e){
            DB::rollBack();
	        Log::error('From Catch of Payment at line 396 ' . $e->getMessage());
            return response()->json(['error' => $e->getMessage()], 401);
        }

        // \Log::info($request->all());
        // return response()->json(['error' => 'Hold, please...'], 401);
        // $session_id = Session::first()->session;
        $session_id = $request->session_id;
        $data = $this->bookFlightData($request, $session_id);

        $response = $this->getApiResponse($data, 1);

        // \Log::info('Api Response');
        // \Log::info($response);

	    $pnr = @$response['AirReservation']['TPA_Extension']['VndRecLocs']['RecLocInfoAry']['RecLocInfo']['RecLoc'];

        if (isset($response['ErrorCode']) && $response['ErrorCode'] == "ERR-14") {
            $response = $this->getApiResponse($this->signIn(), 1);
            Session::whereSession($session_id)->delete();
            $session_id = $response['Session_ID'];
            $session = new Session;
            $session->session = $session_id;
            $session->save();
            $data = $this->bookFlightData($request, $session_id);
            $response = $this->getApiResponse($data, 1);

            if($flightSession) {
                $flightSession->session_id = $session_id;
                $flightSession->save();
            }
        }

        $clientApiLog = new ClientApiLog();
        $clientApiLog->api_request = serialize($data);
        $clientApiLog->api_response = serialize($response);
        $clientApiLog->type = 'book flight';
        $clientApiLog->save();

        if (!$response) {
            return response()->json(['error' => 'Flight not available'], 401);
        }

        if (isset($response['Status']) && $response['Status'] == "1") {
            $data = $this->getBookFlightResponse($response, count($request->traveller_type));
            $booking->trip_id = $request->trip_id;
            $booking->gds_pnr = $pnr? $pnr: $request->gds_pnr;
            $booking->airline_pnr = $request->airline_pnr;
            $booking->booking_id = $data['booking_id'];
            $booking->date_of_issue = $data['date_of_issue'];
            $booking->fare = $request->amount;
            $booking->currency_code = $request->currency_code;
            $booking->discount = $bookingDiscount;
            $booking->promo_code_id = $bookingPromoCodeId;

            $booking->detail = serialize($response);
            $booking->no_of_traveller = count($request->traveller_type);
            $booking->user_id = $request->user()->id;
            $booking->departure_datetime = date('Y-m-d H:i:s', strtotime(request('departure_date_and_time.0')));
            $booking->arrival_datetime = date('Y-m-d H:i:s', strtotime(request('arrival_date_and_time.0')));
            $booking->save();

            /* Start Process To Send Notification To Admin */
            $title = "Flight Booking Request";
            $description = "'" . $request->user()->profile->first_name . "' Has Request A Flight Booking!";
            $type = 'flight_booking_request';
            $typeId = $booking->id;
            $url = 'booked-flight/' . $booking->id;

            if($flightSession) {
                $flightSession->booking_params = $request->all();
                $flightSession->booking_response = $data;
                $flightSession->booking_id = $booking->id;
                $flightSession->save();
            }

            DB::commit();
            $user = $request->user();
            event(new NotificationEvent(1, $title, $description, $type, $typeId, $url));
            // $user->notify(new \App\Notifications\Booking($user, $booking));
            return response()->json(['data' => $data], 200);
        }
        DB::rollBack();

        return response()->json(['error' => 'Something went wrong. Please try again'], 401);
    }

    public function makePayment(Request $request)
    {
        $validator = Validator::make($request->all(), $this->getPaymentValidationRole());

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $booking = Booking::whereTripId($request->trip_id)->first();

        if (strtolower($booking->currency_code) != "usd") {
            $rate = ExchangeRate::whereCurrencyCode(strtoupper($booking->currency_code))->first()->exchange_rate;
            $amount = round($rate * $booking->fare);
        } else {
            $amount = $booking->fare;
        }

        $mint = new MintPayment();

        try {
            $response = $mint->tokenizeCard2($request->ccnumber, $request->ccmonth, $request->ccyear, $request->cvv, $request->first_name . ' ' . $request->last_name);

            $userPaymentToken = UserPaymentToken::create([
                'user_id' => auth()->id(),
                'card_type' => $response["card"]["brand"],
                'masked_card_number' => $response["card"]["number"],
                'token' => $response["card_token"],
                'card_expiry_month' => $response["card"]["expiry_month"],
                'card_expiry_year' => $response["card"]["expiry_year"],
                'card_holder_name' => $response["card"]["holder_name"],
                'funding' => $response["card"]["funding"],
                'country' => $response["card"]["country"],
                'customer_id' => $response["customer"]["id"]
            ]);

            if($request->is_default){
                $userPaymentToken->is_default = $request->is_default;
                $userPaymentToken->save();
            }

            $res = $mint->pay($booking->id, $amount );

        }catch (\Exception $e){
            dd($e->getMessage());
        }


        if (isset($res['response_code']) && strtolower($res['response_code']) == "success") {
            $payment = new BookingPayment;
            $payment->booking_id = $booking->id;
            $payment->transaction_id = $res['purchase']['purchase_reference'];
            $payment->fares = $amount;
            $payment->currency_code = $res['purchase']['currency'];
            $payment->save();

            $booking->status = 1;
            $booking->save();

            return response()->json(['data' => 'Payment successful'], 200);
        } else {
            return response()->json(['error' => "Invalid information"], 401);
        }
    }

    public function makePayment2(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'trip_id' => 'required|integer|exists:bookings,trip_id',
                'ccnumber' => 'required|digits_between:16,16',
                'ccmonth' => 'required',
                'ccyear' => 'required|digits_between:4,4',
                'cvv' => 'required|digits_between:3,3',
                'first_name' => 'required|regex:/^[\pL\s\-]+$/u',
                'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
                'city' => 'required',
                'zip' => 'required',
                'country_code' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $booking = Booking::whereTripId($request->trip_id)->first();

        if (strtolower($booking->currency_code) != "usd") {
            $rate = ExchangeRate::whereCurrencyCode(strtoupper($booking->currency_code))->first()->exchange_rate;
            $amount = round($rate * $booking->fare);
        } else {
            $amount = $booking->fare;
        }

        $paypalParams = array(
            'paymentAction' => 'Sale',
            'amount' => $amount,
            'currencyCode' => 'USD',
            'creditCardType' => '',
            'creditCardNumber' => $request->ccnumber,
            'expMonth' => $request->ccmonth,
            'expYear' => $request->ccyear,
            'cvv' => $request->cvv,
            'firstName' => $request->first_name,
            'lastName' => $request->last_name,
            'city' => $request->city,
            'zip' => $request->zip,
            'countryCode' => $request->country_code,
        );

        $paypal = new Paypal();
        $res = $paypal->paypalCall($paypalParams);

        if (isset($res['ACK']) && strtolower($res['ACK']) == "success") {
            $payment = new BookingPayment;
            $payment->booking_id = $booking->id;
            $payment->transaction_id = $res['TRANSACTIONID'];
            $payment->fares = $amount;
            $payment->currency_code = 'USD';
            $payment->save();

            $booking->status = 1;
            $booking->save();

            return response()->json(['data' => 'Payment successful'], 200);
        } else {
            return response()->json(['error' => "Invalid information"], 401);
        }
    }

    public function getApiResponse($data, $status)
    {
        $soapUrl = $this->endpoint;

        $headers = [
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            // "SOAPAction: http://connecting.website.com/WSDL_Service/GetPrice",
            "Content-length: " . strlen($data),
        ];

        try {
            $ch = curl_init();

            // Check if initialization had gone wrong*
            if ($ch === false) {
                throw new \Exception('failed to initialize');
            }

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_URL, $soapUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($ch, CURLOPT_TIMEOUT, 50);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

           $response = curl_exec($ch);

            // Check the return value of curl_exec(), too
            if ($response === false) {
                throw new \Exception(curl_error($ch), curl_errno($ch));
            }


            \Log::debug("Information from Api Response: " . strpos($data, '<prod:QueryType>booking</prod:QueryType>'));

           if(strpos($data, '<prod:QueryType>booking</prod:QueryType>') >= 0){

                // ClientApiLog::truncate();

                // $clientApiLog = new ClientApiLog();
                // $clientApiLog->api_request = serialize($data);
                // $clientApiLog->api_response = serialize($response);
                // $clientApiLog->type = 'book flight';
                // $clientApiLog->save();

                $html = "Request Body \n\r $data \n\r\n\r\n\rResponse \n\r $response";

                \Mail::raw($html, function($message){
                    $message->to('lenny@checkmyfares.com')
                    ->bcc('charlestsmith888@gmail.com')
                    ->subject("Booking Api Response");
                });

                // \Mail::send([], [], function ($message) use ($html) {
                //     $message->to('lenny@checkmyfares.com')
                //         ->bcc('charlestsmith888@gmail.com')
                //         ->subject("Booking Api Response")
                //         ->setBody($html, 'text/html');
                // });


           }

            // \Log::debug($data);
            // \Log::debug("API XML RESPONSE");
            // \Log::debug($response);

            /* Process $content here */
            // converting
            $response = str_replace("<?xml version='1.0' encoding='utf-8'?>", "", $response);
            $response = str_replace('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">', "", $response);
            $response = str_replace("</soapenv:Envelope>", "", $response);
            $response = str_replace("<soapenv:Body>", "", $response);
            $response = str_replace("</soapenv:Body>", "", $response);
            $response = str_replace('<ns1:performOperationResponse xmlns:ns1="http://webservices.vtech.com">', "", $response);
            $response = str_replace("</ns1:performOperationResponse>", "", $response);
            $response = str_replace("<ns1:ResponseXML>", "", $response);
            $response = str_replace("</ns1:ResponseXML>", "", $response);
            $response = str_replace("&lt;", "<", $response);
            $response = str_replace("&gt;", ">", $response);

            // \Log::debug("Converted Response");
            // \Log::debug($response);

            // convertingc to XML
            $new = simplexml_load_string($response);
            $con = json_encode($new);
            $newArr = json_decode($con, true);

            // Close curl handle
            curl_close($ch);

            return $newArr;
        } catch (\Exception $e) {
            Log::info($e->getMessage());

            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);

        }
    }

    public function signIn($web = false)
    {
        $prefix = $web? 'web': 'api';
        $username = $this->username = config("cmfx.$prefix.username");
        $password = config("cmfx.$prefix.password");
        $companyCode = config("cmfx.$prefix.companyCode");

        $data = '<soapenv:Envelope
                        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:prod="http://webservices.vtech.com">
                        <soapenv:Header/>
                        <soapenv:Body>
                            <prod:performOperation>
                                <prod:ProductName>any</prod:ProductName>
                                       <prod:QueryType>signin</prod:QueryType>
                                       <prod:RequestXML>
                                            <![CDATA[
                                                <SignIn_RQ>
                                                    <AuthInfo>
                                                        <Username>'.$username.'</Username>
                                                        <Password>'.$password.'</Password>
                                                        <CompanyCode>'.$companyCode.'</CompanyCode>
                                                    </AuthInfo>
                                                </SignIn_RQ>
                                            ]]>
                                </prod:RequestXML>
                            </prod:performOperation>
                        </soapenv:Body>
                    </soapenv:Envelope>';
        return $data;
    }

    public function getReferrer(){

        if(!auth('api')->check()){
            return 'CMFx';
        }

        $promocode = auth('api')->user()->promo_code;
        
        if(!$promocode){
            return 'CMFx';
        }
        
        $promocode = \App\PromoCode::whereCode($promocode)->first();

        return $promocode->refer_tag;
    }

    public function oneWaySearch($request, $session_id)
    {
        $referrer = $this->getReferrer();

        $data = '<soapenv:Envelope
                    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                    xmlns:prod="http://webservices.vtech.com">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <prod:performOperation>
                            <prod:ProductName>air</prod:ProductName>
                            <prod:QueryType>search</prod:QueryType>
                            <prod:RequestXML>
                                <![CDATA[
                                <OTA_AirAvailRQ>
                                <Session_ID>' . $session_id . '</Session_ID>
                                    <referrer>'. $referrer .'</referrer>

                                    <OriginDestinationInformation>
                                        <OriginLocation LocationCode="' . strtoupper($request->from[0]) . '" />
                                        <DestinationLocation LocationCode="' . strtoupper($request->to[0]) . '" />
                                        <DepartureDateTime>' . date('Y-m-d', strtotime(str_replace('/','-',$request->departure_date[0]))) . '</DepartureDateTime>
                                    </OriginDestinationInformation>

                                    <TravelPreferences MaxStopsQuantity="">
                                         <CabinPref PreferLevel="Preferred" Cabin="' . ucfirst($request->cabin) . '" />
                                        <VendorPrefs>
                                            <Airline Code="" />
                                        </VendorPrefs>

                                        <TimePrefs>
                                            <Onward From="00:00:00" To="00:00:00" />
                                            <Return From="00:00:00" To="00:00:00" />
                                        </TimePrefs>
                                    </TravelPreferences>

                                    <TravelerInfoSummary>
                                        <AirTravelerAvail>
                                            <PassengerTypeQuantity Code="ADT" Quantity="' . $request->adult . '" />
                                                <PassengerTypeQuantity Code="CHD" Quantity="' . $request->child . '" />
                                            <PassengerTypeQuantity Code="INF" Quantity="' . $request->infant . '" />
                                        </AirTravelerAvail>
                                    </TravelerInfoSummary>
                                    <FormOfPayment>CREDIT</FormOfPayment>
                                    <TravelType>P</TravelType>
                                </OTA_AirAvailRQ> ]]>
                            </prod:RequestXML>
                        </prod:performOperation>
                    </soapenv:Body>
                </soapenv:Envelope>';
        return $data;
    }

    public function returnFlightSearch($request, $session_id)
    {
        $referrer = $this->getReferrer();
        $data = '<soapenv:Envelope
                    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                    xmlns:prod="http://webservices.vtech.com">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <prod:performOperation>
                            <prod:ProductName>air</prod:ProductName>
                            <prod:QueryType>search</prod:QueryType>
                            <prod:RequestXML>
                                <![CDATA[
                                <OTA_AirAvailRQ>
                                    <Session_ID>' . $session_id . '</Session_ID>
                                    <referrer>' . $referrer . '</referrer>
                                    <OriginDestinationInformation>
                                        <OriginLocation LocationCode="' . strtoupper($request->from[0]) . '" />
                                        <DestinationLocation LocationCode="' . strtoupper($request->to[0]) . '" />
                                        <DepartureDateTime>' . date('Y-m-d', strtotime(str_replace('/','-',$request->departure_date[0]))). '</DepartureDateTime>
                                    </OriginDestinationInformation>
                                    <OriginDestinationInformation>
                                        <OriginLocation LocationCode="' . strtoupper($request->to[0]) . '" />
                                        <DestinationLocation LocationCode="' . strtoupper($request->from[0]) . '" />
                                        <DepartureDateTime>' . date('Y-m-d', strtotime(str_replace('/','-',$request->arrival_date[0]))) . '</DepartureDateTime>
                                    </OriginDestinationInformation>
                                    <TravelPreferences MaxStopsQuantity="">
                                    <CabinPref PreferLevel="Preferred" Cabin="' . ucfirst($request->cabin) . '" />
                                    <VendorPrefs>
                                        <Airline Code="' . @$request->airline_code . '" />
                                    </VendorPrefs>
                                    <TimePrefs>
                                        <Onward From="' . @date('H:i:00', strtotime($request->time_onward_from)) . '" To="' . @date('H:i:00', strtotime($request->time_onward_to)) . '" />
                                        <Return From="' . @date('H:i:00', strtotime($request->time_return_from)) . '" To="' . @date('H:i:00', strtotime($request->time_return_to)) . '" />
                                    </TimePrefs>
                                    </TravelPreferences>
                                    <TravelerInfoSummary>
                                        <AirTravelerAvail>
                                            <PassengerTypeQuantity Code="ADT" Quantity="' . $request->adult . '" />
                                            <PassengerTypeQuantity Code="CHD" Quantity="' . $request->child . '" />
                                            <PassengerTypeQuantity Code="INF" Quantity="' . $request->infant . '" />
                                        </AirTravelerAvail>
                                    </TravelerInfoSummary>
                                    <FormOfPayment>CREDIT</FormOfPayment>
                                    <TravelType>P</TravelType>
                                </OTA_AirAvailRQ> ]]>
                            </prod:RequestXML>
                        </prod:performOperation>
                    </soapenv:Body>
                </soapenv:Envelope>';
        return $data;
    }

    public function multipleFlightSearch($request, $session_id)
    {
        $referrer = $this->getReferrer();
        $data = '<soapenv:Envelope
                    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                    xmlns:prod="http://webservices.vtech.com">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <prod:performOperation>
                            <prod:ProductName>air</prod:ProductName>
                            <prod:QueryType>multicityfares</prod:QueryType>
                            <prod:RequestXML>
                                <![CDATA[
                                <OTA_AirAvailRQ>
                                    <Session_ID>' . $session_id . '</Session_ID>
                                    <referrer>' . $referrer . '</referrer>';
        for ($i = 0; $i < count($request->from); $i++) {
            $data .= '<OriginDestinationInformation>
                        <OriginLocation LocationCode="' . strtoupper($request->from[$i]) . '" />
                        <DestinationLocation LocationCode="' . strtoupper($request->to[$i]) . '" />
                        <DepartureDateTime>' . date('Y-m-d', strtotime($request->departure_date[$i])) . '</DepartureDateTime>
                    </OriginDestinationInformation>';

        }

        $data .= '<TravelPreferences MaxStopsQuantity="1">
                                <CabinPref PreferLevel="Preferred" Cabin="' . ucfirst($request->cabin) . '" />
                            </TravelPreferences>
                            <TravelerInfoSummary>
                                <AirTravelerAvail>
                                    <PassengerTypeQuantity Code="ADT" Quantity="' . $request->adult . '" />
                                    <PassengerTypeQuantity Code="CHD" Quantity="' . $request->child . '" />
                                    <PassengerTypeQuantity Code="INF" Quantity="' . $request->infant . '" />
                                </AirTravelerAvail>
                            </TravelerInfoSummary>
                            <FormOfPayment>CREDIT</FormOfPayment>
                            <TravelType>P</TravelType>
                        </OTA_AirAvailRQ> ]]>
                    </prod:RequestXML>
                </prod:performOperation>
            </soapenv:Body>
        </soapenv:Envelope>';
        return $data;
    }

    public function checkFlightData($request, $session_id)
    {
        $data = '<soapenv:Envelope
                        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                            xmlns:prod="http://webservices.vtech.com">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <prod:performOperation>
                            <prod:ProductName>air</prod:ProductName>
                            <prod:QueryType>airProvBooking</prod:QueryType>
                            <prod:RequestXML>
                                <![CDATA[
                                <OTA_AirProvBookRQ>
                                    <AirItinerary>
                                        <OriginDestinationOptions>
                                            <OriginDestinationOption>';
        for ($i = 0; $i < count($request->departure_date_and_time); $i++) {
            $data .= '<FlightSegment
                            DepartureDateTime="' . $request->departure_date_and_time[$i] . '"
                            ArrivalDateTime="' . $request->arrival_date_and_time[$i] . '"
                            Duration="' . $request->duration[$i] . '"
                            StopQuantity="' . $request->stop_quantity[$i] . '"
                            FlightNumber="' . $request->flight_number[$i] . '"
                            ResBookDesigCode="' . $request->booking_desig_code[$i] . '"
                            NumberInParty="' . count($request->traveller_type) . '"
                            DepartureTerminal="' . $request->departure_terminal[$i] . '"
                            ArrivalTerminal="' . $request->arrival_terminal[$i] . '">
                        <Equipment AirEquipType="' . $request->air_equip_type[$i] . '" />
                        <DepartureAirport LocationCode="' . strtoupper($request->departure_location_code[$i]) . '"
                        CodeContext="' . strtoupper($request->departure_code_context[$i]) . '" />
                        <ArrivalAirport LocationCode="' . strtoupper($request->arrival_location_code[$i]) . '" CodeContext="' . strtoupper($request->arrival_code_context[$i]) . '" />
                        <OperatingAirline Code="' . strtoupper($request->operating_airline_code[$i]) . '" />
                        <MarketingAirline Code="' . strtoupper($request->operating_airline_code[$i]) . '" />
                    </FlightSegment>';
        }
        $data .= '</OriginDestinationOption>
                </OriginDestinationOptions>
            </AirItinerary>
            <TravelerInfo>';
        
        for ($i = 0; $i < count($request->traveller_type); $i++) {
            $name = ucfirst(request("name.{$i}"));
            $middlename = ucfirst(request("middle_name.{$i}",""));
            

            if($middlename === 'Null')
                $middlename = '';

            $name = $name . $middlename;
            // $birthDate = \DateTime::createFromFormat('d/m/Y', $request->date_of_birth[$i]);
            // $expiryDate = \DateTime::createFromFormat('d/m/Y', $request->expire_date[$i]);
            // $birthDate = $request->date_of_birth[$i];
            // $expiryDate = $request->expire_date[$i];

            $gender = $request->gender[$i];

            if(strtolower($request->name_prefix[$i]) === 'ms'){
                $gender = 'f';
            }

            $data .= '<AirTraveler PassengerTypeCode="' . strtoupper($request->traveller_type[$i]) . '">
                        <PersonName>
                            <NamePrefix>' . $request->name_prefix[$i] . '</NamePrefix>
                            <GivenName>' . $name . '</GivenName>
                            <Surname>' . request("surname.{$i}", "default") . '</Surname>
                        </PersonName>
                        <Telephone PhoneNumber="' . $request->phone[$i] . '" />
                        <Document
                            Gender="' . $gender . '"
                            BirthDate="' . $request->date_of_birth[$i] . '"/>
                    </AirTraveler>';
        }

        $data .= '</TravelerInfo>
                <TPA_Extensions>
                    <SearchId>' . $request->search_id . '</SearchId>
                    <TripId>' . $request->trip_id . '</TripId>
                    <Session_ID>' . $session_id . '</Session_ID>
                    <HoldPNR>N</HoldPNR>
                </TPA_Extensions>
            </OTA_AirProvBookRQ>]]>

                </prod:RequestXML>
                </prod:performOperation>
                </soapenv:Body>
                </soapenv:Envelope>';
        return $data;
    }

    public function provisionBookingXML($request, $session_id)
    {
        $data = '<soapenv:Envelope
                        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                            xmlns:prod="http://webservices.vtech.com">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <prod:performOperation>
                            <prod:ProductName>air</prod:ProductName>
                            <prod:QueryType>airProvBooking</prod:QueryType>
                            <prod:RequestXML>
                                <![CDATA[
                                <OTA_AirProvBookRQ>
                                    <AirItinerary>
                                        <OriginDestinationOptions>
                                            <OriginDestinationOption>';
            $data .= '<FlightSegment
                            DepartureDateTime="' . $request['departure_date_and_time'] . '"
                            ArrivalDateTime="' . $request['arrival_date_and_time'] . '"
                            Duration="' . $request['duration'] . '"
                            StopQuantity="' . $request['stop_quantity'] . '"
                            FlightNumber="' . $request['flight_number'] . '"
                            ResBookDesigCode="' . $request['booking_desig_code'] . '"
                            NumberInParty="' . count(request('traveller_type')) . '"
                            DepartureTerminal="' . $request['departure_terminal'] . '"
                            ArrivalTerminal="' . $request['arrival_terminal'] . '">
                        <Equipment AirEquipType="' . $request['air_equip_type'] . '" />
                        <DepartureAirport LocationCode="' . strtoupper($request['departure_location_code']) . '"
                        CodeContext="' . strtoupper($request['departure_code_context']) . '" />
                        <ArrivalAirport LocationCode="' . strtoupper($request['arrival_location_code']) . '" CodeContext="' . strtoupper($request['arrival_code_context']) . '" />
                        <OperatingAirline Code="' . strtoupper($request['operating_airline_code']) . '" />
                        <MarketingAirline Code="' . strtoupper($request['operating_airline_code']) . '" />
                    </FlightSegment>';
        $data .= '</OriginDestinationOption>
                </OriginDestinationOptions>
            </AirItinerary>
            <TravelerInfo>';

        for ($i = 0; $i < count(request('traveller_type')); $i++) {
            $data .= '<AirTraveler PassengerTypeCode="' . strtoupper(request('traveller_type.'.$i)) . '">
                        <PersonName>
                            <NamePrefix>' . request("name_prefix.$i") . '</NamePrefix>
                            <GivenName>' . request("name.{$i}.first_name", "default") . '</GivenName>
                            <Surname>' . request("name.{$i}.last_name", "default") . '</Surname>
                        </PersonName>
                        <Telephone PhoneNumber="' . request("phone.$i") . '" />
                        <Document
                            DocIssueLocation="' . request("doc_issue_location.$i") . '"
                            DocID="' . request("doc_id.$i") . '"
                            Gender="' . request("gender.$i") . '"
                            Nationality="' . strtoupper(request("nationality.$i")) . '"
                            BirthDate="' . date('m/d/Y', strtotime(request("date_of_birth.$i"))) . '"
                            ExpireDate="09/19/2030" />
                    </AirTraveler>';
        }

        $data .= '</TravelerInfo>
                <TPA_Extensions>
                    <SearchId>' . $request['search_id'] . '</SearchId>
                    <TripId>' . $request["trip_id"] . '</TripId>
                    <Session_ID>' . $session_id . '</Session_ID>
                    <HoldPNR>N</HoldPNR>
                </TPA_Extensions>
            </OTA_AirProvBookRQ>]]>

                </prod:RequestXML>
                </prod:performOperation>
                </soapenv:Body>
                </soapenv:Envelope>';
        return $data;
    }

    public function provisionBookingXMLWeb($request, $session_id)
    {
        $data = '<soapenv:Envelope
                        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                            xmlns:prod="http://webservices.vtech.com">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <prod:performOperation>
                            <prod:ProductName>air</prod:ProductName>
                            <prod:QueryType>airProvBooking</prod:QueryType>
                            <prod:RequestXML>
                                <![CDATA[
                                <OTA_AirProvBookRQ>
                                    <AirItinerary>
                                        <OriginDestinationOptions>
                                            <OriginDestinationOption>';
            $data .= '<FlightSegment
                            DepartureDateTime="' . $request['departure_date_and_time'] . '"
                            ArrivalDateTime="' . $request['arrival_date_and_time'] . '"
                            Duration="' . $request['duration'] . '"
                            StopQuantity="' . $request['stop_quantity'] . '"
                            FlightNumber="' . $request['flight_number'] . '"
                            ResBookDesigCode="' . $request['booking_desig_code'] . '"
                            NumberInParty="' . count(request('traveller_type')) . '"
                            DepartureTerminal="' . $request['departure_terminal'] . '"
                            ArrivalTerminal="' . $request['arrival_terminal'] . '">
                        <Equipment AirEquipType="' . $request['air_equip_type'] . '" />
                        <DepartureAirport LocationCode="' . strtoupper($request['departure_location_code']) . '"
                        CodeContext="' . strtoupper($request['departure_code_context']) . '" />
                        <ArrivalAirport LocationCode="' . strtoupper($request['arrival_location_code']) . '" CodeContext="' . strtoupper($request['arrival_code_context']) . '" />
                        <OperatingAirline Code="' . strtoupper($request['operating_airline_code']) . '" />
                        <MarketingAirline Code="' . strtoupper($request['operating_airline_code']) . '" />
                    </FlightSegment>';
        $data .= '</OriginDestinationOption>
                </OriginDestinationOptions>
            </AirItinerary>
            <TravelerInfo>';

        for ($i = 0; $i < count(request('traveller_type')); $i++) {

            $dob = (\DateTime::createFromFormat('d/m/Y', request("date_of_birth.$i")))->format('m/d/Y');

            $data .= '<AirTraveler PassengerTypeCode="' . strtoupper(request('traveller_type.'.$i)) . '">
                        <PersonName>
                            <NamePrefix>' . request("name_prefix.$i") . '</NamePrefix>
                            <GivenName>' . request("name.{$i}.first_name", "default") . '</GivenName>
                            <Surname>' . request("name.{$i}.last_name", "default") . '</Surname>
                        </PersonName>
                        <Telephone PhoneNumber="' . request("phone.$i") . '" />
                        <Document
                            DocIssueLocation="' . request("doc_issue_location.$i") . '"
                            DocID="ABC1234"
                            Gender="' . request("gender.$i") . '"
                            Nationality="' . strtoupper(request("nationality.$i")) . '"
                            BirthDate="' . $dob . '"
                            ExpireDate="09/19/2030" />
                    </AirTraveler>';
        }

        $data .= '</TravelerInfo>
                <TPA_Extensions>
                    <SearchId>' . $request['search_id'] . '</SearchId>
                    <TripId>' . $request["trip_id"] . '</TripId>
                    <Session_ID>' . $session_id . '</Session_ID>
                    <HoldPNR>N</HoldPNR>
                </TPA_Extensions>
            </OTA_AirProvBookRQ>]]>

                </prod:RequestXML>
                </prod:performOperation>
                </soapenv:Body>
                </soapenv:Envelope>';
        return $data;
    }

    public function bookFlightData($request, $session_id)
    {
        // \Log::info('-------------------------------------BOOKING REQUEST-------------------------------------');
        // \Log::info($request->all());
        $data = '<soapenv:Envelope
                        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                        xmlns:prod="http://webservices.vtech.com">
                        <soapenv:Header/>
                        <soapenv:Body>
                            <prod:performOperation>
                                <prod:ProductName>air</prod:ProductName>
                                <prod:QueryType>booking</prod:QueryType>
                                <prod:RequestXML><![CDATA[<OTA_AirBookRQ>
                            <AirItinerary>
                                <OriginDestinationOptions>
                                <OriginDestinationOption>';
        for ($i = 0; $i < count($request->departure_date_and_time); $i++) {
            $data .= '<FlightSegment
                                        DepartureDateTime="' . $request->departure_date_and_time[$i] . '"
                                        ArrivalDateTime="' . $request->arrival_date_and_time[$i] . '"
                                        Duration="' . $request->duration[$i] . '"
                                        StopQuantity="' . $request->stop_quantity[$i] . '"
                                        FlightNumber="' . $request->flight_number[$i] . '"
                                        ResBookDesigCode="' . $request->booking_desig_code[$i] . '"
                                        Status=""
                                        NumberInParty="' . count($request->traveller_type) . '"
                                        DepartureTerminal="' . $request->departure_terminal[$i] . '"
                                        ArrivalTerminal="' . $request->arrival_terminal[$i] . '">
                                    <Equipment AirEquipType="' . $request->air_equip_type[$i] . '" />
                                    <DepartureAirport LocationCode="' . strtoupper($request->departure_location_code[$i]) . '"
                                    CodeContext="' . strtoupper($request->departure_code_context[$i]) . '" />
                                    <ArrivalAirport LocationCode="' . strtoupper($request->arrival_location_code[$i]) . '" CodeContext="' . strtoupper($request->arrival_code_context[$i]) . '" />
                                    <OperatingAirline Code="' . strtoupper($request->operating_airline_code[$i]) . '" />
                                    <MarketingAirline Code="' . strtoupper($request->operating_airline_code[$i]) . '" />
                                </FlightSegment>';
        }

        $data .= '</OriginDestinationOption>
                                </OriginDestinationOptions>
                            </AirItinerary>
                             <TravelerInfo>';
        for ($i = 0; $i < count($request->traveller_type); $i++) {

            $phone = '04968189932';
            $amount = $request->amount;
            $phone = $request->phone[$i];

            $name = ucfirst(request("name.{$i}"));
            $middlename = ucfirst(request("middle_name.{$i}",""));

            if($middlename === 'Null')
                $middlename = '';
            
            $name = $name . $middlename;

            $data .= '<AirTraveler PassengerTypeCode="' . strtoupper($request->traveller_type[$i]) . '">
                        <PersonName>
                            <NamePrefix>' . $request->name_prefix[$i] . '</NamePrefix>
                            <GivenName>' . $name . '</GivenName>
                            <Surname>' . $request->surname[$i] . '</Surname>
                        </PersonName>
                        <Telephone PhoneNumber="' . $phone . '" />
                        <Document
                            Gender="' . $request->gender[$i] . '"
                            BirthDate="' . date('m/d/Y', strtotime($request->date_of_birth[$i])) . '"
                        />
                    </AirTraveler>';
        }

        $data .= '</TravelerInfo>
                            <TPA_Extensions>
                                <SearchId>' . $request->search_id . '</SearchId>
                                <TripId>' . $request->trip_id . '</TripId>
                                <Session_ID>' . $session_id . '</Session_ID>
                                <PaymentDetails>
                                    <PaymentMode>CR</PaymentMode>
                                    <PaymentRefernceNo>NA</PaymentRefernceNo>
                                    <PaymentRemarks>NA</PaymentRemarks>
                                    <TransactionAmount>' . $amount . '</TransactionAmount>
                                    <PaymentMethod>PT</PaymentMethod>
                                    <LpoNumber/>
                                </PaymentDetails>
                            </TPA_Extensions>
                        </OTA_AirBookRQ>]]>
                    </prod:RequestXML>
                            </prod:performOperation>
                        </soapenv:Body>
                    </soapenv:Envelope>
                    ';
        return $data;
    }

    public function bookFlightDataWeb($request, $session_id)
    {
        $data = '<soapenv:Envelope
                        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                        xmlns:prod="http://webservices.vtech.com">
                        <soapenv:Header/>
                        <soapenv:Body>
                            <prod:performOperation>
                                <prod:ProductName>air</prod:ProductName>
                                <prod:QueryType>booking</prod:QueryType>
                                <prod:RequestXML><![CDATA[<OTA_AirBookRQ>
                            <AirItinerary>
                                <OriginDestinationOptions>
                                <OriginDestinationOption>';
        for ($i = 0; $i < count($request->departure_date_and_time); $i++) {
            $data .= '<FlightSegment
                            DepartureDateTime="' . $request->departure_date_and_time[$i] . '"
                            ArrivalDateTime="' . $request->arrival_date_and_time[$i] . '"
                            Duration="' . $request->duration[$i] . '"
                            StopQuantity="' . $request->stop_quantity[$i] . '"
                            FlightNumber="' . $request->flight_number[$i] . '"
                            ResBookDesigCode="' . $request->booking_desig_code[$i] . '"
                            Status=""
                            NumberInParty="' . count($request->traveller_type) . '"
                            DepartureTerminal="' . $request->departure_terminal[$i] . '"
                            ArrivalTerminal="' . $request->arrival_terminal[$i] . '">
                        <Equipment AirEquipType="' . $request->air_equip_type[$i] . '" />
                        <DepartureAirport LocationCode="' . strtoupper($request->departure_location_code[$i]) . '"
                        CodeContext="' . strtoupper($request->departure_code_context[$i]) . '" />
                        <ArrivalAirport LocationCode="' . strtoupper($request->arrival_location_code[$i]) . '" CodeContext="' . strtoupper($request->arrival_code_context[$i]) . '" />
                        <OperatingAirline Code="' . strtoupper($request->operating_airline_code[$i]) . '" />
                        <MarketingAirline Code="' . strtoupper($request->operating_airline_code[$i]) . '" />
                    </FlightSegment>';
        }
        $data .= '</OriginDestinationOption>
                                </OriginDestinationOptions>
                            </AirItinerary>
                             <TravelerInfo>';
//        dd(request("name", "default"));

        for ($i = 0; $i < count($request->traveller_type); $i++) {
            $dob = (\DateTime::createFromFormat('d/m/Y', $request->date_of_birth[$i]))->format('m/d/Y');
//            $dob = '09/19/1989';
            $data .= '<AirTraveler PassengerTypeCode="' . strtoupper($request->traveller_type[$i]) . '">
                        <PersonName>
                            <NamePrefix>' . $request->name_prefix[$i] . '</NamePrefix>
                            <GivenName>' . request("name.$i.first_name", "default") . '</GivenName>
                            <Surname>' . str_replace(['_', ' '], '', request("name.$i.last_name")) . '</Surname>
                        </PersonName>
                        <Telephone PhoneNumber="' . $request->phone[$i] . '" />
                        <Document
                            DocIssueLocation="' . $request->doc_issue_location[$i] . '"
                            DocID="ABC1234"
                            Gender="' . $request->gender[$i] . '"
                            Nationality="' . strtoupper($request->nationality[$i]) . '"
                            BirthDate="' . $dob . '"
                            ExpireDate="09/19/2030" />
                    </AirTraveler>';
        }

        $data .= '</TravelerInfo>
                            <TPA_Extensions>
                                <SearchId>' . $request->search_id . '</SearchId>
                                <TripId>' . $request->trip_id . '</TripId>
                                <Office Code="007"/>
                                <Session_ID>' . $session_id . '</Session_ID>
                                <PaymentDetails>
                                    <PaymentMode>CR</PaymentMode>
                                    <PaymentRefernceNo>NA</PaymentRefernceNo>
                                    <PaymentRemarks>NA</PaymentRemarks>
                                    <TransactionAmount>' . $request->amount . '</TransactionAmount>
                                    <PaymentMethod>PT</PaymentMethod>
                                    <LpoNumber/>
                                </PaymentDetails>
                            </TPA_Extensions>
                        </OTA_AirBookRQ>]]>
                    </prod:RequestXML>
                            </prod:performOperation>
                        </soapenv:Body>
                    </soapenv:Envelope>
                    ';
        return $data;
    }

    public function bookingLog(Request $request)
    {
        $bookings = Booking::select('trip_id', 'booking_id', 'date_of_issue', 'fare', 'status', 'created_at')->whereUserId($request->user()->id)->get();
        return response()->json(['data' => $bookings], 200);
    }

    public function bookingDetail(Request $request)
    {
        $booking_detail = Booking::with('payment')->whereTripIdAndUserId($request->trip_id, $request->user()->id)->first();
        $detail['trip_id'] = $booking_detail->trip_id;
        $detail['booking_id'] = $booking_detail->booking_id;
        $detail['date_of_issue'] = date('F m Y', strtotime($booking_detail->date_of_issue));
        $detail['fare'] = $booking_detail->fare;
        $detail['currency_code'] = $booking_detail->currency_code;
        $detail['status'] = $booking_detail->status == "1" ? "paid" : "unpaid";
        $detail['date'] = date('F m Y', strtotime($booking_detail->created_at));
        $detail['detail'] = $this->getBookFlightResponse(unserialize($booking_detail->detail), $booking_detail->no_of_traveller);
        $detail['transaction_id'] = $booking_detail->payment['transaction_id'];
        $detail['payment_through'] = $booking_detail->payment['payment_through'];
        $detail['payment_fare'] = $booking_detail->payment['fares'];
        $detail['payment_currency_code'] = $booking_detail->payment['currency_code'];
        return response()->json(['data' => $detail], 200);
    }

    public function paymentLog(Request $request)
    {
        $payments = [];
        $payments['booking'] = BookingPayment::join('bookings', 'bookings.id', '=', 'booking_payments.booking_id')->where('bookings.user_id', $request->user()->id)->select('booking_payments.*')->get();
        $payments['account'] = AccountPayment::whereUserId($request->user()->id)->get();
        return response()->json(['data' => $payments], 200);
    }

    public function getOneWaySearchReponse($response)
    {
        $record = [];
        foreach ($response['PricedItineraries']['PricedItinerary'] as $data)
        {
            $rec = [];
            $flyer = [];
            $flight_details = [];
            $rec['trip_id'] = @$data['@attributes']['SequenceNumber'];
            $rec['search_id'] = $data['@attributes']['SearchId'];
            $rec['onward_stops'] = $data['AirItinerary']['@attributes']['OnwardStops'];
            $flightData = $data['AirItinerary']['OriginDestinationOptions']['OriginDestinationOption']['FlightSegment'];
            if ($rec['onward_stops'] > 0) {
                foreach ($flightData as $segment) {
                    $fli['departure_date_time'] = $segment['@attributes']['DepartureDateTime'];
                    $fli['arrival_date_time'] = $segment['@attributes']['ArrivalDateTime'];
                    $fli['departure_date_formated'] = date('M d Y', strtotime($segment['@attributes']['DepartureDateTime']));
                    $fli['arrival_date_formated'] = date('M d Y', strtotime($segment['@attributes']['ArrivalDateTime']));
                    $fli['departure_time_formated'] = date('H:i', strtotime($segment['@attributes']['DepartureDateTime']));
                    $fli['arrival_time_formated'] = date('H:i', strtotime($segment['@attributes']['ArrivalDateTime']));
                    $fli['duration'] = $segment['@attributes']['Duration'];
                    $fli['duration_format'] = date('H:i', mktime(0, intval($segment['@attributes']['Duration'])));
                    $fli['stop_quantity'] = $segment['@attributes']['StopQuantity'];
                    $fli['flight_number'] = $segment['@attributes']['FlightNumber'];
                    $fli['res_book_desig_code'] = $segment['@attributes']['ResBookDesigCode'];
                    $fli['number_in_party'] = $segment['@attributes']['NumberInParty'];
                    $fli['departure_terminal'] = $segment['@attributes']['DepartureTerminal'];
                    $fli['arrival_terminal'] = $segment['@attributes']['ArrivalTerminal'];
                    $fli['air_equip_type'] = $segment['Equipment']['@attributes']['AirEquipType'];
                    $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                    $fli['departure_location_code'] = $segment['DepartureAirport']['@attributes']['LocationCode'];
                    $fli['airport_departure'] = getAirportDetails($fli['departure_location_code']);
                    $fli['departure_code_context'] = $segment['DepartureAirport']['@attributes']['CodeContext'];
                    $fli['arrival_location_code'] = $segment['ArrivalAirport']['@attributes']['LocationCode'];
                    $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code']);
                    $fli['arrival_code_context'] = $segment['ArrivalAirport']['@attributes']['CodeContext'];
                    $fli['operating_airline'] = $segment['OperatingAirline']['@attributes']['Code'];
                    $fli['operating_airline_name'] = getAirlineName($fli['operating_airline']);
                    $fli['marketing_airline'] = $segment['MarketingAirline']['@attributes']['Code'];
                    $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline']);
                    $fli['cabin_class'] = $segment['CabinClass'];
                    $fli['baggage_allowance'] = @$segment['BaggageAllowance']['Baggage'];

                    $flight_details[] = $fli;
                }
            } else {
                $fli['departure_date_time'] = $flightData['@attributes']['DepartureDateTime'];
                $fli['arrival_date_time'] = $flightData['@attributes']['ArrivalDateTime'];
                $fli['departure_date_formated'] = date('M d Y', strtotime($flightData['@attributes']['DepartureDateTime']));
                $fli['arrival_date_formated'] = date('M d Y', strtotime($flightData['@attributes']['ArrivalDateTime']));
                $fli['departure_time_formated'] = date('H:i', strtotime($flightData['@attributes']['DepartureDateTime']));
                $fli['arrival_time_formated'] = date('H:i', strtotime($flightData['@attributes']['ArrivalDateTime']));
                $fli['duration'] = $flightData['@attributes']['Duration'];
                $fli['duration_format'] = date('H:i', mktime(0, intval($flightData['@attributes']['Duration'])));
                $fli['stop_quantity'] = $flightData['@attributes']['StopQuantity'];
                $fli['flight_number'] = $flightData['@attributes']['FlightNumber'];
                $fli['res_book_desig_code'] = $flightData['@attributes']['ResBookDesigCode'];
                $fli['number_in_party'] = $flightData['@attributes']['NumberInParty'];
                $fli['departure_terminal'] = $flightData['@attributes']['DepartureTerminal'];
                $fli['arrival_terminal'] = $flightData['@attributes']['ArrivalTerminal'];
                $fli['air_equip_type'] = $flightData['Equipment']['@attributes']['AirEquipType'];
                $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                $fli['departure_location_code'] = $flightData['DepartureAirport']['@attributes']['LocationCode'];
                $fli['departure_code_context'] = $flightData['DepartureAirport']['@attributes']['CodeContext'];
                $fli['arrival_location_code'] = $flightData['ArrivalAirport']['@attributes']['LocationCode'];
                $fli['arrival_code_context'] = $flightData['ArrivalAirport']['@attributes']['CodeContext'];
                $fli['operating_airline'] = $flightData['OperatingAirline']['@attributes']['Code'];
                $fli['marketing_airline'] = $flightData['MarketingAirline']['@attributes']['Code'];
                $fli['cabin_class'] = $flightData['CabinClass'];
                $fli['baggage_allowance'] = @$flightData['BaggageAllowance']['Baggage'];

                $fli['airport_departure'] = getAirportDetails($fli['departure_location_code']);
                $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code']);
                $fli['operating_airline_name'] = getAirlineName($fli['operating_airline']);
                $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline']);


                $flight_details[] = $fli;
            }
            $rec['flights'] = $flight_details;

            if (!isset($data['AirItineraryPricingInfo']['TPA_Extensions']['FareType'])) {
                $fly = [];
                foreach ($data['AirItineraryPricingInfo']['TPA_Extensions'] as $passenger) {
                    $fly['fare_type'] = $passenger['FareType'];
                    $fly['fare_reference'] = $passenger['FareReference']['@attributes']['Code'];
                    $fly['base_fare_currency_code'] = $passenger['BaseFare']['@attributes']['CurrencyCode'];
                    $fly['base_fare_amount'] = $passenger['BaseFare']['@attributes']['Amount'];
                    $fly['tax_currency_code'] = $passenger['TotalTax']['@attributes']['CurrencyCode'];
                    $fly['tax_amount'] = $passenger['TotalTax']['@attributes']['Amount'];
                    $fly['total_fare_currency_code'] = $passenger['TotalFare']['@attributes']['CurrencyCode'];
                    $fly['total_fare_amount'] = $passenger['TotalFare']['@attributes']['Amount'];
                    $fly['passenger_type'] = $passenger['Pax']['@attributes']['Type'];
                    $fly['passenger_quantity'] = $passenger['Pax']['@attributes']['Quantity'];
                    $this->appendClass($fly);

                    $flyer[] = $fly;
                }

                $rec['flyer'] = $flyer;
            } else {
                $fly['fare_type'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['FareType'];
                $fly['fare_reference'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['FareReference']['@attributes']['Code'];
                $fly['base_fare_currency_code'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['BaseFare']['@attributes']['CurrencyCode'];
                $fly['base_fare_amount'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['BaseFare']['@attributes']['Amount'];
                $fly['tax_currency_code'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['TotalTax']['@attributes']['CurrencyCode'];
                $fly['tax_amount'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['TotalTax']['@attributes']['Amount'];
                $fly['total_fare_currency_code'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['TotalFare']['@attributes']['CurrencyCode'];
                $fly['total_fare_amount'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['TotalFare']['@attributes']['Amount'];
                $fly['passenger_type'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['Pax']['@attributes']['Type'];
                $fly['passenger_quantity'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['Pax']['@attributes']['Quantity'];

                $this->appendClass($fly);

                $flyer[] = $fly;


                $rec['flyer'] = $flyer;
            }

            $f['base_fare_currency_code'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['BaseFare']['@attributes']['CurrencyCode'];
            $f['base_fare_amount'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['BaseFare']['@attributes']['Amount'];
            $f['tax_currency_code'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalTax']['@attributes']['CurrencyCode'];
            $f['tax_amount'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalTax']['@attributes']['Amount'];
            $f['total_fare_currency_code'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['@attributes']['CurrencyCode'];
//            $commission = Commission::whereStatus(1)->first()->percentage;
//            $f['commission'] = ($commission * $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['@attributes']['Amount']) / 100;
//            $f['commission_percentage'] = $commission;
            $f['total_fare_amount'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['@attributes']['Amount'];
//            $f['total_fare_amount_after_commission'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['@attributes']['Amount'];

            $rec['fare'] = $f;

            $record[] = $rec;
        }

        return $record;
    }

    protected function appendClass(&$fly)
    {
        $starter = \config('jetstar_class.STARTER.codes');
        $flex = \config('jetstar_class.FLEX.codes');
        $plus = \config('jetstar_class.PLUS.codes');
        $max = \config('jetstar_class.MAX.codes');
        $business = \config('jetstar_class.BUSINESS.codes');

        switch (true){
            case in_array($fly['fare_reference'], $starter):
                $fly['carry_on'] = config('jetstar_class.STARTER.carry_on');
                $fly['checked'] = config('jetstar_class.STARTER.checked');
                $fly['class'] = "STARTER";
                break;
            case in_array($fly['fare_reference'], $flex):
                $fly['carry_on'] = config('jetstar_class.FLEX.carry_on');
                $fly['checked'] = config('jetstar_class.FLEX.checked');
                $fly['class'] = "FLEX";
                break;
            case in_array($fly['fare_reference'], $plus):
                $fly['carry_on'] = config('jetstar_class.PLUS.carry_on');
                $fly['checked'] = config('jetstar_class.PLUS.checked');
                $fly['class'] = "PLUS";
                break;
            case in_array($fly['fare_reference'], $max):
                $fly['carry_on'] = config('jetstar_class.MAX.carry_on');
                $fly['checked'] = config('jetstar_class.MAX.checked');
                $fly['class'] = "MAX";
                break;
            case in_array($fly['fare_reference'], $business):
                $fly['carry_on'] = config('jetstar_class.BUSINESS.carry_on');
                $fly['checked'] = config('jetstar_class.BUSINESS.checked');
                $fly['class'] = "BUSINESS";
                break;
            default:
                $fly['carry_on'] = "-";
                $fly['checked'] = "-";
                $fly['class'] = "-";
                break;
        }
    }

    public function getReturnSearchReponse($response)
    {
        $record = [];
        foreach ($response['PricedItineraries']['PricedItinerary'] as $data) {
//            dd($data);
            $rec = [];
            $flyer = [];
            $flight_details = [];
            $rec['trip_id'] = $data['@attributes']['SequenceNumber'];
            $rec['search_id'] = $data['@attributes']['SearchId'];
            $rec['onward_stops'] = $data['AirItinerary']['@attributes']['OnwardStops'];
            $rec['return_stops'] = $data['AirItinerary']['@attributes']['ReturnStops'];

            foreach ($data['AirItinerary']['OriginDestinationOptions']['OriginDestinationOption'] as $segments) {
                $fli = [];
                if (isset($segments['FlightSegment']['@attributes'])) {
                    $fli['departure_date_time'] = $segments['FlightSegment']['@attributes']['DepartureDateTime'];
                    $fli['arrival_date_time'] = $segments['FlightSegment']['@attributes']['ArrivalDateTime'];
                    $fli['departure_date_formated'] = date('M d Y', strtotime($segments['FlightSegment']['@attributes']['DepartureDateTime']));
                    $fli['arrival_date_formated'] = date('M d Y', strtotime($segments['FlightSegment']['@attributes']['ArrivalDateTime']));
                    $fli['departure_time_formated'] = date('H:i', strtotime($segments['FlightSegment']['@attributes']['DepartureDateTime']));
                    $fli['arrival_time_formated'] = date('H:i', strtotime($segments['FlightSegment']['@attributes']['ArrivalDateTime']));
                    $fli['duration'] = $segments['FlightSegment']['@attributes']['Duration'];
                    $fli['duration_format'] = date('H:i', mktime(0, intval($segments['FlightSegment']['@attributes']['Duration'])));
                    $fli['stop_quantity'] = $segments['FlightSegment']['@attributes']['StopQuantity'];
                    $fli['flight_number'] = $segments['FlightSegment']['@attributes']['FlightNumber'];
                    $fli['res_book_desig_code'] = $segments['FlightSegment']['@attributes']['ResBookDesigCode'];
                    $fli['number_in_party'] = $segments['FlightSegment']['@attributes']['NumberInParty'];
                    $fli['departure_terminal'] = $segments['FlightSegment']['@attributes']['DepartureTerminal'];
                    $fli['arrival_terminal'] = $segments['FlightSegment']['@attributes']['ArrivalTerminal'];
                    $fli['air_equip_type'] = $segments['FlightSegment']['Equipment']['@attributes']['AirEquipType'];
                    $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                    $fli['departure_location_code'] = $segments['FlightSegment']['DepartureAirport']['@attributes']['LocationCode'];
                    $fli['departure_code_context'] = $segments['FlightSegment']['DepartureAirport']['@attributes']['CodeContext'];
                    $fli['arrival_location_code'] = $segments['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode'];
                    $fli['arrival_code_context'] = $segments['FlightSegment']['ArrivalAirport']['@attributes']['CodeContext'];
                    $fli['operating_airline'] = $segments['FlightSegment']['OperatingAirline']['@attributes']['Code'];
                    $fli['marketing_airline'] = $segments['FlightSegment']['MarketingAirline']['@attributes']['Code'];
                    $fli['cabin_class'] = $segments['FlightSegment']['CabinClass'];
                    $fli['baggage_allowance'] = @$segments['BaggageAllowance']['Baggage'];

                    $fli['airport_departure'] = getAirportDetails($fli['departure_location_code'])->toArray();
                    $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code'])->toArray();
                    $fli['operating_airline_name'] = getAirlineName($fli['operating_airline'])->toArray();
                    $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline'])->toArray();


                    $flight_details[] = $fli;
                } else {
                    if (isset($segments['FlightSegment'])) {
                        foreach ($segments['FlightSegment'] as $flight) {
                            $fli['departure_date_time'] = $flight['@attributes']['DepartureDateTime'];
                            $fli['arrival_date_time'] = $flight['@attributes']['ArrivalDateTime'];
                            $fli['departure_date_formated'] = date('M d Y', strtotime($flight['@attributes']['DepartureDateTime']));
                            $fli['arrival_date_formated'] = date('M d Y', strtotime($flight['@attributes']['ArrivalDateTime']));
                            $fli['departure_time_formated'] = date('H:i', strtotime($flight['@attributes']['DepartureDateTime']));
                            $fli['arrival_time_formated'] = date('H:i', strtotime($flight['@attributes']['ArrivalDateTime']));
                            $fli['duration'] = $flight['@attributes']['Duration'];
                            $fli['duration_format'] = date('H:i', mktime(0, intval($flight['@attributes']['Duration'])));
                            $fli['stop_quantity'] = $flight['@attributes']['StopQuantity'];
                            $fli['flight_number'] = $flight['@attributes']['FlightNumber'];
                            $fli['res_book_desig_code'] = $flight['@attributes']['ResBookDesigCode'];
                            $fli['number_in_party'] = $flight['@attributes']['NumberInParty'];
                            $fli['departure_terminal'] = $flight['@attributes']['DepartureTerminal'];
                            $fli['arrival_terminal'] = $flight['@attributes']['ArrivalTerminal'];
                            $fli['air_equip_type'] = $flight['Equipment']['@attributes']['AirEquipType'];
                            $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                            $fli['departure_location_code'] = $flight['DepartureAirport']['@attributes']['LocationCode'];
                            $fli['departure_code_context'] = $flight['DepartureAirport']['@attributes']['CodeContext'];
                            $fli['arrival_location_code'] = $flight['ArrivalAirport']['@attributes']['LocationCode'];
                            $fli['arrival_code_context'] = $flight['ArrivalAirport']['@attributes']['CodeContext'];
                            $fli['operating_airline'] = $flight['OperatingAirline']['@attributes']['Code'];
                            $fli['marketing_airline'] = $flight['MarketingAirline']['@attributes']['Code'];
                            $fli['cabin_class'] = $flight['CabinClass'];
                            $fli['baggage_allowance'] = @$flight['BaggageAllowance']['Baggage'];

                            $fli['airport_departure'] = getAirportDetails($fli['departure_location_code'])->toArray();
                            $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code'])->toArray();
                            $fli['operating_airline_name'] = getAirlineName($fli['operating_airline'])->toArray();
                            $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline'])->toArray();


                            $flight_details[] = $fli;
                        }
                    } else {
                        foreach ($segments as $flight) {
                            $fli['departure_date_time'] = $flight['@attributes']['DepartureDateTime'];
                            $fli['arrival_date_time'] = $flight['@attributes']['ArrivalDateTime'];
                            $fli['departure_date_formated'] = date('M d Y', strtotime($flight['@attributes']['DepartureDateTime']));
                            $fli['arrival_date_formated'] = date('M d Y', strtotime($flight['@attributes']['ArrivalDateTime']));
                            $fli['departure_time_formated'] = date('H:i', strtotime($flight['@attributes']['DepartureDateTime']));
                            $fli['arrival_time_formated'] = date('H:i', strtotime($flight['@attributes']['ArrivalDateTime']));
                            $fli['duration'] = $flight['@attributes']['Duration'];
                            $fli['duration_format'] = date('H:i', mktime(0, intval($flight['@attributes']['Duration'])));
                            $fli['stop_quantity'] = $flight['@attributes']['StopQuantity'];
                            $fli['flight_number'] = $flight['@attributes']['FlightNumber'];
                            $fli['res_book_desig_code'] = $flight['@attributes']['ResBookDesigCode'];
                            $fli['number_in_party'] = $flight['@attributes']['NumberInParty'];
                            $fli['departure_terminal'] = $flight['@attributes']['DepartureTerminal'];
                            $fli['arrival_terminal'] = $flight['@attributes']['ArrivalTerminal'];
                            $fli['air_equip_type'] = $flight['Equipment']['@attributes']['AirEquipType'];
                            $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                            $fli['departure_location_code'] = $flight['DepartureAirport']['@attributes']['LocationCode'];
                            $fli['departure_code_context'] = $flight['DepartureAirport']['@attributes']['CodeContext'];
                            $fli['arrival_location_code'] = $flight['ArrivalAirport']['@attributes']['LocationCode'];
                            $fli['arrival_code_context'] = $flight['ArrivalAirport']['@attributes']['CodeContext'];
                            $fli['operating_airline'] = $flight['OperatingAirline']['@attributes']['Code'];
                            $fli['marketing_airline'] = $flight['MarketingAirline']['@attributes']['Code'];
                            $fli['cabin_class'] = $flight['CabinClass'];
                            $fli['baggage_allowance'] = @$flight['BaggageAllowance']['Baggage'];

                            $fli['airport_departure'] = getAirportDetails($fli['departure_location_code'])->toArray();
                            $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code'])->toArray();
                            $fli['operating_airline_name'] = getAirlineName($fli['operating_airline'])->toArray();
                            $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline'])->toArray();


                            $flight_details[] = $fli;
                        }
                    }
                }
            }
            $rec['flights'] = $flight_details;
            if (!isset($data['AirItineraryPricingInfo']['TPA_Extensions']['FareType'])) {
                $fly = [];
                foreach ($data['AirItineraryPricingInfo']['TPA_Extensions'] as $passenger) {
                    $fly['fare_type'] = $passenger['FareType'];
                    $fly['fare_reference'] = $passenger['FareReference']['@attributes']['Code'];
                    $fly['base_fare_currency_code'] = $passenger['BaseFare']['@attributes']['CurrencyCode'];
                    $fly['base_fare_amount'] = $passenger['BaseFare']['@attributes']['Amount'];
                    $fly['tax_currency_code'] = $passenger['TotalTax']['@attributes']['CurrencyCode'];
                    $fly['tax_amount'] = $passenger['TotalTax']['@attributes']['Amount'];
                    $fly['total_fare_currency_code'] = $passenger['TotalFare']['@attributes']['CurrencyCode'];
                    $fly['total_fare_amount'] = $passenger['TotalFare']['@attributes']['Amount'];
                    $fly['passenger_type'] = $passenger['Pax']['@attributes']['Type'];
                    $fly['passenger_quantity'] = $passenger['Pax']['@attributes']['Quantity'];

                    $flyer[] = $fly;
                }

                $rec['flyer'] = $flyer;
            } else {
                $fly['fare_type'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['FareType'];
                $fly['fare_reference'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['FareReference']['@attributes']['Code'];
                $fly['base_fare_currency_code'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['BaseFare']['@attributes']['CurrencyCode'];
                $fly['base_fare_amount'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['BaseFare']['@attributes']['Amount'];
                $fly['tax_currency_code'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['TotalTax']['@attributes']['CurrencyCode'];
                $fly['tax_amount'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['TotalTax']['@attributes']['Amount'];
                $fly['total_fare_currency_code'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['TotalFare']['@attributes']['CurrencyCode'];
                $fly['total_fare_amount'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['TotalFare']['@attributes']['Amount'];
                $fly['passenger_type'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['Pax']['@attributes']['Type'];
                $fly['passenger_quantity'] = $data['AirItineraryPricingInfo']['TPA_Extensions']['Pax']['@attributes']['Quantity'];
                $flyer[] = $fly;

                $rec['flyer'] = $flyer;
            }

            $f['base_fare_currency_code'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['BaseFare']['@attributes']['CurrencyCode'];
            $f['base_fare_amount'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['BaseFare']['@attributes']['Amount'];
            $f['tax_currency_code'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalTax']['@attributes']['CurrencyCode'];
            $f['tax_amount'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalTax']['@attributes']['Amount'];
            $f['total_fare_currency_code'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['@attributes']['CurrencyCode'];
//            $commission = Commission::whereStatus(1)->first()->percentage;
//            $f['commission'] = ($commission * $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['@attributes']['Amount']) / 100;
//            $f['commission_percentage'] = $commission;
            $f['total_fare_amount'] = $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['@attributes']['Amount'];
//            $f['total_fare_amount_after_commission'] = $f['commission'] + $data['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['@attributes']['Amount'];

            $rec['fare'] = $f;

            $record[] = $rec;
        }

        return $record;
    }

    public function getCheckFlightReponse($response, $count, $search_id)
    {
        $rec = [];
        $flyer = [];
        $flight_details = [];
        $rec['trip_id'] = $response['tripid'];
        $rec['gds_pnr'] = is_array($response['gdspnr'])? '': $response['gdspnr'];
        $rec['airline_pnr'] = is_array($response['airlinepnr'])? '': $response['airlinepnr'];
        $rec['last_tkt_date'] = $response['lastTktDate'];
        $rec['Price_changed'] = $response['PriceChanged'];
        $rec['old_fare'] = $response['OldFare'];
        $rec['new_fare'] = $response['NewFare'];
        $rec['merchant_fee'] = setting('merchant_fee', 0);
        $rec['booking_domestic_fee'] = setting('booking_domestic_fee', 0);
        $rec['booking_international_fee'] = setting('booking_international_fee', 0);
        $rec['currency_code'] = $response['ItinTotalFare']['BaseFare']['@attributes']['CurrencyCode'];
        $rec['search_id'] = $search_id;
        foreach ($response['AirItinerary']['OriginDestinationOptions']['OriginDestinationOption'] as $segments) {
            $fli = [];
            if (isset($segments['FlightSegment']['@attributes'])) {
                $fli['departure_date_time'] = $segments['FlightSegment']['@attributes']['DepartureDateTime'];
                $fli['arrival_date_time'] = $segments['FlightSegment']['@attributes']['ArrivalDateTime'];

                $fli['departure_date_formated'] = date('M d Y', strtotime($segments['FlightSegment']['@attributes']['DepartureDateTime']));
                $fli['arrival_date_formated'] = date('M d Y', strtotime($segments['FlightSegment']['@attributes']['ArrivalDateTime']));
                $fli['departure_time_formated'] = date('H:i', strtotime($segments['FlightSegment']['@attributes']['DepartureDateTime']));
                $fli['arrival_time_formated'] = date('H:i', strtotime($segments['FlightSegment']['@attributes']['ArrivalDateTime']));

                $fli['duration'] = $segments['FlightSegment']['@attributes']['Duration'];
                $fli['stop_quantity'] = $segments['FlightSegment']['@attributes']['StopQuantity'];
                $fli['flight_number'] = $segments['FlightSegment']['@attributes']['FlightNumber'];
                $fli['res_book_desig_code'] = $segments['FlightSegment']['@attributes']['ResBookDesigCode'];
                $fli['number_in_party'] = $segments['FlightSegment']['@attributes']['NumberInParty'];
                $fli['departure_terminal'] = $segments['FlightSegment']['@attributes']['DepartureTerminal'];
                $fli['arrival_terminal'] = $segments['FlightSegment']['@attributes']['ArrivalTerminal'];
                $fli['air_equip_type'] = $segments['FlightSegment']['Equipment']['@attributes']['AirEquipType'];
                $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                $fli['departure_location_code'] = $segments['FlightSegment']['DepartureAirport']['@attributes']['LocationCode'];
                $fli['departure_code_context'] = $segments['FlightSegment']['DepartureAirport']['@attributes']['CodeContext'];
                $fli['arrival_location_code'] = $segments['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode'];
                $fli['arrival_code_context'] = $segments['FlightSegment']['ArrivalAirport']['@attributes']['CodeContext'];
                $fli['operating_airline'] = $segments['FlightSegment']['OperatingAirline']['@attributes']['Code'];
                $fli['marketing_airline'] = $segments['FlightSegment']['MarketingAirline']['@attributes']['Code'];

                $fli['airport_departure'] = getAirportDetails($fli['departure_location_code']);
                $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code']);
                $fli['operating_airline_name'] = getAirlineName($fli['operating_airline']);
                $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline']);

                $flight_details[] = $fli;
            } else {
                if (isset($segments['FlightSegment'])) {
                    foreach ($segments['FlightSegment'] as $flight) {
                        $fli['departure_date_time'] = $flight['@attributes']['DepartureDateTime'];
                        $fli['arrival_date_time'] = $flight['@attributes']['ArrivalDateTime'];

                        $fli['departure_date_formated'] = date('M d Y', strtotime($flight['@attributes']['DepartureDateTime']));
                        $fli['arrival_date_formated'] = date('M d Y', strtotime($flight['@attributes']['ArrivalDateTime']));
                        $fli['departure_time_formated'] = date('H:i', strtotime($flight['@attributes']['DepartureDateTime']));
                        $fli['arrival_time_formated'] = date('H:i', strtotime($flight['@attributes']['ArrivalDateTime']));


                        $fli['duration'] = $flight['@attributes']['Duration'];
                        $fli['stop_quantity'] = $flight['@attributes']['StopQuantity'];
                        $fli['flight_number'] = $flight['@attributes']['FlightNumber'];
                        $fli['res_book_desig_code'] = $flight['@attributes']['ResBookDesigCode'];
                        $fli['number_in_party'] = $flight['@attributes']['NumberInParty'];
                        $fli['departure_terminal'] = $flight['@attributes']['DepartureTerminal'];
                        $fli['arrival_terminal'] = $flight['@attributes']['ArrivalTerminal'];
                        $fli['air_equip_type'] = $flight['Equipment']['@attributes']['AirEquipType'];
                        $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                        $fli['departure_location_code'] = $flight['DepartureAirport']['@attributes']['LocationCode'];
                        $fli['departure_code_context'] = $flight['DepartureAirport']['@attributes']['CodeContext'];
                        $fli['arrival_location_code'] = $flight['ArrivalAirport']['@attributes']['LocationCode'];
                        $fli['arrival_code_context'] = $flight['ArrivalAirport']['@attributes']['CodeContext'];
                        $fli['operating_airline'] = $flight['OperatingAirline']['@attributes']['Code'];
                        $fli['marketing_airline'] = $flight['MarketingAirline']['@attributes']['Code'];

                        $fli['airport_departure'] = getAirportDetails($fli['departure_location_code']);
                        $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code']);
                        $fli['operating_airline_name'] = getAirlineName($fli['operating_airline']);
                        $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline']);


                        $flight_details[] = $fli;
                    }
                } else {
                    if (isset($segments['@attributes'])) {
                        $fli['departure_date_time'] = $segments['@attributes']['DepartureDateTime'];
                        $fli['arrival_date_time'] = $segments['@attributes']['ArrivalDateTime'];

                        $fli['departure_date_formated'] = date('M d Y', strtotime($segments['@attributes']['DepartureDateTime']));
                        $fli['arrival_date_formated'] = date('M d Y', strtotime($segments['@attributes']['ArrivalDateTime']));
                        $fli['departure_time_formated'] = date('H:i', strtotime($segments['@attributes']['DepartureDateTime']));
                        $fli['arrival_time_formated'] = date('H:i', strtotime($segments['@attributes']['ArrivalDateTime']));

                        $fli['duration'] = $segments['@attributes']['Duration'];
                        $fli['stop_quantity'] = $segments['@attributes']['StopQuantity'];
                        $fli['flight_number'] = $segments['@attributes']['FlightNumber'];
                        $fli['res_book_desig_code'] = $segments['@attributes']['ResBookDesigCode'];
                        $fli['number_in_party'] = $segments['@attributes']['NumberInParty'];
                        $fli['departure_terminal'] = $segments['@attributes']['DepartureTerminal'];
                        $fli['arrival_terminal'] = $segments['@attributes']['ArrivalTerminal'];
                        $fli['air_equip_type'] = $segments['Equipment']['@attributes']['AirEquipType'];
                        $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                        $fli['departure_location_code'] = $segments['DepartureAirport']['@attributes']['LocationCode'];
                        $fli['departure_code_context'] = $segments['DepartureAirport']['@attributes']['CodeContext'];
                        $fli['arrival_location_code'] = $segments['ArrivalAirport']['@attributes']['LocationCode'];
                        $fli['arrival_code_context'] = $segments['ArrivalAirport']['@attributes']['CodeContext'];
                        $fli['operating_airline'] = $segments['OperatingAirline']['@attributes']['Code'];
                        $fli['marketing_airline'] = $segments['MarketingAirline']['@attributes']['Code'];

                        $fli['airport_departure'] = getAirportDetails($fli['departure_location_code']);
                        $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code']);
                        $fli['operating_airline_name'] = getAirlineName($fli['operating_airline']);
                        $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline']);

                        $flight_details[] = $fli;
                    } else {
                        foreach ($segments as $flight) {
                            $fli['departure_date_time'] = $flight['@attributes']['DepartureDateTime'];
                            $fli['arrival_date_time'] = $flight['@attributes']['ArrivalDateTime'];

                            $fli['departure_date_formated'] = date('M d Y', strtotime($flight['@attributes']['DepartureDateTime']));
                            $fli['arrival_date_formated'] = date('M d Y', strtotime($flight['@attributes']['ArrivalDateTime']));
                            $fli['departure_time_formated'] = date('H:i', strtotime($flight['@attributes']['DepartureDateTime']));
                            $fli['arrival_time_formated'] = date('H:i', strtotime($flight['@attributes']['ArrivalDateTime']));


                            $fli['duration'] = $flight['@attributes']['Duration'];
                            $fli['stop_quantity'] = $flight['@attributes']['StopQuantity'];
                            $fli['flight_number'] = $flight['@attributes']['FlightNumber'];
                            $fli['res_book_desig_code'] = $flight['@attributes']['ResBookDesigCode'];
                            $fli['number_in_party'] = $flight['@attributes']['NumberInParty'];
                            $fli['departure_terminal'] = $flight['@attributes']['DepartureTerminal'];
                            $fli['arrival_terminal'] = $flight['@attributes']['ArrivalTerminal'];
                            $fli['air_equip_type'] = $flight['Equipment']['@attributes']['AirEquipType'];
                            $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                            $fli['departure_location_code'] = $flight['DepartureAirport']['@attributes']['LocationCode'];
                            $fli['departure_code_context'] = $flight['DepartureAirport']['@attributes']['CodeContext'];
                            $fli['arrival_location_code'] = $flight['ArrivalAirport']['@attributes']['LocationCode'];
                            $fli['arrival_code_context'] = $flight['ArrivalAirport']['@attributes']['CodeContext'];
                            $fli['operating_airline'] = $flight['OperatingAirline']['@attributes']['Code'];
                            $fli['marketing_airline'] = $flight['MarketingAirline']['@attributes']['Code'];

                            $fli['airport_departure'] = getAirportDetails($fli['departure_location_code']);
                            $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code']);
                            $fli['operating_airline_name'] = getAirlineName($fli['operating_airline']);
                            $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline']);

                            $flight_details[] = $fli;
                        }
                    }
                }
            }
        }
        $rec['flights'] = $flight_details;
        $fly = [];

        if ($count > 1) {
            foreach ($response['TPA_Extensions']['PaxTypeFare'] as $passenger) {

                if(isset($response['TPA_Extensions']['PaxTypeFare']['Pax'])
                    && isset($response['TPA_Extensions']['PaxTypeFare']['BaseFare'])
                    && isset($response['TPA_Extensions']['PaxTypeFare']['TotalTax'])
                    && isset($response['TPA_Extensions']['PaxTypeFare']['TotalFare'])
                ){
                    $pax = $response['TPA_Extensions']['PaxTypeFare']['Pax'];
                    $base_fare = $response['TPA_Extensions']['PaxTypeFare']['BaseFare'];
                    $total_tax = $response['TPA_Extensions']['PaxTypeFare']['TotalTax'];
                    $total_fare = $response['TPA_Extensions']['PaxTypeFare']['TotalFare'];

                    $fly['type'] = $pax['@attributes']['Type'];
                    $fly['quantity'] = $pax['@attributes']['Quantity'];
                    $fly['base_fare_currency_code'] = $base_fare['@attributes']['CurrencyCode'];
                    $fly['base_fare_amount'] = $base_fare['@attributes']['Amount'];
                    $fly['tax_currency_code'] = $total_tax['@attributes']['CurrencyCode'];
                    $fly['tax_amount'] = $total_tax['@attributes']['Amount'];
                    $fly['total_fare_currency_code'] = $total_fare['@attributes']['CurrencyCode'];
                    $fly['total_fare_amount'] = $total_fare['@attributes']['Amount'];
                    $fly['passenger_type'] = $pax['@attributes']['Type'];

                    $flyer[] = $fly;

                }else{
                    if(is_array($passenger) && isset($passenger[0]['Pax'])){
                        foreach ($passenger as $psngr):
                            $fly['type'] = $psngr['Pax']['@attributes']['Type'];
                            $fly['quantity'] = $psngr['Pax']['@attributes']['Quantity'];
                            $fly['base_fare_currency_code'] = $psngr['BaseFare']['@attributes']['CurrencyCode'];
                            $fly['base_fare_amount'] = $psngr['BaseFare']['@attributes']['Amount'];
                            $fly['tax_currency_code'] = $psngr['TotalTax']['@attributes']['CurrencyCode'];
                            $fly['tax_amount'] = $psngr['TotalTax']['@attributes']['Amount'];
                            $fly['total_fare_currency_code'] = $psngr['TotalFare']['@attributes']['CurrencyCode'];
                            $fly['total_fare_amount'] = $psngr['TotalFare']['@attributes']['Amount'];
                            $fly['passenger_type'] = $psngr['Pax']['@attributes']['Type'];
                        endforeach;
                    }else{
                        $fly['type'] = $passenger['Pax']['@attributes']['Type'];
                        $fly['quantity'] = $passenger['Pax']['@attributes']['Quantity'];
                        $fly['base_fare_currency_code'] = $passenger['BaseFare']['@attributes']['CurrencyCode'];
                        $fly['base_fare_amount'] = $passenger['BaseFare']['@attributes']['Amount'];
                        $fly['tax_currency_code'] = $passenger['TotalTax']['@attributes']['CurrencyCode'];
                        $fly['tax_amount'] = $passenger['TotalTax']['@attributes']['Amount'];
                        $fly['total_fare_currency_code'] = $passenger['TotalFare']['@attributes']['CurrencyCode'];
                        $fly['total_fare_amount'] = $passenger['TotalFare']['@attributes']['Amount'];
                        $fly['passenger_type'] = $passenger['Pax']['@attributes']['Type'];
                    }
                    $flyer[] = $fly;
                }
            }
        } else {
            $passenger = $response['TPA_Extensions']['PaxTypeFare'];
            $fly['type'] = $passenger['Pax']['@attributes']['Type'];
            $fly['quantity'] = $passenger['Pax']['@attributes']['Quantity'];
            $fly['base_fare_currency_code'] = $passenger['BaseFare']['@attributes']['CurrencyCode'];
            $fly['base_fare_amount'] = $passenger['BaseFare']['@attributes']['Amount'];
            $fly['tax_currency_code'] = $passenger['TotalTax']['@attributes']['CurrencyCode'];
            $fly['tax_amount'] = $passenger['TotalTax']['@attributes']['Amount'];
            $fly['total_fare_currency_code'] = $passenger['TotalFare']['@attributes']['CurrencyCode'];
            $fly['total_fare_amount'] = $passenger['TotalFare']['@attributes']['Amount'];
            $fly['passenger_type'] = $passenger['Pax']['@attributes']['Type'];

            $flyer[] = $fly;
        }


        $rec['flyer'] = $flyer;

        $rec['total_fare']['base_fare']['currency_code'] = $response['ItinTotalFare']['BaseFare']['@attributes']['CurrencyCode'];
        $rec['total_fare']['base_fare']['amount'] = $response['ItinTotalFare']['BaseFare']['@attributes']['Amount'];
        $rec['total_fare']['tax']['currency_code'] = $response['ItinTotalFare']['TotalTax']['@attributes']['CurrencyCode'];
        $rec['total_fare']['tax']['amount'] = $response['ItinTotalFare']['TotalTax']['@attributes']['Amount'];
        $rec['total_fare']['total']['currency_code'] = $response['ItinTotalFare']['TotalFare']['@attributes']['CurrencyCode'];
        $rec['total_fare']['total']['amount'] = $response['ItinTotalFare']['TotalFare']['@attributes']['Amount'];

        return $rec;
    }

    public function getBookFlightResponse($response, $count)
    {
        $rec = [];
        $flight_details = [];
        $traveller_details = [];

        $rec['booking_id'] = $response['AirReservation']['BookingReferenceID']['@attributes']['ID'];
        $rec['date_of_issue'] = $response['AirReservation']['BookingReferenceID']['@attributes']['DateOfIssue'];

        foreach ($response['AirReservation']['OriginDestinationOptions']['OriginDestinationOption'] as $segments) {
            $fli = [];
            if (isset($segments['FlightSegment']['@attributes'])) {
                $fli['departure_date_time'] = $segments['FlightSegment']['@attributes']['DepartureDateTime'];
                $fli['arrival_date_time'] = $segments['FlightSegment']['@attributes']['ArrivalDateTime'];
                $fli['duration'] = $segments['FlightSegment']['@attributes']['Duration'];
                $fli['stop_quantity'] = $segments['FlightSegment']['@attributes']['StopQuantity'];
                $fli['flight_number'] = $segments['FlightSegment']['@attributes']['FlightNumber'];
                $fli['res_book_desig_code'] = $segments['FlightSegment']['@attributes']['ResBookDesigCode'];
                $fli['number_in_party'] = $segments['FlightSegment']['@attributes']['NumberInParty'];
                $fli['departure_terminal'] = $segments['FlightSegment']['@attributes']['DepartureTerminal'];
                $fli['arrival_terminal'] = $segments['FlightSegment']['@attributes']['ArrivalTerminal'];
                $fli['air_equip_type'] = $segments['FlightSegment']['Equipment']['@attributes']['AirEquipType'];
                $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                $fli['departure_location_code'] = $segments['FlightSegment']['DepartureAirport']['@attributes']['LocationCode'];
                $fli['departure_code_context'] = $segments['FlightSegment']['DepartureAirport']['@attributes']['CodeContext'];
                $fli['arrival_location_code'] = $segments['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode'];
                $fli['arrival_code_context'] = $segments['FlightSegment']['ArrivalAirport']['@attributes']['CodeContext'];
                $fli['operating_airline'] = $segments['FlightSegment']['OperatingAirline']['@attributes']['Code'];
                $fli['marketing_airline'] = $segments['FlightSegment']['MarketingAirline']['@attributes']['Code'];

                $fli['airport_departure'] = getAirportDetails($fli['departure_location_code']);
                $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code']);
                $fli['operating_airline_name'] = getAirlineName($fli['operating_airline']);
                $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline']);

                $flight_details[] = $fli;
            } else {
                if (isset($segments['FlightSegment'])) {
                    foreach ($segments['FlightSegment'] as $flight) {
                        $fli['departure_date_time'] = $flight['@attributes']['DepartureDateTime'];
                        $fli['arrival_date_time'] = $flight['@attributes']['ArrivalDateTime'];
                        $fli['duration'] = $flight['@attributes']['Duration'];
                        $fli['stop_quantity'] = $flight['@attributes']['StopQuantity'];
                        $fli['flight_number'] = $flight['@attributes']['FlightNumber'];
                        $fli['res_book_desig_code'] = $flight['@attributes']['ResBookDesigCode'];
                        $fli['number_in_party'] = $flight['@attributes']['NumberInParty'];
                        $fli['departure_terminal'] = $flight['@attributes']['DepartureTerminal'];
                        $fli['arrival_terminal'] = $flight['@attributes']['ArrivalTerminal'];
                        $fli['air_equip_type'] = $flight['Equipment']['@attributes']['AirEquipType'];
                        $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                        $fli['departure_location_code'] = $flight['DepartureAirport']['@attributes']['LocationCode'];
                        $fli['departure_code_context'] = $flight['DepartureAirport']['@attributes']['CodeContext'];
                        $fli['arrival_location_code'] = $flight['ArrivalAirport']['@attributes']['LocationCode'];
                        $fli['arrival_code_context'] = $flight['ArrivalAirport']['@attributes']['CodeContext'];
                        $fli['operating_airline'] = $flight['OperatingAirline']['@attributes']['Code'];
                        $fli['marketing_airline'] = $flight['MarketingAirline']['@attributes']['Code'];

                        $fli['airport_departure'] = getAirportDetails($fli['departure_location_code']);
                        $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code']);
                        $fli['operating_airline_name'] = getAirlineName($fli['operating_airline']);
                        $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline']);


                        $flight_details[] = $fli;
                    }
                } else {
                    if (isset($segments['@attributes'])) {
                        $fli['departure_date_time'] = $segments['@attributes']['DepartureDateTime'];
                        $fli['arrival_date_time'] = $segments['@attributes']['ArrivalDateTime'];
                        $fli['duration'] = $segments['@attributes']['Duration'];
                        $fli['stop_quantity'] = $segments['@attributes']['StopQuantity'];
                        $fli['flight_number'] = $segments['@attributes']['FlightNumber'];
                        $fli['res_book_desig_code'] = $segments['@attributes']['ResBookDesigCode'];
                        $fli['number_in_party'] = $segments['@attributes']['NumberInParty'];
                        $fli['departure_terminal'] = $segments['@attributes']['DepartureTerminal'];
                        $fli['arrival_terminal'] = $segments['@attributes']['ArrivalTerminal'];
                        $fli['air_equip_type'] = $segments['Equipment']['@attributes']['AirEquipType'];
                        $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                        $fli['departure_location_code'] = $segments['DepartureAirport']['@attributes']['LocationCode'];
                        $fli['departure_code_context'] = $segments['DepartureAirport']['@attributes']['CodeContext'];
                        $fli['arrival_location_code'] = $segments['ArrivalAirport']['@attributes']['LocationCode'];
                        $fli['arrival_code_context'] = $segments['ArrivalAirport']['@attributes']['CodeContext'];
                        $fli['operating_airline'] = $segments['OperatingAirline']['@attributes']['Code'];
                        $fli['marketing_airline'] = $segments['MarketingAirline']['@attributes']['Code'];

                        $fli['airport_departure'] = getAirportDetails($fli['departure_location_code']);
                        $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code']);
                        $fli['operating_airline_name'] = getAirlineName($fli['operating_airline']);
                        $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline']);

                        $flight_details[] = $fli;
                    } else {
                        foreach ($segments as $flight) {
                            $fli['departure_date_time'] = $flight['@attributes']['DepartureDateTime'];
                            $fli['arrival_date_time'] = $flight['@attributes']['ArrivalDateTime'];
                            $fli['duration'] = $flight['@attributes']['Duration'];
                            $fli['stop_quantity'] = $flight['@attributes']['StopQuantity'];
                            $fli['flight_number'] = $flight['@attributes']['FlightNumber'];
                            $fli['res_book_desig_code'] = $flight['@attributes']['ResBookDesigCode'];
                            $fli['number_in_party'] = $flight['@attributes']['NumberInParty'];
                            $fli['departure_terminal'] = $flight['@attributes']['DepartureTerminal'];
                            $fli['arrival_terminal'] = $flight['@attributes']['ArrivalTerminal'];
                            $fli['air_equip_type'] = $flight['Equipment']['@attributes']['AirEquipType'];
                            $fli['air_equip_name'] = @getAirEquipmentName($fli['air_equip_type'])->equipment_name;
                            $fli['departure_location_code'] = $flight['DepartureAirport']['@attributes']['LocationCode'];
                            $fli['departure_code_context'] = $flight['DepartureAirport']['@attributes']['CodeContext'];
                            $fli['arrival_location_code'] = $flight['ArrivalAirport']['@attributes']['LocationCode'];
                            $fli['arrival_code_context'] = $flight['ArrivalAirport']['@attributes']['CodeContext'];
                            $fli['operating_airline'] = $flight['OperatingAirline']['@attributes']['Code'];
                            $fli['marketing_airline'] = $flight['MarketingAirline']['@attributes']['Code'];

                            $fli['airport_departure'] = getAirportDetails($fli['departure_location_code']);
                            $fli['airport_arrival'] = getAirportDetails($fli['arrival_location_code']);
                            $fli['operating_airline_name'] = getAirlineName($fli['operating_airline']);
                            $fli['marketing_airline_name'] = getAirlineName($fli['marketing_airline']);


                            $flight_details[] = $fli;
                        }
                    }
                }
            }
        }
        $rec['flights'] = $flight_details;

        if ($count > 1) {
            foreach ($response['AirReservation']['TravelerInfo']['AirTraveler'] as $traveller) {
                $trav = [];
                $trav['id'] = $traveller['PersonName']['TravelerId'];
                $trav['prefix'] = $traveller['PersonName']['NamePrefix'];
                $trav['name'] = $traveller['PersonName']['GivenName'];
                $trav['sur_name'] = $traveller['PersonName']['Surname'];
                $trav['ticket_document_number'] = $traveller['TicketDocument']['@attributes']['TicketDocumentNbr'];
                $trav['ticket_document_type'] = $traveller['TicketDocument']['@attributes']['Type'];
                $trav['date_of_ticket_issue'] = $traveller['TicketDocument']['@attributes']['DateOfIssue'];

                $traveller_details[] = $trav;
            }
        } else {
            $traveller = $response['AirReservation']['TravelerInfo']['AirTraveler'];
            $trav['id'] = $traveller['PersonName']['TravelerId'];
            $trav['prefix'] = $traveller['PersonName']['NamePrefix'];
            $trav['name'] = $traveller['PersonName']['GivenName'];
            $trav['sur_name'] = $traveller['PersonName']['Surname'];
            $trav['ticket_document_number'] = $traveller['TicketDocument']['@attributes']['TicketDocumentNbr'];
            $trav['ticket_document_type'] = $traveller['TicketDocument']['@attributes']['Type'];
            $trav['date_of_ticket_issue'] = $traveller['TicketDocument']['@attributes']['DateOfIssue'];

            $traveller_details[] = $trav;
        }
        $rec['traveller_details'] = $traveller_details;

        return $rec;
    }

    public function getPaginatedDataFromSearchResponse($search_session_id, $page_no = 1, $searchId = null, $sessionId = null)
    {
        $rec = [];
        $recordsPerPage = Config::get('constants.records_per_page');
        $search = UserSearchResult::whereSearchSessionId($search_session_id)->first();
        if (empty($search)) {
            return $rec;
        }
        $data = unserialize($search->search_result);
        $recordsPerPage = count($data);
        $page_no = 1;
        $total_pages = ceil(count($data) / $recordsPerPage);
        $start = ($page_no - 1) * $recordsPerPage;
        $end = $start + ($recordsPerPage - 1);
        for ($i = $start; $i <= $end; $i++) {
            if (isset($data[$i])) {
                $rec[] = (array)$data[$i];
            }
        }

        $record['data'] = $rec;
        $rec = [];
        $rec['total_pages'] = $total_pages;
        $rec['current_page'] = $page_no;
        $rec['search_session_id'] = $search_session_id;
        $rec['search_id'] = $searchId;
        $rec['session_id'] = $sessionId;
        $record['details'] = $rec;
        return $record;
    }

    public function getSingleFlightDetail($search_session_id, $trip_id)
    {

    }

    public function getSessionId(){
        $session = $this->getApiResponse($this->signIn('web'), 1);
        return @$session['Session_ID'];
    }

    private function getPaymentValidationRole()
    {
        return [
            'trip_id' => 'required|integer|exists:bookings,trip_id',
            'ccnumber' => 'required|digits_between:16,16',
            'ccmonth' => 'required',
            'ccyear' => 'required|digits_between:4,4',
            'cvv' => 'required|digits_between:3,3',
            'first_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'city' => 'required',
            'zip' => 'required',
            'country_code' => 'required',
        ];
    }
}
