<?php

namespace App\Http\Controllers\api;

use App\FrequentFlyer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class FrequentFlyerController extends Controller
{
    public function create(Request $request)
    {
        Log::info($request->all());
        DB::beginTransaction();

        $user = $request->user();

        $fieldsToValidate = [
            'traveller_type' => ['required', 'string', 'max:50'],
            'name_prefix' => ['required', 'string', 'max:50', Rule::in(['Mr','Mstr','Dr(Male)', 'Miss','Mstr','Ms','Dr(Female)'])],
            'name' => ['required', 'string', 'max:50'],
            'surname' => ['required', 'string', 'max:50'],
            'phone' => ['required', 'string', 'max:50'],
            'doc_issue_location' => ['nullable', 'string', 'max:50'],
            'doc_id' => ['nullable', 'string', 'max:50'],
            'gender' => ['string', 'max:50'],
            'nationality' => ['nullable', 'string', 'max:50'],
            'date_of_birth' => ['required', 'string', 'max:50'],
            'expiry_date' => ['nullable', 'string', 'max:50'],
            'is_default' => ['nullable', 'integer', Rule::in([1])]
        ];

        $request->validate($fieldsToValidate);

        $chk = FrequentFlyer::where('user_id',  $user->id)
        ->where('name', $request->name)
        ->where('surname', $request->surname)
        ->where('middle_name', $request->middle_name)
        ->first();

        if($chk) {
            return response()->json(['message' => 'The Frequent flyer is already added.'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {

            if(in_array($request->name_prefix,['Mr','Mstr','Dr(Male)'])){
                $gender = 'M';
            }

            if(in_array($request->name_prefix,['Miss','Mstr','Ms','Dr(Female)'])){
                $gender = 'F';
            }

            \Log::info($request->all());

            $frequentFlyer = new FrequentFlyer();
            $frequentFlyer->user_id = $user->id;
            $frequentFlyer->traveller_type = $request->traveller_type;
            $frequentFlyer->name_prefix = $request->name_prefix;
            $frequentFlyer->name = $request->name;
            $frequentFlyer->surname = $request->surname;
            $frequentFlyer->middle_name = $request->middle_name;
            $frequentFlyer->phone = $request->phone;
            $frequentFlyer->doc_issue_location = $request->doc_issue_location;
            $frequentFlyer->doc_id = $request->doc_id;
            $frequentFlyer->gender = $gender;
            $frequentFlyer->nationality = $request->nationality;
            $frequentFlyer->date_of_birth = $request->date_of_birth;
            $frequentFlyer->expiry_date = $request->expiry_date;
            $frequentFlyer->postal_code = $request->postal_code;
            $frequentFlyer->save();

            if ($request->is_default) {
                $frequentFlyer->is_default = $request->is_default;
                $frequentFlyer->save();
                FrequentFlyer::whereUserId($user->id)->where('id', '!=', $frequentFlyer->id)->update(['is_default' => 0]);
            }

            DB::commit();

            return response()->json(['message' => 'success', 'details' => $frequentFlyer], 200);


        } catch (\Exception $ex) {

            DB::rollBack();

            Log::error($ex->getMessage());

            return response()->json(['message' => $ex->getMessage()], 500);
        }


    }

    public function update(Request $request)
    {
        DB::beginTransaction();

        $user = $request->user();

        $fieldsToValidate = [
            'id' => ['required', 'integer', Rule::exists('frequent_flyers')->where(function ($q) use ($request) {
                return $q->where(['user_id' => $request->user()->id, 'id' => $request->id]);
            })],
            'traveller_type' => ['required', 'string', 'max:50'],
            'name_prefix' => ['required', 'string', 'max:50'],
            'name' => ['required', 'string', 'max:50'],
            'surname' => ['required', 'string', 'max:50'],
            'phone' => ['required', 'string', 'max:50'],
            'doc_issue_location' => ['nullable', 'string', 'max:50'],
            'doc_id' => ['nullable', 'string', 'max:50'],
            'gender' => ['string', 'max:50'],
            'nationality' => ['nullable', 'string', 'max:50'],
            'date_of_birth' => ['required', 'string', 'max:50'],
            'expiry_date' => ['nullable', 'string', 'max:50'],
            'is_default' => ['nullable', 'integer', Rule::in([1])]
        ];

        $request->validate($fieldsToValidate);

        try {

            $frequentFlyer = FrequentFlyer::find($request->id);
            $frequentFlyer->user_id = $user->id;
            $frequentFlyer->traveller_type = $request->traveller_type;
            $frequentFlyer->name_prefix = $request->name_prefix;
            $frequentFlyer->name = $request->name;
            $frequentFlyer->surname = $request->surname;
            $frequentFlyer->middle_name = $request->middle_name;
            $frequentFlyer->phone = $request->phone;
            $frequentFlyer->doc_issue_location = $request->doc_issue_location;
            $frequentFlyer->doc_id = $request->doc_id;
            $frequentFlyer->gender = $request->gender;
            $frequentFlyer->nationality = $request->nationality;
            $frequentFlyer->date_of_birth = $request->date_of_birth;
            $frequentFlyer->expiry_date = $request->expiry_date;
            $frequentFlyer->postal_code = $request->postal_code;
            $frequentFlyer->save();

            if ($request->is_default) {
                $frequentFlyer->is_default = $request->is_default;
                $frequentFlyer->save();
                FrequentFlyer::whereUserId($user->id)->where('id', '!=', $frequentFlyer->id)->update(['is_default' => 0]);
            }

            DB::commit();

            return response()->json(['message' => 'success', 'details' => $frequentFlyer], 200);


        } catch (\Exception $ex) {

            DB::rollBack();

            Log::error($ex->getMessage());

            return response()->json(['message' => $ex->getMessage()], 500);
        }


    }

    public function delete(Request $request)
    {
        DB::beginTransaction();

        $fieldsToValidate = ['id' => ['required', 'integer', Rule::exists('frequent_flyers')->where(function ($query) use ($request) {
            return $query->where(['user_id' => $request->user()->id, 'id' => $request->id]);
        })]];

        $request->validate($fieldsToValidate);


        try {

            FrequentFlyer::destroy($request->id);

            DB::commit();

            return response()->json(['message' => 'success'], 200);


        } catch (\Exception $ex) {

            DB::rollBack();

            Log::error($ex->getMessage());

            return response()->json(['message' => $ex->getMessage()], 500);
        }


    }
}
