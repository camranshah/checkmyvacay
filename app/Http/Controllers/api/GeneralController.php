<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Airport;
use App\Airlines;

class GeneralController extends Controller
{
    public function getFilters(Request $request)
    {
        $take = ($request->take) ? $request->take : 10;
        $skip = ($request->skip) ? $request->skip : 0;
        $priceStart = ['label' => 'Price From', 'value' => '100'];
        $sortBy = [
            'label' => 'Sort By',
            'values' => [
                ['label' => 'Price Low To High', 'value' => 'price_low_to_high'],
                ['label' => 'Price High To Low', 'value' => 'price_high_to_low'],
            ]];

        $noOfStops = [
            'label' => 'No. Of Stops',
            'values' => [
                ['label' => 'Direct flights only', 'value' => 1],
                ['label' => 'Max 1 stop', 'value' => 2],
                ['label' => 'No preference', 'value' => 3],
            ]];

        $departureTimes = [
            'label' => 'Departure Times',
            'values' => [
                ['label' => 'Inbound Flights', 'values' => [
                    ['label' => 'Morning', 'start' => '06:00', 'end' => '12:00'],
                    ['label' => 'Evening', 'start' => '06:00', 'end' => '12:00'],
                    ['label' => 'Afternoon', 'start' => '06:00', 'end' => '12:00'],
                    ['label' => 'Night', 'start' => '06:00', 'end' => '12:00'],
                    ['label' => 'Choose a time', 'start' => '00:00', 'end' => '00:00']]
                ],
                ['label' => 'Outbound Flights', 'values' => [
                    ['label' => 'Morning', 'start' => '06:00', 'end' => '12:00'],
                    ['label' => 'Evening', 'start' => '06:00', 'end' => '12:00'],
                    ['label' => 'Afternoon', 'start' => '06:00', 'end' => '12:00'],
                    ['label' => 'Night', 'start' => '06:00', 'end' => '12:00'],
                    ['label' => 'Choose a time', 'start' => '00:00', 'end' => '00:00']]
                ]
            ],
        ];

        $airlines = ['label' => 'Airlines', 'total_airlines' => Airlines::count(), 'values' => Airlines::take($take)->skip($skip)->orderBy('name', 'asc')->get()];

        return $filters = [
            'price_start' => $priceStart,
            'sort_by' => $sortBy,
            'departure_times' => $departureTimes,
            'airlines' => $airlines,
            'no_of_stops' => $noOfStops];

    }


}
