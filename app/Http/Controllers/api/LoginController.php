<?php

namespace App\Http\Controllers\api;

use App\Events\NotificationEvent;
use App\Notifications\SignupActivate;
use App\PasswordReset;
use App\Traits\SmsTrait;
use App\User;
use App\UserProfile;
use App\UserRelationRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    use SmsTrait;

    public $successStatus = 200;

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'device_id' => 'nullable|string',
            'device_type' => ['required_with:device_id', Rule::in(['android', 'ios'])]
        ], [
            'email.required' => 'Please provide email',
            'email.email' => 'Please provide valid email',
            'password.required' => 'Please provide password',
            'password.min' => 'Password is required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }
        $user = User::whereEmail($request->email)->first();

        if (!empty($user)) {

            if ($user->email_verified == 0) {
                $user->activation_code = getRandom4Digit();
                $user->save();
                $user->notify(new SignupActivate($user));

                return response()->json(['error' => 'We have sent email verification code to your email. Please verify.'], 401);
            }
//
//            if($user->number_verified == 0){
//                return response()->json(['error' => 'Please verify your phone number'],401);
//            }

//            $payment = AccountPayment::whereUserId($user->id)->orderBy('id','DESC')->first();
//            if(!empty($payment)){
//                if(date('Y-m-d',strtotime($payment->end_date)) < date('Y-m-d')){
//                    return response()->json(['error' => 'Your account has been expired. Please pay your annual charges'],401);
//                }
//            }else{
//                return response()->json(['error' => 'Please goto payment section for further process'],401);
//            }

            if ($user->status == 0) {
                return response()->json(['error' => 'Your account has been deactivated by admin. Please contact admin'], 401);
            }

            $role = UserRelationRole::with('role')->whereUserId($user->id)->first();
            if ($role->role['role'] == "user") {
                if (Hash::check($request->password, $user->password)) {
                    $credentials = ['email' => $request->email, 'password' => $request->password];
                    if (Auth::attempt($credentials)) {

                        $user = Auth::user();
                        $user->device_id = $request->device_id;
                        $user->device_type = $request->device_type;
                        $user->save();

                        $success['token'] = $user->createToken('MyApp')->accessToken;

                        return response()->json(['success' => $success], $this->successStatus);
                    }
                } else {
                    return response()->json(['error' => 'Invalid Password'], 401);
                }
            } else {
                return response()->json(['error' => 'Only user can login from this portal'], 401);
            }
        } else {
            return response()->json(['error' => 'Invalid Email'], 401);
        }
    }

    public function register(Request $request)
    {
//        $result = $this->sendSms("+16507709489", "Testing Message");
        $fieldsToValidate = [
            'title' => 'required',
            'first_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'middle_name' => 'nullable|regex:/^[\pL\s\-]+$/u',
            'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|unique:users',
            'phone_code' => 'required',
            'phone' => 'required',
            'dob' => 'nullable',
            'country' => 'nullable|string',
            'password' => 'required',
            'device_id' => 'nullable|string',
            'postal_code' => 'required',
            'device_type' => ['required_with:device_id', Rule::in(['android', 'ios'])]
        ];

        $validationMessages = [
            'title.required' => 'Please provide title',
            'first_name.required' => 'Please provide first name',
            'first_name.regex' => 'Please provide valid first name',
            'middle_name.regex' => 'Please provide valid middle name',
            'last_name.required' => 'Please provide last name',
            'last_name.regex' => 'Please provide valid last name',
            'email.required' => 'Please provide email',
            'email.email' => 'Please provide valid email',
            'email.unique' => 'Email already registered',
            'phone_code.required' => 'Please provide phone code',
            'phone.required' => 'Please provide phone number',
            'password.required' => 'Please provide password',
            'postal_code.required' => 'Please provide postal code',
            'password.min' => 'Password is required',
        ];

        $validator = Validator::make($request->all(), $fieldsToValidate, $validationMessages);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        try {
            DB::beginTransaction();
            $user = new User;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->device_id = $request->device_id;
            $user->device_type = $request->device_type;
            $user->activation_code = getRandom4Digit();
            $user->status = 1;
            $user->save();

            $relation = new UserRelationRole;
            $relation->user_id = $user->id;
            $relation->role_id = '2';
            $relation->save();

            $profile = new UserProfile;
            $profile->title = $request->title;
            $profile->first_name = $request->first_name;
            $profile->middle_name = $request->middle_name;
            $profile->last_name = $request->last_name;
            $profile->phone_code = $request->phone_code;
            $profile->phone = $request->phone;
            $profile->dob = $request->dob;
            $profile->country = $request->country;
            $profile->user_id = $user->id;
            $profile->postal_code = $request->postal_code;
            $profile->save();

            $to = $request->phone_code.$request->phone;
            $message = 'Your Activation Code is ' . $user->activation_code;
            $result = $this->sendSms($to, $message);

            if ($result['status'] == false) {
                DB::rollback();
                return response()->json([
                    'message' => $result['message'],
                    'number' => $to,
                ], 401);
            }

//            $user->notify(new SignupActivate($user));

            /* Start Process To Send Notification To Admin */
            $title = "New User Registered";
            $description = "New User '" . $profile->first_name . "' Has Been Registered";
            $type = 'signup';
            $typeId = $user->id;
            $url = 'user/' . $user->id;
            event(new NotificationEvent(1, $title, $description, $type, $typeId, $url));
            /* End Process To Send Notification To Admin */

            DB::commit();

            return response()->json(['success' => 'Successfully created user!'], $this->successStatus);

        } catch (\Exception $ex) {
//            return $ex;
            return response()->json(['error' => $ex->getMessage(), 'user' => []], 500);
        }

    }

    public function forgetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
        ], [
            'email.required' => 'Please provide email',
            'email.email' => 'Please provide valid email',
            'email.exists' => 'Email not found'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $reset = PasswordReset::whereEmail($request->email)->first();

        if (empty($reset)) {
            $token = getRandom4Digit();
            $reset = new PasswordReset;
            $reset->email = $request->email;
            $reset->token = $token;
            $reset->save();
        } else {
            $token = $reset->token;
        }

        if ($reset->save()) {
            $data = [
                'link' => '',
                'from' => 'cmv@admin.com',
                'to' => $request->email,
                'subject' => 'Password Recovery',
                'sender_name' => 'CMV',
                'token' => $token,
            ];

            $user = User::whereEmail($request->email)->first();
            $to = $user->profile->phone_code.$user->profile->phone;
            $message = 'Your reset password code is ' . $token;
            $result = $this->sendSms($to, $message);

            if ($result['status'] == false) {
                return response()->json([
                    'message' => $result['message'],
                    'number' => $to,
                ], 401);
            }


//            Mail::send('email.forget-password', ['data' => $data], function ($message) use ($data) {
//                $message->from($data['from'], $data['sender_name']);
//                $message->to($data['to'])->subject($data['subject']);
//            });
            return response()->json(['success' => 'Your reset code has been sent through SMS'], $this->successStatus);
        }
    }

    public function verifyToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
        ], [
            'token.required' => 'Please provide token',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }
        $reset = PasswordReset::whereToken($request->token)->first();
        if (empty($reset)) {
            return response()->json(['error' => 'Token not found, Please try again'], 401);
        } else {
            $user = User::whereEmail($reset->email)->first();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        }
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ], [
            'password.required' => 'Please provide password',
            'password.min' => 'Password is required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $user = User::whereId($request->user()->id)->first();
        $user->password = Hash::make($request->password);
        if ($user->save()) {
            PasswordReset::whereEmail($user->email)->delete();
            return response()->json(['success' => 'Password reset successfully'], $this->successStatus);
        } else {
            return response()->json(['error' => 'Something went wrong'], 401);
        }
    }

    public function signupActivate($token)
    {
        $user = User::where('activation_code', $token)->first();

        if (!$user) {
            return response()->json([
                'message' => 'This activation code is invalid.'
            ], 404);
        }

        $user->activation_code = '';
        $user->email_verified = 1;
        $user->save();

        $token = $user->createToken('MyApp')->accessToken;

        return response()->json([
            'message' => 'Your account has been activated!',
            'access_token' => $token,
        ], 200);
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->device_id = null;
        $user->device_type = null;
        $user->save();

        $user->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ], 200);
    }

    public function resendSignupActivateEmail(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => ['required','email', 'exists:users,email']
        ],[
            'email.required' => "Please provide email address",
            'email.exists' => "Email address not found"
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $user = User::whereEmail($request->email)->first();
        $user->activation_code = getRandom4Digit();
        $user->save();

        $to = $user->profile->phone_code.$user->profile->phone;
        $message = 'Your Activation Code is ' . $user->activation_code;
        $result = $this->sendSms($to, $message);

        if ($result['status'] == false) {
            return response()->json([
                'message' => $result['message'],
                'number' => $to,
            ], 401);
        }

        $user->notify(new SignupActivate($user));

        return response()->json(['success' => 'Activation code has been sent to your phone number!'], 200);

    }
}
