<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $take = ($request->take) ? $request->take : 50;
        $skip = ($request->skip) ? $request->skip : 0;

        $notification = new Notification();
        $notification = $notification->whereUserId($user->id);
        $totalCount = $notification->count();
        $unreadCount = $notification->whereIsRead(0)->count();
        $notification = $notification->take($take)->skip($skip);

        return ['total_count' => $totalCount, 'unread_count' => $unreadCount, 'details' => $notification->get()];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = $request->user();

        $validator = Validator::make($request->all(),
            [
                'ids' => ['required', 'array'],
                'ids.*' => ['integer']
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        Notification::whereUserId($user->id)->whereIn('id', $request->ids)->update(['is_read' => 1]);

        return response()->json(['message' => 'success'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = $request->user();

        $validator = Validator::make($request->all(),
            [
                'ids' => ['required', 'array'],
                'ids.*' => ['integer']
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        Notification::whereUserId($user->id)->whereIn('id', $request->ids)->delete();

        return response()->json(['message' => 'success'], 200);

    }
}
