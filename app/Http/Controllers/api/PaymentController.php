<?php

namespace App\Http\Controllers\api;

use App\AccountPayment;
use App\Booking;
use App\BookingPayment;
use App\Events\NotificationEvent;
use App\ExchangeRate;
use App\PromoCode;
use App\Setting;
use App\ThirdParty\BPoint;
use App\ThirdParty\MintPayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PaymentController extends Controller
{
    public function subscription(Request $request)
    {
        DB::beginTransaction();

        $user = $request->user();

        $subscriptionStartDate = date('Y-m-d');
        $subscriptionEndDate = date('Y-m-d', strtotime('+1 years'));
        $amount = Setting::find(1)->subscription_amount;

//        return $request->all();
        $validator = Validator::make($request->all(),
            [
                'token' => ['required'],
                'coupon' => ['nullable', 'string', 'exists:promo_codes,code']
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        try {

            if (!$request->coupon) {

                Log::info('we are here . ' . $request->token);

                $mint = new MintPayment();
                $mintResponse = $mint->purchaseWithToken($user->id, $request->ip(), $request->token, date("ymdhis").'-subscription', $amount);

                if (isset($mintResponse['response_code']) && $mintResponse['response_code'] != 'success') {
                    return response()->json(['message' => $mintResponse['response_message']], 422);
                }

                if (isset($mintResponse['error_code'])) {
                    return response()->json(['message' => $mintResponse['error_message']], 422);
                }

                $payment = new AccountPayment();
                $payment->user_id = $user->id;
                //$payment->transaction_id = $mintResponse['purchase']['purchase_reference'];
                $payment->transaction_id = 'test';
                //$payment->payment_through = $mintResponse['card']['number'];
                $payment->payment_through = 'test';
                $payment->amount = $amount;
                $payment->start_date = $subscriptionStartDate;
                $payment->end_date = $subscriptionEndDate;
                $payment->save();

            }else{
                $payment = new AccountPayment();
                $payment->user_id = $user->id;
                $payment->transaction_id = 'test';
                $payment->payment_through = 'test';
                $payment->amount = $amount;
                $payment->promo_code_id = PromoCode::whereCode($request->coupon)->first()->id;
                $payment->start_date = $subscriptionStartDate;
                $payment->end_date = $subscriptionEndDate;
                $payment->save();

            }

            $user->is_subscribed = 1;
            $user->subscribed_at = date('Y-m-d H:i:s');
            $user->save();

            DB::commit();

            return response()->json(['message' => 'success'], 200);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            DB::rollBack();

            return response()->json(['message' => $ex->getMessage()], 500);
        }

    }

    public function booking(Request $request)
    {
        DB::beginTransaction();

        $user = $request->user();

        $validator = Validator::make($request->all(),
            [
                'trip_id' => ['required', 'integer', Rule::exists('bookings')->where(function ($q) use ($request) {
                    return $q->where(['user_id' => $request->user()->id, 'trip_id' => $request->trip_id, 'status' => 0]);
                })],
                'token' => ['required', Rule::exists('user_payment_tokens')->where(function ($q) use ($request) {
                    return $q->where(['user_id' => $request->user()->id, 'token' => $request->token]);
                })],
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        try {

            $booking = Booking::whereTripId($request->trip_id)->first();
            $amount = $booking->fare - $booking->discount;

            if (strtolower($booking->currency_code) != "usd") {
                $rate = ExchangeRate::whereCurrencyCode(strtoupper($booking->currency_code))->first()->exchange_rate;
                $amount = round($rate * $booking->fare);
            }

            $mint = new MintPayment();
            $mintResponse = $mint->purchaseWithToken($user->id, $request->ip(), $request->token, $booking->id."-booking", $amount, $booking->currency_code);

            if (isset($mintResponse['response_code']) && $mintResponse['response_code'] != 'success') {
                return response()->json(['message' => $mintResponse['response_message']], 422);
            }

            $payment = new BookingPayment;
            $payment->booking_id = $booking->id;
            //$payment->transaction_id = $mintResponse['purchase']['purchase_reference'];
            $payment->transaction_id = 'test';
            $payment->fares = $amount;
            $payment->currency_code = $booking->currency_code;
            $payment->save();

            $booking->status = 1;
            $booking->save();

            /* Start Process To Send Notification To Admin */
            $title = "Payment for Booking# " . $booking->id;
            $description = "'" . $request->user()->profile->first_name . "' Has Paid For Booking# " . $booking->id;
            $type = 'booking_payment';
            $typeId = $payment->id;
            $url = 'payment-log/' . $booking->id;
            event(new NotificationEvent(1, $title, $description, $type, $typeId, $url));
            /* End Process To Send Notification To Admin */


            DB::commit();

            return response()->json(['message' => 'success'], 200);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            DB::rollBack();

            return response()->json(['message' => $ex->getMessage()], 500);
        }

    }


}
