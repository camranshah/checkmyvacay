<?php

namespace App\Http\Controllers\api;

use App\PromoCode;
use App\UserPromoCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PromoCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([ 'promo_code' => 'required' ]);

        $promo = PromoCode::whereCode($request->promo_code)->first();

        if (!$promo) {
            return response()->json(['message' => 'Promo Code is not valid'], 404);
        }

        if($promo->guest_user){
            $this->makeTransactionPaymentIfRequired($promo);
            $this->subscribeUser();
            return response()->json(['message' => 'Promo Code is valid', 'promo' => $promo], 200);
        }

        if (!$promo->allowed_users()->where('user_promo_codes.user_id', auth()->id())->exists()) {
            return response()->json(['message' => 'Promo Code is not valid'], 404);
        }

        $this->subscribeUser();
        return response()->json(['message' => 'Promo Code is valid', 'promo' => $promo], 200);
    }

    protected function subscribeUser()
    {
        if(!auth()->check()) return;

        $user = auth()->user();
        $user->is_subscribed = 1;
        $user->promo_code = request('promo_code');
        $user->save();
    }

    protected function makeTransactionPaymentIfRequired($promo)
    {
        if(strtolower($promo->code) !== 'free7') return;

        $user = auth()->user();

        $payment = new \App\AccountPayment;
        $payment->transaction_id = $promo->code;
        $payment->amount = 0;
        $payment->user_id = $user->id;
        $payment->start_date = date('Y-m-d');
        $payment->end_date = date('Y-m-d', strtotime('+7 days'));
        $payment->save();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index2(Request $request)
    {
        $request->validate([ 'promo_code' => 'required' ]);

        $userPromoCode = UserPromoCode::with('promo_code')
            ->whereUserId($request->user()->id)
            ->whereIsUsed(0)
            ->whereHas('promo_code', function ($query) use ($request) {
                $query->where(['code' => $request->promo_code, 'status' => 1]);
            })->first();

        if (!$userPromoCode) {
            return response()->json(['message' => 'Promo Code is not valid'], 404);
        }
        return response()->json(['message' => 'Promo Code is valid'], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
