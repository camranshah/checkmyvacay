<?php

namespace App\Http\Controllers\api;

use App\AccountPayment;
use App\FrequestFlyerDetail;
use App\Paypal;
use App\User;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'current_password' => 'required',
                'password' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $user = User::whereId($request->user()->id)->first();
        if (Hash::check($request->current_password, $user->password)) {
            $user->password = Hash::make($request->password);
            if ($user->save()) {
                return response()->json(['success' => "Password updated successfully"], $this->successStatus);
            }
        } else {
            return response()->json(['error' => "Invalid old password"], 401);
        }
    }

    public function profile(Request $request)
    {
        $user = User::with(['completed_flights', 'upcoming_flights', 'payment_tokens', 'frequent_flyers', 'profile', 'notifications'])->withCount('notifications as unread_notification_count')->whereId($request->user()->id)->first();

        if ($request->method() == "POST") {

            $profile = UserProfile::whereUserId($user->id)->first();

            $validator = Validator::make($request->all(),
                [
                    'title' => 'required',
                    'first_name' => 'required|regex:/^[\pL\s\-]+$/u',
                    'middle_name' => 'nullable|regex:/^[\pL\s\-]+$/u',
                    'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
                    'email' => 'required|email|unique:users,email,' . $request->user()->id,
                    'phone_code' => 'required',
                    'phone' => 'required|digits_between:10,10',
                    'dob' => 'nullable',
                    'country' => 'nullable|string',
                ],
                [
                    'title.required' => 'Please provide title',
                    'first_name.required' => 'Please provide first name',
                    'first_name.regex' => 'Please provide valid first name',
                    'middle_name.regex' => 'Please provide valid middle name',
                    'last_name.required' => 'Please provide last name',
                    'last_name.regex' => 'Please provide valid last name',
                    'email.required' => 'Please provide email',
                    'email.email' => 'Please provide valid email',
                    'email.unique' => 'Email already registered',
                    'phone_code.required' => 'Please provide phone code',
                    'phone.required' => 'Please provide phone number',
                    'phone.digits_between' => 'Only 10 numbers allowed',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()->first()], 401);
            }

            $user->email = $request->email;
            if ($user->save()) {
                $profile->title = $request->title;
                $profile->first_name = $request->first_name;
                $profile->middle_name = $request->middle_name;
                $profile->last_name = $request->last_name;
                $profile->phone_code = $request->phone_code;
                $profile->phone = $request->phone;
                $profile->dob = $request->dob;
                $profile->country = $request->country;
                $profile->postal_code = $request->postal_code;
                $profile->save();
                return response()->json(['success' => "Profile updated successfully"], $this->successStatus);
            } else {
                return response()->json(['error' => "Something went wrong"], 401);
            }
        }
        $user->profile->email = $user->email;
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function accountPayment(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'amount' => 'required',
                'currency' => 'required|min:3|max:3',
                'ccnumber' => 'required|digits_between:16,16',
                'ccmonth' => 'required',
                'ccyear' => 'required|digits_between:4,4',
                'cvv' => 'required|digits_between:3,3',
                'first_name' => 'required|regex:/^[\pL\s\-]+$/u',
                'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
                'city' => 'required',
                'zip' => 'required',
                'country_code' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $paypalParams = array(
            'paymentAction' => 'Sale',
            'amount' => $request->amount,
            'currencyCode' => strtoupper($request->currency),
            'creditCardType' => '',
            'creditCardNumber' => $request->ccnumber,
            'expMonth' => $request->ccmonth,
            'expYear' => $request->ccyear,
            'cvv' => $request->cvv,
            'firstName' => $request->first_name,
            'lastName' => $request->last_name,
            'city' => $request->city,
            'zip' => $request->zip,
            'countryCode' => $request->country_code,
        );


        $paypal = new Paypal();
        $response = $paypal->paypalCall($paypalParams);
        if (isset($response['ACK']) && strtolower($response['ACK']) == "success") {
            $payment = new AccountPayment;
            $payment->transaction_id = $response['TRANSACTIONID'];
            $payment->amount = $request->amount;
            $payment->user_id = $request->user()->id;
            $payment->start_date = date('Y-m-d');
            $payment->end_date = date('Y-m-d', strtotime('+1 years'));
            if ($payment->save()) {
                return response()->json(['success' => "payment succeeded"], $this->successStatus);
            } else {
                return response()->json(['error' => "Something went wrong"], 401);
            }
        } else {
            return response()->json(['error' => "Invalid information"], 401);
        }
    }

    public function frequentFlyerDetail(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'gender' => 'nullable|in:m,f',
                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                'family_name' => 'required|regex:/^[\pL\s\-]+$/u',
                'email' => 'email',
                'phone' => 'digits_between:10,10',
                'dob' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $detail = new FrequestFlyerDetail;
        $detail->gender = $request->gender;
        $detail->name = $request->name;
        $detail->family_name = $request->family_name;
        $detail->email = $request->email;
        $detail->phone = $request->phone;
        $detail->dob = $request->dob;
        $detail->user_id = $request->user()->id;
        if ($detail->save()) {
            return response()->json(['success' => "details saved"], $this->successStatus);
        } else {
            return response()->json(['error' => "Something went wrong"], 401);
        }
    }
}
