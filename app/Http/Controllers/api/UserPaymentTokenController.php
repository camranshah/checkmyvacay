<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ThirdParty\MintPayment;
use App\UserPaymentToken;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class UserPaymentTokenController extends Controller
{
    public function create(Request $request)
    {
        DB::beginTransaction();

        $fieldsToValidate = [
            'credit_card_number' => ['required'],
            'expiry_month' => ['required'],
            'expiry_year' => ['required'],
            'cvv' => ['required'],
            'card_holder_name' => ['required'],
            'is_default' => ['nullable', 'integer', Rule::in([1])]
        ];

        $request->validate($fieldsToValidate);

        /** @var UserPaymentToken $userPaymentToken*/
        $userPaymentToken = UserPaymentToken::where('card_number', request('credit_card_number'))
                                                ->whereUserId(auth()->id())->first();

        try {

            $user = $request->user();

            $ipAddress = $request->ip();
            $timeZone = "Australia/Sydney";
            $mint = new MintPayment();

            // $mintResponse = $mint->tokenizeCard($user->id, $ipAddress, $timeZone, $request->credit_card_number, $request->expiry_month, $request->expiry_year, $request->cvv, $request->card_holder_name);
	        $mintResponse = $mint->tokenizeCard2($request->credit_card_number, $request->expiry_month, $request->expiry_year, $request->cvv, $request->card_holder_name);


            if(isset($mintResponse['error_code'])){
                \Log::info($mintResponse);
                return response()->json(['message' => $mintResponse['error_message']], 422);
            }

            if ($mintResponse == "") {
                \Log::info($mintResponse);
                return response()->json(['message' => "Something went wrong. Please try later"], 422);
            }

            if (isset($mintResponse["response_code"]) && $mintResponse["response_code"] != "success") {
                \Log::info($mintResponse);
                return response()->json(['message' => $mintResponse['response_message']], 422);
            }
            
            if($userPaymentToken)
            {
                $userPaymentToken->forceFill([
                    'card_type' => $mintResponse["card"]["brand"],
                    'masked_card_number' => $mintResponse["card"]["number"],
                    'token' => $mintResponse["card_token"],
                    'card_expiry_month' => $mintResponse["card"]["expiry_month"],
                    'card_expiry_year' => $mintResponse["card"]["expiry_year"],
                    'card_holder_name' => $mintResponse["card"]["holder_name"],
                    'funding' => $mintResponse["card"]["funding"],
                    'country' => $mintResponse["card"]["country"],
                    'customer_id' => $mintResponse["customer"]["id"],
                    'card_number' => $request->credit_card_number,
                ]);
                $user->save();

                return response()->json(['message' => 'success', 'details' => $userPaymentToken], 200);
            }

            $userPaymentToken = UserPaymentToken::create([
                'user_id' => $user->id,
                'card_type' => $mintResponse["card"]["brand"],
                'masked_card_number' => $mintResponse["card"]["number"],
                'token' => $mintResponse["card_token"],
                'card_expiry_month' => $mintResponse["card"]["expiry_month"],
                'card_expiry_year' => $mintResponse["card"]["expiry_year"],
                'card_holder_name' => $mintResponse["card"]["holder_name"],
                'funding' => $mintResponse["card"]["funding"],
                'country' => $mintResponse["card"]["country"],
                'customer_id' => $mintResponse["customer"]["id"],
                'card_number' => $request->credit_card_number,
            ]);

            if ($request->is_default) {
                $userPaymentToken->is_default = $request->is_default;
                $userPaymentToken->save();
                UserPaymentToken::whereUserId($user->id)->where('id', '!=', $userPaymentToken->id)->update(['is_default' => 0]);
            }

            DB::commit();

            return response()->json(['message' => 'success', 'details' => $userPaymentToken], 200);

        } catch (\Exception $ex) {

            DB::rollBack();

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Invalid card number'], 500);
        }

    }

    public function update(Request $request)
    {
        DB::beginTransaction();

        $fieldsToValidate = [
            'id' => ['required', 'integer', Rule::exists('user_payment_tokens')->where(function ($q) use ($request) {
                return $q->where(['user_id' => $request->user()->id, 'id' => $request->id]);
            })],
            'is_default' => ['required', 'integer', Rule::in([1])]
        ];

        $request->validate($fieldsToValidate);

        try {

            $userPaymentToken = UserPaymentToken::find($request->id);
            $userPaymentToken->is_default = $request->is_default;
            $userPaymentToken->save();
            UserPaymentToken::whereUserId($request->user()->id)->where('id', '!=', $userPaymentToken->id)->update(['is_default' => 0]);

            DB::commit();

            return response()->json(['message' => 'success', 'details' => $userPaymentToken], 200);


        } catch (\Exception $ex) {

            DB::rollBack();

            Log::error($ex->getMessage());

            return response()->json(['message' => $ex->getMessage()], 500);
        }

    }

    public function delete(Request $request)
    {
        DB::beginTransaction();

        $fieldsToValidate = [
            'id' => ['required', 'integer', Rule::exists('user_payment_tokens')->where(function ($q) use ($request) {
                return $q->where(['user_id' => $request->user()->id, 'id' => $request->id]);
            })],
        ];

        $request->validate($fieldsToValidate);

        try {

            $userPaymentToken = UserPaymentToken::find($request->id);
            $token = $userPaymentToken->token;

            $mint = new MintPayment();
            $mintResponse = $mint->invalidateToken($token);

            if (isset($mintResponse["response_code"]) && $mintResponse["response_code"] != "success") {
                return response()->json(['message' => $mintResponse['response_message']], 422);
            }

            $userPaymentToken->delete();

            DB::commit();

            return response()->json(['message' => 'success'], 200);


        } catch (\Exception $ex) {

            DB::rollBack();

            Log::error($ex->getMessage());

            return response()->json(['message' => $ex->getMessage()], 500);
        }
    }
}
