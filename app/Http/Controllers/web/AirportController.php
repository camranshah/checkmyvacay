<?php

namespace App\Http\Controllers\web;

use App\Airport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AirportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->page;
        $resultCount = 100;
        $skip = ($page - 1) * $resultCount;
        $take = $skip + $resultCount;
        $airports = new Airport();
        if ($request->for && $request->for == 'website'):
            $airports = $airports->select(DB::raw("sort, iata AS id, CONCAT_WS(', ',iata,name,city,country) AS text, city"));
        else:
            $airports = $airports->select(DB::raw("id, CONCAT_WS(', ',iata,name,city,country) AS text, city"));
        endif;
        $airports = $airports->where('name', 'like', '%' . $request->keyword . '%');
        $airports = $airports->orWhere('city', 'like', '%' . $request->keyword . '%');
        $airports = $airports->orWhere('country', 'like', '%' . $request->keyword . '%');
        $airports = $airports->orWhere('iata', 'like', '%' . $request->keyword . '%');
        $airports = $airports->take($take)->skip($skip);
        $airports = $airports->toBase()->get();

        return response()->json(['results' => $airports]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
