<?php

namespace App\Http\Controllers\web;

use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BookingStatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $year = ($request->year) ? $request->year : date('Y');

        $bookings = Booking::select(DB::raw("count(*) as count, date_format(created_at, '%M') as month"))
            ->where(DB::raw('YEAR(created_at)'), '=', $year)
            ->groupBy('month')
            ->get();

        $months = [];

        for ($m = 1; $m <= 12; $m++) {
            $months[] = ['month' => date('F', mktime(0, 0, 0, $m, 1, date('Y'))), 'count' => 0];
        }

        foreach ($bookings as $key) {
            foreach ($months as $month => &$value) {
                if ($value['month'] == $key->month) {
                    $value['count'] = $key->count;
                }
            }
        }

        return response()->json($months, 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
