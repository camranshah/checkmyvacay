<?php

namespace App\Http\Controllers\web;

use App\Http\Requests\BroadcastMessageStoreRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\ThirdParty\PushNotifications;

class BroadcastMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['Users'] = User::with(['user_role'])->whereHas('profile')->whereHas('user_role', function ($q) {
            $q->whereRoleId(2);
        })->whereStatus(1)->get();

        return view('broadcast-message.index', $data);
    }

    public function store(BroadcastMessageStoreRequest $request)
    {
        $users = User::whereIn('id', $request->users)
            ->where('device_id', '!=', null)->get();

        foreach ($users as $user) {
            $pushNotif = new PushNotifications();
            $pushNotif->addDevice($user->device_id, $user->device_type);
            $pushNotif->send(
                $request->message,
                $request->message,
                'message', ['message' => $request->message, 'type' => 'message', 'content_available' => 1]
            );
        }

        return back()->with('success', 'Message Has Been Sent!');
    }

}
