<?php

namespace App\Http\Controllers\web;

use App\ActivityLog;
use App\Booking;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $fromDate = date('Y-m-d', strtotime($request->from_date));
            $toDate = date('Y-m-d', strtotime($request->to_date));

            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;

            $data['TotalUsers'] = User::whereBetween('created_at', [$fromDate, $toDate])->whereHas('user_role', function ($q) {
                $q->whereRoleId(2);
            })->count();

            $data['TotalBookings'] = Booking::whereBetween('created_at', [$fromDate, $toDate])->whereStatus(1)->count();

            return $data;

        }

        $user = $request->user();

        $data['TotalUsers'] = User::whereHas('user_role', function ($q) {
            $q->whereRoleId(2);
        })->count();

        $data['ActivityLog'] = $request->user()->activity_log()->get();

        $data['TotalBookings'] = Booking::whereStatus(1)->count();

        return view('dashboard.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
