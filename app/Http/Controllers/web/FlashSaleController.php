<?php

namespace App\Http\Controllers\web;

use App\ActivityLog;
use App\FlashSale;
use App\FlashSaleInterval;
use App\FlashSaleLocation;
use App\Http\Requests\FlashSaleStoreRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FlashSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['FlashSales'] = FlashSale::orderBy('id', 'desc')->get();

        return view('flashsale.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('flashsale.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FlashSaleStoreRequest $request)
    {

        DB::beginTransaction();

        try {
            $flashSale = new FlashSale();
            $flashSale->title = $request->title;
            $flashSale->discount_type = $request->discount_type;
            $flashSale->discount_value = $request->discount_value;
            $flashSale->tour_details = $request->tour_details;
            $flashSale->departure_details = $request->departure_details;
            $flashSale->what_to_expect = $request->what_to_expect;
            $flashSale->image = 'default-banner.jpg';
            $flashSale->expiry_date = $request->expiry_date;
            $flashSale->type = $request->type;
            if ($request->type == 'package'):
                $flashSale->url = $request->url;
            endif;


            $flashSale->status = 1;
            $flashSale->save();

            if ($request->hasFile('image')):
                $file = $request->file('image');
                $image = date('ymdhis') . '.' . $file->getClientOriginalExtension();
                Storage::put('public/images/' . $image, file_get_contents($file->getRealPath()));
                $flashSale->image = $image;
                $flashSale->save();
            endif;


            if ($request->type == 'flight'):
                foreach ($request->departure_locations as $id) {
                    $depLoc = new FlashSaleLocation();
                    $depLoc->flash_sale_id = $flashSale->id;
                    $depLoc->type = 1;
                    $depLoc->airport_id = $id;
                    $depLoc->save();
                }

                foreach ($request->arrival_locations as $id) {
                    $arrLoc = new FlashSaleLocation();
                    $arrLoc->flash_sale_id = $flashSale->id;
                    $arrLoc->type = 2;
                    $arrLoc->airport_id = $id;
                    $arrLoc->save();
                }

                $firstInterval = new FlashSaleInterval();
                $firstInterval->flash_sale_id = $flashSale->id;
                $firstInterval->departure_date_1 = $request->first_interval_departure_1;
                $firstInterval->departure_date_2 = $request->first_interval_departure_2;
                $firstInterval->arrival_date_1 = $request->first_interval_arrival_1;
                $firstInterval->arrival_date_2 = $request->first_interval_arrival_2;
                $firstInterval->save();

                $secondInterval = new FlashSaleInterval();
                $secondInterval->flash_sale_id = $flashSale->id;
                $secondInterval->departure_date_1 = $request->second_interval_departure_1;
                $secondInterval->departure_date_2 = $request->second_interval_departure_2;
                $secondInterval->arrival_date_1 = $request->second_interval_arrival_1;
                $secondInterval->arrival_date_2 = $request->second_interval_arrival_2;
                $secondInterval->save();

                $thirdInterval = new FlashSaleInterval();
                $thirdInterval->flash_sale_id = $flashSale->id;
                $thirdInterval->departure_date_1 = $request->third_interval_departure_1;
                $thirdInterval->departure_date_2 = $request->third_interval_departure_2;
                $thirdInterval->arrival_date_1 = $request->third_interval_arrival_1;
                $thirdInterval->arrival_date_2 = $request->third_interval_arrival_2;
                $thirdInterval->save();
            endif;

            DB::commit();

            $request->user()->activity_log()->create(['title' => 'Create Flash Sale Banner']);

            return redirect()->route('flash-sale.show', ['id' => $flashSale->id])->with('success', 'Flash Sale Banner Has Been Created!');


        } catch (\Exception $ex) {

            return $ex->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['FlashSale'] = FlashSale::findOrFail($id);
        return view('flashsale.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->ajax()) {
            $flash = FlashSale::find($request->id);
            $flash->status = $request->status;
            $flash->save();

            $request->user()->activity_log()->create(['title' => 'Update Flash Sale Status']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
