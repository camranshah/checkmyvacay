<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['User'] = $request->user();
        return view('profile.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = $request->user();

        if ($request->ajax()) {

            if (!(Hash::check($request->current_password, $user->password))) {
                return response()->json([
                    'message' => ["Your current password does not matches with the password you provided. Please try again."], 'class' => 'alert-danger', 'status' => false
                ]);
            }

            if (strcmp($request->current_password, $request->password) == 0) {

                return response()->json([
                    'message' => ["New Password cannot be same as your current password. Please choose a different password."], 'class' => 'alert-danger', 'status' => false
                ]);

            }

            $validator = \Validator::make($request->all(), [
                'current_password' => ['required'],
                'password' => ['required', 'string', 'min:8', 'max:50', 'confirmed'],
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->all(), 'class' => 'alert-success', 'status' => false]);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            $request->user()->activity_log()->create(['title'=>'Update Password']);

            return response()->json(['message' => ['Your password has been changed!'], 'class' => 'alert-success', 'status' => true]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
