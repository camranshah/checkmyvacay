<?php

namespace App\Http\Controllers\web;

use App\Http\Requests\PromoCodeStoreRequest;
use App\PromoCode;
use App\UserPromoCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PromoCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['PromoCodes'] = PromoCode::query()->withoutGlobalScope('not_expired')->with('bookings')->orderBy('id', 'desc')->get();

        return view('promo-code.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PromoCode $code)
    {
        return view('promo-code.create', compact('code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|string
     */
    public function store(PromoCodeStoreRequest $request, PromoCode $promoCode)
    {
        try {
            DB::beginTransaction();

            $promoCode->code = $request->code;
            $promoCode->type = $request->type;
            $promoCode->discount_type = $request->discount_type;
            $promoCode->discount_value = $request->discount_value;
            $promoCode->refer_tag = $request->refer_tag;
            $promoCode->start_date = date('Y-m-d');
            $promoCode->end_date = date('Y-m-d', strtotime($request->end_date));
            $promoCode->status = 1;
            $promoCode->guest_user = request('guest_user');
            $promoCode->valid_for_raw = $request->end_date;
            $promoCode->save();

            foreach(request('users', []) as $id){
                UserPromoCode::updateOrcreate(['user_id' => $id, 'promo_code_id' => $promoCode->id]);
            }

            DB::commit();

            $request->user()->activity_log()->create(['title' => 'Add Promo Code']);

            return redirect()->route('promo-code.show', ['id' => $promoCode->id])->with('success', 'Promo Code Has Been Created!');

        } catch (\Exception $ex) {

            return $ex->getMessage();

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['PromoCode'] = PromoCode::with('bookings')->findOrFail($id);
        return view('promo-code.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PromoCode $code)
    {
        return view('promo-code.create', compact('code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->ajax()) {
            $flash = PromoCode::find($request->id);
            $flash->status = $request->status;
            $flash->save();

            $request->user()->activity_log()->create(['title' => 'Update Promo Code Status']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updatePromoCode(PromoCodeStoreRequest $request, PromoCode $code)
    {
        return $this->store($request, $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
