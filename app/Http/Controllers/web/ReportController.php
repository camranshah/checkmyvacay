<?php

namespace App\Http\Controllers\web;

use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bookings = new Booking();

        switch ($request->type):
            case 'unpaid-booking':
                $view = 'report.unpaid-booking';
                $result = $bookings->doesnthave('payment');
                break;
            case 'unsuccessful-booking':
                $view = 'report.unsuccessful-booking';
                $result = $bookings->whereStatus(1);
                break;
            case 'flash-sale-flight':
                $view = 'report.flash-sale-flight';
                $result = $bookings->where('flash_sale_id', '!=', null);
                break;
            case 'total-sales-made';
                $view = 'report.total-sales-made';
                $result = $bookings->whereHas('payment');
                break;
            default:
                return back();
        endswitch;

        $data['Bookings'] = $result->orderBy('id','desc')->get();
        return view($view, $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
