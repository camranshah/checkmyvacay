<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['Users'] = User::whereHas('profile')->orderBy('id', 'desc')->get();

        return view('user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['User'] = User::findOrFail($id);

        return view('user.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if ($request->ajax()) {

            $user = User::find($request->id);

            $user->profile()->update([
                    'title' => $request->title,
                    'first_name' => $request->first_name,
                    'middle_name' => $request->middle_name,
                    'phone' => $request->phone_no,
                    'phone_code' => $request->phone_code,
                    'dob' => $request->dob
                ]);

            $request->user()->activity_log()->create(['title'=>'Edit User Profile']);

            $request->session()->flash('success', 'User profile has been updated!');

            return response()->json(['message' => ['User profile has been updated!'], 'class' => 'alert-success', 'status' => true]);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->ajax()) {
            $user = User::find($request->id);
            $user->status = $request->status;
            $user->Save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
