<?php

namespace App\Http\Controllers\website;

use App\Booking;
use App\ClientApiLog;
use App\Events\NotificationEvent;
use App\Http\Controllers\api\FlightController;
use App\Http\Requests\BookingRequest;
use App\Http\Requests\ProvisionBookingRequest;
use App\Session;
use App\UserPromoCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class BookingController extends FlightController
{
    public function __construct(){
        $this->endpoint = config('cmfx.web.endpoint');
        $this->username = config('cmfx.web.username');
        $this->password = config('cmfx.web.password');
        $this->companyCode = config('cmfx.web.companyCode');
    }

    public function create(Request $request)
    {
//        $flightDetails = preg_replace_callback('!s:\d+:"(.*?)";!s', function($m) { return "s:" . strlen($m[1]) . ':"'.$m[1].'";'; }, $request->flight_details);
//        $data['flightDetails'] = unserialize($flightDetails);
        $data['flightDetails'] = json_decode($request->flight_details, true);

        $data['request'] = json_decode(request('request'), true);

        if($data['request']['type'] === 'one_way')
        {
//            $data['request']['departure'] = json_encode((object)$data['flightDetails']);
//            $data['request']['arrival'] = json_encode((object)$data['flightDetails']);
//            $request->merge(['request' => json_encode($data['request'])]);
        }

        return view('website.booking.create', $data);
    }

    public function provisionBooking(ProvisionBookingRequest $request)
    {
        $session_id = \request('session_id');
        $cRequest = json_decode(request('request'));

        if($cRequest->type === 'one_way'){
            return $this->oneWayProvisionBooking($request, $session_id);
        }

        if($cRequest->type === 'return'){
            return $this->returnProvisionBooking($request, $session_id);
        }


        /*$api_response = unserialize(session('provision_api_response'));

        if(!$request->dev)
        {
            $api_response = [];
            foreach ($flights as $key => $flight){
                $data = $this->provisionBookingXML($flight, $session_id);
                $res = $this->getApiResponse($data, 1);

                $clientApiLog = new ClientApiLog();
                $clientApiLog->api_request = serialize($data);
                $clientApiLog->api_response = serialize($res);
                $clientApiLog->type = 'provision booking';
                $clientApiLog->save();
                $api_response[$key] = $res;
            }
            session(['provision_api_response' => serialize($api_response)]);
        }

//        dd(collect($api_response)->flatten(1)->groupBy('ErrorCode'), $request->all());

        $payload = ['provisionFlightDetails' => $request->toArray()];
        foreach ($api_response as $key => $response)
        {
            switch (true){
                case $response === "":
                    return response()->json(['error' => 'Flight not available. Please try another'], 401);
                case isset($response['ErrorCode']) && $response['ErrorCode'] == "ERR-95":
                case isset($response['ErrorCode']) && $response['ErrorCode'] == "ERR-1":
                    return response()->json(['error1' => $response['ErrorMessage']], 401);
                case isset($response['success']) && $response['success'] == "true":
                    $result = $this->getCheckFlightReponse($response, count($request->traveller_type), request($key . '.search_id'));
                    $payload[$key . '_flight_detail'] = $result;
                    break;
                default:
                    return response()->json(['error3' => 'Something went wrong'], 401);
            }

        }
//        dd($payload);
        return view('website.booking.confirm', $payload);*/

    }

    public function oneWayProvisionBooking(ProvisionBookingRequest $request, $session_id)
    {
        $api_response = unserialize(session('provision_api_response'));

        // this is because api needs to create multiple booking and payment for return flight.
        $flights = ['departure' => $request->departure, 'arrival' => $request->arrival];

        if(!$request->dev)
        {
            $api_response = [];
            $key = 'departure'; $flight = $flights['departure'];
            $data = $this->provisionBookingXMLWeb($flight, $session_id);
            $res = $this->getApiResponse($data, 1);

            $clientApiLog = new ClientApiLog();
            $clientApiLog->api_request = serialize($data);
            $clientApiLog->api_response = serialize($res);
            $clientApiLog->type = 'provision booking';
            $clientApiLog->is_app = false;
            $clientApiLog->save();
            $api_response[$key] = $res;
            $api_response['arrival'] = $res;

            session(['provision_api_response' => serialize($api_response)]);
        }

//        dd(collect($api_response)->flatten(1)->groupBy('ErrorCode'), $request->all());

        $payload = ['provisionFlightDetails' => $request->toArray()];
        foreach ($api_response as $key => $response)
        {
            switch (true){
                case $response === "":
                    return response()->json(['error' => 'Flight not available. Please try another'], 401);
                case isset($response['ErrorCode']) && $response['ErrorCode'] == "ERR-95":
                case isset($response['ErrorCode']) && $response['ErrorCode'] == "ERR-1":
                    return response()->json(['error1' => $response['ErrorMessage']], 401);
                case isset($response['success']) && $response['success'] == "true":
                    $result = $this->getCheckFlightReponse($response, count($request->traveller_type), request($key . '.search_id'));
                    $payload[$key . '_flight_detail'] = $result;
                    break;
                default:
                    return response()->json(['error3' => 'Something went wrong'], 401);
            }
        }
        return view('website.booking.confirm', $payload);
    }

    public function returnProvisionBooking(ProvisionBookingRequest $request, $session_id)
    {
        $api_response = unserialize(session('provision_api_response'));
        // this is because api needs to create multiple booking and payment for return flight.
        $flights = ['departure' => $request->departure, 'arrival' => $request->arrival];
        $api_response = [];

        if(!$request->dev)
        {
            foreach ($flights as $key => $flight){
                $data = $this->provisionBookingXML($flight, $session_id);
                $res = $this->getApiResponse($data, 1);

                $clientApiLog = new ClientApiLog();
                $clientApiLog->api_request = serialize($data);
                $clientApiLog->api_response = serialize($res);
                $clientApiLog->type = 'provision booking';
                $clientApiLog->save();
                $api_response[$key] = $res;
            }
            session(['provision_api_response' => serialize($api_response)]);
        }

        $payload = ['provisionFlightDetails' => $request->toArray()];
        foreach ($api_response as $key => $response)
        {
            switch (true){
                case $response === "":
                    return response()->json(['error' => 'Flight not available. Please try another'], 401);
                case isset($response['ErrorCode']) && $response['ErrorCode'] == "ERR-95":
                case isset($response['ErrorCode']) && $response['ErrorCode'] == "ERR-1":
                    return response()->json(['error1' => $response['ErrorMessage']], 401);
                case isset($response['success']) && $response['success'] == "true":
                    $result = $this->getCheckFlightReponse($response, count($request->traveller_type), request($key . '.search_id'));
                    $payload[$key . '_flight_detail'] = $result;
                    break;
                default:
                    return response()->json(['error3' => 'Something went wrong'], 401);
            }

        }

        return view('website.booking.confirm', $payload);

    }


    protected function returnProvisionResponse()
    {
        if (isset($response['ErrorCode'])) {
            if ($response['ErrorCode'] == "ERR-95") {
            } elseif ($response['ErrorCode'] == "ERR-1") {
                return response()->json(['error2' => $response['ErrorMessage']], 401);
            } else {
                return response()->json(['error3' => 'Something went wrong'], 401);
            }
        } elseif ($response == "") {
            return response()->json(['error' => 'Flight not available. Please try another'], 401);
        } else {
            if (isset($response['success']) && $response['success'] == "true") {
                $result = $this->getCheckFlightReponse($response, count($request->traveller_type), $request->search_id);
                $payload = ['flightDetails' => $result, 'provisionFlightDetails' => $request->toArray()];
                return view('website.booking.confirm', $payload);
            }
        }
    }

    public function confirmBooking(BookingRequest $request)
    {
        $req = json_decode(request('request'));
        $departureDetail = json_decode($request->departure_flight_detail);
        $arrivalDetail = json_decode($request->arrival_flight_detail);

        $validator = Validator::make($request->all(), $this->getValidationForBookingFlight());

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        switch ($req->type) {
            case 'one_way':
                return $this->confirmOneWayBooking($request);
        }

        $bookingPromoCodeId = null;
        $bookingDiscount = 0;
        $session_id = session('session_id');

//        dd($departureDetail);

        $departureDetail->departure_date_and_time = $request->departure_date_and_time;
        $departureDetail->arrival_date_and_time = $request->arrival_date_and_time;
        $departureDetail->duration = $request->duration;
        $departureDetail->duration = $request->duration;

        $data = $this->bookFlightDataWeb($request, $session_id);

//        dd($request->all(), $departureDetail, $arrivalDetail);

        $response = $this->getApiResponse($data, 1);


        dd($response, $data);

        $clientApiLog = new ClientApiLog();
        $clientApiLog->api_request = serialize($data);
        $clientApiLog->api_response = serialize($response);
        $clientApiLog->type = 'book flight';
        $clientApiLog->is_app = false;
        $clientApiLog->save();

        if (isset($response['ErrorCode']) && $response['ErrorCode'] == "ERR-14") {
            $response = $this->getApiResponse($this->signIn(), 1);
            Session::whereSession($session_id)->delete();
            $session_id = $response['Session_ID'];
            $session = new Session;
            $session->session = $session_id;
            $session->save();
            $data = $this->bookFlightData($request, $session_id);
            $response = $this->getApiResponse($data, 1);
        }

        if (!$response) {
            return response()->json(['error' => 'Flight not available'], 401);
        }

        if (!isset($response['Status']) && $response['Status'] != "1") {
            return response()->json(['error' => 'Something went wrong. Please try again'], 401);
        }

        $data = $this->getBookFlightResponse($response, count($request->traveller_type));
        $booking = new Booking;
        $booking->trip_id = $request->trip_id;
        $booking->user_id = @$request->user()->id;
        $booking->gds_pnr = is_array($request->gds_pnr)? $request->gds_pnr[0]: $request->gds_pnr;
        $booking->airline_pnr = is_array($request->airline_pnr)? $request->airline_pnr[0]: $request->airline_pnr;
        $booking->booking_id = $data['booking_id'];
        $booking->date_of_issue = $data['date_of_issue'];
        $booking->fare = $request->amount;
        $booking->currency_code = $request->currency_code;
        $booking->discount = $bookingDiscount;
        $booking->promo_code_id = $bookingPromoCodeId;
        $booking->detail = serialize($response);
        $booking->no_of_traveller = count($request->traveller_type);
        $booking->status = 1;
        //$booking->user_id = $request->user()->id;
        $booking->save();

        $booking->departure_datetime = date('Y-m-d H:i:s', strtotime($booking['originDepartureDateTime']));
        $booking->arrival_datetime = date('Y-m-d H:i:s', strtotime($booking['originArrivalDateTime']));
        $booking->save();

        $payload = ['booking' => $booking];
        return view('website.booking.complete', $payload);
    }

    private function confirmOnewayBooking($request)
    {
        $bookingPromoCodeId = null;
        $bookingDiscount = 0;
        $session_id = session('session_id');

        $data = $this->bookFlightDataWeb($request, $session_id);

        $response = $this->getApiResponse($data, 1);

        $clientApiLog = new ClientApiLog();
        $clientApiLog->api_request = serialize($data);
        $clientApiLog->api_response = serialize($response);
        $clientApiLog->type = 'book flight';
        $clientApiLog->is_app = false;
        $clientApiLog->save();

        if (isset($response['ErrorCode']) && $response['ErrorCode'] == "ERR-14") {
            $response = $this->getApiResponse($this->signIn(), 1);
            Session::whereSession($session_id)->delete();
            $session_id = $response['Session_ID'];
            $session = new Session;
            $session->session = $session_id;
            $session->save();
            $data = $this->bookFlightData($request, $session_id);
            $response = $this->getApiResponse($data, 1);
        }

        if (!$response) {
            return response()->json(['error' => 'Flight not available'], 401);
        }

        if (!isset($response['Status']) && $response['Status'] != "1") {
            return response()->json(['error' => 'Something went wrong. Please try again'], 401);
        }

        $data = $this->getBookFlightResponse($response, count($request->traveller_type));
        $booking = new Booking;
        $booking->trip_id = $request->trip_id;
        $booking->user_id = @$request->user()->id;
        $booking->gds_pnr = is_array($request->gds_pnr)? $request->gds_pnr[0]: $request->gds_pnr;
        $booking->airline_pnr = is_array($request->airline_pnr)? $request->airline_pnr[0]: $request->airline_pnr;
        $booking->booking_id = $data['booking_id'];
        $booking->date_of_issue = $data['date_of_issue'];
        $booking->fare = $request->amount;
        $booking->currency_code = $request->currency_code;
        $booking->discount = $bookingDiscount;
        $booking->promo_code_id = $bookingPromoCodeId;
        $booking->detail = serialize($response);
        $booking->no_of_traveller = count($request->traveller_type);
        $booking->status = 1;
        //$booking->user_id = $request->user()->id;
        $booking->save();

        $booking->departure_datetime = date('Y-m-d H:i:s', strtotime($booking['originDepartureDateTime']));
        $booking->arrival_datetime = date('Y-m-d H:i:s', strtotime($booking['originArrivalDateTime']));
        $booking->save();

        /* Start Process To Send Notification To Admin */
//                $title = "Flight Booking Request";
//                $description = "'" . $request->user()->profile->first_name . "' Has Request A Flight Booking!";
//                $type = 'flight_booking_request';
//                $typeId = $booking->id;
//                $url = 'booked-flight/' . $booking->id;
        //event(new NotificationEvent(1, $title, $description, $type, $typeId, $url));

        //$user = $request->user();
        //$user->notify(new \App\Notifications\Booking($user, $booking));

        $payload = ['booking' => $booking];
        return view('website.booking.complete', $payload);
    }

    private function getValidationForBookingFlight()
    {
        return [
            'departure_date_and_time' => 'required|array',
            'arrival_date_and_time' => 'required|array',
            'duration' => 'required|array',
            'stop_quantity' => 'required|array',
            'flight_number' => 'required|array',
            'booking_desig_code' => 'required|array',
            'departure_terminal' => 'required|array',
            'arrival_terminal' => 'required|array',
            'air_equip_type' => 'required|array',
            'departure_location_code' => 'required|array',
            'departure_code_context' => 'required|array',
            'arrival_location_code' => 'required|array',
            'arrival_code_context' => 'required|array',
            'operating_airline_code' => 'required|array',
            'marketing_airline_code' => 'required|array',
            'traveller_type' => 'required|array',
            'name_prefix' => 'required|array',
            'name' => 'required|array',
            'phone' => 'required|array',
            'doc_issue_location' => 'nullable|array',
            'doc_id' => 'nullable|array',
            'gender' => 'required|array',
            'nationality' => 'required|array',
            'date_of_birth' => 'required|array',
            'expire_date' => 'nullable|array',
            'search_id' => 'required',
            'trip_id' => 'required',
            'gds_pnr' => 'required',
            'airline_pnr' => 'required',
            'amount' => 'required',
            'currency_code' => 'required',
            'flash_sale_id' => 'nullable|integer|exists:flash_sales,id',
            'promo_code' => 'nullable|string|exists:promo_codes,code'
        ];
    }

}
