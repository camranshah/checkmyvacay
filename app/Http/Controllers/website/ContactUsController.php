<?php

namespace App\Http\Controllers\website;

use App\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ContactUsController extends Controller
{
    public function index()
    {
        return view('website.contact.index');
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            ContactUs::create($request->all());
            DB::commit();
            return response()->json(['message' => 'Your message has been sent!', 'status' => true]);
        } catch (\Exception $ex) {
            return response()->json(['message' => $ex->getMessage(), 'status' => false]);
        }
    }
}
