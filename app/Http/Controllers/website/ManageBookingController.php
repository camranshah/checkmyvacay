<?php

namespace App\Http\Controllers\website;

use App\Booking;
use App\Http\Requests\ManageBookingRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('website.manage-booking.index');
    }

    public function show(ManageBookingRequest $request)
    {
        $data['Booking'] = Booking::whereBookingId($request->booking_no)->first();
        return view('website.manage-booking.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
