<?php

namespace App\Http\Controllers\website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('website.profile.index');
    }

    public function update(Request $request)
    {
        if ($request->ajax()) {
            DB::beginTransaction();
            try {
                $user = $request->user();
                $user->profile()->update($request->all());
                DB::commit();
                return response()->json(['message' => "Profile has been updated.", 'status' => true], 200);
            } catch (\Exception $ex) {
                return response()->json(['message' => $ex->getMessage(), 'status' => false], 200);
            }
        }
    }

    public function changePassword(Request $request)
    {
        $user = $request->user();

        if ($request->ajax()) {

            if (!(Hash::check($request->current_password, $user->password))) {
                return response()->json([
                    'message' => "Your current password does not matches with the password you provided. Please try again.", 'status' => false
                ]);
            }

            if (strcmp($request->current_password, $request->password) == 0) {

                return response()->json([
                    'message' => "New Password cannot be same as your current password. Please choose a different password.", 'status' => false
                ]);

            }

            $validator = \Validator::make($request->all(), [
                'current_password' => ['required'],
                'password' => ['required', 'string', 'min:8', 'max:50', 'confirmed'],
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->all()[0], 'status' => false]);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['message' => 'Your password has been changed!', 'status' => true]);

        }
    }
}
