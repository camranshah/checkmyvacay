<?php

namespace App\Http\Controllers\website;

use App\ClientApiLog;
use App\Http\Controllers\api\FlightController;
use App\Http\Requests\SearchFlightRequest;
use App\Session;
use App\UserSearchResult;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;

class SearchFlightController extends FlightController
{
    public function __construct(){
        $this->endpoint = config('cmfx.web.endpoint');
        $this->username = config('cmfx.web.username');
        $this->password = config('cmfx.web.password');
        $this->companyCode = config('cmfx.web.companyCode');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(SearchFlightRequest $request)
    {
        switch ($request->type){
            case 'one_way':
                return $this->getOneWaySearch($request);
            case 'return':
                return $this->getReturnSearch($request);
            case 'multiple':
                return $this->getMultipleSearch($request);
        }
        return $this->getReturnSearch($request);


        $session_id = $this->getSessionId();
        $data = $this->getXmlData($request, $session_id, \request('type') === 'return');

        if(request()->query('dev')) {
            $response = session('returnData');
        }else{
            $response = $this->getApiResponse($data, 1);
            session(['returnData' => $response]);
        }

        $this->createApiLog($data, $response);

        if (empty($response)) {
            return response()->json(['error' => 'Something went wrong'], 401);
        }
        if (isset($response['Error'])) {
            return view('website.search.no-result-found');
        }
        if (isset($response['PricedItineraries'])) {
            $this->convertResponse($request, $response, $record);

            $search_session_id = date('Ymdhis');
            $user_search = new UserSearchResult;
            $user_search->user_search = serialize($request->all());
            $user_search->search_result = serialize($record);
            $user_search->search_session_id = $search_session_id;
            $user_search->save();
            $searchId = @$response['PricedItineraries']['PricedItinerary'][0]['@attributes']['SearchId'];
            session(['session_id' => $session_id]);

            $record = $this->getPaginatedDataFromSearchResponse($search_session_id, 1, $searchId, $session_id);

            $data = $record['data'];

            usort($data, function ($item1, $item2) {
                return $item1['onward_stops'] <=> $item2['onward_stops'];
            });

            $data = $this->sortRecords($request, $data);

            $record['data'] = $data;
            $record['requested'] = $request->all();

            return view("website.search.index-{$request->type}", $record);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getReturnSearch(SearchFlightRequest $request)
    {
//        $session_id = $request->query('session', $this->getSessionId());
        $session_id = $request->query('session');

        if(!request()->wantsJson()){
            return view("website.search.index-return-vue", compact('session_id'));
        }

        $returnRequest = $this->returnRequest($request);

        $data = $this->getXmlData($request, $session_id);
        $returnData = $this->getXmlData($returnRequest, $session_id);

        if($request->dev){
            $record['requested'] = $request->all();
            $returnRecord['requested'] = $returnRequest->all();
            $x = session('records');
            return view("website.search.index-{$request->type}", compact('record', 'returnRecord', 'x', 'session_id'));

            $response = unserialize(ClientApiLog::find(session('api_response'))->api_response);
            $returnResponse = unserialize(ClientApiLog::find(session('api_response'))->api_response);
        }else{
            $response = $this->getApiResponse($data, 1);
            $returnResponse = $this->getApiResponse($returnData, 1);
            $log1 = $this->createApiLog($data, $response);
            $log2 = $this->createApiLog($returnData, $returnResponse);
            session()->put(['api_response' => $log1->id, 'return_api_response' => $log2->id]);
        }

        if (empty($response) || empty($returnResponse)) {
            return response()->json(['error' => 'Something went wrong'], 401);
        }
        if (isset($response['Error']) || isset($returnResponse['Error'])) {
            return view('website.search.no-result-found');
        }
        if (!isset($response['PricedItineraries']) || !isset($returnResponse['PricedItineraries'])) {
            return "Something went wrong.";
//            return view('website.search.no-result-found');
        }

        $this->convertResponse($request, $response, $record);
        $this->convertResponse($returnRequest, $response, $returnRecord);

        $search_session_id = date('Ymdhis');
        $user_search = new UserSearchResult;
        $user_search->user_search = serialize($request->all());
        $user_search->search_result = serialize($record);
        $user_search->search_session_id = $search_session_id;
        $user_search->save();
        $searchId = @$response['PricedItineraries']['PricedItinerary'][0]['@attributes']['SearchId'];
        $returnSearchId = @$returnResponse['PricedItineraries']['PricedItinerary'][0]['@attributes']['SearchId'];
        session(['session_id' => $session_id]);

        $record = $this->getPaginatedDataFromSearchResponse($search_session_id, 1, $searchId, $session_id);
        $returnRecord = $this->getPaginatedDataFromSearchResponse($search_session_id, 1, $returnSearchId, $session_id);

        $data = $record['data'];
        $returnData = $returnRecord['data'];

        $data = $this->sortRecords($request, $data);
        $returnData = $this->sortRecords($returnRequest, $returnData);

//        $record['data'] = $data;
        $record['requested'] = $request->all();
//        $returnRecord['data'] = $returnData;
        $returnRecord['requested'] = $returnRequest->all();

        $x = $this->mapJsData($data, $returnData);
        session(['records' => $x]);

        return view("website.search.index-{$request->type}", compact('record', 'returnRecord', 'x', 'session_id'));
    }

    public function getOneWaySearch(SearchFlightRequest $request)
    {
        $session_id = $this->getSessionId();

        $data = $this->getXmlData($request, $session_id);

        if($request->dev){
            $record['requested'] = $request->all();
            $x = session('records');
            return view("website.search.index-{$request->type}", compact('record', 'returnRecord', 'x', 'session_id'));

            $response = unserialize(ClientApiLog::find(session('api_response'))->api_response);
        }else{
            $response = $this->getApiResponse($data, 1);
            $log = $this->createApiLog($data, $response);
            session()->put(['api_response' => $log->id]);
        }

//        dd($response);

        if (empty($response)) {
            return response()->json(['error' => 'Something went wrong'], 401);
        }
        if (isset($response['Error'])) {
            return view('website.search.no-result-found');
        }
        if (!isset($response['PricedItineraries'])) {
            return "Something went wrong.";
        }

        $this->convertResponse($request, $response, $record);

        $search_session_id = date('Ymdhis');
        $user_search = new UserSearchResult;
        $user_search->user_search = serialize($request->all());
        $user_search->search_result = serialize($record);
        $user_search->search_session_id = $search_session_id;
        $user_search->save();
        $searchId = @$response['PricedItineraries']['PricedItinerary'][0]['@attributes']['SearchId'];
        session(['session_id' => $session_id]);

        $record = $this->getPaginatedDataFromSearchResponse($search_session_id, 1, $searchId, $session_id);

        $data = $record['data'];

        $data = $this->sortRecords($request, $data);

        $record['requested'] = $request->all();

        $x = $this->mapJsData($data);

        session(['records' => $x]);

        return view("website.search.index-one_way", compact('record', 'x', 'session_id'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index_backup(SearchFlightRequest $request)
    {
        $session_id = $this->getSessionId();
        $data = $this->getXmlData($request, $session_id);

        if(request()->query('dev')) {
            $response = session('returnData');
        }else{
            $response = $this->getApiResponse($data, 1);
            session(['returnData' => $response]);
        }

        $this->createApiLog($data, $response);

        if (empty($response)) {
            return response()->json(['error' => 'Something went wrong'], 401);
        }
        if (isset($response['Error'])) {
            return view('website.search.no-result-found');
        }
        if (isset($response['PricedItineraries'])) {
            $this->convertResponse($request, $response, $record);

            $search_session_id = date('Ymdhis');
            $user_search = new UserSearchResult;
            $user_search->user_search = serialize($request->all());
            $user_search->search_result = serialize($record);
            $user_search->search_session_id = $search_session_id;
            $user_search->save();
            $searchId = @$response['PricedItineraries']['PricedItinerary'][0]['@attributes']['SearchId'];
            session(['session_id' => $session_id]);

            $record = $this->getPaginatedDataFromSearchResponse($search_session_id, 1, $searchId, $session_id);

            $data = $record['data'];

            $data = $this->sortRecords($request, $data);

            $record['data'] = $data;
            $record['requested'] = $request->all();

            return view("website.search.index-{$request->type}", $record);
        }
    }

    /**
     * @param SearchFlightRequest $request
     * @param $session_id
     * @return string
     */
    public function getXmlData(SearchFlightRequest $request, $session_id): string
    {
        if ($request->type == "one_way") {
            $data = $this->oneWaySearch($request, $session_id);
        } elseif ($request->type == "return") {
            $data = $this->oneWaySearch($request, $session_id);
//            $data = $this->returnFlightSearch($request, $session_id);
        } else {
            $data = $this->multipleFlightSearch($request, $session_id);
        }
        return $data;
    }

    /**
     * @param string $data
     * @param $response
     */
    public function createApiLog(string $data, $response): ClientApiLog
    {
        $clientApiLog = new ClientApiLog();
        $clientApiLog->api_request = serialize($data);
        $clientApiLog->api_response = serialize($response);
        $clientApiLog->type = 'search flight';
        $clientApiLog->is_app = false;
        $clientApiLog->save();
        return $clientApiLog;
    }

    /**
     * @param SearchFlightRequest $request
     * @param $data
     * @return mixed
     */
    public function sortRecords(SearchFlightRequest $request, &$data)
    {
        usort($data, function ($item1, $item2) {
            return $item1['onward_stops'] <=> $item2['onward_stops'];
        });

        if ($request->sort_by) {
            switch ($request->sort_by):
                case 'price_low_to_high':
                    usort($data, function ($item1, $item2) {
                        return $item1['fare']['total_fare_amount'] <=> $item2['fare']['total_fare_amount'];
                    });
                    break;
                case 'price_high_to_low':
                    usort($data, function ($item1, $item2) {
                        return $item2['fare']['total_fare_amount'] <=> $item1['fare']['total_fare_amount'];
                    });
                    break;
            endswitch;
        }
        return $data;
    }

    /**
     * @param SearchFlightRequest $request
     * @param $response
     * @param $record
     */
    public function convertResponse(SearchFlightRequest $request, $response, &$record): void
    {
        if ($request->type == "one_way") {
            $record = $this->getOneWaySearchReponse($response);
        } elseif ($request->type == "return") {
//            $record = $this->getReturnSearchReponse($response);
            $record = $this->getOneWaySearchReponse($response);
        } else {
            $record = $this->getOneWaySearchReponse($response);
        }
    }

    private function returnRequest(SearchFlightRequest $request)
    {
        $returnRequest = clone $request;
        $fromCity = request('from.0');
        $toCity = request('to.0');
        $departureDate = request('departure_date.0');
        $arrivalDate = request('arrival_date.0');

        $returnRequest['from'] = [$toCity];
        $returnRequest['to'] = [$fromCity];
        $returnRequest['departure_date'] = [$arrivalDate];
        $returnRequest['arrival_date'] = [$departureDate];

        return $returnRequest;
    }

    private function mapJsData($onGoingFlights, $returnFlights = [])
    {
        $onGoingFlights = collect($onGoingFlights)
            ->map(function($flights) {
                return collect($flights['flights'])->map(function($flight) use($flights){
                    $flight['fare'] = $flights['fare'];
                    $flight['flyer'] = $flights['flyer'];
                    $flight['trip_id'] = $flights['trip_id'];
                    $flight['search_id'] = $flights['search_id'];
                    $flight['onward_stops'] = $flights['onward_stops'];
                    $flight['selected'] = false;
                    $flight['airline_name'] = $flight['operating_airline_name']['name'];
                    $flight['price'] = 0;
                    return $flight;
                });
            })
            ->flatten(1)->groupBy('operating_airline_name.name');

        $returnFlights = collect($returnFlights)
            ->map(function($flights) {
                return collect($flights['flights'])->map(function($flight) use($flights){
                    $flight['fare'] = $flights['fare'];
                    $flight['flyer'] = $flights['flyer'];
                    $flight['trip_id'] = $flights['trip_id'];
                    $flight['search_id'] = $flights['search_id'];
                    $flight['onward_stops'] = $flights['onward_stops'];
                    $flight['selected'] = false;
                    $flight['airline_name'] = $flight['operating_airline_name']['name'];
                    $flight['price'] = 0;
                    return $flight;
                });
            })
            ->flatten(1)->groupBy('operating_airline_name.name');

        $data = [];

        $onGoingFlights->each(function($flight, $key) use($returnFlights, $onGoingFlights, &$data) {
            $data[$key] = [
                'ongoing' => $onGoingFlights->get($key, []),
                'return' => $returnFlights->get($key, []),
            ];
        });

        return ($data);
    }




}
