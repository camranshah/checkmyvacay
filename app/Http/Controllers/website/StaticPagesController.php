<?php

namespace App\Http\Controllers\website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticPagesController extends Controller
{
    public function aboutUs(){
        return view('website.static-pages.about-us');
    }
    public function customerSupport(){
        return view('website.static-pages.customer-support');
    }
    public function faqs(){
        return view('website.static-pages.faqs');
    }
    public function termsAndConditions(){
        return view('website.static-pages.terms-and-conditions');
    }
    public function privacyPolicy(){
        return view('website.static-pages.privacy-policy');
    }
    public function refundPolicy(){
        return view('website.static-pages.refund-policy');
    }
}
