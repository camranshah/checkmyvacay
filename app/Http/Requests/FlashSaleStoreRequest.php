<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FlashSaleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'type' => ['required',Rule::in(['flight','package'])],
            'image' => ['nullable'],
            'tour_details' => ['nullable','string'],
            'what_to_expect' => ['nullable', 'string'],
            'departure_details' => ['nullable', 'string'],
            'departure_locations' => ['required_if:type,flight', 'array'],
            'departure_locations.*' => ['integer','exists:airports,id'],
            'arrival_locations' => ['required_if:type,flight', 'array'],
            'arrival_locations.*' => ['integer','exists:airports,id'],
            'first_interval_departure_1' => ['required_if:type,flight'],
            'first_interval_departure_2' => ['required_if:type,flight'],
            'first_interval_arrival_1' => ['required_if:type,flight'],
            'first_interval_arrival_2' => ['required_if:type,flight'],
            'second_interval_departure_1' => ['required_if:type,flight'],
            'second_interval_departure_2' => ['required_if:type,flight'],
            'second_interval_arrival_1' => ['required_if:type,flight'],
            'second_interval_arrival_2' => ['required_if:type,flight'],
            'third_interval_departure_1' => ['required_if:type,flight'],
            'third_interval_departure_2' => ['required_if:type,flight'],
            'third_interval_arrival_1' => ['required_if:type,flight'],
            'third_interval_arrival_2' => ['required_if:type,flight'],
            'url' => ['required_if:type,package'],
            'expiry_date' => ['required','date']
        ];
    }
}
