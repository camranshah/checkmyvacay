<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ManageBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'booking_no' => ['required', 'exists:bookings,booking_id'],
            'last_name' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'booking_no.required' => 'Please enter booking no.',
            'booking_no.exists' => 'Incorrect booking no.',
            'last_name' => 'Please enter last name'
        ];
    }
}
