<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\RequiredIf;

class PromoCodeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' =>
                $this->method() === 'PUT'?
                    ['required', 'string', 'max:50',
                        Rule::unique('promo_codes')->where(function ($query) {
                            return $query->where('code', $this->code)
                            ->where('id', '!=', $this->route()->parameter('code')->id);
                        })
                    ] : ['required', 'string', 'max:50','unique:promo_codes,code'],
            'type' => ['required', 'string', Rule::in(['subscription', 'booking'])],
            'discount_type' => ['required', 'string', Rule::in(['percentage', 'fixed'])],
            'discount_value' => ['required', 'numeric'],
            'refer_tag' => ['required', 'string', 'max:50'],
            'end_date' => ['required'],
            'users' => [new RequiredIf(!$this->guest_user), 'array'],
            'users.*' => ['integer', 'exists:users,id']
        ];
    }
}
