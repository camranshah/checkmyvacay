<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchFlightRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search_id' => 'nullable',
            'type' => 'required_without:search_id|in:one_way,return,multiple',
            'from' => 'required_without:search_id|array|min:1',
            'to' => 'required_without:search_id',
            'departure_date' => 'required_without:search_id|array',
            'arrival_date' => 'required_if:type,return|array|min:1',
            'cabin' => 'nullable',
            'adult' => 'nullable|integer',
            'child' => 'nullable|integer',
            'infant' => 'nullable|integer',
            'page_no' => 'required_with:search_id',
            'arline_code' => 'nullable',
            'time_onward_from' => 'nullable',
            'time_onward_to' => 'nullable',
            'time_return_from' => 'nullable',
            'time_return_to' => 'nullable',
        ];
    }
}
