<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $table = 'notifications';

    public $fillable = ['user_id', 'title', 'description', 'url', 'is_read'];

    public $appends = ['time_ago'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('defaultOrdering', function (Builder $builder) {
            $builder->orderBy('is_read', 'desc')->orderBy('id', 'desc');
        });
    }

    public function scopeUnread($query)
    {
        return $query->whereIsRead(0);
    }

    public function scopeRead($query)
    {
        return $query->whereIsRead(1);
    }

    public function scopeDefaultTake($query)
    {
        return $query->take(5);
    }

    public function getTimeAgoAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }
}
