<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoCode extends Model
{
    use SoftDeletes, ChangeDateFormat;

    public $table = 'promo_codes';

    public $fillable = ['code', 'type', 'discount_type', 'discount_value', 'start_date', 'end_date', 'refer_tag', 'status'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('not_expired', function (Builder $builder) {
            $builder->where('end_date', '>', date('Y-m-d'));
        });

    }

    public function bookings()
    {
        return $this->hasMany('App\Booking', 'promo_code_id')->orderBy('id', 'desc');
    }

    public function user_promo_codes(){
        return $this->hasMany('App\UserPromoCode','promo_code_id');
    }

    public function allowed_users(){
        return $this->belongsToMany(User::class, 'App\UserPromoCode');
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = date('Y-m-d', strtotime($value));
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = date('Y-m-d', strtotime($value));
    }

    public function setStatusAttribute($value)
    {
        switch ($value):
            case 'active':
                $bool = 1;
                break;
            case 'inactive':
                $bool = 0;
                break;
            default:
                $bool = 1;
                break;
        endswitch;

        $this->attributes['status'] = $bool;
    }
}
