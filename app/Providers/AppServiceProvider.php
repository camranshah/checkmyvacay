<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $log = [];
        $log['url'] = \Request::fullUrl();
        $log['method'] = \Request::method();
        $log['ip'] = \Request::ip();
        $log['agent'] = \Request::header('user-agent');
        $log['params'] = \Request::all();
        // $log['user_id'] = auth()->check() ? auth()->user()->id : 1;
        \Log::info($log);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
