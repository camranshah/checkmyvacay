<?php

namespace App\Providers;

use App\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();

        view()->composer('*', function ($view) {
            if (Auth::check()):
                $user = Auth::user();
                $view->with('GlobalData', [
                    'Notifications' => Notification::whereUserId($user->id)->defaultTake()->get(),
                    'NotificationUnreadCount' => Notification::whereUserId($user->id)->unread()->count()
                ]);
            endif;
        });
    }
}
