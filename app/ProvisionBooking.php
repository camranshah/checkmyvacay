<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProvisionBooking extends Model
{
    use SoftDeletes;

    protected $table = 'provision_bookings';

    protected $fillable = ['user_id', 'trip_id', 'gds_pnr', 'airline_pnr', 'details'];

}
