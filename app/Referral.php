<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Referral extends Model
{
    use SoftDeletes, ChangeDateFormat;

    protected $table = 'referrals';

    protected $fillable = ['code'];

}
