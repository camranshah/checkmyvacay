<?php

namespace App\Soap\Request;


Class Login{
    /**
     * @var string
     */
    protected $Username;

    /**
     * @var string
     */
    protected $Password;

    /**
     * @var string
     */
    protected $CompanyCode;

    /**
     * @var string
     */
    protected $Amount;

    /**
     * GetConversionAmount constructor.
     *
     * @param string $CurrencyFrom
     * @param string $CurrencyTo
     * @param string $RateDate
     * @param string $Amount
     */
    public function __construct($Username, $Password, $CompanyCode)
    {
        $this->Username = $Username;
        $this->Password   = $Password;
        $this->CompanyCode     = $CompanyCode;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->Username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * @return string
     */
    public function getCompanyCode()
    {
        return $this->CompanyCode;
    }
}
