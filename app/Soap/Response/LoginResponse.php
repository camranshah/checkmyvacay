<?php

namespace App\Soap\Response;

class LoginResponse{
    /**
     * @var string
     */
    protected $getLoginResponse;

    /**
     * GetConversionAmountResponse constructor.
     *
     * @param string
     */
    public function __construct($getLoginResponse)
    {
        $this->getLoginResponse = $getLoginResponse;
    }

    /**
     * @return string
     */
    public function getLoginResponse()
    {
        return $this->getLoginResponse;
    }
}
