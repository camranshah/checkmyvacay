<?php

namespace App\ThirdParty;

class BPoint
{
    protected $endpoint;
    protected $username;
    protected $password;
    protected $merchantNo;
    protected $client;
    protected $response;

    public function __construct()
    {
        $this->endpoint = "https://www.bpoint.com.au/evolve/service.asmx?WSDL";
        $this->username = config('app.BPOINT_USERNAME');
        $this->password = config('app.BPOINT_PASS');
        $this->merchantNo = config('app.BPOINT_MERCHANT');
        $this->response = ['response' => ['ResponseCode' => '', 'ResponseMessage' => '']];

        $this->client = new \SoapClient($this->endpoint);

    }

    public function toArray()
    {
        return json_decode(json_encode($this->response), true);
    }

    public function addToken($cardNumber = '', $expiryDate = '', $crn1 = '', $crn2 = '', $crn3 = '')
    {
        try {

            $result = $this->client->addToken([
                'username' => $this->username,
                'password' => $this->password,
                'merchantNumber' => $this->merchantNo,
                'CardNumber' => $cardNumber,
                'CRN1' => $crn1,
                'CRN2' => $crn2,
                'CRN3' => $crn3,
                'ExpiryDate' => $expiryDate
            ]);

            $this->response = $result;

        } catch (\Exception $ex) {

            $this->response['response']['ResponseCode'] = 'ERROR';
            $this->response['response']['ResponseMessage'] = $ex->getMessage();

        }

        return $this;
    }

    public function deleteToken($token = '')
    {
        try {

            $result = $this->client->deleteToken([
                'username' => $this->username,
                'password' => $this->password,
                'merchantNumber' => $this->merchantNo,
                'token' => $token,
            ]);

            $this->response = $result;

        } catch (\Exception $ex) {

            $this->response['response']['ResponseCode'] = 'ERROR';
            $this->response['response']['ResponseMessage'] = $ex->getMessage();

        }

        return $this;
    }

    public function processPayment($trans = [])
    {
        try {

            $result = $this->client->processPayment([
                'username' => $this->username,
                'password' => $this->password,
                'merchantNumber' => $this->merchantNo,
                'txnReq' => $trans
            ]);

            $this->response = $result;

        } catch (\Exception $ex) {

            $this->response['response']['ResponseCode'] = 'ERROR';
            $this->response['response']['ResponseMessage'] = $ex->getMessage();

        }

        return $this;
    }
}
