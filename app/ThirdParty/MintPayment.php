<?php

namespace App\ThirdParty;

class MintPayment
{
    protected $endpoint;
    protected $companyToken;
    protected $authToken;


    public function __construct()
    {

//        API Key : 8d853075-011e-361d-be1e-72c370d1c1aa
//        Company Token : GXjyxXC7g94vvO2UBYcZ01H3G0aZz3v

        // $this->url = "https://secure.mintpayments.net/mpay/v2";
        $this->url = "https://secure-uatsb.mintpayments.net/mpay/v2";
        $this->url = config('app.MINT_URL');
        $this->companyToken = config('app.MINT_COMPANY_TOKEN');
        $this->authToken = config('app.MINT_AUTH_TOKEN');

    }

    public function transactionToken()
    {

        $array = [
            "company_token" => $this->companyToken
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . "/transaction_token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($array),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Bearer " . $this->authToken,
                "company_token: " . $this->companyToken,
                "X-Requested-With: XMLHttpRequest",
                "Content-Type: text/plain"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response, true);

    }

    public function tokenizeCard($userId, $ipAddress, $timeZone = "Australia/Sydney", $cardNumber, $expiryMonth, $expiryYear, $cvc, $cardHolderName)
    {
        $array = [

            "company_token" => $this->companyToken,

            "customer" => [
                "customer_reference" => $userId,
                "ip_address" => $ipAddress,
                "timezone" => $timeZone,
                "store_card_permission" => "true"
            ],
            "card" => [
                "number" => $cardNumber,
                "expiry_month" => $expiryMonth,
                "expiry_year" => $expiryYear,
                "cvc" => $cvc,
                "holder_name" => $cardHolderName
            ]
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . "/tokenize_card",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($array),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Bearer " . $this->authToken,
                "company_token: " . $this->companyToken,
                "X-Requested-With: XMLHttpRequest",
                "Content-Type: text/plain"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response, true);

    }


    public function tokenizeCard2($cardNumber, $expiryMonth, $expiryYear, $cvc, $cardHolderName, $timeZone = "Australia/Sydney")
    {

        $array = [
            "company_token" => $this->companyToken,
            "customer" => [
                "customer_reference" => '',
                "store_card_permission" => "true"
            ],
            "card" => [
                "number" => $cardNumber,
                "expiry_month" => $expiryMonth,
                "expiry_year" => $expiryYear,
                "cvc" => $cvc,
                "holder_name" => $cardHolderName
            ]
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . "/tokenize_card",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($array),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Bearer " . $this->authToken,
                "company_token: " . $this->companyToken,
                "X-Requested-With: XMLHttpRequest",
                "Content-Type: text/plain"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return json_decode($response, true);

    }

    public function invalidateToken($cardToken)
    {

        $array = [
            "company_token" => $this->companyToken,
            "card_token" => $cardToken,
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . "/invalidate_card_token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($array),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Bearer " . $this->authToken,
                "company_token: " . $this->companyToken,
                "X-Requested-With: XMLHttpRequest",
                "Content-Type: text/plain"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response, true);

    }

    public function purchaseWithToken($userId, $ipAddress, $cardToken, $invoiceNumber, $amount, $currency = "AUD", $shouldMintApplySurcharge = false)
    {

        $array = [
            "token" => $this->transactionToken(),
            "customer" => [
                "customer_reference" => $userId,
                "ip_address" => $ipAddress,
                "timezone" => "Australia/Sydney"
            ],
            "purchase" => [
                "card_token" => $cardToken,
                "invoice_number" => $invoiceNumber,
                "amount" => $amount,
                "should_mint_apply_surcharge" => $shouldMintApplySurcharge,
                "currency" => $currency
            ]
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . "/purchase_with_token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($array),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Bearer " . $this->authToken,
                "company_token: " . $this->companyToken,
                "X-Requested-With: XMLHttpRequest",
                "Content-Type: text/plain"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response, true);

    }

    public function pay($invoiceNumber, $amount, $currency = "AUD", $shouldMintApplySurcharge = false)
    {
        $paymentToken = auth()->user()->payment_tokens()->first();

        return $this->purchaseWithToken(auth()->id(), request()->ip(), request('payment_token'), $invoiceNumber, $amount, $currency, $shouldMintApplySurcharge);
    }
}
