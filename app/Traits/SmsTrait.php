<?php

namespace App\Traits;

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;
use Twilio\Exceptions\RestException;

trait SmsTrait
{
    // gate 4 shop 80 Zeeshan
    public function sendSms($to, $message)
    {
        $accountSid = config('services.twillo.TWILIO_ACCOUNT_SID');
        $authToken = config('services.twillo.TWILIO_AUTH_TOKEN');
        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create($to,
                array(
                    'from' =>  "CMFx",
                    'body' => $message
                )
            );

//            $client->messages->create($to,
//                array(
//                    'from' =>  "+12064018553",
//                    'body' => $message
//                )
//            );
            return ['status' => true, 'message' => 'Message Sent!'];

        } catch (RestException $e) {
            try{
                $client->messages->create($to,
                    array(
                        'from' =>  "+12064018553",
                        'body' => $message
                    )
                );
                return ['status' => true, 'message' => 'Message Sent!'];
            }catch (\Exception $d){
                // $message = $d->getMessage();
                $message = "Unable to send SMS to " . request('phone_code') . request('phone') . ". Please enter a valid phone number.";
                return ['status' => false, 'message' =>  $message];
                // return ['status' => false, 'message' =>  substr($message, strpos($message, 'The'))];
            }
        }


    }

}
