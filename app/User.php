<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, ChangeDateFormat;

    protected $with = ['profile', 'user_role', 'account_payments'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'remember_token',
        'activation_code',
        'email_verified',
        'number_verified',
        'is_subscribed',
        'subscribed_at',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function account_payments()
    {
        return $this->hasMany('App\AccountPayment', 'user_id')->orderBy('id', 'desc');
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking', 'user_id')->orderBy('id', 'desc');
    }

    public function activity_log()
    {
        return $this->hasMany('App\ActivityLog', 'user_id')
            ->orderBy('id', 'desc')
            ->take(50);
    }

    public function flash_sale()
    {
        return $this->belongsTo('App\FlashSale', 'flash_sale_id');
    }

    public function user_role()
    {
        return $this->hasOne('App\UserRelationRole', 'user_id');
    }

    public function profile()
    {
        return $this->hasOne('App\UserProfile', 'user_id');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification', 'user_id')->orderBy('id', 'desc')->whereIsRead(0);

    }

    public function frequent_flyers()
    {
        return $this->hasMany('App\FrequentFlyer', 'user_id')
            ->orderBy('is_default', 'desc')
            ->orderBy('id', 'desc')
            ->groupBy('name', 'surname');
    }

    public function payment_tokens()
    {
        return $this->hasMany('App\UserPaymentToken', 'user_id')->orderBy('is_default', 'desc')->orderBy('id', 'desc');
    }

    public function upcoming_flights()
    {
        return $this->hasMany('App\Booking', 'user_id')->where('departure_datetime', '>', date('Y-m-d H:i:00'))->orderBy('departure_datetime', 'asc')->take(5);
    }

    public function completed_flights()
    {
        return $this->hasMany('App\Booking', 'user_id')->where('arrival_datetime', '<', date('Y-m-d H:i:00'))->orderBy('arrival_datetime', 'asc')->take(5);
    }

    public function user_promo_codes(){
        return $this->hasMany('App\UserPromoCode','user_id')->latest();
    }

    public function setStatusAttribute($value)
    {
        switch ($value):
            case 'active':
                $bool = 1;
                break;
            case 'inactive':
                $bool = 0;
                break;
            default:
                $bool = 1;
                break;
        endswitch;

        $this->attributes['status'] = $bool;
    }


}
