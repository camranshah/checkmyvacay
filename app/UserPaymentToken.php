<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPaymentToken extends Model
{
    use SoftDeletes;

    public $table = 'user_payment_tokens';

    public $fillable = [
        'user_id',
        'card_type',
        'masked_card_number',
        'token',
        'card_expiry_month',
        'card_expiry_year',
        'card_holder_name',
        'funding',
        'country',
        'customer_id',
        'is_default'
    ];

    protected static function boot()
    {
        parent::boot();
        self::creating(function(self $token){
            $token->card_number = request('ccnumber', request('credit_card_number', request('number')));
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
