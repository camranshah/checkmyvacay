<?php

namespace App;

use App\Traits\ChangeDateFormat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    use ChangeDateFormat;

    public $table = 'user_profiles';

    public $fillable = ['title', 'first_name', 'middle_name', 'last_name','phone', 'phone_code', 'dob', 'country', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getDobAttribute($date)
    {
        if($date){
            return date('m/d/Y',strtotime($date));
        }
        return '';
    }

    public function setDobAttribute($date){
//        $this->attributes['dob'] = date('Y-m-d',strtotime($date));
        $dob = \DateTime::createFromFormat('d/m/Y', $date);
        $this->attributes['dob'] = $dob->format('Y-m-d');
    }

}
