<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPromoCode extends Model
{
    use SoftDeletes;

    protected $table = 'user_promo_codes';

    protected $fillable = ['user_id', 'promo_code_id', 'is_used'];

    public function promo_code()
    {
        return $this->belongsTo('App\PromoCode', 'promo_code_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
