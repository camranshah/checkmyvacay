<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRelationRole extends Model
{
    public $timestamps = false;

    protected $fillable = ['user_id', 'role_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }
}
