<?php

return [
    'web' => [
        'endpoint' => env('WEB_API_ENDPOINT'),
        'username' => env('WEB_API_USERNAME'),
        'password' => env("WEB_API_PASSWORD"),
        'companyCode' => env("WEB_API_COMPANY_CODE")
    ],
    'api' => [
        'endpoint' => env('APP_API_ENDPOINT'),
        'username' => env('APP_API_USERNAME'),
        'password' => env("APP_API_PASSWORD"),
        'companyCode' => env("APP_API_COMPANY_CODE")
    ],
];



