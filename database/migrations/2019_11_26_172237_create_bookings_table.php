<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trip_id',20);
            $table->string('booking_id');
            $table->string('date_of_issue');
            $table->string('fare');
            $table->string('currency_code');
            $table->text('detail');
            $table->integer('no_of_traveller',false);
            $table->integer('user_id',false);
            $table->integer('status',false)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
