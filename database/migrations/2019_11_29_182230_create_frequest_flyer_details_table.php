<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrequestFlyerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frequest_flyer_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gender',1);
            $table->string('name');
            $table->string('family_name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('dob',10);
            $table->integer('user_id',false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frequest_flyer_details');
    }
}
