<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrequentFlyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frequent_flyers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('traveller_type', 50)->nullable();
            $table->string('name_prefix', 50)->nullable();
            $table->string('name', 50)->nullable();
            $table->string('surname', 50)->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('doc_issue_location', 50)->nullable();
            $table->string('doc_id', 50)->nullable();
            $table->string('gender', 50)->nullable();
            $table->string('nationality', 50)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->date('expiry_date')->nullable();
            $table->text('extra_attribute_1')->nullable();
            $table->text('extra_attribute_2')->nullable();
            $table->text('extra_attribute_3')->nullable();
            $table->text('extra_attribute_4')->nullable();
            $table->text('extra_attribute_5')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frequent_flyers');
    }
}
