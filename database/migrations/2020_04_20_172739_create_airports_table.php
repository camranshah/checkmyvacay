<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("icao",50)->nullable();
            $table->string("iata",50)->nullable();
            $table->string("name",50)->nullable();
            $table->string("city",50)->nullable();
            $table->string("state",50)->nullable();
            $table->string("country",50)->nullable();
            $table->string("elevation",50)->nullable();
            $table->string("lat",50)->nullable();
            $table->string("lon",50)->nullable();
            $table->string("tz",50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airports');
    }
}
