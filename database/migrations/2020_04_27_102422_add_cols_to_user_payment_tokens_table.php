<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsToUserPaymentTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_payment_tokens', function (Blueprint $table) {
            $table->string("card_expiry_month",50)->after('token')->nullable();
            $table->string("card_expiry_year",50)->after('card_expiry_month')->nullable();
            $table->string("card_holder_name",50)->after('card_expiry_year')->nullable();
            $table->string("funding",50)->after('card_holder_name')->nullable();
            $table->string("country",50)->after('funding')->nullable();
            $table->text("customer_id")->after('country')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_payment_tokens', function (Blueprint $table) {
            //
        });
    }
}
