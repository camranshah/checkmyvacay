<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlashSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flash_sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title');
            $table->string('image', 50)->nullable();
            $table->string('discount_type', 50);
            $table->string('discount_value', 50);
            $table->text('tour_details')->nullable();
            $table->text('what_to_expect')->nullable();
            $table->text('departure_details')->nullable();
            $table->string('status',50)->default('active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_sales');
    }
}
