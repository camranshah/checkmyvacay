<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToFlashSaleIntervalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flash_sale_intervals', function (Blueprint $table) {
            $table->renameColumn('start_date', 'departure_date_1');
            $table->renameColumn('end_date', 'departure_date_2');
            $table->date('arrival_date_1')->before('created_at')->nullable();
            $table->date('arrival_date_2')->before('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flash_sale_intervals', function (Blueprint $table) {
            //
        });
    }
}
