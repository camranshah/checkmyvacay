<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostalCodeInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('user_profiles', 'postal_code')){
            Schema::table('user_profiles', function (Blueprint $table) {
                $table->string('postal_code')->nullable();
            });
        }

        if(!Schema::hasColumn('frequent_flyers', 'postal_code')){
            Schema::table('frequent_flyers', function (Blueprint $table) {
                $table->string('postal_code')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('user_profiles', 'postal_code')){
            Schema::table('user_profiles', function (Blueprint $table) {
                $table->dropColumn('postal_code');
            });
        }

        if(Schema::hasColumn('frequent_flyers', 'postal_code')){
            Schema::table('frequent_flyers', function (Blueprint $table) {
                $table->dropColumn('postal_code');
            });
        }
    }
}
