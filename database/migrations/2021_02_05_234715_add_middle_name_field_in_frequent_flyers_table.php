<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMiddleNameFieldInFrequentFlyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('frequent_flyers', function (Blueprint $table) {
            if (!Schema::hasColumn('frequent_flyers', 'middle_name'))
                $table->string('middle_name')->default('')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('frequent_flyers', function (Blueprint $table) {
            if (Schema::hasColumn('frequent_flyers', 'middle_name'))
                $table->dropColumn('middle_name');
        });
    }
}
