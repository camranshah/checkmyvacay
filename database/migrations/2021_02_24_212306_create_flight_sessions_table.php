<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('flight_sessions')) {
            Schema::create('flight_sessions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('session_id');
                $table->string('type')->default('one_way');
                $table->text('search_params')->nullable();
                $table->text('search_response')->nullable();
                $table->text('provision_params')->nullable();
                $table->text('provision_response')->nullable();
                $table->text('booking_params')->nullable();
                $table->text('booking_response')->nullable();
                $table->unsignedBigInteger('booking_id');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight_sessions');
    }
}
