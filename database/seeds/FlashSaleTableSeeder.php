<?php

use Illuminate\Database\Seeder;
use App\FlashSale;
use App\FlashSaleLocation;
use App\FlashSaleInterval;

class FlashSaleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $flashSale = FlashSale::create([
            'title' => '50% Off on selected flights Dubai, Malaysia, London',
            'image' => 'test-banner.jpeg',
            'discount_type' => 'percentage',
            'discount_value' => '50',
            'tour_details' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultrices ornare massa sit amet semper. Suspendisse at malesuada sem, eget faucibus libero. Integer interdum nisl urna, vitae mollis risus convallis at. Quisque in nisl quis arcu consectetur condimentum eu quis est. Praesent nec erat sit amet nisl ullamcorper ullamcorper ut ut nulla. Suspendisse sed sollicitudin augue. Maecenas sollicitudin justo est, vel viverra elit mattis vel. Donec sem dui, sagittis nec gravida in, ornare sed lectus. Suspendisse malesuada, tortor in fermentum ultrices, sem urna imperdiet sapien, a rhoncus sapien urna sed magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi sit amet ipsum velit. Ut justo ante, eleifend vitae nisl viverra, interdum molestie velit. Curabitur sit amet fermentum libero.',
            'what_to_expect' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultrices ornare massa sit amet semper. Suspendisse at malesuada sem, eget faucibus libero. Integer interdum nisl urna, vitae mollis risus convallis at. Quisque in nisl quis arcu consectetur condimentum eu quis est. Praesent nec erat sit amet nisl ullamcorper ullamcorper ut ut nulla. Suspendisse sed sollicitudin augue. Maecenas sollicitudin justo est, vel viverra elit mattis vel. Donec sem dui, sagittis nec gravida in, ornare sed lectus. Suspendisse malesuada, tortor in fermentum ultrices, sem urna imperdiet sapien, a rhoncus sapien urna sed magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi sit amet ipsum velit. Ut justo ante, eleifend vitae nisl viverra, interdum molestie velit. Curabitur sit amet fermentum libero.',
            'departure_details' => "<h1>Departure Date:</h1><p>04/05/2020</p><h1>Arival Date:</h1><p>05/05/2020</p>",
            'status' => 1
        ]);

        $flashSaleLocation = FlashSaleLocation::create(['flash_sale_id' => $flashSale->id, 'airport_id' => 6408, 'type' => 1]);
        $flashSaleLocation = FlashSaleLocation::create(['flash_sale_id' => $flashSale->id, 'airport_id' => 6409, 'type' => 2]);

        $flashSaleInterval = FlashSaleInterval::create(['flash_sale_id' => $flashSale->id, 'start_date' => '2020-05-20', 'end_date' => '2020-06-20']);

    }
}
