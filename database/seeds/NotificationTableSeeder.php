<?php

use Illuminate\Database\Seeder;
use App\Notification;

class NotificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Notification::create(['user_id' => 26, 'title' => 'test', 'description' => 'test', 'is_read' => 0]);
    }
}
