function letterCase(string) {
  let text;
  try{
      text = string.charAt(0).toUpperCase() + string.slice(1);
      return text;
  }catch(e){
      return string;
  }
}


$(document).ready(function () {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

    // show and hide outbound div on checkbox click
    // $('#stopover').change(function () {
    //     if (this.checked)
    //         $('.outbound-div').fadeIn('slow');
    //     else
    //         $('.outbound-div').fadeOut('slow');
    // });

    $('.stopover').click(function(){
        $(this).toggleClass('selected')
        $('.outbound-div').toggle('slow');
    });

    // profile edit btn to remove disabled attributes from inputs
});

// modal show on page load
setTimeout(function () {
    $("#paymentunsuccess").modal('show');
}, 3000);

$(".show-notification-dropdown").click(function () {
    $('.login-popup.notification').slideToggle('slow');
});

// $(".show-userLogin-dropdown").click(function () {
//     $('.login-popup.login').slideToggle('slow');
// });

$(".popup-cross.login").click(function () {
    $('.login-popup.login').slideToggle('slow');
});

$(".custom-dropdown-m").on('click', function(e){
    e.preventDefault();
    $(".custom-dropdown-m").next().hide();
    $(this).next().slideToggle();
});

$(document).on('click', function(e){
    // const $trigger = $(".custom-dropdown-m + div");
    // if($trigger !== e.target){
    //     $(".custom-dropdown-m + div").slideUp("fast");
    // }
});

// $(".show-userRegister-dropdown").click(function () {
//     $('.login-popup.register').slideToggle('slow');
// });

$(".popup-cross.register").click(function () {
    $('.login-popup.register').slideToggle('slow');
});

// modal hide one modal show next one
$("#pwd1-btn").click(function () {
    $('#pwdrecovery1').modal('hide');
    $('#pwdrecovery2').modal('show');
});

// modal hide one modal show next one
$("#pwd2-btn").click(function () {
    $('#pwdrecovery2').modal('hide');
    $('#pwdrecovery3').modal('show');
});

// show password on eye icon click
$(".confirm-icon").click(function () {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $(".confirm-input");
    if (input.attr("type") === "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

// show password on eye icon click
$(".enter-icon").click(function () {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $(".enter-input");
    if (input.attr("type") === "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

// show password on eye icon click
$(".current-icon").click(function () {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $(".current-input");
    if (input.attr("type") === "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});




// popular destination homepage slider
$('.slider-nav.homepage-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    arrows: true,
    prevArrow: "<div class='slick-prev-has'><i class='fa fa-arrow-left' aria-hidden='true'></i></div>",
    nextArrow: "<div class='slick-next-has'><i class='fa fa-arrow-right' aria-hidden='true'></i></div>",
    autoplay: true,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 575,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }
    ]
});


function increaseValue(element, elem2) {
    let val = parseInt($(element).prev().val())
    $(element).prev().val(val + 1);
    chaneRoundTripHandler(elem2);
}

function chaneRoundTripHandler(elem2){
    var adult = Number($(elem2).find('input[name=adult]').val());
    var child = Number($(elem2).find('input[name=child]').val());
    var infant = Number($(elem2).find('input[name=infant]').val());
    var cabin = $(elem2).find('select[name=cabin]').val();
    // console.log(elem2, adult, child, infant, cabin);
    var array = [];
    if(adult)
        array.push(adult+' Adult ');
    if(child)
        array.push(child+' Child ');
    if(infant)
        array.push(infant+' Infant ');
    array.push(`${letterCase(cabin)}`);
    let text = `${array.join(',')}`;
    $(elem2).find('.show-package-dropdown span').text(text);
}

function decreaseValue(element, elem2) {
    let val = parseInt($(element).next().val())
    if (val > 0) {
        $(element).next().val(val - 1)
    }
    chaneRoundTripHandler(elem2);
}

$('.date_1').datepicker({
    format: 'dd/mm/yyyy',
    startDate: '+0d',
}).on('changeDate', function(ev){
    $(this).datepicker('hide');
});

$('.dob').datepicker({
    format: 'dd/mm/yyyy',
    endDate: '+0d',
});

//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function () {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'transform': 'scale(' + scale + ')',
                'position': 'absolute'
            });
            next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".previous").click(function () {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});



