/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import HistogramSlider from 'vue-histogram-slider';
import Vue from 'vue';
// window.Vue = require('vue');
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import SearchReturnComponent from "./components/return-search-page";

const routes = [
  { path: '/search-flight', component: SearchReturnComponent }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
    routes,
    mode: 'history',
    base: `${window.base_url}`,
})

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('return-listing-index', require('./components/SearchReturnComponent.vue').default);
Vue.component('one-way-listing-index', require('./components/SearchOneWayComponent.vue').default);
Vue.component('table-header', require('./components/TableHeadComponent').default);
Vue.component('histogramslider', HistogramSlider);

const app = new Vue({
    el: '#app',
    router
});
