@extends('layouts.app', ['Title' => 'API Logs'])

@section('content')

    <div class="app-content content view user">
        <div class="content-wrapper">
            <div class="content-body">
                <div id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Search Logs</h1>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-3">
                                            <div class="col">
                                                <div class="accordion" id="accordionExample">
                                                    @foreach($Logs as $log)
                                                        <div class="card mb-1">
                                                            <div class="card-header" id="{{"heading-$log->id"}}">
                                                                <h2 class="m-0">
                                                                    <button class="btn btn-link btn-block text-left mt-0" type="button" data-toggle="collapse" data-target="#collapse-{{ $log->id }}" aria-expanded="true" aria-controls="collapse-{{ $log->id }}">
                                                                    {{ $log->type }} {{ $log->created_at->format('d/m/Y h:i:s') }}
                                                                    </button>
                                                                </h2>
                                                            </div>

                                                            <div id="collapse-{{ $log->id }}" class="collapse{{ $loop->first? ' show': '' }}" aria-labelledby="{{"heading-$log->id"}}" data-parent="#accordionExample">
                                                                <div class="card-body">
                                                                    <h5 class="card-title">Request From: {{ $log->is_app? 'Mobile': 'Web' }}</h5>
                                                                    <h5 class="card-title">Request:</h5>
                                                                    <pre class="card-text">{{ @unserialize($log->api_request) }}</pre>
                                                                    <h5 class="card-title">Response:</h5>
                                                                    <pre class="card-text">{{ var_dump(unserialize($log->api_response)) }}</pre>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <div class="d-flex justify-content-center mt-5">
                                                    {{ $Logs->links() }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
