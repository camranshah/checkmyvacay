@extends('layouts.app', ['Title' => 'Login'])

@section('content')
    <section class="register loginn">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xl-3"></div>
                <div class="col-md-8 col-xl-6 col-12">
                    <div class="register-main">
                        <img src="{{ asset('custom/images/logo-reg.png') }}" class="img-full" alt="logo">
                        <div class="form-main">
                            <h1>Login</h1>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="fields">
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <input spellcheck="true" type="text" name="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   value="{{ old('email') }}" spellcheck="true"
                                                   placeholder="Email Address">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <i class="fa fa-key"></i>
                                            <input spellcheck="true" type="password" name="password" spellcheck="true"
                                                   class="form-control @error('password') is-invalid @enderror"
                                                   placeholder="Password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-md-6 col-6">
                                            <label class="check" for="remember">Remember me
                                                <input spellcheck="true" type="checkbox" name="remember"
                                                       checked="checked"
                                                       id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        @if (Route::has('password.request'))
                                            <div class="col-md-6 col-6"><a href="{{ route('password.request') }}">Forgot
                                                    Password?</a></div>
                                        @endif
                                    </div>
                                    <button type="submit" class="form-control">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xl-3"></div>
            </div>
        </div>
    </section>
@endsection
