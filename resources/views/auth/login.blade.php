@extends('website.layouts.app', ['Title' => 'Login'])

@section('content')
    <section class="login-page">
        <div class="container">
            <div class="login-card">
                <div class="row ml-0 mr-0">
                    <div class="col-md-6 col-12 pl-0 pr-0 login-left-col"></div>
                    <div class="col-md-6 col-12 pl-0 pr-0 login-right-col">
                        <div class="login-right-content">
                            <h2 class="login-card-heading">LOGIN</h2>
                            <h5 class="login-subheading">Log in to your account</h5>
                            <form action="{{ route('login') }}" method="post">
                                @csrf
                                <div class="form-field">
                                    <label for="" class="site-label">Email Address</label>
                                    <input type="email" class="form-control site-input @error('email') is-invalid @enderror" placeholder="Enter Email Address"
                                           name="email" value="{{ old('email') }}" focus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                                <label for="" class="site-label">Password</label>
                                <div class="form-field">
                                    <input type="password" class="form-control site-input icon enter-input @error('password') is-invalid @enderror"
                                           placeholder="Enter Password" name="password">
                                    <i class="fa fa-eye-slash enter-icon input-right-icon" aria-hidden="true"></i>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                                <div class="below-input-div">
                                    <p class="black-text">
                                        <input type="checkbox" id="stopover"
                                               name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="stopover" class="bordered">Remember Me</label>
                                    </p>
                                    <a href="#" class="forgot-link" data-toggle="modal" data-target="#pwdrecovery1">Forgot Password ?</a>
                                </div>
                                <button type="submit" class="site-btn blue d-block text-center text-uppercase">Login<i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                            </form>
                            <p class="site-text black-text text-center mt-4">Don't have an account? <a href="{{ route('register') }}" class="forgot-link">Register</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
