@extends('layouts.app')

@section('content')
    <section class="register loginn">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xl-3"></div>
                <div class="col-md-8 col-xl-6 col-12">
                    <div class="register-main">
                        <img src="{{ asset('custom/images/logo-reg.png') }}" class="img-full" alt="logo">
                        <div class="form-main">
                            <h1>Forgot Password</h1>
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                <div class="fields">
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <input spellcheck="true" id="email" type="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   spellcheck="true" name="email" value="{{ old('email') }}"
                                                   placeholder="Enter Email Address">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <button type="submit" class="form-control"
                                            onclick="parent.location='forgot-code.html'">Continue
                                    </button>
                                    <a href="{{ route('login') }}" class="login back"><i
                                            class="fa fa-long-arrow-left"></i>back to login</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xl-3"></div>
            </div>
        </div>
    </section>
@endsection
