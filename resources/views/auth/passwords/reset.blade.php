@extends('layouts.app')

@section('content')
    <section class="register loginn">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-xl-3"></div>
                <div class="col-md-8 col-xl-6 col-12">
                    <div class="register-main">
                        <img src="{{ asset('custom/images/logo-reg.png') }}" class="img-full" alt="logo">
                        <div class="form-main">
                            <h1>Forgot Password</h1>
                            <form method="POST" action="{{ route('password.update') }}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="fields">
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                            <input id="email" type="email" name="email"
                                                   value="{{ $email ?? old('email') }}"
                                                   class="form-control @error('email') is-invalid @enderror">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <i class="fa fa-key" aria-hidden="true"></i>
                                            <input id="password" type="password" name="password"
                                                   value="{{ $email ?? old('email') }}"
                                                   class="form-control @error('password') is-invalid @enderror">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <i class="fa fa-key" aria-hidden="true"></i>
                                            <input id="password-confirm" type="password" name="password_confirmation"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="form-control">Continue</button>
                                    <a href="{{ route('login') }}" class="login back"><i
                                            class="fa fa-long-arrow-left"></i>back to login</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xl-3"></div>
            </div>
        </div>
    </section>
@endsection
