@extends('website.layouts.app', ['Title' => 'Register'])
@section('content')
    <section class="inner-pages-pd">
        <div class="container">
            <h3 class="page-heading text-center popsb text-capitalize mt-3">Don't have an account? Create New</h3>
            <p class="page-subheading black-text text-center mt-1">Please make sure the information you enter is the
                same as it on your passport.</p>
            <form action="{{ route('register') }}" class="register-form" method="post">
                @csrf
                <div class="row ml-0 mr-0">
                    <div class="col-md-4 col-12">
                        <label for="title" class="site-label">Select Title <span class="required">*</span> </label>
                        <div class="form-field @error('title') is-invalid @enderror">
                            <select class="form-control site-input icon" name="title" id="title">
                                <option value="">Select</option>
                                <option value="Mr." {{ old('title') == 'Mr.' ? 'selected' : '' }}>Mr.</option>
                                <option value="Mrs." {{ old('title') == 'Mrs.' ? 'selected' : '' }}>Mrs.</option>
                                <option value="Ms." {{ old('title') == 'Ms.' ? 'selected' : '' }}>Ms.</option>
                                <option value="Dr." {{ old('title') == 'Dr.' ? 'selected' : '' }}>Dr.</option>
                            </select>
                            <i class="fa fa-chevron-down input-right-icon" aria-hidden="true"></i>
                            @error('title')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row ml-0 mr-0">
                    <div class="col-md-4 col-12">
                        <div class="form-field">
                            <label for="first_name" class="site-label">First Name <span class="required">*</span>
                            </label>
                            <input type="text" class="form-control site-input @error('first_name') is-invalid @enderror"
                                   placeholder="Enter First Name" name="first_name" id="first_name"
                                   value="{{ old('first_name') }}">
                            @error('first_name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="form-field">
                            <label for="middle_name" class="site-label">Middle Name </label>
                            <input type="text"
                                   class="form-control site-input @error('middle_name') is-invalid @enderror"
                                   placeholder="Enter Middle Name" name="middle_name" id="middle_name"
                                   value="{{ old('middle_name') }}">
                            @error('middle_name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="form-field">
                            <label for="last_name" class="site-label">Last Name <span class="required">*</span> </label>
                            <input type="text" class="form-control site-input @error('last_name') is-invalid @enderror"
                                   placeholder="Enter Last Name" name="last_name" id="last_name"
                                   value="{{ old('last_name') }}">
                            @error('last_name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row ml-0 mr-0 align-items-end">
                    <div class="col-lg-5 col-md-4 col-12">
                        <div class="form-field">
                            <label for="email" class="site-label">Email <span class="required">*</span> </label>
                            <input type="email" class="form-control site-input @error('email') is-invalid @enderror"
                                   placeholder="Enter Your Email" name="email" id="email" value="{{ old('email') }}">
                            @error('email')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-12">
                        <label for="phone_code" class="site-label">Phone # <span class="required">*</span> </label>
                        <div class="form-field">
                            <select class="form-control site-input icon @error('phone_code') is-invalid @enderror"
                                    name="phone_code" id="phone_code">
                                <option value="">Select</option>
                                @foreach(\App\Country::all() as $country)
                                    <option
                                        value="{{ '+'.$country->phonecode }}" {{ old('phone_code') == '+'.$country->phonecode ? 'selected' : '' }}>{{ $country->code.' (+'.$country->phonecode.')' }}</option>
                                @endforeach
                            </select>
                            <i class="fa fa-chevron-down input-right-icon" aria-hidden="true"></i>
                            @error('phone_code')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-4 col-12">
                        <div class="form-field">
                            <input type="text"
                                   class="form-control site-input cm-input-mask-phone-us @error('phone') is-invalid @enderror"
                                   placeholder="Enter Your Phone Number" name="phone" id="" value="{{ old('phone') }}">
                            @error('phone')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row ml-0 mr-0 align-items-end">
                    <div class="col-md-4 col-12">
                        <div class="form-field">
                            <label for="dob" class="site-label">Date of Birth <span class="required">*</span> </label>
                            <div class="input-group date form">
                                <input type="text"
                                       class="form-control date_1 datepicker site-input icon @error('dob') is-invalid @enderror"
                                       id="dob" name="dob" placeholder="DD" autocomplete="off" readonly value="{{ old('dob') }}">
                                @error('dob')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ml-0 mr-0">
                    <div class="col-md-6 col-12">
                        <div class="form-field">
                            <label for="password" class="site-label">Enter password<span
                                    class="required">*</span></label>
                            <input type="password" class="form-control site-input @error('password') is-invalid @enderror"
                                   placeholder="Minimum of 4 digits" name="password" id="password">
                            @error('password')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="form-field">
                            <label for="" class="site-label">Re-type password<span class="required">*</span></label>
                            <input type="password" class="form-control site-input" placeholder="Minimum of 4 digits"
                                   name="password_confirmation" id="">
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="site-btn blue extra-pd mt-3">Register</button>
                    <p class="site-text black-text text-center mt-4">Already registered? <a href="{{ route('login') }}"
                                                                                            class="forgot-link">Back to
                            Login</a></p>
                </div>
            </form>
        </div>
    </section>
@endsection
