@extends('layouts.app', ['Title' => 'Booked Flights'])

@section('content')
    <div class="app-content content view user">
        <div class="content-wrapper">
            <div class="content-body">
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Booked Flights</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2 filter-main justify-content-between">
                                            <div class="col-12">
                                                <label>Sort By:</label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6  form-group">
                                                        <label for="">From:</label>
                                                        <input id="fromSelector" type="text" readonly="">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6  form-group">
                                                        <label for="">To:</label>
                                                        <input id="toSelector" type="text" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 d-flex align-items-center">
                                                <div class="sort-box">
                                                    <ul>
                                                        <li>
                                                            <label class="radio-container">Booked On
                                                                <input class="cm-radio-filter" type="radio"
                                                                       checked="checked"
                                                                       name="radio" value="1">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="radio-container">Departure Date
                                                                <input class="cm-radio-filter" type="radio"
                                                                       name="radio" value="5">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row maain-tabble mt-3">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>Booked On</th>
                                                    <th>User</th>
                                                    <th>Booking ID</th>
                                                    <th>Amount Paid (After Discount)</th>
                                                    <th>Departure Date</th>
                                                    <th>Flight#</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($BookedFlights as $key => $value)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ $value->created_at }}</td>
                                                        <td>
                                                            <a href="{{ route('user.show',['id' => $value->user_id]) }}"><u>{{ $value->user->profile->first_name }}</u></a>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('booked-flight.show',['id' => $value->id]) }}"><u>{{ $value->id}}</u></a>
                                                        </td>
                                                        <td>{{ $value->payment->fares }}</td>
                                                        <td>{{ $value->departure_datetime }}</td>
                                                        <td>{{ $value->flight_number }}</td>
                                                        <td>
                                                            <div class="btn-group mr-1 mb-1">
                                                                <button type="button" class="btn  btn-drop-table btn-sm"
                                                                        data-toggle="dropdown" aria-haspopup="true"
                                                                        aria-expanded="false"><i
                                                                        class="fa fa-ellipsis-v"></i></button>
                                                                <div class="dropdown-menu" x-placement="bottom-start"
                                                                     style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                    <a class="dropdown-item"
                                                                       href="{{ route('booked-flight.show', ['id' => $value->id]) }}"><i
                                                                            class="fa fa-eye"></i>View Details</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
