@extends('layouts.app', ['Title' => 'Booked Flight Details'])

@section('content')
    <div class="app-content content view user flight-details">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Flight Details</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-12">
                                                <div class="tkt-ban-top">
                                                    <label class="green-label">Status
                                                        :<span> {{ @$BookedFlight->flight_details['bookingStatus'] }} </span></label>
                                                    <label>Booking ref
                                                        :<span> {{ @$BookedFlight->flight_details['bookingId'] }}</span></label>
                                                </div>
                                                <div class="tkt-banner-main">
                                                    <div class="tkt-banner">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="tkt-box">

                                                                    @php
                                                                        $airportDepartureTop = getAirportDetails($BookedFlight->flight_details['originDepartureLocationCode']);
                                                                    @endphp

                                                                    <div class="tkt-txt">
                                                                        <h3>{{ @$airportDepartureTop->city }}</h3>
                                                                        <h5>{{ @$airportDepartureTop->iata.', '.@$airportDepartureTop->country }}</h5>
                                                                        <h3>{{ @date('H:i A',strtotime($BookedFlight->flight_details['originDepartureDateTime'])) }}</h3>
                                                                        <h5>{{ @date('M d, Y',strtotime($BookedFlight->flight_details['originDepartureDateTime'])) }}</h5>
                                                                    </div>
                                                                    <div class="tkt-img-box text-center">
                                                                        <img
                                                                            src="{{ asset('custom/images/flight-stop.png') }}"
                                                                            alt="">
                                                                        <h5>{{ @$BookedFlight->flight_details['totalStop']}}
                                                                            stop(s)</h5>
                                                                    </div>

                                                                    @php
                                                                        $airportArrivalTop = getAirportDetails($BookedFlight->flight_details['originArrivalLocationCode']);
                                                                    @endphp

                                                                    <div class="tkt-txt">
                                                                        <h3>{{ @$airportArrivalTop->city }}</h3>
                                                                        <h5>{{ @$airportArrivalTop->iata.', '.@$airportArrivalTop->country }}</h5>

                                                                        <h3>{{ @date('H:i A',strtotime($BookedFlight->flight_details['originArrivalDateTime'])) }}</h3>
                                                                        <h5>{{ @date('M d, Y',strtotime($BookedFlight->flight_details['originArrivalDateTime'])) }}</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tkt-bottom">
                                                        <div class="tkt-box">
                                                            <div class="media align-items-center">
                                                                <img src="{{ asset('custom/images/flight-icon.png') }}"
                                                                     class="mr-2">
                                                                <h5>{{ @$BookedFlight->flight_details['originDepartureAirlineCode'].' '.$BookedFlight->flight_details['originDepartureFlightNo']}}</h5>
                                                            </div>
                                                            <h5>{{ $BookedFlight->flight_details['currencyCode'].' '.$BookedFlight->flight_details['totalFlightAmount'] }}</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pass-details">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h1 class="pull-left">Passenger Details</h1>
                                                </div>
                                                <div class="col-12">
                                                    <div class="row">
                                                        @foreach($BookedFlight->flight_details['airTravelers'] as $info)
                                                            <div class="col-lg-4 col-md-6 col-12">
                                                                <div class="pass-box">
                                                                    <div class="pass-top">
                                                                        <h4>{{ $info['name'] }}</h4>
                                                                    </div>
                                                                    <ul>
                                                                        <li>Traveller Id: {{ $info['travelerId'] }}</li>
                                                                        <li>Ticket Document# {{ $info['ticketDocumentNo'] }}</li>
                                                                        <li>Type: {{ $info['ticketDocumentType'] }}</li>
                                                                        <li>Date Of Issue: {{ $info['ticketDocumentDateOfIssue'] }}</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pass-details">
                                            <div class="row seats-details">
                                                <div class="col-12">
                                                    <h1 class="pull-left">Flight Details</h1>
                                                </div>
                                            </div>

                                                @foreach($BookedFlight->flight_details['segments'] as $info)
                                                    <div class="flight-det-top">
                                                        <div class="media align-items-center">
                                                            <img src="{{ asset('custom/images/flight-icon.png') }}"
                                                                 class="mr-2">
                                                            <h5>{{ @getAirlineName($info['operatingAirlineCode'])->name.' '.@$info['operatingAirlineCode'].' '.@$info['flightNo']}}</h5>
                                                        </div>
                                                    </div>
                                                    <div class="flight-time mt-2">
                                                        <div class="row">
                                                            <div class="col-md-3 col-12">
                                                                <div class="flight-text">
                                                                    @php
                                                                        $airportDeparture = getAirportDetails($info['departureAirportLocationCode']);
                                                                    @endphp
                                                                    <h3>{{ @$airportDeparture->city }}
                                                                        <br><span>{{ @$airportDeparture->iata.', '.@$airportDeparture->country }}</span>
                                                                    </h3>
                                                                    <h4>{{ @date('H:i A',strtotime($info['departureDateTime'])) }}
                                                                        <br><span>{{ @date('M d, Y',strtotime($info['departureDateTime'])) }}</span>
                                                                    </h4>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12 text-center">
                                                                <h3>{{ $info['duration'] }}</h3>
                                                                <img src="{{ asset('custom/images/baggage-bg.png') }}"
                                                                     alt="">
                                                            </div>
                                                            <div class="col-md-3 col-12 text-right">
                                                                <div class="flight-text">
                                                                    @php
                                                                        $airportArrival = getAirportDetails($info['arrivalAirportLocationCode']);
                                                                    @endphp
                                                                    <h3>{{ @$airportArrival->city }}
                                                                        <br><span>{{ @$airportArrival->iata.', '.@$airportArrival->country }}</span>
                                                                    </h3>
                                                                    <h4>{{ @date('H:i A',strtotime($info['arrivalDateTime'])) }}
                                                                        <br><span>{{ @date('M d, Y',strtotime($info['arrivalDateTime'])) }}</span>
                                                                    </h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach

                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <h1 class="pull-left mb-2">Total Booking Amount</h1>
                                                </div>
                                                <div class="col-12">
                                                    <div class="total-main">
                                                        <div class="total-inner">
                                                            <div class="tot-amnt-title">
                                                                <h4>Details</h4>
                                                                <h4>Sub Totals</h4>
                                                            </div>
                                                                @foreach($BookedFlight->flight_details['amountDetails'] as $info)
                                                                    <div class="total-det-box">
                                                                        <div class="det-left">
                                                                            <h5>{{ @$info['qty'].' '.$info['type'] }}</h5>
                                                                            <p>{{ @$info['currencyCode'].' '.$info['baseFareAmount'] }} (Ticket Price) + {{ @$info['currencyCode'].' '.$info['totalTaxAmount'] }} (Taxes & Other Fee)</p>
                                                                        </div>
                                                                        <div class="det-right">
                                                                            <h6>{{ @$info['currencyCode'].' '.$info['totalFareAmount'] }}</h6>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            <div class="total-det-box">
                                                                <div class="det-left">
                                                                    <h5>Discount</h5>
                                                                </div>
                                                                <div class="det-right">
                                                                    <h6>{{ $BookedFlight->flight_details['currencyCode'].' '.$BookedFlight->flight_details['discount'] }}</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tot-bottom">
                                                            <div class="det-left">
                                                                <h5>Total</h5>
                                                            </div>
                                                            <div class="det-right">
                                                                <h5>{{ $BookedFlight->flight_details['currencyCode'].' '.$BookedFlight->flight_details['totalFlightAmount'] }}</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
