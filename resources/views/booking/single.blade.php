@extends('layout')
@section('title','CMV - View Booking Details')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-1">
                    <h3 class="content-header-title">View Booking Details</h3>
                </div>
                <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('booking.view')}}">View Booking</a>
                            </li>
                            <li class="breadcrumb-item active">View Booking Details
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <section class="card">
                <div class="content-body"><!-- Table row borders end-->
                    <div class="row mt-2">
                        <div class="col-xl-12 col-md-12 col-12">
                            <div class="card profile-card-with-cover">
                                <div class="card-content">
                                    <div class="profile-card-with-cover-content text-center">
                                        <div class="profile-details mt-2">
                                            <h4 class="card-title"><small>Trip ID :</small> {{$booking->trip_id}}</h4>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="" ><small>Booking ID</small></label>
                                                    <p>{{$booking->booking_id}}</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="" ><small>Date of Issue</small></label>
                                                    <p>{{date('F m Y', strtotime($booking->date_of_issue))}}</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="" ><small>Fare</small></label>
                                                    <p>{{number_format($booking->fare,2,'.',',')}}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="" ><small>Currency Code</small></label>
                                                    <p>{{$booking->currency_code}}</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="" ><small>User name</small></label>
                                                    <p>{{$booking->user['profile']['first_name']}}</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="" ><small>User email</small></label>
                                                    <p>{{$booking->user['email']}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        @php($details = unserialize($booking->detail)['AirReservation'])
                                        @foreach($details['OriginDestinationOptions']['OriginDestinationOption'] as $detail)
                                            @if(isset($detail['FlightSegment']['@attributes']))
                                                <div class="row" style="border: 1px solid lightgrey;">
                                                    <div class="col-md-3">
                                                        <p>Airline : <br>{{$detail['FlightSegment']['OperatingAirline']['@attributes']['Code']}}</p>
                                                    </div>
                                                    <div class="col-md-9 text-left">
                                                        <div class="row">
                                                            <div class="col-md-6"><p>Departure : <br>{{date('F d Y h:i a',strtotime($detail['FlightSegment']['@attributes']['DepartureDateTime']))}}</p></div>
                                                            <div class="col-md-6"><p>Arrival : <br>{{date('F d Y h:i a',strtotime($detail['FlightSegment']['@attributes']['ArrivalDateTime']))}}</p></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"><p>From : <br>{{$detail['FlightSegment']['DepartureAirport']['@attributes']['LocationCode']}}</p></div>
                                                            <div class="col-md-6"><p>To : <br>{{$detail['FlightSegment']['ArrivalAirport']['@attributes']['LocationCode']}}</p></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"><p>Duration : <br>{{$detail['FlightSegment']['@attributes']['Duration']}}</p></div>
                                                            <div class="col-md-6"><p>Stop Quantity : <br>{{$detail['FlightSegment']['@attributes']['StopQuantity']}}</p></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"><p>Flight Number : <br>{{$detail['FlightSegment']['@attributes']['FlightNumber']}}</p></div>
                                                            <div class="col-md-6"><p>Departure Terminal : <br>{{$detail['FlightSegment']['@attributes']['DepartureTerminal']}}</p></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"><p>Arrival Terminal : <br>{{$detail['FlightSegment']['@attributes']['ArrivalTerminal']}}</p></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                @if(isset($detail['FlightSegment']))
                                                    @foreach($detail['FlightSegment'] as $segment)
                                                        <pre>{{print_r($segment)}}</pre>
                                                        @php(exit)
                                                        <div class="row" style="border: 1px solid lightgrey;">
                                                            <div class="col-md-3">
                                                                <p>Airline : <br>{{$segment['OperatingAirline']['@attributes']['Code']}}</p>
                                                            </div>
                                                            <div class="col-md-9 text-left">
                                                                <div class="row">
                                                                    <div class="col-md-6"><p>Departure : <br>{{date('F d Y h:i a',strtotime($segment['@attributes']['DepartureDateTime']))}}</p></div>
                                                                    <div class="col-md-6"><p>Arrival : <br>{{date('F d Y h:i a',strtotime($segment['@attributes']['ArrivalDateTime']))}}</p></div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6"><p>From : <br>{{$segment['DepartureAirport']['@attributes']['LocationCode']}}</p></div>
                                                                    <div class="col-md-6"><p>To : <br>{{$segment['ArrivalAirport']['@attributes']['LocationCode']}}</p></div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6"><p>Duration : <br>{{$segment['@attributes']['Duration']}}</p></div>
                                                                    <div class="col-md-6"><p>Stop Quantity : <br>{{$segment['@attributes']['StopQuantity']}}</p></div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6"><p>Flight Number : <br>{{$segment['@attributes']['FlightNumber']}}</p></div>
                                                                    <div class="col-md-6"><p>Departure Terminal : <br>{{$segment['@attributes']['DepartureTerminal']}}</p></div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6"><p>Arrival Terminal : <br>{{$segment['@attributes']['ArrivalTerminal']}}</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <div class="row" style="border: 1px solid lightgrey;">
                                                        <div class="col-md-3">
                                                            <p>Airline : <br>{{$detail['OperatingAirline']['@attributes']['Code']}}</p>
                                                        </div>
                                                        <div class="col-md-9 text-left">
                                                            <div class="row">
                                                                <div class="col-md-6"><p>Departure : <br>{{date('F d Y h:i a',strtotime($detail['@attributes']['DepartureDateTime']))}}</p></div>
                                                                <div class="col-md-6"><p>Arrival : <br>{{date('F d Y h:i a',strtotime($detail['@attributes']['ArrivalDateTime']))}}</p></div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6"><p>From : <br>{{$detail['DepartureAirport']['@attributes']['LocationCode']}}</p></div>
                                                                <div class="col-md-6"><p>To : <br>{{$detail['ArrivalAirport']['@attributes']['LocationCode']}}</p></div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6"><p>Duration : <br>{{$detail['@attributes']['Duration']}}</p></div>
                                                                <div class="col-md-6"><p>Stop Quantity : <br>{{$detail['@attributes']['StopQuantity']}}</p></div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6"><p>Flight Number : <br>{{$detail['@attributes']['FlightNumber']}}</p></div>
                                                                <div class="col-md-6"><p>Departure Terminal : <br>{{$detail['@attributes']['DepartureTerminal']}}</p></div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6"><p>Arrival Terminal : <br>{{$detail['@attributes']['ArrivalTerminal']}}</p></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                        <div class="row" style="border: 1px solid lightgrey;">
                                            @foreach($details['TravelerInfo'] as $traveller)
                                                <div class="col-md-4 text-left">
                                                    <div class="row">
                                                        <div class="col-md-6"><p>Traveller ID : <br>{{$traveller['PersonName']['TravelerId']}}</p></div>
                                                        <div class="col-md-6"><p>Name : <br>{{$traveller['PersonName']['NamePrefix']." . ".$traveller['PersonName']['GivenName']." ".$traveller['PersonName']['Surname']}}</p></div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection
