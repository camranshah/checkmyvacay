@extends('layout')
@section('title','CMV - View Bookings')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-1">
                    <h3 class="content-header-title">View Bookings</h3>
                </div>
                <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">View Bookings
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <section class="card">
                <div class="content-body"><!-- Table row borders end-->
                    <!-- Double border end-->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <thead>
                                            <tr class="border-double">
                                                <th>Trip ID</th>
                                                <th>Booking ID</th>
                                                <th>Fare</th>
                                                <th>User name</th>
                                                <th>Payment Status</th>
                                                <th>Created date & time</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($bookings as $booking)
                                                <tr>
                                                    <td>{{$booking->trip_id}}</td>
                                                    <td>{{$booking->booking_id}}</td>
                                                    <td>{{number_format($booking->fare,2,'.',',')}}</td>
                                                    <td>{{$booking->user['profile']['first_name']}}</td>
                                                    <td><span class="btn btn-{{$booking->status == '1' ? "success" : "danger"}}">{{$booking->status == '1' ? "Paid" : "Unpaid"}}</span></td>
                                                    <td>{{date('F d Y h:i a',strtotime($booking->created_at))}}</td>
                                                    <td>
                                                        <a href="{{route('booking.view',$booking->id)}}" class="btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="View Detail"><i class="fa fa-eye"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Double border end -->
                </div>
            </section>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection
