@extends('layouts.app', ['Title' => 'Broadcast Message'])

@section('script')
    <script>
        $.fn.select2.amd.require([
            'select2/utils',
            'select2/dropdown',
            'select2/dropdown/attachBody'
        ], function (Utils, Dropdown, AttachBody) {
            function SelectAll() {
            }

            SelectAll.prototype.render = function (decorated) {
                var $rendered = decorated.call(this);
                var self = this;

                var $selectAll = $(
                    '<button type="button">Select All</button>'
                );

                $rendered.find('.select2-dropdown').prepend($selectAll);

                $selectAll.on('click', function (e) {
                    var $results = $rendered.find('.select2-results__option[aria-selected=false]');

                    // Get all results that aren't selected
                    $results.each(function () {
                        var $result = $(this);

                        // Get the data object for it
                        var data = $result.data('data');

                        // Trigger the select event
                        self.trigger('select', {
                            data: data
                        });
                    });

                    self.trigger('close');
                });

                return $rendered;
            };

            $("#cm-select-users").select2({
                placeholder: "Select Users...",
                dropdownAdapter: Utils.Decorate(
                    Utils.Decorate(
                        Dropdown,
                        AttachBody
                    ),
                    SelectAll
                ),
            });
        });
    </script>
@endsection

@section('content')
    <div class="app-content content view user profile edit-profile create-banner">
        <div class="content-wrapper">
            <div class="content-body">
                @include('includes.flash-messages')
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Broadcast Message</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-12">
                                                <form action="{{ route('broadcast-message.store') }}" method="post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <select class="form-control"
                                                                    name="users[]" id="cm-select-users" multiple="multiple" required>
                                                                @foreach($Users as $user)
                                                                    <option
                                                                        value="{{ $user->id }}">{{ $user->profile->first_name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <label for="">Message</label>
                                                            <textarea class="form-control" name="message"
                                                                      placeholder="Your Message Here..."
                                                                      required></textarea>
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-4 col-12 form-group">
                                                            <button type="submit">Submit</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
