@extends('layout')
@section('title','CMV - View Commission Detail')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-1">
                    <h3 class="content-header-title">View Commission Detail</h3>
                </div>
                <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">View Commission Detail
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <section class="card">
                <div class="content-body"><!-- Table row borders end-->
                    <!-- Double border end-->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        @if(Session::has('type'))
                                            <div class="alert alert-{{Session::get('type')}}">
                                                <p>{{Session::get('msg')}}</p>
                                            </div>
                                        @endif
                                        {!! Form::open(['route' => 'commission','class' => 'form','method' => 'post']) !!}
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="form-group col-md-12 mb-2">
                                                    <label for="userinput1">Percentage</label>
                                                    {!! Form::input('number','percentage',$currentCommission,['class' => 'form-control form-control-xl input-xl','step' => '0.1','min' => '1', 'max' => '100']) !!}
                                                    @if($errors->create_commission->first('percentage'))
                                                        <p class="badge badge-danger" style="margin-top: 10px;">{{ $errors->create_commission->first('percentage') }}</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button type="button" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-check-square-o"></i> Save
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <thead>
                                            <tr class="border-double">
                                                <th>S. No #</th>
                                                <th>Percentage</th>
                                                <th>Status</th>
                                                <th>Valid Till</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php($i = 1)
                                            @foreach($commissions as $commission)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{number_format($commission->percentage,2,'.',',')}} %</td>
                                                    <td><span class="btn btn-{{$commission->status == 0? "danger" : "success"}}">{{$commission->status == 0? "Expired" : "Active"}}</span></td>
                                                    <td>{{$commission->status == 0? date('F d Y h:i:s a',strtotime($commission->updated_at)) : "-"}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Double border end -->
                </div>
            </section>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection
