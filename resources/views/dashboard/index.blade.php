@extends('layouts.app', ['Title' => 'Dashboard'])

@section('script')
    <script>
        $(document).ready(function () {

            $(document).on('click', '.cm-btn-stat',function(){
                $.ajax({
                    url: "{{ route('dashboard.index') }}",
                    method: "GET",
                    data: {
                        "from_date": $('.cm-stat-from-date').val(),
                        "to_date": $('.cm-stat-to-date').val()
                    },
                    success: function (response) {
                        $('#cm-total-users').text(response.TotalUsers)
                        $('#cm-total-bookings').text(response.TotalBookings)
                    }
                });
            })

            $('.cm-stat-to-date').datepicker({
                uiLibrary: 'bootstrap4',
            });

            $('.cm-stat-from-date').datepicker({
                uiLibrary: 'bootstrap4',
            });

            $.ajax({
                url: "{{ url('booking-stats') }}",
                method: "GET",
                success: function (response) {
                    new Morris.Bar({
                        element: 'cm-booking-stats-chart',
                        data: response,
                        xkey: "month",
                        ykeys: ["count"],
                        labels: ['Bookings']
                    });
                }
            });

            $(document).on('change', '#cm-change-chart-year', function () {
                let year = $(this).val()
                $("#cm-booking-stats-chart").empty();
                $.LoadingOverlay("show");
                $.ajax({
                    url: "{{ url('booking-stats') }}" + "?year=" + year,
                    method: "GET",
                    success: function (response) {
                        $.LoadingOverlay("hide");
                        new Morris.Bar({
                            element: 'cm-booking-stats-chart',
                            data: response,
                            xkey: "month",
                            ykeys: ["count"],
                            labels: ['Bookings']
                        });
                    }
                });
            })

        })


    </script>
@endsection

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <section id="combination-charts">
                    <div class="row">
                        <div class="col-12">
                            <div class="admin-overview-main dashboard">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 d-flex">
                                        <div class="card card-box card-box1 w-100">
                                            <div class="card-content">
                                                <div class="card-body cleartfix">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" name="from_date"
                                                                    class="form-control cm-stat-from-date"
                                                                    placeholder="From Date" readonly/>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" name="to_date"
                                                                   class="form-control cm-stat-to-date"
                                                                   placeholder="To Date" readonly/>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="button"
                                                                   class="btn btn-primary cm-btn-stat" value="Proceed"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 d-flex">
                                        <div class="card card-box card-box1 w-100">
                                            <div class="card-content">
                                                <div class="card-body cleartfix">
                                                    <h2>Total Flights Booked</h2>
                                                    <div class="media justify-content-between align-items-center">
                                                        <img src="{{ asset('custom/images/card-1-img.png') }}">
                                                        <h3 id="cm-total-bookings">{{ @$TotalBookings }}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 d-flex">
                                        <div class="card card-box card-box2 w-100">
                                            <div class="card-content">
                                                <div class="card-body cleartfix">
                                                    <h2>Total Users</h2>
                                                    <div class="media justify-content-between align-items-center">
                                                        <img src="{{ asset('custom/images/card-2-img.png') }}">
                                                        <h3 id="cm-total-users">{{ @$TotalUsers }}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--col chart end-->
                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                        <div class="card pad-20">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="row justify-content-between">
                                                        <div class="col-md-9 col-12">
                                                            <h2>Flights Booked Per Month</h2>
                                                        </div>
                                                        <div class="col-md-3 col-12">
                                                            <label>Selected Year:</label>
                                                            <select id="cm-change-chart-year">
                                                                <option value="{{ date("Y") }}">{{ date("Y") }}</option>
                                                                <option
                                                                    value="{{ date("Y",strtotime("-1 year")) }}">{{ date("Y",strtotime("-1 year")) }}</option>
                                                                <option
                                                                    value="{{ date("Y",strtotime("-2 year")) }}">{{ date("Y",strtotime("-2 year")) }}</option>
                                                                <option
                                                                    value="{{ date("Y",strtotime("-3 year")) }}">{{ date("Y",strtotime("-3 year")) }}</option>
                                                                <option
                                                                    value="{{ date("Y",strtotime("-4 year")) }}">{{ date("Y",strtotime("-4 year")) }}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="media home-chart align-items-center mt-2">
                                                        <h5 class="text-center">Flights</h5>
                                                        <div class="media-body">
                                                            <div id="cm-booking-stats-chart"></div>
                                                            <h5 class="text-center month-text">Months</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                        <div class="card pad-20">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h2 class="m-0">Activity Log</h2>
                                                        </div>
                                                    </div>
                                                    <div class="row maain-tabble">
                                                        <table
                                                            class="table table-striped table-bordered zero-configuration m-0">
                                                            <thead>
                                                            <tr>
                                                                <th>Activity</th>
                                                                <th>Date</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($ActivityLog as $log)
                                                                <tr>
                                                                    <td>{{ $log->title }}</td>
                                                                    <td>{{ $log->created_at }}</td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
