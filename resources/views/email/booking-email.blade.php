<html>
<head>
    <title>ITINERARY | TAX INVOICE</title>
</head>
<body>
<table align="center" border="0" cellpadding="1" cellspacing="1" style="height:10px;width:900px;">
    <thead>
    </thead>
    <tbody>
    <tr>
        <td colspan="4" style="background-color: rgb(0, 0, 255);">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" rowspan="1">
        	<span style="font-family:arial;">
        		<span style="font-size:20px;">
        			<strong>ITINERARY | TAX INVOICE</strong>
        		</span>
        	</span>
        </td>
        <td colspan="2">
        	<span style="font-family:arial;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt="CMFx logo" src="https://www.checkmyvacay.com/cmf/Images/CMFx_Logo_Sml.png" style="float: right; width: 100px; height: 51px;"/> &nbsp; &nbsp;
        	</span>
        </td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="text-align: center; vertical-align: middle; width: 5%; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221); height: 30px;"><span style="font-family:arial;"><span style="font-size:16px;"><b><img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/passenger.png" style="width: 15px; height: 15px;"/></b></span></span></td>
        <td style="width: 45%; vertical-align: middle; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221); height: 30px;"><span style="font-family:arial;"><span style="font-size:16px;"><b>&nbsp;PASSENGERS</b></span></span></td>
        <td style="width: 50%; vertical-align: middle; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221); height: 30px; text-align: right;">
            <span style="font-family:arial;">
                <strong>
                    <span style="font-size:16px;">BOOKING REF:&nbsp; &nbsp;</span>
                </strong>
                <span style="font-size:16px;">
                    <strong>
                        <span style="color:#0000FF;">{{ $data->booking->booking_id }}&nbsp;</span>
                    </strong>
                </span>
            </span>
        </td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="height:10px;width:900px;">
    <thead>
    </thead>
    <tbody>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td style="width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="height:10px;width:900px;">
    <tbody>
    <tr>
        <td colspan="3" rowspan="1" style="vertical-align: middle; width: 70%;">
            @foreach($data->booking_response['traveller_details'] as $traveller)
            <p>
                <span style="font-family:arial;">
                    <span style="font-size:14px;">&nbsp;{{ $traveller['prefix'] }} {{ $traveller['name'] }} {{ $traveller['sur_name'] }} |&nbsp;</span>
                    <span style="font-size:12px;">Frequent&nbsp;Flyer: 1234567890</span>
                </span>
            </p>
            @endforeach
        </td>
        <td style="text-align: right; vertical-align: middle; width: 30%;">

            @php($bookingDate = new DateTime($data['date_of_issue']))
            <p>
                <span style="font-family:arial;">
                    <span style="font-size:14px;">BOOKING DATE:&nbsp; &nbsp;&nbsp;<strong>{{ $bookingDate->format('d M Y') }}&nbsp;</strong></span>
                </span>
            </p>
            <p>
                <span style="font-family:arial;">
                    <span style="font-size:14px;">ADMIN REF:&nbsp; &nbsp;<strong>{{ $data['booking_id'] }}&nbsp;</strong></span>
                </span>
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="vertical-align: middle; width: 70%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: middle; width: 30%;">&nbsp;</td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="text-align: center; vertical-align: middle; width: 5%; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221);">
            <span style="font-family:arial;">
                <span style="font-size:16px;"><b>
                        <img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/flight.png" style="width: 26px; height: 18px;"/>
                    </b>
                </span>
            </span>
        </td>
        <td style="width: 45%; vertical-align: middle; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221); height: 30px;">
            <span style="font-family:arial;">
                <span style="font-size:16px;">
                    <strong>&nbsp;{{ $data->booking_response['flights'][0]['departure_location_code'] }}&nbsp; &gt;&nbsp; {{ $data->booking_response['flights'][0]['arrival_location_code'] }}&nbsp; 


					@php($departure = \Carbon\Carbon::parse($data->booking_response['flights'][0]['departure_date_time']))
                            
                    |&nbsp; {{ $departure->format('l d F Y') }} </strong>
                </span>
            </span>
        </td>
        <td style="width: 50%; vertical-align: middle; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221); text-align: right;">
            <span style="font-family:arial;">
                <span style="font-size:16px;">
                    <strong>AIRLINE REF:&nbsp; &nbsp;</strong>
                </span>
                <span style="font-size:16px;">
                    <strong>
                        <span style="color:#0000FF;">{{ 'TODO' }}&nbsp;</span>
                    </strong>
                </span>
            </span>
        </td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width: 900px;">
    <tbody>
    <tr>
        <td style="width: 33%; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td colspan="2" style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 33%; border-color: rgb(204, 204, 204);">
            <span style="font-size:16px;">
                <strong><span style="font-family:arial;">&nbsp;{{ $data->booking_response['flights'][0]['operating_airline_name']['name'] }}</span></strong>
            </span>
        </td>
        <td style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">Departing </span>
            </span>
            <span style="color:#0000FF;">

                <strong>
                    <span style="font-size:18px;">
                        <span style="font-family:arial;">
                            {{ $departure->format('H:i') }}
                        </span>
                    </span>
                </strong>
            </span>
        </td>
        <td colspan="2" style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">Arriving</span>
            </span>
            <span style="color:#0000FF;">
                <span style="font-size:14px;">
                    <span style="font-family:arial;"> </span>
                </span>
                <strong>
                    <span style="font-size:18px;">
                        <span style="font-family:arial;">
                            @php($arrival = \Carbon\Carbon::parse($data->booking_response['flights'][0]['arrival_date_time']))
                            {{ $arrival->format('H:i') }}&nbsp;
                        </span>
                    </span>
                </strong>
            </span>
        </td>
    </tr>
    <tr>
        <td style="width: 33%; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">&nbsp;QF123</span>
            </span>
        </td>
        <td style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">{{ $departure->format('l d F Y') }}</span>
            </span>
        </td>
        <td colspan="2" style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">{{ $arrival->format('l d F Y') }}&nbsp;</span>
            </span>
        </td>
    </tr>
    <tr>
        <td style="width: 33%; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 33%; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td colspan="2" style="width: 33%; border-color: rgb(204, 204, 204);">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 33%; border-color: rgb(204, 204, 204);">
            <span style="font-family:arial;">
                <span style="font-size:16px;">
                    <b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/duration.png" style="width: 13px; height: 13px;"/>&nbsp;</b>
                </span>
            </span>
            <span style="font-size:14px;">
                <span style="font-family:arial;">&nbsp;</span>
            </span>
            <span style="font-size:12px;">
                <span style="font-family:arial;">{{ minutes_to_minutes_and_hours($data->booking_response['flights'][0]['duration']) }}</span>
            </span>
        </td>
        <td style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:16px;"><font face="arial"><b>{{ $data->booking_response['flights'][0]['airport_departure']['iata'] }} | {{ $data->booking_response['flights'][0]['airport_departure']['city'] }}</b></font></span>
        </td>
        <td colspan="2" style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:16px;">
                <span style="font-family:arial;"><strong>{{ $data->booking_response['flights'][0]['airport_arrival']['iata'] }} | {{ $data->booking_response['flights'][0]['airport_arrival']['city'] }}</strong></span>
            </span>
            <span style="font-size:14px;">
                <span style="font-family:arial;"><strong>&nbsp;</strong></span>
            </span>
        </td>
    </tr>
    <tr>
        <td style="width: 33%; border-color: rgb(204, 204, 204);">
            <span style="font-family:arial;">
                <span style="font-size:16px;">
                    <b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/aircraft.png" style="width: 13px; height: 14px;"/>&nbsp;&nbsp;</b>
                </span>
                <span style="font-size:12px;">Boeing 777-800</span>
            </span>
        </td>
        <td style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">{{ $data->booking_response['flights'][0]['airport_departure']['city'] }} International Airport</span>
            </span>
        </td>
        <td colspan="2" style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">{{ $data->booking_response['flights'][0]['airport_arrival']['city'] }} International Airport&nbsp;</span>
            </span>
        </td>
    </tr>
    <tr>
        <td style="width: 33%; border-color: rgb(204, 204, 204);">
            <span style="font-family:arial;">
                <span style="font-size:16px;">
                    <b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/cabin.png" style="width: 13px; height: 13px;"/>&nbsp;&nbsp;</b>
                </span>
                <span style="font-size:12px;">{{ ucwords($data->search_params['cabin']) }}</span>
            </span>
        </td>
        <td style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">Departure Terminal: {{ $data->provision_response['flights'][0]['departure_terminal'][0] }}</span>
            </span>
        </td>
        <td colspan="2" style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">Arrival Terminal: {{ $data->provision_response['flights'][0]['arrival_terminal'][0] }}&nbsp;</span>
            </span>
        </td>
    </tr>
    <tr>
        <td style="width: 33%; border-color: rgb(204, 204, 204);">
            <span style="font-family:arial;">
                <span style="font-size:16px;">
                    <b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/direct.png" style="width: 13px; height: 13px;"/>&nbsp;&nbsp;</b>
                </span>
                <span style="font-size:12px;">
                    <strong>{{ $data->provision_response['flights'][0]['stop_quantity'][0] === "0"? "Direct Flight": "Connecting Flight" }}</strong>
                </span>
            </span>
        </td>
        <td style="width: 33%; border-color: rgb(204, 204, 204); text-align: center;">&nbsp;</td>
        <td colspan="2" style="width: 33%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="width: 20%; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">
                    <b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/baggage.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>
                    Baggage&nbsp;&nbsp;
                </span>
            </span>
        </td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">
                    <b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/seat.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>
                    Seat&nbsp;&nbsp;
                </span>
            </span>
        </td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">
                    <b>&nbsp;&nbsp;</b>Meal&nbsp;&nbsp;<b>
                        <img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/meal.png" style="width: 15px; height: 15px; float: right;"/></b>
                </span>
            </span>
        </td>
        <td colspan="2" style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">
            <span style="font-size:14px;">
                <span style="font-family:arial;">
                    <b>&nbsp;</b>Assistance<b><img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/assistance.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>&nbsp;
                </span>
            </span>
        </td>
    </tr>
    <tr>
        <td style="width: 20%; border-color: rgb(204, 204, 204);"><span style="font-size:14px;"><strong><span style="font-family:arial;">&nbsp;Mr Test Testy</span></strong></span></td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);"><span style="font-size:14px;"><strong><span style="font-family:arial;">1 x 30 kgs checked</span></strong></span></td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);"><span style="font-size:14px;"><strong><span style="font-family:arial;">23B</span></strong></span></td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);"><span style="font-size:14px;"><strong><span style="font-family:arial;">Vegetarian</span></strong></span></td>
        <td colspan="2" style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="width: 20%; border-color: rgb(204, 204, 204);"><span style="font-size:14px;"><span style="font-family:arial;"><strong>&nbsp;Mrs Test Testy</strong></span></span></td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);"><span style="font-size:14px;"><strong><span style="font-family:arial;">1 x 30 kgs checked</span></strong></span></td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);"><span style="font-size:14px;"><strong><span style="font-family:arial;">23A</span></strong></span></td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);"><span style="font-size:14px;"><strong><span style="font-family:arial;">Halal</span></strong></span></td>
        <td colspan="2" style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);"><span style="font-size:14px;"><strong><span style="font-family:arial;">Wheelchair&nbsp;</span></strong></span></td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);"><span style="font-size:14px;"><strong><span style="font-family:arial;">1 x 7 kgs carry-on</span></strong></span></td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right; border-color: rgb(204, 204, 204);">&nbsp;</td>
    </tr>
    </tbody>
</table>

@if($data->type === 'return')

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="text-align: center; vertical-align: middle; width: 5%; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221);"><span style="font-family:arial;"><span style="font-size:16px;"><b><img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/flight_return.png" style="width: 27px; height: 19px;"/></b></span></span></td>
        <td style="width: 45%; vertical-align: middle; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221); height: 30px;"><span style="font-family:arial;"><span style="font-size:16px;"><strong>&nbsp;LAX&nbsp; &gt;&nbsp; BNE&nbsp; |&nbsp; FRIDAY 22 FEBRUARY 2021</strong></span></span></td>
        <td style="width: 50%; vertical-align: middle; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221); text-align: right;"><span style="font-family:arial;"><span style="font-size:16px;"><strong>AIRLINE REF:&nbsp; &nbsp;<span style="color:#0000FF;">NOPQRS&nbsp;</span></strong></span></span></td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width: 900px;">
    <tbody>
    <tr>
        <td colspan="4" style="width: 33%;">&nbsp;</td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width: 900px;">
    <tbody>
    <tr>
        <td style="width: 33%;"><span style="font-size:16px;"><strong><span style="font-family:arial;">&nbsp;Singapore Airlines</span></strong></span></td>
        <td style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Departing </span></span><span style="color:#0000FF;"><strong><span style="font-size:18px;"><span style="font-family:arial;">18:10</span></span></strong></span></td>
        <td colspan="2" style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Arriving</span></span><span style="color:#0000FF;"><span style="font-size:14px;"><span style="font-family:arial;"> </span></span><strong><span style="font-size:18px;"><span style="font-family:arial;">10:40</span></span></strong></span></td>
    </tr>
    <tr>
        <td style="width: 33%;"><span style="font-size:14px;"><span style="font-family:arial;">&nbsp;SQ123</span></span></td>
        <td style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Friday, 22 February 2021</span></span></td>
        <td colspan="2" style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Saturday 23 February 2021</span></span></td>
    </tr>
    <tr>
        <td style="width: 33%;">&nbsp;</td>
        <td style="width: 33%;">&nbsp;</td>
        <td colspan="2" style="width: 33%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 33%;"><span style="font-family:arial;"><span style="font-size:16px;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/duration.png" style="width: 13px; height: 13px;"/>&nbsp;</b></span></span><span style="font-size:14px;"><span style="font-family:arial;">&nbsp;</span></span><span style="font-size:12px;"><span style="font-family:arial;">12 hrs, 50 mins</span></span></td>
        <td style="width: 33%; text-align: right;"><span style="font-size:16px;"><span style="font-family:arial;"><strong>LAX | Los Angeles</strong></span></span></td>
        <td colspan="2" style="width: 33%; text-align: right;"><span style="font-size:16px;"><font face="arial"><b>SIN | Singapore</b></font></span></td>
    </tr>
    <tr>
        <td style="width: 33%;"><span style="font-family:arial;"><span style="font-size:16px;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/aircraft.png" style="width: 13px; height: 14px;"/>&nbsp;&nbsp;</b></span><span style="font-size:12px;">Boeing 777-800</span></span></td>
        <td style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Brisbane International Airport</span></span></td>
        <td colspan="2" style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Changi International Airport</span></span></td>
    </tr>
    <tr>
        <td style="width: 33%;"><span style="font-family:arial;"><span style="font-size:16px;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/cabin.png" style="width: 13px; height: 13px;"/>&nbsp;&nbsp;</b></span><span style="font-size:12px;">Economy</span></span></td>
        <td style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Departure Terminal: D</span></span></td>
        <td colspan="2" style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Arrival Terminal: 5</span></span></td>
    </tr>
    <tr>
        <td colspan="4" style="width: 33%; height: 5px;"><span style="font-family:arial;"><span style="font-size:16px;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/direct.png" style="width: 13px; height: 13px;"/>&nbsp;&nbsp;</b></span><span style="font-size:12px;"><strong>1 Stop</strong></span></span></td>
    </tr>
    <tr>
        <td colspan="4" style="width: 33%; height: 5px;">&nbsp;</td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="width: 20%;">&nbsp;</td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/baggage.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>Baggage&nbsp;&nbsp;</span></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/seat.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>Seat&nbsp;&nbsp;</span></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;"><b>&nbsp;&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/meal.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>Meal&nbsp;&nbsp;</span></span></td>
        <td colspan="2" style="width: 20%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/assistance.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>Assistance&nbsp;&nbsp;</span></span></td>
    </tr>
    <tr>
        <td style="width: 20%;"><span style="font-size:14px;"><span style="font-family:arial;">&nbsp;Mr Test Testy</span></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">1 x Piece checked</span></strong></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">19D</span></strong></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">Vegetarian</span></strong></span></td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">1 x 7 kgs carry-on</span></strong></span></td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="width: 20%;"><span style="font-size:14px;"><span style="font-family:arial;">&nbsp;Mrs Test Testy</span></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">1 x Piece checked</span></strong></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">19E</span></strong></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">Halal</span></strong></span></td>
        <td colspan="2" style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">Wheelchair</span></strong></span></td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">1 x 7 kgs carry-on</span></strong></span></td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="height:10px;width:900px;">
    <tbody>
    <tr>
        <td style="vertical-align: top; width: 10%;">&nbsp;</td>
        <td rowspan="1" style="width: 80%; text-align: center; background-color: rgb(221, 221, 221); color:#00000;"><span style="font-size:14px;"><strong><span style="font-family:arial;">LAYOVER&nbsp; &nbsp;|&nbsp; &nbsp;2 Hrs, 10 Mins</span></strong></span></td>
        <td style="text-align: right; vertical-align: top; width: 10%;">&nbsp;</td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width: 900px;">
    <tbody>
    <tr>
        <td colspan="4" style="width: 33%; height: 5px;">
            <table align="center" border="0" cellpadding="1" cellspacing="1" style="width: 900px;">
                <tbody>
                <tr>
                    <td style="width: 33%;">&nbsp;</td>
                    <td style="width: 33%; text-align: right;">&nbsp;</td>
                    <td colspan="2" style="width: 33%; text-align: right;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 33%;"><span style="font-size:16px;"><strong><span style="font-family:arial;">&nbsp;Virgin Australia</span></strong></span></td>
                    <td style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Departing </span></span><span style="color:#0000FF;"><strong><span style="font-size:18px;"><span style="font-family:arial;">12:50</span></span></strong></span></td>
                    <td colspan="2" style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Arriving</span></span><span style="color:#0000FF;"><span style="font-size:14px;"><span style="font-family:arial;"> </span></span><strong><span style="font-size:18px;"><span style="font-family:arial;">21:40</span></span></strong></span></td>
                </tr>
                <tr>
                    <td style="width: 33%;"><span style="font-size:14px;"><span style="font-family:arial;">&nbsp;VA456</span></span></td>
                    <td style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Friday, 23 February 2021</span></span></td>
                    <td colspan="2" style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Saturday 23 February 2021</span></span></td>
                </tr>
                <tr>
                    <td style="width: 33%;">&nbsp;</td>
                    <td style="width: 33%;">&nbsp;</td>
                    <td colspan="2" style="width: 33%;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 33%;"><span style="font-family:arial;"><span style="font-size:16px;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/duration.png" style="width: 13px; height: 13px;"/>&nbsp;</b></span></span><span style="font-size:14px;"><span style="font-family:arial;">&nbsp;</span></span><span style="font-size:12px;"><span style="font-family:arial;">12 hrs, 50 mins</span></span></td>
                    <td style="width: 33%; text-align: right;"><span style="font-size:16px;"><font face="arial"><b>SIN | Singapore</b></font></span></td>
                    <td colspan="2" style="width: 33%; text-align: right;"><span style="font-size:16px;"><font face="arial"><b>BNE | Brisbane</b></font></span></td>
                </tr>
                <tr>
                    <td style="width: 33%;"><span style="font-family:arial;"><span style="font-size:16px;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/aircraft.png" style="width: 13px; height: 14px;"/>&nbsp;&nbsp;</b></span><span style="font-size:12px;">Boeing A380</span></span></td>
                    <td style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Changi International Airport</span></span></td>
                    <td colspan="2" style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Los Angeles International Airport</span></span></td>
                </tr>
                <tr>
                    <td style="width: 33%;"><span style="font-family:arial;"><span style="font-size:16px;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/cabin.png" style="width: 13px; height: 13px;"/>&nbsp;&nbsp;</b></span><span style="font-size:12px;">Business</span></span></td>
                    <td style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Departure Terminal: D</span></span></td>
                    <td colspan="2" style="width: 33%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;">Arrival Terminal: 3</span></span></td>
                </tr>
                <tr>
                    <td colspan="4" style="width: 33%; height: 5px;">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="width: 20%;">&nbsp;</td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/baggage.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>Baggage&nbsp;&nbsp;</span></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/seat.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>Seat&nbsp;&nbsp;</span></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/meal.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>Meal&nbsp;&nbsp;</span></span></td>
        <td colspan="2" style="width: 20%; text-align: right;"><span style="font-size:14px;"><span style="font-family:arial;"><b>&nbsp;<img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/assistance.png" style="width: 15px; height: 15px; float: right;"/>&nbsp;</b>Assistance&nbsp;&nbsp;</span></span></td>
    </tr>
    <tr>
        <td style="width: 20%;"><span style="font-size:14px;"><span style="font-family:arial;">&nbsp;Mr Test Testy</span></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">2 x 23 kgs checked</span></strong></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">17F</span></strong></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">Vegetarian</span></strong></span></td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">1 x 9 kgs carry-on</span></strong></span></td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="width: 20%;"><span style="font-size:14px;"><span style="font-family:arial;">&nbsp;Mrs Test Testy</span></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">2 x 23 kgs&nbsp;checked</span></strong></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">17G</span></strong></span></td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">Halal</span></strong></span></td>
        <td colspan="2" style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">Wheelchair</span></strong></span></td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;"><span style="font-size:14px;"><strong><span style="font-family:arial;">1 x 9 kgs carry-on</span></strong></span></td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td style="width: 20%; text-align: right;">&nbsp;</td>
        <td colspan="2" style="width: 20%; text-align: right;">&nbsp;</td>
    </tr>
    </tbody>
</table>


@endif



<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="text-align: center; vertical-align: middle; width: 5%; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221);"><span style="font-family:arial;"><span style="font-size:16px;"><b><img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/payment.png" style="width: 19px; height: 19px;"/></b></span></span></td>
        <td rowspan="1" style="width: 95%; vertical-align: middle; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221); height: 30px;"><span style="font-size:16px;"><span style="font-family:arial;"><b>&nbsp;PAYMENT SUMMARY</b></span></span></td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="height:10px;width:900px;">
    <tbody>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td style="width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
    </tr>

 	@dd(@$data->provision_response)


    @php($srch = collect(@$data->search_params)->only(['adult', 'child', 'infant']))
 	@php($air = collect(@$data->booking->detail['AirReservation']))
 	@php($fares = collect(@$air['TPA_Extensions']['PaxTypeFare']))
 	

 	@php($adultFare = @$fares->where('Pax.@attributes.Type', 'ADT')->first())
 	@php($childFare = @$fares->where('Pax.@attributes.Type', 'CHD')->first())
 	@php($infantFare = @$fares->where('Pax.@attributes.Type', 'INT')->first())
 	@php($name_prefix = $data->booking->name_prefix)
 	@php($name = $data->booking->name)
 	@php($surname = $data->booking->surname)
 	@php($extensions = ['ADT' => $adultFare, 'CHD' => $childFare, 'INT' => $infantFare])

    @foreach(@$data->booking_params['traveller_type'] as $travelerType)
    <tr>
        <td style="vertical-align: top; width: 25%;">
        	<span style="font-size:14px;">
        		
        		<span style="font-family:arial;">&nbsp;{{ @$name_prefix[$loop->index] }} {{ @$name[$loop->index] }} {{ @$surname[$loop->index] }}</span>
        	</span>
        </td>
        <td colspan="2" rowspan="1" style="width: 25%;">
        	<span style="font-size:14px;">
        		<span style="font-family:arial;">Airfare (including ${{ @$extensions[$travelerType]['TotalTax']['@attributes']['Amount'] }} Taxes)</span>
        	</span>
        </td>
        <td style="text-align: right; vertical-align: top; width: 25%;">
        	<span style="font-size:14px;">
        		<span style="font-family:arial;">${{ @$extensions[$travelerType]['TotalFare']['@attributes']['Amount'] }}</span>
        	</span>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td colspan="2" rowspan="1" style="width: 25%;"><span style="font-size:14px;"><span style="font-family:arial;">Baggage</span></span></td>
        <td style="text-align: right; vertical-align: top; width: 25%;"><span style="font-size:14px;"><span style="font-family:arial;">$0.00</span></span></td>
    </tr>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td colspan="2" rowspan="1" style="width: 25%;"><span style="font-size:14px;"><span style="font-family:arial;">Meal Voucher</span></span></td>
        <td style="text-align: right; vertical-align: top; width: 25%;"><span style="font-size:14px;"><span style="font-family:arial;">$0.00</span></span></td>
    </tr>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td colspan="2" rowspan="1" style="width: 25%;"><span style="font-size:14px;"><span style="font-family:arial;">Seat Premium</span></span></td>
        <td style="text-align: right; vertical-align: top; width: 25%;"><span style="font-size:14px;"><span style="font-family:arial;">$0.00</span></span></td>
    </tr>

    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td colspan="2" rowspan="1" style="width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
    </tr>
    @endforeach
    

    @php($totalFare = @$data->booking->detail['AirReservation']['ItinTotalFare'])
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td colspan="2" rowspan="1" style="width: 25%;"><span style="font-size:14px;"><span style="font-family:arial;">Discount Voucher</span></span></td>
        <td style="text-align: right; vertical-align: top; width: 25%;"><span style="font-size:14px;"><span style="font-family:arial;">-${{ $data->booking->discount }}</span></span></td>
    </tr>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td colspan="2" rowspan="1" style="width: 25%;"><span style="font-size:14px;"><span style="font-family:arial;">Merchant Fee</span></span></td>
        <td style="text-align: right; vertical-align: top; width: 25%;"><span style="font-size:14px;"><span style="font-family:arial;">${{ setting('merchant_fee') }}</span></span></td>
    </tr>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td colspan="2" rowspan="1" style="width: 25%;">
        	<span style="font-size:14px;">
        		<span style="font-family:arial;">GST</span>
        	</span>
        </td>
        <td style="text-align: right; vertical-align: top; width: 25%;">
        	<span style="font-size:14px;">
        		<span style="font-family:arial;">${{ $totalFare['TotalTax']['@attributes']['Amount'] }}</span>
        	</span>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td colspan="2" rowspan="1" style="width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="vertical-align: top; width: 25%;"><strong><span style="font-size:14px;"><span style="font-family:arial;">TOTAL PAID</span></span></strong></td>
        <td colspan="2" rowspan="1" style="width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;"><strong><span style="font-size:14px;"><span style="font-family:arial;">${{ $totalFare['TotalFare']['@attributes']['Amount'] }}</span></span></strong></td>
    </tr>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td colspan="2" style="width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
    </tr>
    </tbody>
</table>


<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:900px;">
    <tbody>
    <tr>
        <td style="text-align: center; vertical-align: middle; width: 5%; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221);"><span style="font-family:arial;"><span style="font-size:16px;"><b><img alt="" src="https://www.checkmyfares.com/cmf/Itinerary/information.png" style="width: 19px; height: 19px;"/></b></span></span></td>
        <td rowspan="1" style="width: 95%; vertical-align: middle; background-color: rgb(221, 221, 221); border-color: rgb(221, 221, 221); height: 30px;"><font face="arial"><b>&nbsp;IMPORTANT INFORMATION</b></font></td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="height:10px;width:900px;">
    <tbody>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td style="width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4" style="vertical-align: top; width: 25%;">
            <ul>
                <li><span style="font-size:11px;"><span style="font-family:arial;">This is your Travel Itinerary, which forms part of your Contract Of Carriage. </span></span></li>
                <li><span style="font-size:11px;"><span style="font-family:arial;">Your Electronic Ticket is recorded in the airline&#39;s reservation system. </span></span></li>
                <li><span style="font-size:11px;"><span style="font-family:arial;">You may need to show this Itinerary to enter the airport and/or to prove return or onward travel to Customs and Immigration Officials. </span></span></li>
                <li><span style="font-size:11px;"><span style="font-family:arial;">All amounts are displayed in Australian Dollars.</span></span></li>
                <li><span style="font-size:11px;"><span style="font-family:arial;">Refund, Re-issue or re-validation are subject to Airline and/or Agency policies &amp; fees applicable.</span></span></li>
                <li><span style="font-size:11px;"><span style="font-family:arial;">Ensure that all passengers are holding valid travel documents &amp; visas for destination/transit countries.</span></span></li>
                <li><span style="font-size:11px;"><span style="font-family:arial;">All timings given are local time, in 24-hour format,&nbsp;and are subject to change without notice.</span></span></li>
                <li><span style="font-size:11px;"><span style="font-family:arial;">It is passengers responsibility to obtain updated information on your flight t</span></span><span style="font-size:11px;"><span style="font-family:arial;">imings and departure/arrival airport/terminal. </span></span></li>
                <li><span style="font-size:11px;"><span style="font-family:arial;">Carriage and other services provided by the airline are subject to conditions of carriage, which are hereby incorporated by reference. </span></span></li>
                <li><span style="font-size:11px;"><span style="font-family:arial;">The&nbsp;conditions of carriage may be obtained directly from the airline. </span></span></li>
                <li><span style="font-size:11px;"><span style="font-family:arial;">Standard airport check-in begins 3 hours before scheduled departure time and any late arrival at the check-in counter may prevent you from&nbsp;boarding the flights.</span></span></li>
            </ul>
        </td>
    </tr>
    </tbody>
</table>

<table align="center" border="0" cellpadding="1" cellspacing="1" style="height:10px;width:900px;">
    <tbody>
    <tr>
        <td colspan="4" style="vertical-align: middle; width: 25%; height: 30px; background-color: rgb(0, 0, 255); border-color: rgb(0, 0, 255); text-align: center;"><span style="color:#FFFFFF;"><span style="font-size:12px;"><font face="arial"><b>W&nbsp; </b>CMFx.com.au<b>&nbsp; &nbsp;|&nbsp; &nbsp;A&nbsp;&nbsp;</b>Suite 5, Level 2, 2 Fortune Street, Coomera Q 4209<b>&nbsp; &nbsp;|&nbsp; &nbsp;T&nbsp;&nbsp;</b>+61 2 8320 3285<b>&nbsp; &nbsp;|&nbsp; &nbsp;E&nbsp;&nbsp;</b>Customer.Care@CMFx.com.au</font></span></span></td>
    </tr>
    <tr>
        <td style="vertical-align: top; width: 25%;">&nbsp;</td>
        <td style="width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
        <td style="text-align: right; vertical-align: top; width: 25%;">&nbsp;</td>
    </tr>
    </tbody>
</table>
</body>
</html>
