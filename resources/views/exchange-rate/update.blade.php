@extends('layout')
@section('title','CMV - Update Exchange Rate')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-1">
                    <h3 class="content-header-title">Update Exchange Rate</h3>
                </div>
                <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Update Exchange Rate
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <section class="card">
                <div class="content-body"><!-- Table row borders end-->
                    <!-- Double border end-->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        @if(Session::has('type'))
                                            <div class="alert alert-{{Session::get('type')}}">
                                                <p>{{Session::get('msg')}}</p>
                                            </div>
                                        @endif
                                        {!! Form::open(['route' => ['exchange.rate.update',$exchangeRate->id],'class' => 'form','method' => 'post']) !!}
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="form-group col-md-6 mb-2">
                                                    <label for="userinput1">Currency Code</label>
                                                    {!! Form::text('currency_code',$exchangeRate->currency_code,['class' => 'form-control form-control-xl input-xl']) !!}
                                                    @if($errors->update_exchange_rate->first('currency_code'))
                                                        <p class="badge badge-danger" style="margin-top: 10px;">{{ $errors->update_exchange_rate->first('currency_code') }}</p>
                                                    @endif
                                                </div>
                                                <div class="form-group col-md-6 mb-2">
                                                    <label for="userinput1">Exchange Rate</label>
                                                    {!! Form::input('number','exchange_rate',$exchangeRate->exchange_rate,['class' => 'form-control form-control-xl input-xl','step' => '0.0001','min' => '0.0001']) !!}
                                                    @if($errors->update_exchange_rate->first('exchange_rate'))
                                                        <p class="badge badge-danger" style="margin-top: 10px;">{{ $errors->update_exchange_rate->first('exchange_rate') }}</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button type="button" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-check-square-o"></i> Save
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Double border end -->
                </div>
            </section>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection
