@extends('layout')
@section('title','CMV - View Exchange Rates')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-1">
                    <h3 class="content-header-title">View Exchange Rates</h3>
                </div>
                <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">View Exchange Rates
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <section class="card">
                <div class="content-body"><!-- Table row borders end-->
                    <!-- Double border end-->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <thead>
                                            <tr class="border-double">
                                                <th>S. No</th>
                                                <th>Currency Code</th>
                                                <th>Exchange Rate</th>
                                                <th>Created date & time</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php($i = 1)
                                            @foreach($exchangeRates as $exchangeRate)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$exchangeRate->currency_code}}</td>
                                                    <td>{{$exchangeRate->exchange_rate}}</td>
                                                    <td>{{date('F d Y h:i a',strtotime($exchangeRate->created_at))}}</td>
                                                    <td>
                                                        <a href="{{route('exchange.rate.update',$exchangeRate->id)}}" class="btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Update"><i class="fa fa-pencil"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Double border end -->
                </div>
            </section>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection
