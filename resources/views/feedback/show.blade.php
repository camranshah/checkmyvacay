@extends('layouts.app', ['Title' => 'Feedback Details'])

@section('content')
    <div class="app-content content view user profile">
        <div class="content-wrapper">
            <div class="content-body">
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Feedback Details</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-12">
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <label for="">Name</label>
                                                            <input type="text" value="{{ $Feedback->name }}"
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="">Email</label>
                                                            <input type="text" value="{{ $Feedback->email }}"
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="">Date</label>
                                                            <input type="text" value="{{ $Feedback->created_at }}" disabled=""
                                                                   disabled="">
                                                        </div>
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="">Subject</label>
                                                            <input type="text" value="{{ $Feedback->subject }}"
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <label for="">Message</label>
                                                            <textarea class="form-control"
                                                                      disabled="">{{ $Feedback->message }}</textarea>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
