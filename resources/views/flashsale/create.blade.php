@extends('layouts.app', ['Title' => 'Create A Banner'])

@section('script')
    <script>

        function cmPreviewImageBanner(input, block, id) {
            var fileTypes = ['jpg', 'jpeg', 'png'];
            var extension = input.files[0].name.split('.').pop().toLowerCase();  /*se preia extensia*/
            var isSuccess = fileTypes.indexOf(extension) > -1; /*se verifica extensia*/

            if (isSuccess) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    block.css("background-image", "url(" + e.target.result + ")");
                    //block.css('background', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                alert('The selected file is not supported!');
            }

        }

        $('.cm-select-airports').select2({
            ajax: {
                url: '{{ url('web/airports') }}',
                data: function (params) {
                    var query = {
                        keyword: params.term,
                        page: params.page || 1
                    }
                    return query;
                }
            }
        });

        $(document).on('change', '.cm-change-image', function () {
            let id = $(this).attr('id');
            cmPreviewImageBanner(this, $('.cm-banner-bg button'), id);
        });

        $('.cm-select-date-1').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-2').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-3').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-4').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-5').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-6').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-7').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-8').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-9').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-10').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-11').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-select-date-12').datepicker({
            uiLibrary: 'bootstrap4'
        });


        $('.cm-select-expiry-date').datepicker({
            uiLibrary: 'bootstrap4'
        });

        ClassicEditor.create(document.querySelector('#cm-ck-editor-tour-details')).then(editor => {
        }).catch(error => {
            console.error(error);
        });

        ClassicEditor.create(document.querySelector('#cm-ck-editor-what-to-expect')).then(editor => {
        }).catch(error => {
            console.error(error);
        });

        ClassicEditor.create(document.querySelector('#cm-ck-editor-departure-details')).then(editor => {
        }).catch(error => {
            console.error(error);
        });

    </script>
@endsection

@section('content')
    <div class="app-content content view user profile edit-profile create-banner">
        <div class="content-wrapper">
            <div class="content-body">
                @include('includes.flash-messages')
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Create A Banner</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-12">
                                                <form action="{{ route('flash-sale.store') }}"
                                                      id="cm-form-create-banner" enctype="multipart/form-data"
                                                      method="post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <label for="">Banner Title*</label>
                                                            <input type="text" name="title" value="{{ old('title') }}"
                                                                   placeholder="Banner Title"
                                                                   class="form-control @error('title') is-invalid @enderror">
                                                            @error('title')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <label for="">Banner Image*</label>
                                                            <div class="pro-img-main cm-banner-bg">
                                                                <button type="button" name="file"
                                                                        onclick="document.getElementById('upload').click()">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    <p>Add Image</p></button>
                                                                <input type="file" class="cm-change-image" id="upload"
                                                                       name="image">
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Type</label>
                                                            <select class="@error('type') is-invalid @enderror"
                                                                    name="type">
                                                                <option value="flight" {{ old('type') == 'flight' ? 'selected' : '' }}>Flight</option>
                                                                <option value="package" {{ old('type') == 'package' ? 'selected' : '' }}>Package</option>
                                                            </select>
                                                            @error('type')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Expiry Date</label>
                                                            <input name="expiry_date"
                                                                   value="{{ old('expiry_date') }}"
                                                                   class="cm-select-expiry-date @error('expiry_date') is-invalid @enderror"
                                                                   placeholder="Expiry Date">
                                                            @error('expiry_date')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>

                                                        <div
                                                            class="col-sm-12 col-12 form-group ">
                                                            <label for="">URL</label>
                                                            <input name="url" class="@error('url') is-invalid @enderror"
                                                                   value="{{ old('url') }}"
                                                                   placeholder="Enter URL">
                                                            @error('url')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>

                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Specify Departure Locations</label>
                                                            <select name="departure_locations[]"
                                                                    class="cm-select-airports form-control @error('departure_locations') is-invalid @enderror"
                                                                    multiple="multiple">

                                                            </select>
                                                            @error('departure_locations')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Specify Arrival Locations</label>
                                                            <select name="arrival_locations[]"
                                                                    class="cm-select-airports form-control @error('arrival_locations') is-invalid @enderror"
                                                                    multiple="multiple">
                                                            </select>
                                                            @error('arrival_locations')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <h3>1st Interval</h3>
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Departure Date</label>
                                                            <input name="first_interval_departure_1"
                                                                   value="{{ old('first_interval_departure_1') }}"
                                                                   class="cm-select-date-1 @error('first_interval_departure_1') is-invalid @enderror"
                                                                   placeholder="Valid From">
                                                            @error('first_interval_departure_1')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Arrival Date</label>
                                                            <input name="first_interval_arrival_1"
                                                                   value="{{ old('first_interval_arrival_1') }}"
                                                                   class="cm-select-date-2 @error('first_interval_arrival_1') is-invalid @enderror"
                                                                   placeholder="Valid To">
                                                            @error('first_interval_arrival_1')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Departure Date</label>
                                                            <input name="first_interval_departure_2"
                                                                   value="{{ old('first_interval_departure_2') }}"
                                                                   class="cm-select-date-3 @error('first_interval_departure_2') is-invalid @enderror"
                                                                   placeholder="Valid From">
                                                            @error('first_interval_departure_2')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Arrival Date</label>
                                                            <input name="first_interval_arrival_2"
                                                                   value="{{ old('first_interval_arrival_2') }}"
                                                                   class="cm-select-date-4 @error('first_interval_arrival_2') is-invalid @enderror"
                                                                   placeholder="Valid To">
                                                            @error('first_interval_arrival_2')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 form-group">
                                                            <h3>2nd Interval</h3>
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Departure Date</label>
                                                            <input name="second_interval_departure_1"
                                                                   value="{{ old('second_interval_departure_1') }}"
                                                                   class="cm-select-date-5 @error('second_interval_departure_1') is-invalid @enderror"
                                                                   placeholder="Valid From">
                                                            @error('second_interval_departure_1')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Arrival Date</label>
                                                            <input name="second_interval_arrival_1"
                                                                   value="{{ old('second_interval_arrival_1') }}"
                                                                   class="cm-select-date-6 @error('second_interval_arrival_1') is-invalid @enderror"
                                                                   placeholder="Valid To">
                                                            @error('second_interval_arrival_1')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Departure Date</label>
                                                            <input name="second_interval_departure_2"
                                                                   value="{{ old('second_interval_departure_2') }}"
                                                                   class="cm-select-date-7 @error('second_interval_departure_2') is-invalid @enderror"
                                                                   placeholder="Valid From">
                                                            @error('second_interval_departure_2')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Arrival Date</label>
                                                            <input name="second_interval_arrival_2"
                                                                   value="{{ old('second_interval_arrival_2') }}"
                                                                   class="cm-select-date-8 @error('second_interval_arrival_2') is-invalid @enderror"
                                                                   placeholder="Valid To">
                                                            @error('second_interval_arrival_2')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 form-group">
                                                            <h3>3rd Interval</h3>
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Departure Date</label>
                                                            <input name="third_interval_departure_1"
                                                                   value="{{ old('third_interval_departure_1') }}"
                                                                   class="cm-select-date-9 @error('third_interval_departure_1') is-invalid @enderror"
                                                                   placeholder="Valid From">
                                                            @error('third_interval_departure_1')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Arrival Date</label>
                                                            <input name="third_interval_arrival_1"
                                                                   value="{{ old('third_interval_arrival_1') }}"
                                                                   class="cm-select-date-10 @error('third_interval_arrival_1') is-invalid @enderror"
                                                                   placeholder="Valid To">
                                                            @error('third_interval_arrival_1')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Departure Date</label>
                                                            <input name="third_interval_departure_2"
                                                                   value="{{ old('third_interval_departure_2') }}"
                                                                   class="cm-select-date-11 @error('third_interval_departure_2') is-invalid @enderror"
                                                                   placeholder="Valid From">
                                                            @error('third_interval_departure_2')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>
                                                        <div
                                                            class="col-sm-6 col-12 form-group ">
                                                            <label for="">Arrival Date</label>
                                                            <input name="third_interval_arrival_2"
                                                                   value="{{ old('third_interval_arrival_2') }}"
                                                                   class="cm-select-date-12 @error('third_interval_arrival_2') is-invalid @enderror"
                                                                   placeholder="Valid To">
                                                            @error('third_interval_arrival_2')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong></span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-lg-3 col-md-4 col-sm-4 col-12 form-group">
                                                            <button type="submit">Create</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
