@extends('layouts.app', ['Title' => 'Flash Sales'])

@section('content')
    <div class="app-content content view user">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Flash Sales</h1>
                                                </div>
                                                <div class="right">
                                                    <a href="{{ route('flash-sale.create') }}">Create A Banner</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2 filter-main justify-content-between">
                                            <div class="col-12">
                                                <label>Sort By:</label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-6 col-sm-6  form-group">
                                                        <label for="">From:</label>
                                                        <input id="fromSelector" type="text" readonly="">
                                                    </div>
                                                    <div class="col-lg-3 col-md-6 col-sm-6  form-group">
                                                        <label for="">To:</label>
                                                        <input id="toSelector" type="text" readonly="">
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 d-flex align-items-center">
                                                        <div class="sort-box">
                                                            <ul>
                                                                <li>
                                                                    <label class="radio-container">Expiry Date
                                                                        <input class="cm-radio-filter" type="radio"
                                                                               name="radio" value="2" checked>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row maain-tabble mt-3">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>Expiry Date</th>
                                                    <th>Banner Title</th>
                                                    <th>Discount</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                @foreach($FlashSales as $key => $value)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
{{--                                                        <td>{{ json_encode(optional($value)->expiry_date) }}</td>--}}
                                                        <td>{{ $value->title }}</td>
                                                        <td>{{ $value->discount_value }}</td>
                                                        <td><input type="checkbox" class="cm-status-toggle"
                                                                   data-url="{{ route('flash-sale.update') }}"
                                                                   data-entityId="{{ $value->id }}" data-toggle="toggle"
                                                                   data-on="Active"
                                                                   data-off="Inactive" {{ ($value->status) ? 'checked': '' }} >
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mr-1 mb-1">
                                                                <button type="button" class="btn  btn-drop-table btn-sm"
                                                                        data-toggle="dropdown" aria-haspopup="true"
                                                                        aria-expanded="false"><i
                                                                        class="fa fa-ellipsis-v"></i></button>
                                                                <div class="dropdown-menu" x-placement="bottom-start"
                                                                     style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                    <a class="dropdown-item"
                                                                       href="{{ route('flash-sale.show', ['id' => $value->id]) }}"><i
                                                                            class="fa fa-eye"></i>View Details</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
