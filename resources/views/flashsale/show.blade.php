@extends('layouts.app', ['Title' => 'Flash Sale Details'])

@section('script')
    <script>
        $('.cm-pre-select-airports').select2({});
    </script>
@endsection

@section('content')
    <div class="app-content content view user create-banner ">
        <div class="content-wrapper">
            <div class="content-body">
                @include('includes.flash-messages')
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Flash Sales</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2 profile u-profile">
                                            <div class="col-12">
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <img src="{{ $FlashSale->image }}">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Banner Title</label>
                                                            <input type="text" value="{{ $FlashSale->title }}"
                                                                   placeholder="Banner Title" id=""
                                                                   class="form-control" disabled="">
                                                        </div>

                                                        @if($FlashSale->type == 'package')
                                                            <div class="col-md-6 col-sm-6 col-12 form-group">
                                                                <label for="">URL</label>
                                                                <input type="text" value="{{ $FlashSale->url }}"
                                                                       placeholder="URL" id=""
                                                                       class="form-control" disabled="">
                                                            </div>
                                                        @endif

                                                        @if($FlashSale->type == 'flight')
                                                            <div class="col-md-6 col-sm-6 col-12 form-group">
                                                                <label for="">Departure Locations</label>
                                                                @foreach($FlashSale->locations()->whereType(1)->get() as $loc)
                                                                    <span
                                                                        class="badge badge-secondary">{{ $loc->airport->iata.', '.$loc->airport->name.', '.$loc->airport->city.', '.$loc->airport->country }}</span>
                                                                @endforeach
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-12 form-group">
                                                                <label for="">Arrival Locations</label>
                                                                @foreach($FlashSale->locations()->whereType(2)->get() as $loc)
                                                                    <span
                                                                        class="badge badge-secondary">{{ $loc->airport->iata.', '.$loc->airport->name.', '.$loc->airport->city.', '.$loc->airport->country }}</span>
                                                                @endforeach
                                                            </div>
                                                            <div class="col-12 form-group">
                                                                <h3>1st Interval</h3>
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Departure Date</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[0]->departure_date_1 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Arrival Date</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[0]->arrival_date_1 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Departure Date</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[0]->departure_date_2 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Arrival Date</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[0]->arrival_date_2 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-12 form-group">
                                                                <h3>2nd Interval</h3>
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Departure Date</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[1]->departure_date_1 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Arrival Date</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[1]->arrival_date_1 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Departure Date</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[1]->departure_date_2 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Arrival Date</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[1]->arrival_date_2 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-12 form-group">
                                                                <h3>3rd Interval</h3>
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Departure From</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[2]->departure_date_1 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Arrival Date</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[2]->arrival_date_1 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Departure From</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[2]->departure_date_2 }}"
                                                                    disabled="">
                                                            </div>
                                                            <div class="col-sm-6 col-12 form-group">
                                                                <label for="">Arrival Date</label>
                                                                <input
                                                                    placeholder="{{ @$FlashSale->intervals[2]->arrival_date_2 }}"
                                                                    disabled="">
                                                            </div>
                                                        @endif
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="seperator"></div>
                                            </div>
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Usage Log</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2 filter-main justify-content-between">
                                            <div class="col-12">
                                                <label>Sort By:</label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-6 col-sm-6  form-group">
                                                        <label for="">To:</label>
                                                        <input id="select-date" type="text" disabled="">
                                                    </div>
                                                    <div class="col-lg-3 col-md-6 col-sm-6  form-group">
                                                        <label for="">From:</label>
                                                        <input id="select-date2" type="text" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row maain-tabble mt-2">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>Date Used</th>
                                                    <th>User Name</th>
                                                    <th>User Id</th>
                                                    <th>Booking Id</th>
                                                    <th>Flight #</th>
                                                    <th>Departure From</th>
                                                    <th>Total Flight Amount</th>
                                                    <th>Flight Amount After Discount</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>01</td>
                                                    <td>Jan-02-2020</td>
                                                    <td>John</td>
                                                    <td>12345</td>
                                                    <td>12345</td>
                                                    <td>6E 1234</td>
                                                    <td>brisbane</td>
                                                    <td>US$100</td>
                                                    <td>US$100</td>
                                                    <td>
                                                        <div class="btn-group mr-1 mb-1">
                                                            <button type="button" class="btn  btn-drop-table btn-sm"
                                                                    data-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false"><i
                                                                    class="fa fa-ellipsis-v"></i></button>
                                                            <div class="dropdown-menu" x-placement="bottom-start"
                                                                 style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                <a class="dropdown-item" href="a-user-profile.html"><i
                                                                        class="fa fa-eye"></i>View Details</a>

                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>02</td>
                                                    <td>Jan-02-2020</td>
                                                    <td>John</td>
                                                    <td>12345</td>
                                                    <td>12345</td>
                                                    <td>6E 1234</td>
                                                    <td>brisbane</td>
                                                    <td>US$100</td>
                                                    <td>US$100</td>
                                                    <td>
                                                        <div class="btn-group mr-1 mb-1">
                                                            <button type="button" class="btn  btn-drop-table btn-sm"
                                                                    data-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false"><i
                                                                    class="fa fa-ellipsis-v"></i></button>
                                                            <div class="dropdown-menu" x-placement="bottom-start"
                                                                 style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                <a class="dropdown-item" href="a-user-profile.html"><i
                                                                        class="fa fa-eye"></i>View Details</a>

                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>03</td>
                                                    <td>Jan-02-2020</td>
                                                    <td>John</td>
                                                    <td>12345</td>
                                                    <td>12345</td>
                                                    <td>6E 1234</td>
                                                    <td>brisbane</td>
                                                    <td>US$100</td>
                                                    <td>US$100</td>
                                                    <td>
                                                        <div class="btn-group mr-1 mb-1">
                                                            <button type="button" class="btn  btn-drop-table btn-sm"
                                                                    data-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false"><i
                                                                    class="fa fa-ellipsis-v"></i></button>
                                                            <div class="dropdown-menu" x-placement="bottom-start"
                                                                 style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                <a class="dropdown-item" href="a-user-profile.html"><i
                                                                        class="fa fa-eye"></i>View Details</a>

                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>04</td>
                                                    <td>Jan-02-2020</td>
                                                    <td>John</td>
                                                    <td>12345</td>
                                                    <td>12345</td>
                                                    <td>6E 1234</td>
                                                    <td>brisbane</td>
                                                    <td>US$100</td>
                                                    <td>US$100</td>
                                                    <td>
                                                        <div class="btn-group mr-1 mb-1">
                                                            <button type="button" class="btn  btn-drop-table btn-sm"
                                                                    data-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false"><i
                                                                    class="fa fa-ellipsis-v"></i></button>
                                                            <div class="dropdown-menu" x-placement="bottom-start"
                                                                 style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                <a class="dropdown-item" href="a-user-profile.html"><i
                                                                        class="fa fa-eye"></i>View Details</a>

                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
