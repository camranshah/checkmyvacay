@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($messages = $errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        @foreach($errors->all() as $message)
            <p><strong>{{ '*'.$message }}</strong></p>
        @endforeach
    </div>
@endif

