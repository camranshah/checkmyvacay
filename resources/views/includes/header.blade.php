<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-light navbar-border">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a
                        class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                            class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item"><a class="navbar-brand" href="{{ route('dashboard.index') }}"> <img
                            class="brand-logo"
                            alt="stack admin logo"
                            src="{{ asset('custom/images/logo.png') }}">
                    </a></li>
                <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse"
                                                  data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                </ul>
                <ul class="nav navbar-nav float-right">
                    <div class="cm-div-notif">
                        @include('includes.notifications')
                    </div>
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"
                           aria-expanded="false"> <span class="avatar avatar-online"> <img
                                    src="{{ asset('custom/images/student-pro-girl.png') }}" alt="avatar"></span> <span
                                class="user-name">{{ __('Admin') }}</span> </a>
                        <div class="dropdown-menu dropdown-menu-right profile-drp-down">
                            <!-- <h3>Account</h3> -->
                            <ul class="">
                                <li><a href="{{ route('profile.index') }}"><i class="fa fa-user-circle"
                                                                              aria-hidden="true"></i>Profile</a></li>
                                <li><a href="{{ route('setting.index') }}"><i class="fa fa-cogs"
                                                                              aria-hidden="true"></i>Subscription
                                        Amount Management </a></li>
                                <li><a href="{{ route('broadcast-message.index') }}"><img
                                            src="{{ asset('custom/images/p-clip-img.png') }}">Broadcast Messages</a>
                                </li>
                                <li><a href="{{ route('api-log.index') }}"><img
                                            src="{{ asset('custom/images/p-clip-img.png') }}">API Logs</a>
                                </li>
                                <li><a href="javascript:void(0)"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                            class="fa fa-long-arrow-right" aria-hidden="true"></i>Logout</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs"
                                                              href="#"><i class="ft-menu"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<form id="logout-form" action="{{ route('logout') }}" method="POST"
      style="display: none;">
    @csrf
</form>
