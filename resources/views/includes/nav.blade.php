<div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item {{ Request::segment(1) === 'dashboard' ? 'active' : null }}"><a
                    href="{{ route('dashboard.index') }}"><i class="fa fa-area-chart" aria-hidden="true"></i><span
                        class="menu-title" data-i18n="">Dashboard</span></a></li>
            <li class="nav-item {{ Request::segment(1) === 'user' ? 'active' : null }}"><a
                    href="{{ route('user.index') }}"><i class="fa fa-users" aria-hidden="true"></i><span
                        class="menu-title" data-i18n="">Users</span></a></li>
            <li class="nav-item {{ Request::segment(1) === 'flash-sale' ? 'active' : null }}"><a
                    href="{{ route('flash-sale.index') }}"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span
                        class="menu-title" data-i18n="">Flash Sales</span></a></li>
            <li class="nav-item {{ Request::segment(1) === 'promo-code' ? 'active' : null }}"><a
                    href="{{ route('promo-code.index') }}"><i class="fa fa-th-large" aria-hidden="true"></i><span
                        class="menu-title" data-i18n="">Promo Codes</span></a></li>
            <li class="nav-item {{ Request::segment(1) === 'report' ? 'active' : null }}"><a href="javascript:void(0)"><img src="{{ asset('custom/images/clip-img.png') }}"><span
                        class="menu-title" data-i18n="">Reports</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item {{ Request::segment(2) === 'unpaid-booking' ? 'active' : null }}" href="{{ route('report.index',['type'=> 'unpaid-booking']) }}">Unpaid Bookings</a></li>
                    <li><a class="menu-item {{ Request::segment(2) === 'unsuccessful-booking' ? 'active' : null }}" href="{{ route('report.index',['type'=> 'unsuccessful-booking']) }}">Unsuccessful Bookings</a></li>
                    <li><a class="menu-item {{ Request::segment(2) === 'flash-sale-flight' ? 'active' : null }}" href="{{ route('report.index',['type'=> 'flash-sale-flight']) }}">Flash Sale Flights</a></li>
                    <li><a class="menu-item {{ Request::segment(2) === 'total-sales-made' ? 'active' : null }}" href="{{ route('report.index',['type'=> 'total-sales-made']) }}">Total Sales Made</a></li>
                </ul>
            </li>
            <li class="nav-item {{ Request::segment(1) === 'booked-flight' ? 'active' : null }}"><a
                    href="{{ route('booked-flight.index') }}"><i class="fa fa-plane" aria-hidden="true"></i><span
                        class="menu-title" data-i18n="">Booked Flights</span></a></li>
            <li class="nav-item {{ Request::segment(1) === 'payment-log' ? 'active' : null }}"><a
                    href="{{ route('payment-log.index') }}"><i class="fa fa-money" aria-hidden="true"></i><span
                        class="menu-title" data-i18n="">Payment log</span></a></li>
            <li class="nav-item {{ Request::segment(1) === 'feedback' ? 'active' : null }}"><a
                    href="{{ route('feedback.index') }}"><i class="fa fa-list-ul" aria-hidden="true"></i><span
                        class="menu-title" data-i18n="">Feedback</span></a></li>
            <li class="nav-item {{ Request::segment(1) === 'referral' ? 'active' : null }}"><a
                    href="{{ route('referral.index') }}"><i class="fa fa-list-ul" aria-hidden="true"></i><span
                        class="menu-title" data-i18n="">Referrals</span></a></li>
        </ul>
    </div>
</div>
