<li class="dropdown dropdown-notification nav-item">
    <a class="nav-link nav-link-label cm-notif-bell-icon" href="#" data-toggle="dropdown">
        <i class="fa fa-bell" aria-hidden="true"></i>
        <span class="badge badge-pill badge-danger badge-up cm-notif-bell-icon-count"
              @if(!$GlobalData['NotificationUnreadCount']) style="display:none" @endif>{{ @$GlobalData['NotificationUnreadCount'] }}</span>
    </a>
    <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
        <li class="scrollable-container media-list ps-container ps-theme-dark ps-active-y"
            data-ps-id="cbae8718-1b84-97ac-6bfa-47d792d8ad89">
            @foreach($GlobalData['Notifications'] as $notification)
                <a href="{{ url($notification->url) }}">
                    <div class="media">
                        <div class="media-body">
                            <p class="notification-text font-small-3 text-muted">{{ $notification->title }}</p>
                        </div>
                    </div>
                </a>
            @endforeach
        </li>
        <li class="dropdown-menu-footer">
            <a class="dropdown-item text-muted text-center"
               href="{{ route('notification.index') }}">{{ __('View All') }}</a>
        </li>
    </ul>
</li>
