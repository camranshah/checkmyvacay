
<!-- - var menuBorder = true--><!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-menu-template-light/dashboard-analytics.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 Jan 2018 16:19:51 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="{{asset('app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/vendors.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/app.css')}}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <!-- link(rel='stylesheet', type='text/css', href='#{app_assets_path}/css#{rtl}/pages/users.css')-->
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <!-- END Custom CSS-->
    @stack('styles')
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">

<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-light navbar-border">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item"><a href="index-2.html" class="navbar-brand"><img alt="stack admin logo" src="{{asset('app-assets/images/logo/stack-logo.png')}}" class="brand-logo">
                        <h2 class="brand-text">Stack</h2></a></li>
                <li class="nav-item d-md-none"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div id="navbar-mobile" class="collapse navbar-collapse">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu"></i></a></li>
                </ul>
                <ul class="nav navbar-nav float-right">
                    {{--<li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon ft-bell"></i><span class="badge badge-pill badge-default badge-danger badge-default badge-up">5</span></a>--}}
                        {{--<ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">--}}
                            {{--<li class="dropdown-menu-header">--}}
                                {{--<h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span><span class="notification-tag badge badge-default badge-danger float-right m-0">5 New</span></h6>--}}
                            {{--</li>--}}
                            {{--<li class="scrollable-container media-list"><a href="javascript:void(0)">--}}
                                    {{--<div class="media">--}}
                                        {{--<div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h6 class="media-heading">You have new order!</h6>--}}
                                            {{--<p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p><small>--}}
                                                {{--<time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">30 minutes ago</time></small>--}}
                                        {{--</div>--}}
                                    {{--</div></a><a href="javascript:void(0)">--}}
                                    {{--<div class="media">--}}
                                        {{--<div class="media-left align-self-center"><i class="ft-download-cloud icon-bg-circle bg-red bg-darken-1"></i></div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h6 class="media-heading red darken-1">99% Server load</h6>--}}
                                            {{--<p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p><small>--}}
                                                {{--<time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Five hour ago</time></small>--}}
                                        {{--</div>--}}
                                    {{--</div></a><a href="javascript:void(0)">--}}
                                    {{--<div class="media">--}}
                                        {{--<div class="media-left align-self-center"><i class="ft-alert-triangle icon-bg-circle bg-yellow bg-darken-3"></i></div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h6 class="media-heading yellow darken-3">Warning notifixation</h6>--}}
                                            {{--<p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p><small>--}}
                                                {{--<time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Today</time></small>--}}
                                        {{--</div>--}}
                                    {{--</div></a><a href="javascript:void(0)">--}}
                                    {{--<div class="media">--}}
                                        {{--<div class="media-left align-self-center"><i class="ft-check-circle icon-bg-circle bg-cyan"></i></div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h6 class="media-heading">Complete the task</h6><small>--}}
                                                {{--<time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Last week</time></small>--}}
                                        {{--</div>--}}
                                    {{--</div></a><a href="javascript:void(0)">--}}
                                    {{--<div class="media">--}}
                                        {{--<div class="media-left align-self-center"><i class="ft-file icon-bg-circle bg-teal"></i></div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h6 class="media-heading">Generate monthly report</h6><small>--}}
                                                {{--<time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Last month</time></small>--}}
                                        {{--</div>--}}
                                    {{--</div></a></li>--}}
                            {{--<li class="dropdown-menu-footer"><a href="javascript:void(0)" class="dropdown-item text-muted text-center">Read all notifications</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon ft-mail"></i><span class="badge badge-pill badge-default badge-warning badge-default badge-up">3</span></a>--}}
                        {{--<ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">--}}
                            {{--<li class="dropdown-menu-header">--}}
                                {{--<h6 class="dropdown-header m-0"><span class="grey darken-2">Messages</span><span class="notification-tag badge badge-default badge-warning float-right m-0">4 New</span></h6>--}}
                            {{--</li>--}}
                            {{--<li class="scrollable-container media-list"><a href="javascript:void(0)">--}}
                                    {{--<div class="media">--}}
                                        {{--<div class="media-left"><span class="avatar avatar-sm avatar-online rounded-circle"><img src="../../../app-assets/images/portrait/small/avatar-s-1.png" alt="avatar"><i></i></span></div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h6 class="media-heading">Margaret Govan</h6>--}}
                                            {{--<p class="notification-text font-small-3 text-muted">I like your portfolio, let's start.</p><small>--}}
                                                {{--<time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Today</time></small>--}}
                                        {{--</div>--}}
                                    {{--</div></a><a href="javascript:void(0)">--}}
                                    {{--<div class="media">--}}
                                        {{--<div class="media-left"><span class="avatar avatar-sm avatar-busy rounded-circle"><img src="../../../app-assets/images/portrait/small/avatar-s-2.png" alt="avatar"><i></i></span></div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h6 class="media-heading">Bret Lezama</h6>--}}
                                            {{--<p class="notification-text font-small-3 text-muted">I have seen your work, there is</p><small>--}}
                                                {{--<time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Tuesday</time></small>--}}
                                        {{--</div>--}}
                                    {{--</div></a><a href="javascript:void(0)">--}}
                                    {{--<div class="media">--}}
                                        {{--<div class="media-left"><span class="avatar avatar-sm avatar-online rounded-circle"><img src="../../../app-assets/images/portrait/small/avatar-s-3.png" alt="avatar"><i></i></span></div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h6 class="media-heading">Carie Berra</h6>--}}
                                            {{--<p class="notification-text font-small-3 text-muted">Can we have call in this week ?</p><small>--}}
                                                {{--<time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Friday</time></small>--}}
                                        {{--</div>--}}
                                    {{--</div></a><a href="javascript:void(0)">--}}
                                    {{--<div class="media">--}}
                                        {{--<div class="media-left"><span class="avatar avatar-sm avatar-away rounded-circle"><img src="../../../app-assets/images/portrait/small/avatar-s-6.png" alt="avatar"><i></i></span></div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h6 class="media-heading">Eric Alsobrook</h6>--}}
                                            {{--<p class="notification-text font-small-3 text-muted">We have project party this saturday.</p><small>--}}
                                                {{--<time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">last month</time></small>--}}
                                        {{--</div>--}}
                                    {{--</div></a></li>--}}
                            {{--<li class="dropdown-menu-footer"><a href="javascript:void(0)" class="dropdown-item text-muted text-center">Read all messages</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online"><img src="{{asset('app-assets/images/portrait/small/avatar-s-1.png')}}" alt="avatar" style="height: 31px;"><i></i></span><span class="user-name">{{ucwords(Auth::user()->name)}}</span></a>
                        <div class="dropdown-menu dropdown-menu-right"><a href="{{route('edit.profile')}}" class="dropdown-item"><i class="fa fa-user"></i> Edit Profile</a><a href="{{route('changepassword')}}" class="dropdown-item"><i class="fa fa-lock"></i> Change Password</a>
                            <div class="dropdown-divider"></div><a href="{{route('logout')}}" class="dropdown-item"><i class="ft-power"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<!-- ////////////////////////////////////////////////////////////////////////////-->


<!-- - var menuBorder = true-->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion">
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
            <li class=" navigation-header"><span>General</span><i data-toggle="tooltip" data-placement="right" data-original-title="General" class=" ft-minus"></i>
            </li>
            <li class="{{request()->is('dashboard') ? "active" : ""}} nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard </span></a></li>
            <li class="{{request()->is('user/view') ? "active" : ""}} nav-item"><a href="{{route('user.view')}}"><i class="fa fa-user"></i><span data-i18n="" class="menu-title">View Users </span></a></li>
            <li class="{{request()->is('booking/view') ? "active" : ""}} nav-item"><a href="{{route('booking.view')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">View Bookings </span></a></li>
{{--            <li class="{{request()->is('commission') ? "active" : ""}} nav-item"><a href="{{route('commission')}}"><i class="fa fa-percent"></i><span data-i18n="" class="menu-title">Commission </span></a></li>--}}
            <li class=" nav-item"><a href="#"><i class="fa fa-money"></i><span data-i18n="" class="menu-title">Manage Exchange Rates</span></a>
                <ul class="menu-content">
                    <li class="{{request()->is('exchange-rate/view') ? "active" : ""}}"><a href="{{route('exchange.rate.view')}}" class="menu-item">View Exchange Rates</a></li>
                    <li class="{{request()->is('exchange-rate/add') ? "active" : ""}}"><a href="{{route('exchange.rate.add')}}" class="menu-item">Add New Exchange Rate</a></li>
                </ul>
            </li>
            <li class=" nav-item"><a href="#"><i class="fa fa-money"></i><span data-i18n="" class="menu-title">Payments</span></a>
                <ul class="menu-content">
{{--                    <li class="{{request()->is('payment/account') ? "active" : ""}}"><a href="{{route('payment.account')}}" class="menu-item">Account Payments</a></li>--}}
                    <li class="{{request()->is('payment/flight') ? "active" : ""}}"><a href="{{route('payment.flight')}}" class="menu-item">Flight Payments</a></li>
                </ul>
            </li>
{{--            <li class=" nav-item"><a href="#"><i class="fa fa-signal"></i><span data-i18n="" class="menu-title">Reports</span></a>--}}
{{--                <ul class="menu-content">--}}
{{--                    <li class="{{request()->is('report/account') ? "active" : ""}}"><a href="{{route('report.account')}}" class="menu-item">Account Reports</a></li>--}}
{{--                    <li class="{{request()->is('report/flight') ? "active" : ""}}"><a href="{{route('report.flight')}}" class="menu-item">Flight Reports</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            @if(in_array('access-admin',getPermissions()) || in_array('company-view',getPermissions()) || in_array('company-create',getPermissions()) || in_array('company-edit',getPermissions()) || in_array('company-delete',getPermissions()))--}}
{{--                <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Manage Companies</span></a>--}}
{{--                    <ul class="menu-content">--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('company-view',getPermissions()) || in_array('company-edit',getPermissions()) || in_array('company-delete',getPermissions()))--}}
{{--                            <li class="{{request()->is('company/view') ? "active" : ""}}"><a href="{{route('company.view')}}" class="menu-item">View Companies</a></li>--}}
{{--                        @endif--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('company-create',getPermissions()))--}}
{{--                            <li class="{{request()->is('company/create') ? "active" : ""}}"><a href="{{route('company.create')}}" class="menu-item">Create Company</a></li>--}}
{{--                        @endif--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endif--}}
{{--            @if(in_array('access-admin',getPermissions()) || in_array('department-view',getPermissions()) || in_array('department-create',getPermissions()) || in_array('department-edit',getPermissions()) || in_array('department-delete',getPermissions()))--}}
{{--                <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Manage Departments</span></a>--}}
{{--                    <ul class="menu-content">--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('department-view',getPermissions()) || in_array('department-edit',getPermissions()) || in_array('department-delete',getPermissions()))--}}
{{--                            <li class="{{request()->is('department/view') ? "active" : ""}}"><a href="{{route('department.view')}}" class="menu-item">View Departments</a></li>--}}
{{--                        @endif--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('department-create',getPermissions()))--}}
{{--                                <li class="{{request()->is('department/create') ? "active" : ""}}"><a href="{{route('department.create')}}" class="menu-item">Create Department</a></li>--}}
{{--                        @endif--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endif--}}
{{--            @if(in_array('access-admin',getPermissions()) || in_array('user-view',getPermissions()) || in_array('user-create',getPermissions()) || in_array('user-edit',getPermissions()) || in_array('user-delete',getPermissions()))--}}
{{--                <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Manage Users</span></a>--}}
{{--                    <ul class="menu-content">--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('user-view',getPermissions()) || in_array('user-edit',getPermissions()) || in_array('user-delete',getPermissions()))--}}
{{--                            <li class="{{request()->is('user/view') ? "active" : ""}}"><a href="{{route('user.view')}}" class="menu-item">View Users</a></li>--}}
{{--                        @endif--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('user-create',getPermissions()))--}}
{{--                                <li class="{{request()->is('user/create') ? "active" : ""}}"><a href="{{route('user.create')}}" class="menu-item">Create User</a></li>--}}
{{--                        @endif--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endif--}}
{{--            @if(in_array('access-admin',getPermissions()) || in_array('designation-view',getPermissions()) || in_array('designation-create',getPermissions()) || in_array('designation-edit',getPermissions()) || in_array('designation-delete',getPermissions()))--}}
{{--                <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Manage Designations</span></a>--}}
{{--                    <ul class="menu-content">--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('designation-view',getPermissions()) || in_array('designation-edit',getPermissions()) || in_array('designation-delete',getPermissions()))--}}
{{--                            <li class="{{request()->is('designation/view') ? "active" : ""}}"><a href="{{route('designation.view')}}" class="menu-item">View Designations</a></li>--}}
{{--                        @endif--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('designation-create',getPermissions()))--}}
{{--                                <li class="{{request()->is('designation/create') ? "active" : ""}}"><a href="{{route('designation.create')}}" class="menu-item">Create Designation</a></li>--}}
{{--                        @endif--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endif--}}
{{--            @if(in_array('access-admin',getPermissions()) || in_array('team-view',getPermissions()) || in_array('team-create',getPermissions()) || in_array('team-edit',getPermissions()) || in_array('team-delete',getPermissions()))--}}
{{--                <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Manage Teams</span></a>--}}
{{--                    <ul class="menu-content">--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('team-view',getPermissions()) || in_array('team-edit',getPermissions()) || in_array('team-delete',getPermissions()))--}}
{{--                            <li class="{{request()->is('team/view') ? "active" : ""}}"><a href="{{route('team.view')}}" class="menu-item">View Teams</a></li>--}}
{{--                        @endif--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('team-create',getPermissions()))--}}
{{--                            <li class="{{request()->is('team/create') ? "active" : ""}}"><a href="{{route('team.create')}}" class="menu-item">Create Team</a></li>--}}
{{--                        @endif--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endif--}}
{{--            @if(in_array('access-admin',getPermissions()) || in_array('project-view',getPermissions()) || in_array('project-type-view',getPermissions()) || in_array('project-create',getPermissions()) || in_array('project-type-create',getPermissions()) || in_array('project-edit',getPermissions()) || in_array('project-type-edit',getPermissions()) || in_array('project-delete',getPermissions()) || in_array('project-type-delete',getPermissions()))--}}
{{--                <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Manage Projects</span></a>--}}
{{--                    <ul class="menu-content">--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('project-view',getPermissions()) || in_array('project-edit',getPermissions()) || in_array('project-delete',getPermissions()) || in_array('project-create',getPermissions()))--}}
{{--                            <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Projects</span></a>--}}
{{--                                <ul class="menu-content">--}}
{{--                                    @if(in_array('access-admin',getPermissions()) || in_array('project-view',getPermissions()) || in_array('project-edit',getPermissions()) || in_array('project-delete',getPermissions()))--}}
{{--                                        <li class="{{request()->is('project/view') ? "active" : ""}}"><a href="{{route('project.view')}}" class="menu-item">View Projects</a></li>--}}
{{--                                    @endif--}}
{{--                                    @if(in_array('access-admin',getPermissions()) || in_array('project-create',getPermissions()))--}}
{{--                                        <li class="{{request()->is('project/create') ? "active" : ""}}"><a href="{{route('project.create')}}" class="menu-item">Create Project</a></li>--}}
{{--                                    @endif--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                        @endif--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('project-type-view',getPermissions()) || in_array('project-type-edit',getPermissions()) || in_array('project-type-delete',getPermissions()) || in_array('project-type-create',getPermissions()))--}}
{{--                            <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Project Types</span></a>--}}
{{--                                <ul class="menu-content">--}}
{{--                                    @if(in_array('access-admin',getPermissions()) || in_array('project-type-view',getPermissions()) || in_array('project-type-edit',getPermissions()) || in_array('project-type-delete',getPermissions()))--}}
{{--                                        <li class="{{request()->is('project/type/view') ? "active" : ""}}"><a href="{{route('project.type.view')}}" class="menu-item">View Project Types</a></li>--}}
{{--                                    @endif--}}
{{--                                    @if(in_array('access-admin',getPermissions()) || in_array('project-type-create',getPermissions()))--}}
{{--                                        <li class="{{request()->is('project/type/create') ? "active" : ""}}"><a href="{{route('project.type.create')}}" class="menu-item">Create Project Type</a></li>--}}
{{--                                    @endif--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                        @endif--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endif--}}
{{--            @if(in_array('access-admin',getPermissions()) || in_array('milestone-view',getPermissions()) || in_array('milestone-create',getPermissions()) || in_array('milestone-edit',getPermissions()) || in_array('milestone-delete',getPermissions()))--}}
{{--                <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Manage Milestones</span></a>--}}
{{--                    <ul class="menu-content">--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('milestone-view',getPermissions()) || in_array('milestone-edit',getPermissions()) || in_array('milestone-delete',getPermissions()))--}}
{{--                            <li class="{{request()->is('milestone/view') ? "active" : ""}}"><a href="{{route('milestone.view')}}" class="menu-item">View Milestones</a></li>--}}
{{--                        @endif--}}
{{--                        @if(in_array('access-admin',getPermissions()) || in_array('milestone-create',getPermissions()))--}}
{{--                            <li class="{{request()->is('milestone/create') ? "active" : ""}}"><a href="{{route('milestone.create')}}" class="menu-item">Create Milestone</a></li>--}}
{{--                        @endif--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endif--}}
{{--            @if(in_array('access-admin',getPermissions()) || in_array('attendence-view',getPermissions()) || in_array('attendence-view',getPermissions()))--}}
{{--                <li class="nav-item {{request()->is('attendence/view') ? "active" : ""}}"><a href="{{route('attendence.view')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Attendence </span></a></li>--}}
{{--            @endif--}}
{{--            @if(in_array('access-admin',getPermissions()) || in_array('report-view',getPermissions()))--}}
{{--                <li class="nav-item {{request()->is('report/view') ? "active" : ""}}"><a href="{{route('report.view')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">View Report </span></a></li>--}}
{{--            @endif--}}
{{--            @if(getGroupByUserId()->name == "team-lead")--}}
{{--                <li class="nav-item {{request()->is('myteam') ? "active" : ""}}"><a href="{{route('my.team')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">My Team </span></a></li>--}}
{{--            @endif--}}

{{--            @if(getGroupByUserId()->name == "employee")--}}
{{--                <li class="nav-item {{request()->is('milestone/view-my-milestone') ? "active" : ""}}"><a href="{{route('milestone.view.my.milestone')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">View My Milestone </span></a></li>--}}
{{--            @endif--}}
{{--            @if(in_array('open-project',getPermissions()))--}}
{{--                <li class="nav-item {{request()->is('openproject') ? "active" : ""}}"><a href="{{route('open.project')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Open Project </span></a></li>--}}
{{--            @endif--}}
{{--                <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Manage Departments</span></a>--}}
{{--                    <ul class="menu-content">--}}
{{--                        <li class="{{request()->is('department/view') ? "active" : ""}}"><a href="{{route('department.view')}}" class="menu-item">View Departments</a></li>--}}
{{--                        <li class="{{request()->is('department/create') ? "active" : ""}}"><a href="{{route('department.create')}}" class="menu-item">Create Department</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class=" nav-item"><a href="#"><i class="ft-monitor"></i><span data-i18n="" class="menu-title">Manage Teams</span></a>--}}
{{--                    <ul class="menu-content">--}}
{{--                        <li class="{{request()->is('team/view') ? "active" : ""}}"><a href="{{route('team.view')}}" class="menu-item">View Teams</a></li>--}}
{{--                        <li class="{{request()->is('team/create') ? "active" : ""}}"><a href="{{route('team.create')}}" class="menu-item">Create Team</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
        </ul>
    </div>
</div>

@yield('content')
<!-- ////////////////////////////////////////////////////////////////////////////-->


<footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2 content"><span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span></p>
</footer>

<!-- BEGIN VENDOR JS-->
<script src="{{asset('app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/colors/palette-climacon.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/fonts/simple-line-icons/style.min.css')}}">
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{asset('app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/js/core/app.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
@stack('scripts')
<!-- END STACK JS-->
</body>

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-menu-template-light/dashboard-analytics.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 18 Jan 2018 16:19:56 GMT -->
</html>
