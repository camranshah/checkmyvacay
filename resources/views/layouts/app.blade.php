<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ isset($Title) ? $Title.' - '.config('app.name') : config('app.name') }}</title>
    <link rel="shortcut icon" href="{{ asset('custom/images/favicon.png') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/app-assets/css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/app-assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('custom/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('custom/app-assets/vendors/css/forms/selects/select2.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('custom/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/assets/css/responsive.css') }}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
          integrity="sha256-uKEg9s9/RiqVVOIWQ8vq0IIqdJTdnxDMok9XhiqnApU=" crossorigin="anonymous"/>
    <link href="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css"
          rel="stylesheet" type="text/css"/>
</head>
<body class="vertical-layout vertical-menu 2-columns menu-expanded @auth {{ 'fixed-navbar'}} @endauth" data-open="click"
      data-menu="vertical-menu" data-col="2-columns">

@auth
    @include('includes.header')
    @include('includes.nav')
@endauth

@yield('content')

<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<script src="{{ asset('custom/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('custom/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('custom/app-assets/js/scripts/tables/datatables/datatable-basic.js') }}"
        type="text/javascript"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="{{ asset('custom/app-assets/vendors/js/extensions/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('custom/app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>
<script src="{{ asset('custom/app-assets/js/core/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('custom/app-assets/js/scripts/customizer.js') }}" type="text/javascript"></script>
<script src="{{ asset('custom/app-assets/js/scripts/modal/components-modal.js') }}" type="text/javascript"></script>
<script src="{{ asset('custom/app-assets/vendors/js/forms/select/select2.full.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('custom/app-assets/vendors/js/charts/raphael-min.js') }}"></script>
<script src="{{ asset('custom/app-assets/vendors/js/charts/morris.min.js') }}"></script>
<script src="{{ asset('custom/app-assets/js/scripts/charts/morris/smooth-area.min.js') }}"></script>
<script src="{{ asset('custom/app-assets/js/scripts/forms/select/form-select2.js') }}" type="text/javascript"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"
        integrity="sha256-tQ3x4V2JW+L0ew/P3v2xzL46XDjEWUExFkCDY0Rflqc=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script
    src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
        integrity="sha256-Kg2zTcFO9LXOc7IwcBx1YeUBJmekycsnTsq2RuFHSZU=" crossorigin="anonymous"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script>

<script
    src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<script src="http://dev68.onlinetestingserver.com:7010/socket.io/socket.io.js"></script>
<script type="text/javascript">

    function toTimestamp(strDate) {
        let datum = Date.parse(strDate);
        return datum / 1000;
    }

    function ajaxFormSubmit(formId, messageId) {

        var message = $('#' + messageId);
        var form_data = new FormData();
        var other_data = $('#' + formId).serializeArray();
        $.each(other_data, function (key, input) {
            form_data.append(input.name, input.value);
        });

        if ($('.cm-change-image').length) {
            $.each($('.cm-change-image'), function (key, input) {
                var ID = $(this).attr('id');
                var name = $(this).attr('name');
                var leng = document.getElementById(ID).files.length;
                if (leng == 1) {
                    form_data.append(name, document.getElementById(ID).files[0]);
                }
            });
        }

        var formUrl = $('#' + formId).attr('action');

        $.ajax({
            url: formUrl, // point to server-side PHP script
            dataType: 'JSON', // what to expect back from the PHP script
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'POST',
            beforeSend: function () {

            },
            complete: function () {

            },
            success: function (data) {
                console.log("SUCCESS")
                console.log(data);

            },
            error: function (err) {
                console.log("ERROR")
                console.log(err)
            }
        });
    }

    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.cm-input-mask-number').mask('000000');
        $('.cm-input-mask-percent').mask('00.00');
        $('.cm-input-mask-cvv').mask('000');
        $('.cm-input-mask-phone-us').mask('(000) 000-0000');
        $('.cm-input-mask-credit-card-expiry-date').mask('00/0000');
        $('.cm-input-mask-credit-card-number').mask('0000-0000-0000-0000');


        // $.validate({
        //     modules: 'location, date, security, file',
        //     form: '#cm-form-create-banner'
        // });

        $(".dataTables_filter input").attr("placeholder", "Search");
        $('#select-date').datepicker({
            uiLibrary: 'bootstrap4'
        });
        $('#select-date2').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('.cm-status-toggle').change(function (e) {

            let status = ($(this).prop('checked')) ? 'active' : 'inactive';
            let entityId = $(this).attr('data-entityId');
            let url = $(this).attr('data-url');

            $.ajax({
                url: url,
                method: 'post',
                data: {
                    'status': status,
                    'id': entityId,
                },
                success: function (response) {

                }

            });
        })

        $('#fromSelector').datepicker({
            uiLibrary: 'bootstrap4',
            onSelect: function () {
                table.draw();
            },
            changeMonth: true,
            changeYear: true
        });

        $('#toSelector').datepicker({
            uiLibrary: 'bootstrap4',
            onSelect: function () {
                table.draw();
            },
            changeMonth: true,
            changeYear: true
        });

        var FilterIndex = 1;

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                let min = $('#fromSelector').val() != '' ? toTimestamp($('#fromSelector').val()) : null;
                let max = $('#toSelector').val() != '' ? toTimestamp($('#toSelector').val()) : null;
                let startDate = moment(data[FilterIndex], 'MM/DD/YYYY').format('MM/DD/YYYY');
                startDate = toTimestamp(startDate);
                if (min == null && max == null) return true;
                if (min == null && startDate <= max) return true;
                if (max == null && startDate >= min) return true;
                if (startDate <= max && startDate >= min) return true;
                return false;
            }
        );

        // Event listener to the two range filtering inputs to redraw on input
        $('#fromSelector, #toSelector').change(function () {
            $('.zero-configuration').DataTable().draw();
        });

        $('.cm-radio-filter').change(function(){
            FilterIndex = $(this).val()
            $('.zero-configuration').DataTable().draw();
        })

        $('.cm-tags-input').tagsinput();

        $('.cm-btn-export').click("click", function() {
            $('.buttons-csv').click()
        })

    });
</script>

@auth
    <script>

        $(document).on('click', '.cm-notif-bell-icon', function (e) {

            e.preventDefault()

            $('.cm-notif-bell-icon-count').hide()

            $.ajax({
                url: "{{ route('notification.update') }}",
                method: 'post',
            });

        });

        let socket = io('http://dev68.onlinetestingserver.com:7010');

        var notificationChannel = "{{ 'check_my_fares_express_database_notification.'.Auth::user()->id }}";

        socket.emit('subscribe', {
            channel: notificationChannel
        }).on('App\\Events\\NotificationEvent', function (channel, data) {
            console.log(data)
            if (data.status == 'success') {
                $('.cm-div-notif').html(data.view)
            }
        });
    </script>
@endauth

@yield('script')

</body>
</html>
