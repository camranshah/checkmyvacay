@extends('layout2')
@section('title','CMV - Login')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body"><section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-md-4 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <div class="p-1"><img src="{{asset('app-assets/images/logo/stack-logo-dark.png')}}" alt="branding logo"></div>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Login with Stack</span></h6>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        @if(Session::has('type'))
                                            <div class="alert alert-{{Session::get('type')}}">
                                                <p>{{Session::get('msg')}}</p>
                                            </div>
                                        @endif
                                        {!! Form::open(['route' => 'login','method' => 'POST','class' => 'form-horizontal form-simple']) !!}
                                        <fieldset class="form-group position-relative has-icon-left mb-0">
                                            {!! Form::email('email','',['class' => 'form-control form-control-lg input-lg','placeholder' => 'Your Email']) !!}
                                            <div class="form-control-position">
                                                <i class="ft-user"></i>
                                            </div>
                                            @if($errors->login->first('email'))
                                                <p class="badge badge-danger" style="margin-top: 10px;">{{ $errors->login->first('email') }}</p>
                                            @endif
                                        </fieldset>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            {!! Form::password('password',['class' => 'form-control form-control-lg input-lg','placeholder' => 'Your Password']) !!}
                                            <div class="form-control-position">
                                                <i class="fa fa-key"></i>
                                            </div>
                                            @if($errors->login->first('password'))
                                                <p class="badge badge-danger" style="margin-top: 10px;">{{ $errors->login->first('password') }}</p>
                                            @endif
                                        </fieldset>
                                        <div class="form-group row">
                                            <div class="col-md-6 col-12 text-center text-md-left">
                                                <fieldset>
                                                    <input type="checkbox" id="remember-me" class="chk-remember">
                                                    <label for="remember-me"> Remember Me</label>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6 col-12 text-center text-md-right"><a href="{{route('forget.password')}}" class="card-link">Forgot Password?</a></div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Login</button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection
