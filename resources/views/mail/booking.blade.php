@component('mail::message')
    <div class="row mt-2">
        <div class="col-12">
            <div class="tkt-ban-top">
                <label class="green-label">Status
                    :<span> {{ @$BookedFlight->flight_details['bookingStatus'] }} </span></label>
                <label>Booking ref
                    :<span> {{ @$BookedFlight->flight_details['bookingId'] }}</span></label>
            </div>
            <div class="tkt-banner-main">
                <div class="tkt-banner">
                    <div class="row">
                        <div class="col-12">
                            <div class="tkt-box">

                                @php
                                    $airportDepartureTop = getAirportDetails($BookedFlight->flight_details['originDepartureLocationCode']);
                                @endphp

                                <div class="tkt-txt">
                                    <h3>{{ @$airportDepartureTop->city }}</h3>
                                    <h5>{{ @$airportDepartureTop->iata.', '.@$airportDepartureTop->country }}</h5>
                                    <h3>{{ @date('H:i A',strtotime($BookedFlight->flight_details['originDepartureDateTime'])) }}</h3>
                                    <h5>{{ @date('M d, Y',strtotime($BookedFlight->flight_details['originDepartureDateTime'])) }}</h5>
                                </div>
                                <div class="tkt-img-box text-center">
                                    <img
                                        src="{{ asset('custom/images/flight-stop.png') }}"
                                        alt="">
                                    <h5>{{ @$BookedFlight->flight_details['totalStop']}}
                                        stop(s)</h5>
                                </div>

                                @php
                                    $airportArrivalTop = getAirportDetails($BookedFlight->flight_details['originArrivalLocationCode']);
                                @endphp

                                <div class="tkt-txt">
                                    <h3>{{ @$airportArrivalTop->city }}</h3>
                                    <h5>{{ @$airportArrivalTop->iata.', '.@$airportArrivalTop->country }}</h5>

                                    <h3>{{ @date('H:i A',strtotime($BookedFlight->flight_details['originArrivalDateTime'])) }}</h3>
                                    <h5>{{ @date('M d, Y',strtotime($BookedFlight->flight_details['originArrivalDateTime'])) }}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tkt-bottom">
                    <div class="tkt-box">
                        <div class="media align-items-center">
                            <img src="{{ asset('custom/images/flight-icon.png') }}"
                                 class="mr-2">
                            <h5>{{ @$BookedFlight->flight_details['originDepartureAirlineCode'].' '.$BookedFlight->flight_details['originDepartureFlightNo']}}</h5>
                        </div>
                        <h5>{{ $BookedFlight->flight_details['currencyCode'].' '.$BookedFlight->flight_details['totalFlightAmount'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endcomponent
