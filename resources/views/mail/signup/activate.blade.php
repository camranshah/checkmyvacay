@component('mail::message')
    Hello <b>{{ $User->profile->first_name }}</b>,

    Thanks for signup! Please before you begin, you must confirm your account.

    Your activation code is: {{ $Code }}

    Thank you for using our application!

    {!! config('app.name') !!}

@endcomponent
