@extends('layouts.app', ['Title' => 'Notifications'])

@section('content')
    <div class="app-content content view user all-notifications">
        <div class="content-wrapper">
            <div class="content-body">
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1 col-12">
                            <div class="add-user">
                                <div class="card rounded pad-20">
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h1 class="pull-left"><a href="a-users.html" class="back"></a>Notifications
                                                    </h1>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    @if(count($Notifications))
                                                        @foreach($Notifications as $notification)
                                                            <div class="noti-inner-cards mb-3">
                                                                <div class="card mt-3">
                                                                    <div class="notification-title">
                                                                        <h5>{{ $notification->title }}</h5>
                                                                        @if($notification->url)
                                                                            <a href="{{ url($notification->url) }}">View</a>
                                                                        @endif
                                                                    </div>
                                                                    <div class="noti-content">
                                                                        <div class="media align-items-center">
                                                                            <i class="fa fa-bell-o"
                                                                               aria-hidden="true"></i>
                                                                            <div class="media-body">
                                                                                <p>{{ $notification->description }}</p>

                                                                            </div>
                                                                            <h5>{{ $notification->time_ago }}</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <p>Notification not available!</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
