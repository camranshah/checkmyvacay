@extends('layouts.app', ['Title' => 'Payment Log'])

@section('content')
    <div class="app-content content view user">
        <div class="content-wrapper">
            <div class="content-body">
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Payment Log</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2 filter-main justify-content-between">
                                            <div class="col-12">
                                                <label>Sort By:</label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6  form-group">
                                                        <label for="">From:</label>
                                                        <input id="fromSelector" type="text" readonly="">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6  form-group">
                                                        <label for="">To:</label>
                                                        <input id="toSelector" type="text" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row maain-tabble mt-3">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>Booked On</th>
                                                    <th>User</th>
                                                    <th>Booking ID</th>
                                                    <th>Amount (Before Discount)</th>
                                                    <th>Discount Applicable</th>
                                                    <th>Amount (After Discount)</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($PaymentLog as $key => $value)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ $value->created_at }}</td>
                                                        <td>
                                                            <a href="{{ route('user.show',['id' => $value->booking->user_id]) }}"><u>{{ $value->booking->user->profile->first_name }}</u></a>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('booked-flight.show',['id' => $value->booking->id]) }}"><u>{{ $value->booking->id}}</u></a>
                                                        </td>
                                                        <td>{{ $value->booking->fare }}</td>
                                                        <td>{{ $value->booking->discount }}</td>
                                                        <td>{{ $value->fares }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
