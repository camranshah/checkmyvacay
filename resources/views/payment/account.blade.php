@extends('layout')
@section('title','CMV - View Account Payments')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-1">
                    <h3 class="content-header-title">View Account Payments</h3>
                </div>
                <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">View Account Payments
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <section class="card">
                <div class="content-body"><!-- Table row borders end-->
                    <!-- Double border end-->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="table-responsive">
                                        <table class="table mb-0 table-bordered">
                                            <thead>
                                            <tr class="border-double">
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Payment Date</th>
                                                <th>Amount</th>
                                                <th>Transaction ID</th>
                                                <th>End Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($accounts as $account)
                                                @php($i = 0)
                                                @foreach($account as $user)
                                                    @if($i == 0)
                                                        <tr>
                                                            <td valign="bottom" rowspan="{{count($account)}}">{{ucwords($user->user['profile']['first_name']." ".$user->user['profile']['last_name'])}}</td>
                                                            <td valign="bottom" rowspan="{{count($account)}}">{{$user->user['email']}}</td>
                                                            <td valign="bottom" rowspan="{{count($account)}}">{{$user->user['profile']['phone']}}</td>
                                                            <td>{{date('F d Y',strtotime($user->created_at))}}</td>
                                                            <td>{{$user->transaction_id}}</td>
                                                            <td>{{number_format($user->amount,2,'.',',')}}</td>
                                                            <td>{{date('F d Y',strtotime($user->end_date))}}</td>
                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td>{{date('F d Y',strtotime($user->created_at))}}</td>
                                                            <td>{{number_format($user->amount,2,'.',',')}}</td>
                                                            <td>{{$user->transaction_id}}</td>
                                                            <td>{{date('F d Y',strtotime($user->end_date))}}</td>
                                                        </tr>
                                                    @endif
                                                    @php($i++)
                                                @endforeach
{{--                                                <tr>--}}
{{--                                                    <td>{{$account}}</td>--}}
{{--                                                    <td>{{$user->email}}</td>--}}
{{--                                                    <td>{{$user->profile['phone']}}</td>--}}
{{--                                                    <td>{{date('F d Y h:i a',strtotime($user->created_at))}}</td>--}}
{{--                                                    <td>--}}
{{--                                                        <a href="{{route('user.view',$user->id)}}" class="btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="View Detail"><i class="fa fa-eye"></i></a>--}}
{{--                                                        <a href="{{route('user.change-status',$user->id)}}" class="btn-sm btn-{{$user->status == 1 ? "danger" : "success"}}" data-toggle="tooltip" data-placement="top" title="{{$user->status == 1 ? "Deactivate User" : "Activate User"}}"><i class="fa fa-{{$user->status == 1 ? "ban" : "check"}}"></i></a>--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Double border end -->
                </div>
            </section>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection
