@extends('layout')
@section('title','CMV - View Booking Payments')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-1">
                    <h3 class="content-header-title">View Booking Payments</h3>
                </div>
                <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">View Booking Payments
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <section class="card">
                <div class="content-body"><!-- Table row borders end-->
                    <!-- Double border end-->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="table-responsive">
                                        <table class="table mb-0 table-bordered">
                                            <thead>
                                            <tr class="border-double">
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Payment Date</th>
                                                <th>Fare Amount</th>
                                                <th>Transaction ID</th>
                                                <th>Currency Code</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($bookings as $booking)
                                                <tr>
                                                    <td>{{ucwords($booking->booking['user']['profile']['first_name']." ".$booking->booking['user']['profile']['last_name'])}}</td>
                                                    <td>{{$booking->booking['user']['email']}}</td>
                                                    <td>{{date('F d Y',strtotime($booking->created_at))}}</td>
                                                    <td>{{number_format($booking->fares,2,'.',',')}}</td>
                                                    <td>{{$booking->transaction_id}}</td>
                                                    <td>{{$booking->currency_code}}</td>
                                                    <td>
                                                        <a href="{{route('booking.view',$booking->booking_id)}}" class="btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="View Booking Detail"><i class="fa fa-eye"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Double border end -->
                </div>
            </section>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection
