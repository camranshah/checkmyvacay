@extends('layouts.app', ['Title' => 'Dashboard'])

@section('script')
    <script>
        $(document).ready(function () {
            $('#cm-form-change-password').on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let formData = form.serialize();
                let action = form.attr('action')

                $.LoadingOverlay("show");

                $.ajax({
                    url: action,
                    method: 'post',
                    data: formData,
                    success: function (response) {

                        $.LoadingOverlay("hide");

                        $('.cm-flash-message').html('');

                        if (response.message) {
                            $.each(response.message, function (key, value) {
                                $('.cm-flash-message').show();
                                $('.cm-flash-message').append("<div class='alert " + response.class + "'>" + value + "</div>");
                                $.when($('.cm-flash-message').delay(5000).fadeOut(400)).done(function () {
                                    if (response.status) {
                                        $('#cm-modal-change-password').modal('hide')
                                        form[0].reset();
                                    }
                                });
                            });
                        }
                    }

                });
            })
        })
    </script>
@endsection

@section('content')
    <div class="app-content content view user profile">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Profile</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="attached">
                                                    <img src="{{ asset('custom/images/student-pro-girl.png') }}"
                                                         class="img-full" alt="">
                                                    <h2>{{ __('Admin') }}</h2>
                                                    <p>{{ @$User->email }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-12">
                                                <div class="col-12 form-group">
                                                    <a href="javascript:void(0)" data-toggle="modal"
                                                       data-target="#cm-modal-change-password">Change
                                                        Password</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!--modal  start here-->
                                        <div class="login-fail-main user">
                                            <div class="featured inner">
                                                <div class="modal fade bd-example-modal-lg"
                                                     id="cm-modal-change-password"
                                                     tabindex="-1" role="dialog"
                                                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form id='cm-form-change-password'
                                                                  action="{{ route('profile.update') }}" method="post">
                                                                @csrf
                                                                <div class="payment-modal-main">
                                                                    <div class="payment-modal-inner">
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <h1>Change Password</h1>
                                                                                <div class="cm-flash-message"
                                                                                     style="display:none"></div>
                                                                                <div class="col-12 form-group">
                                                                                    <label
                                                                                        for="">{{ __('Current Password*') }}</label>
                                                                                    <input type="password"
                                                                                           class="form-control"
                                                                                           spellcheck="true"
                                                                                           name="current_password"
                                                                                           placeholder="Enter Current Password"
                                                                                           required>
                                                                                </div>
                                                                                <div class="col-12 form-group">
                                                                                    <label
                                                                                        for="">{{ __('New Password*') }}</label>
                                                                                    <input type="password"
                                                                                           class="form-control"
                                                                                           spellcheck="true"
                                                                                           name="password"
                                                                                           placeholder="Enter New Password"
                                                                                           required>
                                                                                </div>
                                                                                <div class="col-12 form-group">
                                                                                    <label
                                                                                        for="">{{ __('Retype Password*') }}</label>
                                                                                    <input type="password"
                                                                                           class="form-control"
                                                                                           spellcheck="true"
                                                                                           name="password_confirmation"
                                                                                           placeholder="Retype Password"
                                                                                           required>
                                                                                </div>
                                                                                <div
                                                                                    class="col-12 form-group text-center">
                                                                                    <button type="submit" class="can">
                                                                                        save
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--modal end here-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection
