@extends('layouts.app', ['Title' => 'Create Promo Code'])

@section('script')
    <script>

        function RunSelect2(){
            $('.cm-select-users').select2({
                allowClear: true,
                closeOnSelect: false,
            }).on('select2:open', function() {

                setTimeout(function() {
                    $(".select2-results__option .select2-results__group").bind( "click", selectAlllickHandler );
                }, 0);

            });
        }

        RunSelect2();


        var selectAlllickHandler = function() {
            $(".select2-results__option .select2-results__group").unbind( "click", selectAlllickHandler );
            $('.cm-select-users').select2('destroy').find('option').prop('selected', 'selected').end();
            RunSelect2();
        };

    </script>
@endsection

@section('content')
    <div class="app-content content view user profile edit-profile create-banner">
        <div class="content-wrapper">
            <div class="content-body">
                @include('includes.flash-messages')
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">{{ $code->id? 'Edit': 'Create New' }} Promo Code</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-12">
                                                <form action="{{ $code->id? route('promo-code.updatePromoCode', [$code->id]): route('promo-code.store') }}" method="post">
                                                    @csrf
                                                    @if($code->id)
                                                        @method('put')
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="">Discount Code</label>
                                                            <input type="text" name="code" value="{{ old('code', $code->code) }}" placeholder="Discount Code" id="" class="form-control" maxlength="50">
                                                        </div>
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="">Promo Code Type</label>
                                                            <select name="type">
                                                                <option value="" disabled="">Select</option>
                                                                <option value="subscription" {{ old('type', $code->type) == 'subscription' ? 'selected' : '' }}>Subscription</option>
                                                                <option value="booking" {{ old('type', $code->type) == 'booking' ? 'selected' : '' }}>Booking</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="">Discount Type</label>
                                                            <select name="discount_type">
                                                                <option value="" disabled="">Select</option>
                                                                <option value="percentage" {{ old('discount_type', $code->discount_type) == 'percentage' ? 'selected' : '' }}>Percentage</option>
                                                                <option value="fixed" {{ old('discount_type', $code->discount_type) == 'fixed' ? 'selected' : '' }}>Fixed</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="">Discount Amount</label>
                                                            <input type="text" name="discount_value" value="{{ old('discount_value', $code->discount_value) }}" placeholder="Discount Amount" id="cm-discount-amount" class="form-control cm-input-mask-percent">
                                                        </div>
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="">Valid Till</label>
                                                            <select name="end_date">
                                                                <option value="" disabled="">Select</option>
                                                                <option value="+7 day" {{ old('end_date', $code->valid_for_raw) == '+7 day' ? 'selected' : '' }}>7 days</option>
                                                                <option value="+1 months" {{ old('end_date', $code->valid_for_raw) == '+1 months' ? 'selected' : '' }}>1 month</option>
                                                                <option value="+2 months" {{ old('end_date', $code->valid_for_raw) == '+2 months' ? 'selected' : '' }}>2 months</option>
                                                                <option value="+3 months" {{ old('end_date', $code->valid_for_raw) == '+3 months' ? 'selected' : '' }}>3 months</option>
                                                                <option value="+4 months" {{ old('end_date', $code->valid_for_raw) == '+4 months' ? 'selected' : '' }}>4 months</option>
                                                                <option value="+5 months" {{ old('end_date', $code->valid_for_raw) == '+5 months' ? 'selected' : '' }}>5 months</option>
                                                                <option value="+6 months" {{ old('end_date', $code->valid_for_raw) == '+6 months' ? 'selected' : '' }}>6 months</option>
                                                                <option value="+7 months" {{ old('end_date', $code->valid_for_raw) == '+7 months' ? 'selected' : '' }}>7 months</option>
                                                                <option value="+8 months" {{ old('end_date', $code->valid_for_raw) == '+8 months' ? 'selected' : '' }}>8 months</option>
                                                                <option value="+9 months" {{ old('end_date', $code->valid_for_raw) == '+9 months' ? 'selected' : '' }}>9 months</option>
                                                                <option value="+10 months" {{ old('end_date', $code->valid_for_raw) == '+10 months' ? 'selected' : '' }}>10 months</option>
                                                                <option value="+11 months" {{ old('end_date', $code->valid_for_raw) == '+11 months' ? 'selected' : '' }}>11 months</option>
                                                                <option value="+12 months" {{ old('end_date', $code->valid_for_raw) == '+12 months' ? 'selected' : '' }}>12 months</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="">Refer Tag</label>
                                                            <input type="text" name="refer_tag" value="{{ old('refer_tag', $code->code) }}" placeholder="Refer Tag" id="" class="form-control" maxlength="50">
                                                        </div>
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="">Select Users</label>
                                                            <select name="users[]" class="cm-select-users form-control" multiple="multiple" data-validation="required" data-validation-error-msg="Please select users">
                                                                <optgroup label="select all">
                                                                    @foreach(\App\User::whereHas('user_role', function ($q) {$q->whereRoleId(2);})->get() as $user)
                                                                        <option value="{{ $user->id }}">{{ @$user->profile->first_name }}</option>
                                                                    @endforeach
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <label for="guest_user">All Users Including Guest Users</label>
                                                            <input id="guest_user" type="checkbox" style="height:auto" name="guest_user" {{ old('guest_user', $code->guest_user)? 'checked': '' }} value="1">
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-4 col-12 form-group">
                                                            <button type="submit">Create</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
