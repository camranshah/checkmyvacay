@extends('layouts.app', ['Title' => 'Promo Code Details'])

@section('content')
    <div class="app-content content view user ">
        <div class="content-wrapper">
            <div class="content-body">
                @include('includes.flash-messages')
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Promo Code Details</h1>
                                                </div>
                                                <div class="float-right">
                                                    <a class="btn btn-primary float-right" href="{{ route('promo-code.edit', [$PromoCode->id]) }}">Edit</a>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2 profile u-profile">
                                            <div class="col-12">
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Discount Code</label>
                                                            <input type="text" value="{{ $PromoCode->code }}"
                                                                   placeholder="Discount Code" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Promo Code Type</label>
                                                            <input type="text" value="{{ $PromoCode->type }}"
                                                                   placeholder="Promo Code Type"
                                                                   id="" class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Discount Type</label>
                                                            <input type="text" value="{{ $PromoCode->discount_type }}"
                                                                   placeholder="Discount Type" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Discount Amount</label>
                                                            <input type="text" value="{{ $PromoCode->discount_value }}"
                                                                   placeholder="Discount Amount"
                                                                   id="" class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Valid Till</label>
                                                            <input type="text" value="{{ $PromoCode->end_date }}"
                                                                   placeholder="Valid Till" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Refer Tag</label>
                                                            <input type="text" value="{{ $PromoCode->refer_tag }}"
                                                                   placeholder="Refer Tag" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        @if($PromoCode->user_promo_codes->isNotEmpty())
                                                            <div class="col-md-6 col-sm-6 col-12 form-group">
                                                                <label for="">users</label>
                                                                @foreach($PromoCode->user_promo_codes as $key)
                                                                    <span
                                                                        class="badge badge-pill badge-secondary">{{ @$key->user->profile->first_name }}</span>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="seperator"></div>
                                            </div>
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Usage Log</h1>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-2 filter-main justify-content-between">
                                            <div class="col-12">
                                                <label>Sort By:</label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-6 col-sm-6  form-group">
                                                        <label for="">From:</label>
                                                        <input id="fromSelector" type="text" readonly>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6 col-sm-6  form-group">
                                                        <label for="">To:</label>
                                                        <input id="toSelector" type="text" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row maain-tabble mt-2">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>Date Used</th>
                                                    <th>User</th>
                                                    <th>Booking ID</th>
                                                    <th>Flight No.</th>
                                                    <th>Departure From</th>
                                                    <th>Total Flight Amount</th>
                                                    <th>Flight Amount After Discount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($PromoCode->bookings as $key => $value)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ $value->created_at }}</td>
                                                        <td>
                                                            <a href="{{ route('user.show',['id' => $value->user_id]) }}"><u>{{ $value->user->profile->first_name }}</u></a>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('booked-flight.show',['id' => $value->id]) }}"><u>{{ $value->id}}</u></a>
                                                        </td>
                                                        <td>{{ $value->flight_number }}</td>
                                                        <td></td>
                                                        <td>{{ $value->fare }}</td>
                                                        <td>{{ $value->fare - $value->discount }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
