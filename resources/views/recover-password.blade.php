@extends('layout2')
@section('title','CMV - Recover Password')
@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-md-4 col-10 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                            <div class="card-header border-0 pb-0">
                                <div class="card-title text-center">
                                    <img src="{{asset('app-assets/images/logo/stack-logo-dark.png')}}" alt="branding logo">
                                </div>
                                <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>We will send you a link to reset password.</span></h6>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    @if(Session::has('type'))
                                        <div class="alert alert-{{Session::get('type')}}">
                                            <p>{{Session::get('msg')}}</p>
                                        </div>
                                    @endif
                                    {!! Form::open(['route' => ['recover.password',$token],'method' => 'POST','class' => 'form-horizontal']) !!}
                                        <fieldset class="form-group position-relative has-icon-left">
                                            {!! Form::password('password',['class' => 'form-control form-control-lg input-lg','placeholder' => 'New Password']) !!}
                                            <div class="form-control-position">
                                                <i class="fa fa-lock"></i>
                                            </div>
                                            @if($errors->recover_password->first('password'))
                                                <p class="badge badge-danger" style="margin-top: 10px;">{{ $errors->recover_password->first('password') }}</p>
                                            @endif
                                        </fieldset>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            {!! Form::password('password_confirmation',['class' => 'form-control form-control-lg input-lg','placeholder' => 'Confirm Password']) !!}
                                            <div class="form-control-position">
                                                <i class="fa fa-lock"></i>
                                            </div>
                                        </fieldset>
                                        <button type="submit" class="btn btn-outline-primary btn-lg btn-block"><i class="ft-unlock"></i> change Password</button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="card-footer border-0">
                                <p class="float-sm-left text-center"><a href="{{route('home')}}" class="card-link">Login</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection
