@extends('layouts.app', ['Title' => 'Create Referral'])

@section('content')
    <div class="app-content content view user profile edit-profile create-banner">
        <div class="content-wrapper">
            <div class="content-body">
                @include('includes.flash-messages')
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Create Referral</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-12">
                                                <form action="{{ route('referral.store') }}" method="post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-sm-6 col-12 form-group">
                                                            <input type="text" name="code" value="{{ old('code') }}" placeholder="Enter Referral Code" id="" class="form-control" maxlength="30">
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-4 col-12 form-group">
                                                            <button type="submit">Create</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
