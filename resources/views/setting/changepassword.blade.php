@extends('layout')
@section('title','CMV - Change Password')
@section('content')
    <div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Change Password</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Change Password
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Form actions layout section start -->
            <section id="form-action-layouts">
                <div class="row justify-content-md-center">
                    <div class="col-md-12">
                        <div class="card">
                            {{--<div class="card-header">--}}
                            {{--<h4 class="card-title" id="from-actions-bottom-right">User Profile</h4>--}}
                            {{--<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>--}}
                            {{--<div class="heading-elements">--}}
                            {{--<ul class="list-inline mb-0">--}}
                            {{--<li><a data-action="collapse"><i class="ft-minus"></i></a></li>--}}
                            {{--<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>--}}
                            {{--<li><a data-action="expand"><i class="ft-maximize"></i></a></li>--}}
                            {{--<li><a data-action="close"><i class="ft-x"></i></a></li>--}}
                            {{--</ul>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="card-content collpase show">
                                <div class="card-body">
                                    @if(Session::has('type'))
                                        <div class="alert alert-icon-right alert-{{Session::get('type')}} alert-dismissible mb-2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <strong>{{Session::get('msg')}}</strong>
                                        </div>
                                    @endif
                                    {!! Form::open(['route' => ['changepassword'],'class' => 'form','method' => 'post']) !!}
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="ft-lock"></i> Password</h4>
                                        <div class="row">
                                            <div class="form-group col-12 mb-2">
                                                <label for="userinput5">Current Password</label>
                                                {!! Form::password('old_password',['class' => 'form-control border-primary','placeholder' => 'Current Password', 'required' => 'true']) !!}
                                                @if($errors->change_password->first('old_password'))
                                                    <p class="badge badge-danger" style="margin-top: 10px;">{{ $errors->change_password->first('old_password') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-12 mb-2">
                                                <label for="userinput6">New Password</label>
                                                {!! Form::password('password',['class' => 'form-control border-primary','placeholder' => 'New Password', 'required' => 'true']) !!}
                                                @if($errors->change_password->first('password'))
                                                    <p class="badge badge-danger" style="margin-top: 10px;">{{ $errors->change_password->first('password') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-12 mb-2">
                                                <label>Retype Password</label>
                                                {!! Form::password('password_confirmation',['class' => 'form-control border-primary','placeholder' => 'Retype Password', 'required' => 'true']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions right">
                                        <button type="button" class="btn btn-warning mr-1">
                                            <i class="ft-x"></i> Cancel
                                        </button>
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-check-square-o"></i> Save
                                        </button>
                                    </div>
                                    {!! Form::close() !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // Form actions layout section end -->
        </div>
    </div>
</div>
@endsection
