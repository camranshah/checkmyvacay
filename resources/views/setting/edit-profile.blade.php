@extends('layout')
@section('title','CMV - Edit Profile')
@section('content')
    <div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Edit Profile</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Edit Profile
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    @if(Session::has('msg'))
                                        <div class="alert alert-{{Session::get('type')}}">
                                            <p>{{Session::get('msg')}}</p>
                                        </div>
                                    @endif
                                        {!! Form::open(['route' => 'edit.profile','method' => 'post','class' => 'form','files' => true,'enctype' => 'multipart/form-data']) !!}
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-user"></i> Personal Info</h4>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Name</label>
                                                        {!! Form::text('name',$user->name,['class' => 'form-control','placeholder' => 'Name']) !!}
                                                        @if($errors->edit_profile->first('name'))
                                                            <p class="badge badge-danger" style="margin-top: 10px;">{{ $errors->edit_profile->first('name') }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="eventInput1">Email </label>
                                                        {!! Form::email('email',$user->email,['class' => 'form-control','placeholder' => 'Email']) !!}
                                                        @if($errors->edit_profile->first('email'))
                                                            <p class="badge badge-danger" style="margin-top: 10px;">{{ $errors->edit_profile->first('email') }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <button type="button" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-check-square-o"></i> Save
                                            </button>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // Basic form layout section end -->
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyDamIuNTXON7fh1TMdf5ik2J0bfQdrdo&libraries=places&callback=initAutocomplete" async defer></script>
<script>
    var autocomplete;

    var componentForm = {
        country: 'long_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('autocomplete'), {types: ['geocode']});

        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();

        // document.getElementById('longitude').value = lng;
        // document.getElementById('latitude').value = lat;

        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }
    </script>
@endpush
