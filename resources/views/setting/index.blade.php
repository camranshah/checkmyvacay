@extends('layouts.app', ['Title' => 'Subscription Amount Management'])

@section('content')
    <div class="app-content content view user profile edit-profile create-banner sub-amount">
        <div class="content-wrapper">
            <div class="content-body">
                @include('includes.flash-messages')
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Subscription Amount Management</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-12">
                                                <form action="{{ route('setting.update') }}" method="post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <p>Please configure subscription amount for all the
                                                                users</p>
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <h3>Annual Subscription Amount:</h3>
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <label for="">Current Amount</label>
                                                            <input type="text"
                                                                   value="{{ @$Settings->subscription_amount }}"
                                                                   placeholder="Create Amount" id=""
                                                                   class="form-control" disabled>
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <label for="">New Amount</label>
                                                            <input type="number"
                                                                   name="subscription_amount"
                                                                   placeholder="New Amount" id=""
                                                                   class="form-control">
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-4 col-12 form-group">
                                                            <button type="submit">Save</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
