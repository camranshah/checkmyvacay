@extends('layouts.app', ['Title' => 'User Booking Log'])

@section('content')
    <div class="app-content content view user ">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">User Profile</h1>
                                                </div>
                                                <div class="right">
                                                    <a href="{{ route('user.show',['id' => $User->id]) }}">Go To
                                                        Profile</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2 profile u-profile">
                                            <div class="col-12">
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Title</label>
                                                            <input type="text" value="{{ $User->profile->title }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">First Name</label>
                                                            <input type="text" value="{{ $User->profile->first_name }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Middle Name</label>
                                                            <input type="text" value="{{ $User->profile->middle_name }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Last Name</label>
                                                            <input type="text" value="{{ $User->profile->last_name }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Email Address</label>
                                                            <input type="Email" value="{{ $User->email }}"
                                                                   placeholder=""
                                                                   id="" class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Phone Number</label>
                                                            <input type="text"
                                                                   value="{{ $User->profile->phone_code.$User->profile->phone }}"
                                                                   placeholder="Phone Number"
                                                                   id="" class="form-control" disabled="">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="seperator"></div>
                                            </div>
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Booking Log</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <ul class="nav nav-tabs nav-underline no-hover-bg">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="base-tab31" data-toggle="tab"
                                                           aria-controls="tab31" href="#tab31" aria-expanded="true">Booked
                                                            Flights</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="base-tab32" data-toggle="tab"
                                                           aria-controls="tab32" href="#tab32" aria-expanded="false">Completed
                                                            Flights</a>
                                                    </li>
                                                </ul>
                                                <div class="row mt-2 filter-main justify-content-between">
                                                    <div class="col-12">
                                                        <label>Sort By:</label>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-6 col-sm-6  form-group">
                                                                <label for="">From:</label>
                                                                <input id="fromSelector" type="text"
                                                                       disabled="">
                                                            </div>
                                                            <div class="col-lg-3 col-md-6 col-sm-6  form-group">
                                                                <label for="">To:</label>
                                                                <input id="toSelector" type="text"
                                                                       disabled="">
                                                            </div>
                                                            <div
                                                                class="col-lg-6 col-md-12 d-flex align-items-center">
                                                                <div class="sort-box">
                                                                    <ul>
                                                                        <li>
                                                                            <label class="radio-container">Booked
                                                                                On
                                                                                <input class="cm-radio-filter"
                                                                                       type="radio"
                                                                                       checked="checked"
                                                                                       name="radio" value="3">
                                                                                <span class="checkmark"></span>
                                                                            </label>
                                                                        </li>
                                                                        <li>
                                                                            <label class="radio-container">Departure
                                                                                Date
                                                                                <input class="cm-radio-filter"
                                                                                       type="radio"
                                                                                       name="radio" value="4">
                                                                                <span class="checkmark"></span>
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="tab31"
                                                         aria-expanded="true" aria-labelledby="base-tab31">
                                                        <div class="row maain-tabble mt-2">
                                                            <table
                                                                class="table table-striped table-bordered zero-configuration">
                                                                <thead>
                                                                <tr>
                                                                    <th>S.no</th>
                                                                    <th>Amount Paid</th>
                                                                    <th>Booking ID</th>
                                                                    <th>Booking On</th>
                                                                    <th>Departure Date</th>
                                                                    <th>Fight No.</th>
                                                                    <th>Departing From</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($User->bookings as $key => $value)
                                                                    @if(!$value->status)
                                                                        <tr>
                                                                            <td>{{ $key + 1 }}</td>
                                                                            <td>{{ $value->flight_details['totalFlightAmount'] }}</td>
                                                                            <td>{{ $value->id }}</td>
                                                                            <td>{{ $value->created_at }}</td>
                                                                            <td>{{ date('m/d/Y', strtotime($value->flight_details['originDepartureDateTime'])) }}</td>
                                                                            <td>{{ $value->flight_details['originDepartureAirlineCode'].' '.$value->flight_details['originDepartureFlightNo']}}</td>
                                                                            <td>{{ $value->flight_details['originDepartureLocationCode'] }}</td>
                                                                            <td>
                                                                                <div class="btn-group mr-1 mb-1">
                                                                                    <button type="button"
                                                                                            class="btn  btn-drop-table btn-sm"
                                                                                            data-toggle="dropdown"
                                                                                            aria-haspopup="true"
                                                                                            aria-expanded="false"><i
                                                                                            class="fa fa-ellipsis-v"></i>
                                                                                    </button>
                                                                                    <div class="dropdown-menu"
                                                                                         x-placement="bottom-start"
                                                                                         style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                                        <a class="dropdown-item"
                                                                                           href="{{ route('booked-flight.show', ['id' => $value->id]) }}"><i
                                                                                                class="fa fa-eye"></i>View
                                                                                            Details</a>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div><!--tab 1 end-->
                                                    <div class="tab-pane" id="tab32" aria-labelledby="base-tab32">
                                                        <div class="row maain-tabble mt-2">
                                                            <table
                                                                class="table table-striped table-bordered zero-configuration">
                                                                <thead>
                                                                <tr>
                                                                    <th>S.no</th>
                                                                    <th>Amount Paid</th>
                                                                    <th>Booking ID</th>
                                                                    <th>Booking On</th>
                                                                    <th>Departure Date</th>
                                                                    <th>Fight No.</th>
                                                                    <th>Departing From</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($User->bookings as $key => $value)
                                                                    @if($value->status)
                                                                        <tr>
                                                                            <td>{{ $key + 1 }}</td>
                                                                            <td>{{ $value->flight_details['totalFlightAmount'] }}</td>
                                                                            <td>{{ $value->id }}</td>
                                                                            <td>{{ $value->created_at }}</td>
                                                                            <td>{{ date('m/d/Y', strtotime($value->flight_details['originDepartureDateTime'])) }}</td>
                                                                            <td>{{ $value->flight_details['originDepartureAirlineCode'].' '.$value->flight_details['originDepartureFlightNo']}}</td>
                                                                            <td>{{ $value->flight_details['originDepartureLocationCode'] }}</td>
                                                                            <td>
                                                                                <div class="btn-group mr-1 mb-1">
                                                                                    <button type="button"
                                                                                            class="btn  btn-drop-table btn-sm"
                                                                                            data-toggle="dropdown"
                                                                                            aria-haspopup="true"
                                                                                            aria-expanded="false"><i
                                                                                            class="fa fa-ellipsis-v"></i>
                                                                                    </button>
                                                                                    <div class="dropdown-menu"
                                                                                         x-placement="bottom-start"
                                                                                         style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                                        <a class="dropdown-item"
                                                                                           href="{{ route('booked-flight.show', ['id' => $value->id]) }}"><i
                                                                                                class="fa fa-eye"></i>View
                                                                                            Details</a>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div><!--tab 2 end-->
                                                </div><!--tab content end-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
