@extends('layouts.app', ['Title' => 'Users'])

@section('content')
    <div class="app-content content view user">
        <div class="content-wrapper">
            <div class="content-body">
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Users</h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2 filter-main justify-content-between">
                                            <div class="col-12">
                                                <label>Sort By:</label>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6  form-group">
                                                        <label for="">From:</label>
                                                        <input id="fromSelector" type="text" readonly="">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6  form-group">
                                                        <label for="">To:</label>
                                                        <input id="toSelector" type="text" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row maain-tabble mt-3">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>Date Registered</th>
                                                    <th>User ID</th>
                                                    <th>User Name</th>
                                                    <th>User Email</th>
                                                    <th>Account Status</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($Users as $key => $user)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ date('d/m/Y', strtotime($user->created_at)) }}</td>
                                                        <td>{{ $user->id }}</td>
                                                        <td>{{ $user->profile->first_name }}</td>
                                                        <td>{{ $user->email }}</td>
                                                        <td><input type="checkbox" class="cm-status-toggle" data-url="{{ route('user.update') }}" data-entityId="{{ $user->id }}" data-toggle="toggle" data-on="Active" data-off="Inactive" {{ ($user->status) ? 'checked': '' }} ></td>
                                                        <td>
                                                            <div class="btn-group mr-1 mb-1">
                                                                <button type="button" class="btn  btn-drop-table btn-sm"
                                                                        data-toggle="dropdown" aria-haspopup="true"
                                                                        aria-expanded="false"><i
                                                                        class="fa fa-ellipsis-v"></i></button>
                                                                <div class="dropdown-menu" x-placement="bottom-start"
                                                                     style="position: absolute; transform: translate3d(4px, 23px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                    <a class="dropdown-item" href="{{ route('user.show',['id' => $user->id]) }}"><i
                                                                            class="fa fa-eye"></i>View Profile</a>
                                                                    <a class="dropdown-item"
                                                                       href="{{ route('user.subscription-log.index',['id' => $user->id]) }}"><i
                                                                            class="fa fa-list-ul"
                                                                            aria-hidden="true"></i>Subscription
                                                                        Log</a>
                                                                    <a class="dropdown-item"
                                                                       href="{{ route('user.booking-log.index', ['id' => $user->id]) }}"><img
                                                                            src="{{ asset('custom/images/p-clip-img.png') }}">Booking Log</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
