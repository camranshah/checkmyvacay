@extends('layouts.app', ['Title' => 'User Profile'])

@section('script')
    <script>
        $(document).ready(function () {
            $('.cm-dob-datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
            });

            $('#cm-form-edit-user').on('submit', function (e) {
                e.preventDefault()
                let form = $(this)
                let formData = form.serialize();
                let action = form.attr('action')

                $.LoadingOverlay("show");

                $.ajax({
                    url: action,
                    method: 'post',
                    data: formData,
                    success: function (response) {
                        $.LoadingOverlay("hide");
                        $('.cm-flash-message').html('');
                        if (response.message) {
                            $.each(response.message, function (key, value) {
                                $('.cm-flash-message').show();
                                $('.cm-flash-message').append("<div class='alert " + response.class + "'>" + value + "</div>");
                                if (response.status) {
                                    location.reload()
                                }
                            });
                        }
                    }

                });
            })
        })
    </script>
@endsection


@section('content')
    <div class="app-content content view user profile u-profile">
        <div class="content-wrapper">
            <div class="content-body">
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">User Profile</h1>
                                                </div>
                                                <a href="javascript:void(0)" data-toggle="modal"
                                                   data-target="#cm-modal-edit-user">Edit User</a>
                                            </div>
                                        </div>
                                        @include('includes.flash-messages')
                                        <div class="row mt-2">
                                            <div class="col-12">
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Title</label>
                                                            <input type="text" value="{{ $User->profile->title }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">First Name</label>
                                                            <input type="text" value="{{ $User->profile->first_name }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Middle Name</label>
                                                            <input type="text" value="{{ $User->profile->middle_name }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Last Name</label>
                                                            <input type="text" value="{{ $User->profile->last_name }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Email Address</label>
                                                            <input type="Email" value="{{ $User->email }}"
                                                                   placeholder=""
                                                                   id="" class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Phone Number</label>
                                                            <input type="text"
                                                                   value="{{ $User->profile->phone_code.$User->profile->phone }}"
                                                                   placeholder="Phone Number"
                                                                   id="" class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Date of Birth</label>
                                                            <input type="text" value="{{ $User->profile->dob }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Subscription</label>
                                                            <input type="text" value="" placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">User Id</label>
                                                            <input type="number" value="{{ $User->id }}" placeholder=""
                                                                   id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Status</label>
                                                            <input type="checkbox" class="cm-status-toggle"
                                                                   data-url="{{ route('user.update') }}"
                                                                   data-entityId="{{ $User->id }}" data-toggle="toggle"
                                                                   data-on="Active"
                                                                   data-off="Inactive" {{ ($User->status) ? 'checked': '' }} >
                                                        </div>

                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Promo Code User</label>
                                                            <input type="text" value="" placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 form-group">
                                                            <a href="{{ route('user.subscription-log.index',['id' => $User->id]) }}">View
                                                                Subscription Log</a>
                                                            <a href="{{ route('user.booking-log.index', ['id' => $User->id]) }}">View
                                                                Booking Log</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


            </div>
        </div>
    </div>


    <div class="login-fail-main user">
        <div class="featured inner">
            <div class="modal fade bd-example-modal-lg"
                 id="cm-modal-edit-user"
                 tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id='cm-form-edit-user'
                              action="{{ route('user.edit') }}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{ $User->id }}"/>
                            <div class="payment-modal-main">
                                <div class="payment-modal-inner">
                                    <div class="row">
                                        <div class="col-12">
                                            <h1>Edit User</h1>
                                            <div class="cm-flash-message"
                                                 style="display:none"></div>
                                            <div class="col-12 form-group">
                                                <label for="">{{ __('Title:') }}</label>
                                                <input type="text"
                                                       class="form-control"
                                                       spellcheck="true"
                                                       name="title"
                                                       value="{{ $User->profile->title }}"
                                                       placeholder="Enter Title"
                                                       required>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label
                                                    for="">{{ __('First Name:') }}</label>
                                                <input type="text"
                                                       class="form-control"
                                                       spellcheck="true"
                                                       name="first_name"
                                                       value="{{ $User->profile->first_name }}"
                                                       placeholder="Enter First Name"
                                                       required>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label
                                                    for="">{{ __('Middle Name:') }}</label>
                                                <input type="text"
                                                       class="form-control"
                                                       spellcheck="true"
                                                       name="middle_name"
                                                       value="{{ $User->profile->middle_name }}"
                                                       placeholder="Enter Middle Name"
                                                       required>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label
                                                    for="">{{ __('Last Name:') }}</label>
                                                <input type="text"
                                                       class="form-control"
                                                       spellcheck="true"
                                                       name="last_name"
                                                       value="{{ $User->profile->last_name }}"
                                                       placeholder="Enter Last Name"
                                                       required>
                                            </div>

                                            <div class="col-12 form-group">
                                                <label
                                                    for="">{{ __('Date of Birth:') }}</label>
                                                <input type="text"
                                                       class="form-control cm-dob-datepicker"
                                                       spellcheck="true"
                                                       name="dob"
                                                       value="{{ $User->profile->dob }}"
                                                       placeholder="Enter Date Of Birth"
                                                       readonly
                                                       required>
                                            </div>

                                            <div class="col-12 form-group">
                                                <label
                                                    for="">{{ __('Phone no.:') }}</label>
                                                <select class="form-control" name="phone_code" required>
                                                    <option value="">-- Select --</option>
                                                    @foreach(\App\Country::all() as $country)
                                                        <option
                                                            value="{{ '+'.$country->phonecode }}" {{ ($User->profile->phone_code == '+'.$country->phonecode ) ? 'selected':'' }}>
                                                            {{ '+'.$country->phonecode }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <input type="text"
                                                       class="form-control cm-input-mask-phone-us"
                                                       spellcheck="true"
                                                       name="phone_no"
                                                       value="{{ $User->profile->phone }}"
                                                       placeholder="Enter Phone no."
                                                       required>
                                            </div>

                                            <div
                                                class="col-12 form-group text-center">
                                                <button type="submit" class="can">
                                                    save
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
