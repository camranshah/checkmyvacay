@extends('layouts.app', ['Title' => 'User Subscription Log'])

@section('content')
    <div class="app-content content view user ">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="search view-cause">
                    <div class="row">
                        <div class="col-12">
                            <div class="card pad-20">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">User Profile</h1>
                                                </div>
                                                <div class="right">
                                                    <a href="{{ route('user.show',['id'=> $User->id]) }}">Go To Profile</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2 profile u-profile">
                                            <div class="col-12">
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Title</label>
                                                            <input type="text" value="{{ $User->profile->title }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">First Name</label>
                                                            <input type="text" value="{{ $User->profile->first_name }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Middle Name</label>
                                                            <input type="text" value="{{ $User->profile->middle_name }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Last Name</label>
                                                            <input type="text" value="{{ $User->profile->last_name }}"
                                                                   placeholder="" id=""
                                                                   class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Email Address</label>
                                                            <input type="Email" value="{{ $User->email }}"
                                                                   placeholder=""
                                                                   id="" class="form-control" disabled="">
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-12 form-group">
                                                            <label for="">Phone Number</label>
                                                            <input type="text"
                                                                   value="{{ $User->profile->phone_code.$User->profile->phone }}"
                                                                   placeholder="Phone Number"
                                                                   id="" class="form-control" disabled="">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="seperator"></div>
                                            </div>
                                            <div class="col-12 d-block d-sm-flex justify-content-between">
                                                <div class="left">
                                                    <h1 class="pull-left">Subscription Log</h1>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-2 filter-main justify-content-between">
                                            <div class="col-12">
                                                <label>Sort By:</label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-6 col-sm-6  form-group">
                                                        <label for="">From:</label>
                                                        <input id="fromSelector" type="text" disabled="">
                                                    </div>
                                                    <div class="col-lg-3 col-md-6 col-sm-6  form-group">
                                                        <label for="">To:</label>
                                                        <input id="toSelector" type="text" disabled="">
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 d-flex align-items-center">
                                                        <div class="sort-box">
                                                            <ul>
                                                                <li>
                                                                    <label class="radio-container">Last Renewal Date
                                                                        <input type="radio" checked="checked"
                                                                               name="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label class="radio-container">Expiration Date
                                                                        <input type="radio" name="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row maain-tabble mt-2">
                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>Amount</th>
                                                    <th>Promo Code Used</th>
                                                    <th>Last Renewal Date</th>
                                                    <th>Expiration Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($User->account_payments as $key => $value)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ $amount }}</td>
                                                        <td>{{ @$value->promo_code->code }}</td>
                                                        <td>{{ $value->start_date }}</td>
                                                        <td>{{ $value->end_date }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
