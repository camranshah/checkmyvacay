@extends('layout')
@section('title','CMV - View Users')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-1">
                    <h3 class="content-header-title">View Users</h3>
                </div>
                <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">View Users
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <section class="card">
                <div class="content-body"><!-- Table row borders end-->
                    <!-- Double border end-->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <thead>
                                            <tr class="border-double">
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Created date & time</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($users as $user)
                                                <tr>
                                                    <td>{{ucwords($user->profile['first_name']." ".$user->profile['last_name'])}}</td>
                                                    <td>{{$user->email}}</td>
                                                    <td>{{$user->profile['phone']}}</td>
                                                    <td>{{date('F d Y h:i a',strtotime($user->created_at))}}</td>
                                                    <td>
                                                        <a href="{{route('user.view',$user->id)}}" class="btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="View Detail"><i class="fa fa-eye"></i></a>
                                                        <a href="{{route('user.change-status',$user->id)}}" class="btn-sm btn-{{$user->status == 1 ? "danger" : "success"}}" data-toggle="tooltip" data-placement="top" title="{{$user->status == 1 ? "Deactivate User" : "Activate User"}}"><i class="fa fa-{{$user->status == 1 ? "ban" : "check"}}"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Double border end -->
                </div>
            </section>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection
