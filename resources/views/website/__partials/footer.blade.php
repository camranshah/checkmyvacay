<footer class="site-footer">
    <div class="container">
        <div class="col-12 pl-0 pr-0">
            <ul class="site-ul footer-ul">
                <li>
                    <a href="{{ route('website.home') }}" class="footer-link">home</a>
                </li>
                <li>
                    <a href="{{ route('website.customer-support') }}" class="footer-link">customer support</a>
                </li>
                <li>
                    <a href="{{ route('website.about-us') }}" class="footer-link">about us</a>
                </li>
                <li>
                    <a href="{{ route('website.privacy-policy') }}" class="footer-link">privacy policy</a>
                </li>
                <li>
                    <i class="fab fa-facebook-f footer-social-link" aria-hidden="true"></i>
                    <i class="fab fa-linkedin-in footer-social-link" aria-hidden="true"></i>
                </li>
                <li>
                    <a href="{{ route('website.faqs') }}" class="footer-link">FAQs</a>
                </li>
                <li>
                    <a href="{{ route('website.terms-and-conditions') }}" class="footer-link">Terms of Conditions</a>
                </li>
                <li>
                    <a href="{{ route('website.refund-policy') }}" class="footer-link">Refund Policy</a>
                </li>
            </ul>
        </div>
        <div class="row ml-0 mr-0 align-items-center">
            <div class="col-md-4 col-12 pl-0 pr-0 left-then-center">
                <img src="{{ asset('website/img/footer-left-logos.png') }}" class="footer-left-img img-fluid" alt="">
            </div>
            <div class="col-md-4 col-12 pl-0 pr-0 text-center">
                <img src="{{ asset('website/img/footer-center-img.png')}}" class="footer-center-img img-fluid" alt="">
            </div>
            <div class="col-md-4 col-12 pl-0 pr-0 text-center">
                <p class="footer-text white-text mb-2">Payment Methods</p>
                <img src="{{ asset('website/footer-right-logos.png') }}'" class="payment-logos img-fluid" alt="">
            </div>
        </div>
        <div class="copyright-div">
            <div class="col-12 pl-0 pr-0 text-center">
                <p class="white-text">© {{ date('Y') }} {{ config('app.name') }} | All rights reserved</p>
            </div>
        </div>
    </div>

</footer>
