{{--<form class="repeater" action="{{ route('website.search-flight') }}" method="get">--}}
{{--    <input type="hidden" name="type" value="multiple"/>--}}
{{--    <input type="hidden" name="adult" value="1"/>--}}
{{--    <input type="hidden" name="cabin" value="economy"/>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-4">--}}
{{--            <select class="banner-select form-control cm-select-from-airports" name="from[]"></select>--}}
{{--        </div>--}}
{{--        <div class="col-md-4">--}}
{{--            <select class="banner-select form-control cm-select-to-airports" name="to[]"></select>--}}
{{--        </div>--}}
{{--        <div class="col-md-4 ">--}}
{{--            <div class="input-group date">--}}
{{--                <input type="text" class="form-control cm-departure-date"--}}
{{--                       name="departure_date[]" placeholder="Departure Date" readonly>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-4">--}}
{{--            <select class="banner-select form-control cm-select-from-airports" name="from[]"></select>--}}
{{--        </div>--}}
{{--        <div class="col-md-4">--}}
{{--            <select class="banner-select form-control cm-select-to-airports" name="to[]"></select>--}}
{{--        </div>--}}
{{--        <div class="col-md-4">--}}
{{--            <div class="input-group date">--}}
{{--                <input type="text" class="form-control cm-departure-date"--}}
{{--                       name="departure_date[]" placeholder="Departure Date" readonly>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            <button type="submit"><i class="fa fa-search banner-search-icon"--}}
{{--                                     aria-hidden="true"></i></button>--}}
{{--            <input data-repeater-create type="button" value="Add"/>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</form>--}}

<form class="repeater" action="{{ route('website.search-flight') }}" method="get">
    <input type="hidden" name="type" value="multiple"/>
{{--    <input type="hidden" name="adult" value="1"/>--}}
{{--    <input type="hidden" name="cabin" value="economy"/>--}}
    <div class="" data-repeater-list="mutlicity-group">
        <div data-repeater-item>
            <ui class="site-ul ul-flex-center multi-row">

                <li class="leave-li">
                    <div class="dropdown">
                        <select class="banner-select form-control cm-select-from-airports" name="from[]"></select>
                    </div>
                    <span id="switcher"></span>
                </li>
                <li>
                    <div class="dropdown">
                        <select class="banner-select form-control cm-select-to-airports" name="to[]"></select>
                    </div>
                </li>
                <li>
                    <div class="input-group date">
                        <input type="text" class="form-control date_1 datepicker site-input banner"
                               placeholder="Departure"
                               autocomplete="off">
                    </div>
                </li>
                <li data-repeater-delete>
                    <i class="fa fa-trash banner-delete-icon" aria-hidden="true"></i>
                </li>

            </ui>
        </div>
    </div>
    <div class="add-flight-div">
        <p class="stopover-text stopover" data-repeater-create>
            <i class="fas fa-plus" aria-hidden="true"></i> Add Flight
        </p>
        <ul class="site-ul ul-flex-center between">
            <li>
                <div class="dropdown position-relative">
                    <div class="banner-select show-package-dropdown">
                        <i class="fa fa-user drop-icon" aria-hidden="true"></i>
                        <span> Economy</span>
                    </div>
                    <div class="package-popup" style="display:none;">
                        <div class="person-category">
                            <p>Adult</p>
                            <div class="plus-minus-div">
                                <button type="button" class="value-button" id="decrease"
                                        onclick="decreaseValue(this, '#pills-multi')" value="Decrease Value">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                                <input type="number" name="adult" class="number-input"
                                       value="0">
                                <button type="button" class="value-button" id="increase"
                                        onclick="increaseValue(this, '#pills-multi')" value="Increase Value">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                        <div class="person-category">
                            <p>Children</p>
                            <div class="plus-minus-div">
                                <button type="button" class="value-button" id="decrease"
                                        onclick="decreaseValue(this, '#pills-multi')" value="Decrease Value">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                                <input type="number" class="number-input" value="0"
                                       name="child">
                                <button type="button" class="value-button" id="increase"
                                        onclick="increaseValue(this, '#pills-multi')" value="Increase Value">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                        <div class="person-category">
                            <p>Infants</p>
                            <div class="plus-minus-div">
                                <button type="button" class="value-button" id="decrease"
                                        onclick="decreaseValue(this, '#pills-multi')" value="Decrease Value">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                                <input type="number" class="number-input" value="0"
                                       name="infant">
                                <button type="button" class="value-button" id="increase"
                                        onclick="increaseValue(this, '#pills-multi')" value="Increase Value">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                        <div class="person-category">
                            <select class="economy-dropdown" onchange="chaneRoundTripHandler('#pills-multi')" name="cabin" id="">
                                <option value="economy">Economy</option>
                                <option value="business">Business</option>
                            </select>
                            <i class="fa fa-chevron-down drop-arrow" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>

            </li>
            <li class="search-li">
                <button type="submit" class="border-0 p-0">
                    <i class="fa fa-search banner-search-icon" aria-hidden="true"></i>
                </button>
            </li>
        </ul>
</form>
