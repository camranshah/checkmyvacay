<form action="{{ route('website.search-flight') }}" method="get">
    <input type="hidden" name="type" value="return"/>
    <ul class="site-ul ul-flex-center">
        <li class="leave-li">
            <div class="dropdown">
                <select class="banner-select form-control cm-select-from-airports" name="from[]"></select>
            </div>
            <span id="switcher"></span>
        </li>
        <li>
            <div class="dropdown">
                <select class="banner-select form-control cm-select-to-airports" name="to[]"></select>
            </div>
        </li>
        <li>
            <div class="input-group date">
                <input type="hidden" name="arrival_date[]" value="{{ date('d/m/Y') }}" class="form-control cm-arrival-date">
                <input type="hidden" name="departure_date[]" value="{{ date('d/m/Y') }}" class="form-control cm-departure-date">
                <input type="text" name="daterange" class="form-control date site-input banner" placeholder="Departure"/>
            </div>
        </li>
        <li>
            <div class="dropdown position-relative">
                <div class="banner-select show-package-dropdown">
                    <i class="fa fa-user drop-icon" aria-hidden="true"></i>
                    <span> Economy</span>
                </div>
                <div class="package-popup" style="display:none;">
                    <div class="person-category">
                        <p>Adult</p>
                        <div class="plus-minus-div">
                            <button type="button" class="value-button" id="decrease"
                                    onclick="decreaseValue(this, '#pills-round')" value="Decrease Value">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                            </button>
                            <input type="number" name="adult" class="number-input"
                                   value="0">
                            <button type="button" class="value-button" id="increase"
                                    onclick="increaseValue(this, '#pills-round')" value="Increase Value">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    <div class="person-category">
                        <p>Children</p>
                        <div class="plus-minus-div">
                            <button type="button" class="value-button" id="decrease"
                                    onclick="decreaseValue(this, '#pills-round')" value="Decrease Value">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                            </button>
                            <input type="number" class="number-input" value="0"
                                   name="child">
                            <button type="button" class="value-button" id="increase"
                                    onclick="increaseValue(this, '#pills-round')" value="Increase Value">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    <div class="person-category">
                        <p>Infants</p>
                        <div class="plus-minus-div">
                            <button type="button" class="value-button" id="decrease"
                                    onclick="decreaseValue(this, '#pills-round')" value="Decrease Value">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                            </button>
                            <input type="number" class="number-input" value="0"
                                   name="infant">
                            <button type="button" class="value-button" id="increase"
                                    onclick="increaseValue(this, '#pills-round')" value="Increase Value">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    <div class="person-category">
                        <select class="economy-dropdown" onchange="chaneRoundTripHandler('#pills-round')" name="cabin"
                                id="">
                            <option value="economy">Economy</option>
                            <option value="business">Business</option>
                        </select>
                        <i class="fa fa-chevron-down drop-arrow" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <button type="submit" class="border-0 p-0">
                <i class="fa fa-search banner-search-icon" aria-hidden="true"></i>
            </button>
        </li>
    </ul>
    <p class="stopover-text stopover">
        <i class="fas fa-plus" aria-hidden="true"></i> Add Stopover
    </p>
    <div class="outbound-div" style="display:none;">
        <div class="row m-0 mr-0">
            <div class="col-lg-6 col-12 pl-0 pr-0 d-flex align-items-end outbound-col">
                <div class="outbound-input-div">
                    <label for="" class="stopover-label">Outbound Stopover</label>
                    <div class="dropdown">
                        <div class="banner-select dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-plane-departure drop-icon"></i>
                            Stop Over Destination
                        </div>
                        <div class="dropdown-menu leaving-dropdown" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">
                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(SYD)
                                </p>
                                <p class="droplist-subtext">Kingsford Smith in Australia</p>
                            </a>
                            <a class="dropdown-item" href="#">
                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(RSE)
                                </p>
                                <p class="droplist-subtext">Sydney Au-Rose B Smith Australia</p>
                            </a>
                            <a class="dropdown-item" href="#">
                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(YQY)
                                </p>
                                <p class="droplist-subtext">Sydney (Canda) B Canada</p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="form-field mb-0">
                    <i class="fa fa-chevron-up drop-arrow-up" aria-hidden="true"></i>
                    <select name="" id="" class="day-night-select">
                        <option value="">Nights</option>
                        <option value="">Days</option>
                    </select>
                    <i class="fa fa-chevron-down drop-arrow" aria-hidden="true"></i>
                </div>

            </div>
            <div class="col-lg-6 col-12 pl-0 pr-0 d-flex align-items-end outbound-col">
                <div class="outbound-input-div">
                    <label for="" class="stopover-label">Return Stopover</label>
                    <div class="dropdown">
                        <div class="banner-select dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-plane-departure drop-icon"></i>
                            Stop Over Destination
                        </div>
                        <div class="dropdown-menu leaving-dropdown" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">
                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(SYD)
                                </p>
                                <p class="droplist-subtext">Kingsford Smith in Australia</p>
                            </a>
                            <a class="dropdown-item" href="#">
                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(RSE)
                                </p>
                                <p class="droplist-subtext">Sydney Au-Rose B Smith Australia</p>
                            </a>
                            <a class="dropdown-item" href="#">
                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(YQY)
                                </p>
                                <p class="droplist-subtext">Sydney (Canda) B Canada</p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="form-field mb-0">
                    <i class="fa fa-chevron-up drop-arrow-up" aria-hidden="true"></i>
                    <select name="" id="" class="day-night-select">
                        <option value="">Nights</option>
                        <option value="">Days</option>
                    </select>
                    <i class="fa fa-chevron-down drop-arrow" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</form>
@push('js')

    <script>
        $(function(){
            $('.package-popup > .person-category:first-child #increase').trigger('click');
        });
    </script>
@endpush
