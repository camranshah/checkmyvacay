@if(request()->route()->getName() !== 'website.home' && false)
    <nav class="inner-page-header">
    <div class="container">
        <div class="row ml-0 mr-0 header-row">
            <div class="col-lg-5 col-md-3 col-12 pl-0 pr-0 left-then-center">
                <div class="logo-container">
                    <a href="{{ route('website.home') }}">
                        <img src="{{ asset('website/img/logo-colored.png') }}" class="logo img-fluid" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-7 col-md-9 col-12 pl-0 pr-0 right-then-center">
                <ul class="site-ul ul-flex-center main-nav-ul">
                    <li>
                        <a href="{{ route('website.manage-booking') }}" class="main-nav-link">manage bookings</a>
                    </li>

                    @guest
                        <li>
                            <a href="{{ route('login') }}" class="main-nav-link">Login</a>
                        </li>
                    @endguest

                    @auth
                        <li class="position-relative hover-link">
                            <a href="#" class="main-nav-link show-notification-dropdown">
                                <i class="fa fa-bell" aria-hidden="true"></i>
                            </a>
                            <div class="login-popup notification" style="display:none;">
                                <div class="notification-menu-div">
                                    <i class="fa fa-bell" aria-hidden="true"></i>
                                    <div class="notification-menu-content">
                                        <p class="notification text-left">You have upcoming flight at nov/15/2020</p>
                                        <div class="row ml-0 mr-0">
                                            <div class="col-6 pl-0 pr-0 left-then-center">
                                                <p class="menu-info">Aug-22-2020</p>
                                            </div>
                                            <div class="col-6 pl-0 pr-0 right-then-center">
                                                <p class="menu-info">10:00 PM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="dropdown-item text-center view-more" href="notifications.php">View More</a>
                            </div>
                        </li>
                        <li class="position-relative">
                            <a href="#" class="profile-dropdown-link" id="dropdownMenuLink" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <img src="{{ asset('website/img/user-dp.png') }}" class="nav-dp img-fluid"
                                     alt=""> {{ optional(optional(auth()->user())->profile)->first_name }}
                                <i class="fas fa-chevron-down"></i>
                            </a>
                            <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('website.profile') }}"><i
                                        class="fas fa-user"></i>Profile</a>
                                <a class="dropdown-item" href="{{ route('website.contact-us') }}"><i
                                        class="fas fa-phone"></i>Contact Us</a>
                                <a class="dropdown-item" href="javascript:void(0)"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                        class="fas fa-sign-out-alt"></i>Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endauth
                </ul>
                <i class="fa fa-bars mobileMenu-toggleBtn" id="sidebarCollapse" aria-hidden="true"></i>
            </div>
        </div>
    </div>
</nav>
@endif

@if(request()->route()->getName() !== 'website.home')
    <nav class="inner-page-header">
    <div class="container">
@else
    @push('css')
        <style>
            #content > .header-row {
                display: none;
            }
        </style>
    @endpush
@endif
<div class="row ml-0 mr-0 header-row @if(request()->route()->getName() === 'website.home') white @endif">
    <div class="col-lg-5 col-md-3 col-12 pl-0 pr-0 left-then-center">
        <div class="logo-container">
            <a href="{{ url('/') }}">
                @if(request()->route()->getName() !== 'website.home')
                    <img src="{{ asset('website/img/logo-colored.png') }}" class="logo img-fluid" alt="">
                @else
                    <img src="{{ asset('website/img/logo_white.png') }}" class="logo img-fluid" alt="">
                @endif
            </a>
        </div>
    </div>
    <div class="col-lg-7 col-md-9 col-12 pl-0 pr-0 right-then-center">
        <ul class="site-ul ul-flex-center main-nav-ul">
            <li class="position-relative hover-link">
                <a href="{{ url('/manage-booking') }}" class="main-nav-link show-booking-dropdown">MANAGE BOOKING</a>
                <div class="booking-popup login" style="display:none;">
                    <i class="fas fa-times popup-cross booking"></i>
                    <form action="" class="popup-form">
                        <div class="col-12 pl-0 pr-0">
                            <div class="form-field">
                                <label for="" class="site-label">Booking Number<span class="required">*</span> </label>
                                <input type="number" class="site-input" placeholder="CMF-2019-12345" name="" id="" required="">
                            </div>
                        </div>
                        <div class="col-12 pl-0 pr-0">
                            <div class="form-field">
                                <label for="" class="site-label">Last Name<span class="required">*</span> </label>
                                <input type="password" class="site-input" placeholder="Enter Last Name" name="" id="" required="">
                            </div>
                        </div>

                        <button type="submit" class="site-btn red d-block">Search</button>
                    </form>
                </div>
            </li>

            @guest
            <li class="position-relative hover-link">
                <a href="#" class="main-nav-link custom-dropdown-m show-userLogin-dropdown">login</a>
                <div class="login-popup login" style="display:none;">
                    <i class="fas fa-times popup-cross login"></i>
                    <form action="{{ route('login') }}" class="popup-form" method="post">
                        @csrf
                        <div class="col-12 pl-0 pr-0">
                            <div class="form-field">
                                <label for="email" class="site-label">Email Address<span class="required">*</span> </label>
                                <input type="text" class="site-input" placeholder="Enter Email Address" name="email" id="email" required="">
                            </div>
                        </div>
                        <div class="col-12 pl-0 pr-0">
                            <div class="form-field">
                                <label for="password" class="site-label">Password<span class="required">*</span> </label>
                                <input type="password" class="site-input" placeholder="Enter Password" name="password" id="password" required>
                            </div>
                        </div>
                        <button type="submit" class="site-btn red d-block">Login</button>
                        <a href="#" class="forgot-link mt-1" data-toggle="modal" data-target="#pwdrecovery1">Forgot Password ?</a>

                        <p class="or-text">OR</p>

                        <p class="text-center black-text sm-text text-capitalize mb-2">Connect Using with</p>

                        <div class="row ml-0 mr-0">
                            <div class="col-6 text-right">
                                <i class="fab fa-facebook-f social-login"></i>
                            </div>
                            <div class="col-6 text-left">
                                <i class="fab fa-google-plus-g social-login"></i>
                            </div>
                        </div>

                    </form>
                </div>
            </li>
            <li class="position-relative hover-link">
                <a href="#" class="main-nav-link custom-dropdown-m show-userRegister-dropdown">register</a>
                <div class="login-popup register" style="display:none;">
                    <i class="fas fa-times popup-cross register"></i>
                    <form action="{{ route('register') }}" class="popup-form" method="post">
                        @csrf
                        <div class="col-12 pl-0 pr-0">
                            <div class="form-field">
                                <label for="email" class="site-label">Email Address<span class="required">*</span> </label>
                                <input type="email" class="site-input" placeholder="Enter Email Address" name="email" id="email" required="">
                            </div>
                        </div>
                        <div class="col-12 pl-0 pr-0">
                            <div class="form-field">
                                <label for="" class="site-label">Password<span class="required">*</span> </label>
                                <input type="password" class="site-input" placeholder="Enter Password" name="" id="" required="">
                            </div>
                        </div>

                        <div class="col-12 pl-0 pr-0">
                            <div class="form-field">
                                <label for="password_confirmation" class="site-label">Confirm Password<span class="required">*</span> </label>
                                <input type="password" class="site-input" placeholder="Confirm Password" name="password_confirmation" id="password_confirmation" required>
                            </div>
                        </div>
                        <div class="row ml-0 mr-0">
                            <div class="col-4 pl-0">
                                <label for="title" class="site-label">Title <span class="required">*</span> </label>
                                <div class="form-field">
                                    <select class="site-input icon phone" name="title" id="title" required>
                                        <option value="">Select</option>
                                        <option value="Mr.">Mr.</option>
                                        <option value="Mrs.">Mrs.</option>
                                        <option value="Ms.">Ms.</option>
                                        <option value="Dr.">Dr.</option>
                                    </select>
                                    <i class="fa fa-chevron-down input-right-icon" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="col-8 pr-0 pl-0">
                                <div class="form-field">
                                    <label for="first_name" class="site-label">First Name<span class="required">*</span> </label>
                                    <input type="text" class="site-input" placeholder="Enter First Name" name="first_name" id="first_name">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pl-0 pr-0">
                            <div class="form-field">
                                <label for="last_name" class="site-label">Last Name<span class="required">*</span> </label>
                                <input type="text" class="site-input" placeholder="Enter Your Last Name" name="last_name" id="last_name" required="">
                            </div>
                        </div>

                        <div class="row ml-0 mr-0">
                            <div class="col-4 pl-0">
                                <div class="form-field">
                                    <label for="phone" class="site-label">Code<span class="required">*</span> </label>
                                    <select class="site-input icon phone" name="phone_code" id="phone_code" required>
                                        @foreach(\App\Country::all() as $country)
                                            <option value="{{ '+'.$country->phonecode }}" {{ old('phone_code') == '+'.$country->phonecode ? 'selected' : '' }}>{{ $country->code.' (+'.$country->phonecode.')' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-8 pl-0 pr-0">
                                <div class="form-field">
                                    <label for="phone" class="site-label">Phone Number<span class="required">*</span> </label>
                                    <input type="tel" class="site-input" placeholder="Enter Your Phone Number" name="phone" id="phone" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 pl-0 pr-0">
                            <div class="form-field">
                                <label for="dob" class="site-label">Date Of Birth<span class="required">*</span> </label>
                                <input type="text" class="site-input dob" placeholder="DOB-DD/MM/YYYY" name="dob" id="dob" required>
                            </div>
                        </div>
                        <button type="submit" class="site-btn red d-block">Register</button>
                    </form>
                </div>
            </li>
            @else
                <li class="position-relative hover-link">
                <a href="#" class="main-nav-link show-notification-dropdown">
                    <i class="fa fa-bell" aria-hidden="true"></i>
                </a>
                <div class="login-popup notification" style="display:none;">
                    <div class="notification-menu-div">
                        <i class="fa fa-bell" aria-hidden="true"></i>
                        <div class="notification-menu-content">
                            <p class="notification text-left">You have upcoming flight at nov/15/2020</p>
                            <div class="row ml-0 mr-0">
                                <div class="col-6 pl-0 pr-0 left-then-center">
                                    <p class="menu-info">Aug-22-2020</p>
                                </div>
                                <div class="col-6 pl-0 pr-0 right-then-center">
                                    <p class="menu-info">10:00 PM</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="dropdown-item text-center view-more" href="notifications.php">View More</a>
                </div>
            </li>
            <li class="position-relative">
                <a href="#" class="profile-dropdown-link" id="dropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <img src="{{ asset('website/img/user-dp.png') }}" class="nav-dp img-fluid"
                         alt=""> {{ optional(optional(auth()->user())->profile)->first_name }}
                    <i class="fas fa-chevron-down"></i>
                </a>
                <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('website.profile') }}"><i
                            class="fas fa-user"></i>Profile</a>
                    <a class="dropdown-item" href="{{ route('website.contact-us') }}"><i
                            class="fas fa-phone"></i>Contact Us</a>
                    <a class="dropdown-item" href="javascript:void(0)"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                            class="fas fa-sign-out-alt"></i>Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
            @endguest

        </ul>
        <i class="fa fa-bars mobileMenu-toggleBtn" id="sidebarCollapse" aria-hidden="true"></i>
    </div>
</div>

@if(request()->route()->getName() !== 'website.home')
    </div>
    </nav>
@endif
