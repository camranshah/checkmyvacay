@extends('website.layouts.app', ['Title' => 'Booking Completed'])

@section('content')
    <section class="inner-page-pd">
        <section class="light-blue-bg-section extra-pd">
            <div class="container">
                <div class="row ml-0 mr-0">
                    <div class="col-md-12 col-12 pl-0 pr-0">
                        <div id="accordion" class="site-accordion">
                            <div class="card">
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="site-modalheading mb-3 popsb">Congratulations!</h5>
                                        <p class="site-text text-center">Your booking has been confirmed against Booking
                                            ID
                                            # {{ @$booking->booking_id }}. </p>
                                        <p class="site-text text-center">Invoice and details will be sent on your email
                                            as
                                            well. </p>
                                        <p class="site-text text-center">You can view your booked flights in my bookings
                                            tab
                                            as well. </p>
                                        <p class="site-text black-text pnr">Your PNR Number is: <span
                                                class="red-text"> {{ @$booking->gds_pnr }}</span></p>
                                        <p class="site-text black-text popMed mb-2">Check-In Instructions:</p>
                                        <p class="site-text">Lorem Ipsum is simply dummy text of the printing and
                                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                            text
                                            ever since the 1500s, when an unknown printer took a galley of type and
                                            scrambled it to make a type specimen book. </p>
                                        <div class="text-center"><a href="{{ url('/') }}"
                                                                    class="site-btn blue text-center text-uppercase mt-4">Go
                                                Back</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
