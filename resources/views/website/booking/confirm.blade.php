@extends('website.layouts.app', ['Title' => 'Booking Confirmation'])

@section('content')

    @php($request = json_decode(\request('request')));

    <section class="inner-page-pd">
        <section class="progress-section">
            <div class="container">
                <ul class="progressbar">
                    <li class="active">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </li>
                    <li class="active">
                        <i class="fa fa-sliders" aria-hidden="true"></i>
                    </li>
                    <li class="active">
                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                    </li>
                </ul>
            </div>
        </section>
        <section class="light-blue-bg-section extra-pd">
            <div class="container">
                <div class="row ml-0 mr-0">
                    <div class="col-lg-9 col-12 pl-0 pr-0 left-col-spacing">
                        <div id="" class="site-accordion">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <button class="btn-link" data-toggle="collapse" data-target="#collapseOne"
                                            aria-expanded="false" aria-controls="collapseOne">
                                        <p class="accordion-heading">Cost Summary</p>
                                        <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                     data-parent="#accordion" style="">
                                    <div class="card-body">
                                        <div class="row ml-0 mr-0">
                                            <div class="col-7 pl-0 pr-0">
                                                <p class="site-text black-text popMed">Details</p>
                                            </div>
                                            <div class="col-5 pl-0 pr-0">
                                                <p class="site-text black-text popMed">Subtotal</p>
                                            </div>
                                        </div>
                                        <div class="cost-summary-info">
                                            @foreach($departure_flight_detail['flyer'] as $key => $flyer)
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-7 pl-0 pr-0">
                                                        <p class="site-text">{{ $flyer['quantity'] }} {{ $flyer['type'] }}</p>
                                                    </div>
                                                    <div class="col-5 pl-0 pr-0">
                                                        @if($request->type === 'one_way')
                                                        <p class="site-text black-text">${{ $flyer['total_fare_amount'] }}</p>
                                                        @else
                                                        <p class="site-text black-text">${{ $flyer['total_fare_amount'] + $departure_flight_detail['flyer'][$key]['total_fare_amount'] }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach

                                            <div class="row ml-0 mr-0">
                                                <div class="col-7 pl-0 pr-0">
                                                    <p class="site-text">Base Fare</p>
                                                </div>
                                                <div class="col-5 pl-0 pr-0">

                                                    @if($request->type === 'one_way')
                                                    <p class="site-text black-text">${{ $departure_flight_detail['total_fare']['base_fare']['amount'] }}</p>
                                                    @else
                                                    <p class="site-text black-text">${{ $departure_flight_detail['total_fare']['base_fare']['amount'] + $arrival_flight_detail['total_fare']['base_fare']['amount'] }}</p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row ml-0 mr-0">
                                                <div class="col-7 pl-0 pr-0">
                                                    <p class="site-text">Tax</p>
                                                </div>
                                                <div class="col-5 pl-0 pr-0">
                                                    @if($request->type === 'one_way')
                                                    <p class="site-text black-text">${{ $departure_flight_detail['total_fare']['tax']['amount'] }}</p>
                                                    @else
                                                    <p class="site-text black-text">${{ $departure_flight_detail['total_fare']['tax']['amount'] + $arrival_flight_detail['total_fare']['tax']['amount'] }}</p>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ml-0 mr-0 total-row">
                                            <div class="col-7 pl-0 pr-0">
                                                <p class="site-text black-text">Total</p>
                                            </div>
                                            <div class="col-5 pl-0 pr-0">

                                                @if($request->type === 'one_way')
                                                <p class="site-text black-text">${{ $departure_flight_detail['total_fare']['total']['amount'] }}</p>
                                                @else
                                                <p class="site-text black-text">${{ $departure_flight_detail['total_fare']['total']['amount'] + $arrival_flight_detail['total_fare']['total']['amount'] }}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="{{ route('website.booking.confirm') }}" method="post">
                            @csrf
                            @foreach($departure_flight_detail['flights'] as $key)
                                <input type="hidden" name="departure_date_and_time[]" value="{{ $key['departure_date_time'] }}"/>
                                <input type="hidden" name="arrival_date_and_time[]" value="{{ $key['arrival_date_time'] }}"/>
                                <input type="number" name="duration[]" value="{{ $key['duration'] }}" hidden/>
                                <input type="number" name="stop_quantity[]" value="{{ $key['stop_quantity'] }}" hidden/>
                                <input type="number" name="flight_number[]" value="{{ $key['flight_number'] }}" hidden/>
                                <input type="hidden" name="booking_desig_code[]" value="{{ $key['res_book_desig_code'] }}"/>
                                <input type="hidden" name="departure_terminal[]" value="{{ $key['departure_terminal'] }}"/>
                                <input type="hidden" name="arrival_terminal[]" value="{{ $key['arrival_terminal'] }}"/>
                                <input type="hidden" name="air_equip_type[]" value="{{ $key['air_equip_type'] }}"/>
                                <input type="hidden" name="departure_location_code[]" value="{{ $key['departure_location_code'] }}"/>
                                <input type="hidden" name="departure_code_context[]" value="{{ $key['departure_code_context'] }}"/>
                                <input type="hidden" name="arrival_location_code[]" value="{{ $key['arrival_location_code'] }}"/>
                                <input type="hidden" name="arrival_code_context[]" value="{{ $key['arrival_code_context'] }}"/>
                                <input type="hidden" name="operating_airline_code[]" value="{{ $key['operating_airline'] }}"/>
                                <input type="hidden" name="marketing_airline_code[]" value="{{ $key['marketing_airline'] }}"/>
                            @endforeach
                            <input type="hidden" name="departure_flight_detail" value="{{ json_encode($departure_flight_detail) }}"/>
                            <input type="hidden" name="arrival_flight_detail" value="{{ json_encode($arrival_flight_detail) }}"/>
                            <input type="hidden" name="request" value="{{ request('request') }}">

                            @foreach($departure_flight_detail['flyer'] as $key)
                                <input type="hidden" name="traveller_type[]" value="{{ $key['type'] }}"/>
                            @endforeach
                            <input type="hidden" name="search_id" value="{{ $departure_flight_detail['search_id'] }}"/>
                            <input type="number" name="trip_id" value="{{ $departure_flight_detail['trip_id'] }}" hidden/>

                            @php($airline_pnr = array_key_exists('airline_pnr', $departure_flight_detail)? is_array($departure_flight_detail['airline_pnr'])?$departure_flight_detail['airline_pnr']:[$departure_flight_detail['airline_pnr']]: [])

                            @foreach($airline_pnr as $airline_pnr)
                                <input type="hidden" name="airline_pnr[]" value="{{ $airline_pnr }}"/>
                            @endforeach
                            @php($gds_pnr = array_key_exists('gds_pnr', $departure_flight_detail)? is_array($departure_flight_detail['gds_pnr'])?$departure_flight_detail['gds_pnr']:[$departure_flight_detail['gds_pnr']]: [])
                            @foreach($gds_pnr as $airline_pnr)
                                <input type="hidden" name="gds_pnr[]" value="{{ $airline_pnr }}"/>
                            @endforeach

                            <input type="hidden" name="currency_code" value="{{ $departure_flight_detail['total_fare']['total']['currency_code'] }}"/>
                            @if($request->type==='one_way')
                                <input type="hidden" name="amount" value="{{ $departure_flight_detail['total_fare']['total']['amount'] }}"/>
                            @else
                                <input type="hidden" name="amount" value="{{ $departure_flight_detail['total_fare']['total']['amount'] + $arrival_flight_detail['total_fare']['total']['amount'] }}"/>
                            @endif

                            @foreach($provisionFlightDetails['name_prefix'] as $key)
                                <input type="hidden" name="name_prefix[]" value="{{ $key }}"/>
                            @endforeach

                            @foreach($provisionFlightDetails['name'] as $index => $key)
                                @if(!is_array($key)) @continue @endif
                                @foreach($key as $type => $value)
                                    <input type="hidden" name="name[{{ $index }}][{{ $type }}]" value="{{ $value ?? "Default $type" }}"/>
                                @endforeach
                            @endforeach

{{--                            @foreach($provisionFlightDetails['surname'] as $key)--}}
{{--                                <input type="hidden" name="surname[]" value="{{ $key }}"/>--}}
{{--                            @endforeach--}}

                            @foreach($provisionFlightDetails['gender'] as $key)
                                <input type="hidden" name="gender[]" value="{{ $key }}"/>
                            @endforeach

                            @foreach($provisionFlightDetails['nationality'] as $key)
                                <input type="hidden" name="nationality[]" value="{{ $key }}"/>
                            @endforeach

                            @foreach($provisionFlightDetails['date_of_birth'] as $key)
                                <input type="hidden" name="date_of_birth[]" value="{{ $key }}"/>
                            @endforeach

                            @foreach($provisionFlightDetails['phone'] as $key)
                                <input type="hidden" name="phone[]" value="{{ $key }}"/>
                            @endforeach

                            @foreach($provisionFlightDetails['doc_issue_location'] as $key)
                                <input type="hidden" name="doc_issue_location[]" value="{{ $key }}"/>
                            @endforeach

                            @foreach($provisionFlightDetails['doc_id'] as $key)
                                <input type="hidden" name="doc_id[]" value="{{ $key }}"/>
                            @endforeach

{{--                            @foreach($provisionFlightDetails['expire_date'] as $key)--}}
{{--                                <input type="text" name="expire_date[]" value="{{ $key }}"/>--}}
{{--                            @endforeach--}}


                            <div id="" class="site-accordion mt-4">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <button class="btn-link" data-toggle="collapse" data-target="#collapse2"
                                                aria-expanded="false" aria-controls="collapseOne">
                                            <p class="accordion-heading">Payment Details</p>
                                            <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div id="collapse2" class="collapse show" aria-labelledby="headingOne"
                                         data-parent="#accordion" style="">
                                        <div class="card-body">
                                            <span class="register-form mt-3"></span>
                                            <div class="row ml-0 mr-0">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="cc_holder_name" class="site-label">Card Holder Name
                                                            <span
                                                                class="required">*</span> </label>
                                                        <input type="text" class="site-input"
                                                               placeholder="Enter Card Holder Name"
                                                               name="cc_holder_name" id="cc_holder_name"
                                                               required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-field">
                                                        <label for="cc_no" class="site-label">Card Number <span
                                                                class="required">*</span> </label>
                                                        <input type="text"
                                                               class="site-input cm-input-mask-credit-card-number"
                                                               placeholder="Enter Card Number" name="cc_no" id="cc_no">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row ml-0 mr-0 align-items-center">
                                                <div class="col-lg-3 col-md-4 col-12">
                                                    <div class="form-field">
                                                        <label for="cvv" class="site-label">CVV <span
                                                                class="required">*</span> </label>
                                                        <input type="text" class="site-input cm-input-mask-cvv"
                                                               placeholder="Enter CVV"
                                                               name="cvv" id="cvv" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-4 col-12">
                                                    <div class="form-field">
                                                        <label for="cc_expiry_date" class="site-label">Expiry Date <span
                                                                class="required">*</span> </label>
                                                        <input type="text"
                                                               class="site-input cm-input-mask-credit-card-expiry-date"
                                                               placeholder="MM/YY"
                                                               name="cc_expiry_date" id="cc_expiry_date" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-4 col-12">
                                                    <div class="form-field">
                                                        <img src="{{ asset('website/img/card-images.png') }}"
                                                             class="card-images img-fluid" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="site-btn red extra-pd mt-3">Pay Now</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-3 col-12 pl-0 pr-0">
                        <div id="" class="site-accordion">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <button class="btn-link" data-toggle="collapse" data-target="#collapse5"
                                            aria-expanded="false" aria-controls="collapse5">
                                        <p class="accordion-heading">Flight Summary</p>
                                        <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <div id="collapse5" class="collapse show" aria-labelledby="collapse5"
                                     data-parent="#accordion" style="">
                                    <div class="card-body p-0">
                                        <div class="upper-div">
                                            <div class="row ml-0 mr-0 mb-2">
                                                <div class="col-6 pl-0 pr-0">
                                                    <p class="site-text black-text">{{ __('TOTAL PASSENGER') }}</p>
                                                </div>
                                                <div class="col-6 pl-0 pr-0 text-right">
                                                    {{--<p class="site-text black-text pr-3">{{ @$departure_flight_detail['flyer'][0]['passenger_quantity'] }}</p>--}}
                                                    <p class="site-text black-text pr-3">{{ @$departure_flight_detail['flyer'][0]['quantity'] }}</p>
                                                </div>
                                            </div>
                                            <div class="row ml-0 mr-0">
                                                <div class="col-6 pl-0 pr-0">
                                                    <p class="site-text black-text">{{ __('TOTAL AMOUNT') }}</p>
                                                </div>
                                                <div class="col-6 pl-0 pr-0 text-right">
                                                    @if($request->type === 'one_way')
                                                    <a class="site-text red-text" data-toggle="collapse" href="#collapseEx2" role="button" aria-expanded="false"
                                                       aria-controls="collapseEx2">${{ $departure_flight_detail['total_fare']['total']['amount'] }}
                                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                    </a>
                                                    @else
                                                    <a class="site-text red-text" data-toggle="collapse" href="#collapseEx2" role="button" aria-expanded="false"
                                                       aria-controls="collapseEx2">${{ $departure_flight_detail['total_fare']['total']['amount'] + $arrival_flight_detail['total_fare']['total']['amount'] }}
                                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="collapse show" id="collapseEx2">
                                            <div class="card card-body side-collapse-pd">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-6 pl-0 pr-0">
                                                        <p class="site-text sm-text black-text popsb">FLIGHTS</p>
                                                    </div>
                                                    <div class="col-6 pl-0 pr-0 text-right">
                                                        <img src="{{ asset('website/img/plane-takeoff.png') }}"
                                                             class="plane img-fluid pr-3" alt="">
                                                    </div>
                                                </div>
                                                @foreach($departure_flight_detail['flights'] as $flight)
                                                    <div class="summary-div">
                                                        <div class="row ml-0 mr-0">
                                                            <div class="col-4 pl-0 pr-0">
                                                                <div class="summary-left">
                                                                    <p class="black-text sm-text popsb">{{ @$flight['departure_time_formated'] }}</p>
                                                                    <p class="site-text sm-text">{{ @$flight['departure_date_formated'] }}</p>
                                                                    <div class="flight-flag">
                                                                        <p class="black-text sm-text popsb mt-1">{{ @$flight['operating_airline'].' '.$flight['flight_number'] }}</p>
                                                                    </div>
                                                                    <p class="black-text sm-text popsb">{{ @$flight['arrival_time_formated'] }}</p>
                                                                    <p class="site-text sm-text">{{ @$flight['arrival_date_formated'] }}</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-8 pl-0 pr-0">
                                                                <ul class="site-ul flight-summary-ul">
                                                                    <li>
                                                                        <p class="black-text sm-text">{{ @$flight['airport_departure']['city'] }}
                                                                            ({{ @$flight['airport_departure']['iata'] }}
                                                                            )</p>
                                                                        <p class="black-text sm-text">{{ @$flight['airport_departure']['name'] }}</p>
                                                                    </li>
                                                                    <li>
                                                                        <p class="black-text sm-text">{{ @$flight['airport_arrival']['city'] }}
                                                                            ({{ @$flight['airport_arrival']['iata'] }}
                                                                            )</p>
                                                                        <p class="black-text sm-text">{{ @$flight['airport_arrival']['name'] }}</p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
