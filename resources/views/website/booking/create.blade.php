@extends('website.layouts.app', ['Title' => 'Booking'])

@section('content')

    <section class="inner-page-pd">
        <section class="progress-section">
            <div class="container">
                <ul class="progressbar">
                    <li class="active">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </li>
                    <li>
                        <i class="fa fa-sliders" aria-hidden="true"></i>
                    </li>
                    <li>
                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                    </li>
                </ul>
            </div>
        </section>
        <section class="light-blue-bg-section">
            <div class="container">
                <div class="row ml-0 mr-0">
                    <div class="col-lg-9 col-12 pl-0 pr-0 left-col-spacing">
                        <div id="accordion" class="site-accordion">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <button class="btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        <p class="accordion-heading">{{ __('Passenger Details') }}</p>
{{--                                        <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>--}}
                                        <div class="text-right">
                                            <p class="white-text sm-text">All flights displayed in Australian Dollars (AUD)</p>
                                            <p class="white-text sm-text">All flight times are local time and in 24 hour format</p>
                                        </div>
                                    </button>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                    <form action="{{ route('website.booking.provision') }}" class="register-form mt-3" method="post">
                                        @csrf
                                        <div class="card-body p-0">
                                            <div class="form-separator-div">
                                                @php
                                                $request = json_decode(request('request'), true);
                                                $departure = json_decode(request('departure'));
                                                $arrival = json_decode(request('arrival'));
                                                @endphp

                                                <input type="hidden" name="request" value="{{ request('request') }}"/>

                                                <input type="hidden" name="session_id" value="{{ request('session_id') }}"/>
                                                <input type="hidden" name="departure[departure_date_and_time]" value="{{ $departure->departure_date_time }}"/>
                                                <input type="hidden" name="departure[arrival_date_and_time]" value="{{ $departure->arrival_date_time }}"/>
                                                <input type="hidden" name="departure[departure_date_and_time]" value=""/>
                                                <input type="hidden" name="departure[arrival_date_and_time]" value=""/>
                                                <input type="hidden" name="departure[duration]" value="{{ $departure->duration }}"/>
                                                <input type="hidden" name="departure[stop_quantity]" value="{{ $departure->stop_quantity }}" />
                                                <input type="hidden" name="departure[flight_number]" value="{{ $departure->flight_number }}"/>
                                                <input type="hidden" name="departure[booking_desig_code]" value="{{ $departure->res_book_desig_code }}"/>
                                                <input type="hidden" name="departure[departure_terminal]" value="{{ $departure->departure_terminal }}"/>
                                                <input type="hidden" name="departure[arrival_terminal]" value="{{ $departure->arrival_terminal }}"/>
                                                <input type="hidden" name="departure[air_equip_type]" value="{{ $departure->air_equip_type }}"/>
                                                <input type="hidden" name="departure[departure_location_code]" value="{{ $departure->departure_location_code }}"/>
                                                <input type="hidden" name="departure[departure_code_context]" value="{{ $departure->departure_code_context }}"/>
                                                <input type="hidden" name="departure[arrival_location_code]" value="{{ $departure->arrival_location_code }}"/>
                                                <input type="hidden" name="departure[arrival_code_context]" value="{{ $departure->arrival_code_context }}"/>
                                                <input type="hidden" name="departure[operating_airline_code]" value="{{ $departure->operating_airline }}"/>
                                                <input type="hidden" name="departure[marketing_airline_code]" value="{{ $departure->marketing_airline }}"/>
                                                <input type="hidden" name="departure[search_id]" value="{{ $departure->search_id }}"/>
                                                <input type="hidden" name="departure[trip_id]" value="{{ $departure->trip_id }}" />


                                                <input type="hidden" name="arrival[departure_date_and_time]" value="{{ $arrival->departure_date_time }}"/>
                                                <input type="hidden" name="arrival[arrival_date_and_time]" value="{{ $arrival->arrival_date_time }}"/>
                                                <input type="hidden" name="arrival[duration]" value="{{ $arrival->duration }}"/>
                                                <input type="hidden" name="arrival[stop_quantity]" value="{{ $arrival->stop_quantity }}" />
                                                <input type="hidden" name="arrival[flight_number]" value="{{ $arrival->flight_number }}"/>
                                                <input type="hidden" name="arrival[booking_desig_code]" value="{{ $arrival->res_book_desig_code }}"/>
                                                <input type="hidden" name="arrival[departure_terminal]" value="{{ $arrival->departure_terminal }}"/>
                                                <input type="hidden" name="arrival[arrival_terminal]" value="{{ $arrival->arrival_terminal }}"/>
                                                <input type="hidden" name="arrival[air_equip_type]" value="{{ $arrival->air_equip_type }}"/>
                                                <input type="hidden" name="arrival[departure_location_code]" value="{{ $arrival->departure_location_code }}"/>
                                                <input type="hidden" name="arrival[departure_code_context]" value="{{ $arrival->departure_code_context }}"/>
                                                <input type="hidden" name="arrival[arrival_location_code]" value="{{ $arrival->arrival_location_code }}"/>
                                                <input type="hidden" name="arrival[arrival_code_context]" value="{{ $arrival->arrival_code_context }}"/>
                                                <input type="hidden" name="arrival[operating_airline_code]" value="{{ $arrival->operating_airline }}"/>
                                                <input type="hidden" name="arrival[marketing_airline_code]" value="{{ $arrival->marketing_airline }}"/>
                                                <input type="hidden" name="arrival[search_id]" value="{{ $arrival->search_id }}"/>
                                                <input type="hidden" name="arrival[trip_id]" value="{{ $arrival->trip_id }}" />
                                                @php
                                                $passengerCount = ["ADT" => (int)$request['adult'], "CHD" => (int)$request['child'], 'INF' => (int)$request['infant']];
                                                $passengerType = ["ADT" => 'adult', "CHD" => 'child', 'INF' => 'infant'];
                                                @endphp

                                                    {{--@dd($request, $passengerCount, $passengerType, $departure, $arrival)--}}

                                                    @foreach($departure->flyer as $flyer => $flyr)
                                                        @for($x = 0; $x < $passengerCount[$flyr->passenger_type]; $x++)

                                                        <input type="hidden" name="expire_date[]" value="{{ (new DateTime())->modify('+1 year')->format('Y-m-d') }}"/>
                                                        <div class="passenger-row @if(!$loop->last) mb-5 @endif">
                                                        <!-- <div class="row ml-0 mr-0 align-items-center"> -->
                                                            <div class="col-12">
                                                                <h5>{{ get_passenger_text($flyr->passenger_type, $x+1) }}</h5>
                                                                <p class="site-text red-text">Please make sure the information you enter is same as it is on your passport.</p>
                                                            </div>
                                                            <!-- <div class="col-md-5 col-12">
                                                                <p class="site-text">
                                                                    <input type="checkbox" id="stopover12" name="radio-group" class="removeMiddleName">
                                                                    <label for="stopover12" class="bordered name">Only have one name?</label>
                                                                </p>
                                                            </div> -->
                                                        <!-- </div> -->
                                                        <fieldset>

                                                        <div class="row ml-0 mr-0 mt-4">
                                                            <div class="col-md-3 col-12">
                                                                <label for="" class="site-label">Select Title <span class="required">*</span> </label>
                                                                <div class="form-field">
                                                                    <select class="site-input icon" name="name_prefix[]" id="title" required>
                                                                        <option value="">Select</option>
                                                                        <option value="Mr">Mr</option>
                                                                        <option value="Mstr">Mstr</option>
                                                                        <option value="Mrs">Mrs</option>
                                                                        <option value="Miss">Miss</option>
                                                                        <option value="Ms">Ms</option>
{{--                                                                    <option value="Dr">Dr</option>--}}
                                                                        <option value="Dr">Dr (Male)</option>
                                                                        <option value="Dr">Dr (Female)</option>

{{--                                                                        Mr, Mrs, Miss, Dr, Mstr, Inf, Ms, Prof, Capt--}}
                                                                        </select>
                                                                    <i class="fa fa-chevron-down input-right-icon" aria-hidden="true"></i>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3 col-12">
                                                                <div class="form-field">
                                                                    <label for="first_name" class="site-label">First Name<span class="required">*</span> </label>
                                                                    <input type="text" class="site-input" placeholder="Enter First Name" name="name[{{ $loop->index }}][first_name]" id="first_name" required>
                                                                    <p class="site-text name">
                                                                        <input type="checkbox" id="stopover{{ $flyr->passenger_type . $x }}" name="radio-group" class="removeMiddleName">
                                                                        <label for="stopover{{ $flyr->passenger_type . $x }}" class="bordered name">Only have one name?</label>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-12">
                                                                <div class="form-field">
                                                                    <label for="surname" class="site-label">Middle Name</label>
                                                                    <input type="text" class="site-input" placeholder="Enter Middle Name" name="name[{{ $loop->index }}][middle_name]" id="surname">
                                                                    <input type="hidden" value="default" name="surname[]" id="defaultMiddleName">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-12">
                                                                <div class="form-field">
                                                                    <label for="last_name" class="site-label">Last Name<span class="required">*</span> </label>
                                                                    <input type="text" class="site-input" placeholder="Enter Last Name" name="name[{{ $loop->index }}][last_name]" id="last_name">
                                                                    <input type="hidden" value="default" name="name[]" id="defaultLastName">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row ml-0 mr-0 align-items-end">
                                                            <div class="col-md-4 col-12">
                                                                <div class="form-field">
                                                                    <label for="" class="site-label">Date of Birth <span class="required">*</span> </label>
                                                                    <div class="input-group date form">
                                                                        <input type="text" name="date_of_birth[]" value="31/01/1990" class="form-control dob datepicker site-input icon" placeholder="DOB-DD/MM/YYYY" autocomplete="off">
{{--                                                                        <input type="text" name="date_of_birth[]" class="form-control dob datepicker site-input icon" placeholder="DOB-DD/MM/YYYY" autocomplete="off">--}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-4 col-12">
                                                                <label for="phone_code" class="site-label">Phone # <span class="required">*</span> </label>
                                                                <div class="form-field">
                                                                    <select class="site-input icon phone" name="phone_code[]" id="phone_code" required>
                                                                        @foreach(\App\Country::all() as $country)
                                                                            <option value="{{ '+'.$country->phonecode }}" {{ (old('phone_code', '+61') && $country->code === 'AU') == '+'.$country->phonecode ? 'selected' : '' }}>{{ $country->code.' (+'.$country->phonecode.')' }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <i class="fa fa-chevron-down input-right-icon" aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-4 col-12">
                                                                <div class="form-field">
                                                                    <input type="text" class="site-input" placeholder="Enter Your Phone Number" name="phone[]" id="phone">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row ml-0 mr-0 align-items-end d-none">
                                                            <div class="col-md-4 col-12">
                                                                <label for="gender" class="site-label">Gender<span class="required">*</span> </label>
                                                                <div class="form-field">
                                                                    <select class="site-input icon" name="gender[]" id="gender" required>
                                                                        <option value="">Select</option>
                                                                        <option value="M" selected>Male</option>
                                                                        <option value="F">Female</option>
                                                                    </select>
                                                                    <i class="fa fa-chevron-down input-right-icon"
                                                                       aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-12">
                                                                <label for="traveller_type" class="site-label">Traveller Type<span class="required">*</span> </label>
                                                                <div class="form-field">
                                                                    <select class="site-input icon" name="traveller_type[]" id="traveller_type" readonly>
                                                                        <option value="">Select</option>
                                                                        <option value="ADT" selected {{ $flyr->passenger_type == 'ADT' ? 'selected' : '' }}>Adult</option>
                                                                        <option value="CHD" {{ $flyr->passenger_type == 'CHD' ? 'selected' : '' }}>Child</option>
                                                                        <option value="INF" {{ $flyr->passenger_type == 'INF' ? 'selected' : '' }}>Infant</option>
                                                                    </select>
                                                                    <i class="fa fa-chevron-down input-right-icon" aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-12">
                                                                <label for="nationality" class="site-label">Nationality<span class="required">*</span> </label>
                                                                <div class="form-field">
                                                                    <select class="site-input icon" name="nationality[]" id="nationality" required>
                                                                        <option value="">Select</option>
                                                                        @foreach(\App\Country::all() as $country)
                                                                            <option @if($country->name === 'Australia') selected @endif value="{{ strtoupper(substr($country->name, 0, 3)) }}">{{ $country->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <i class="fa fa-chevron-down input-right-icon" aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row ml-0 mr-0 align-items-end d-none">
                                                            <div class="col-md-4 col-12">
                                                                <div class="form-field">
                                                                    <label for="doc_id" class="site-label">Document ID:<span class="required">*</span> </label>
                                                                    <input type="text" class="site-input" placeholder="Enter Document ID" value="DEFAULT123" name="doc_id[]" id="doc_id" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-12">
                                                                <label for="doc_issue_location" class="site-label">Doc Issue Location:<span class="required">*</span> </label>
                                                                <div class="form-field">
                                                                    <input type="hidden" name="doc_issue_location[]" value="AUS">
                                                                    {{--<select class="site-input icon" name="doc_issue_location[]" id="doc_issue_location" required>
                                                                        <option value="">Select</option>
                                                                        @foreach(\App\Country::all() as $country)
                                                                            <option @if($country->name === 'Australia') selected @endif value="{{ strtoupper(substr($country->name, 0, 3)) }}">{{ $country->name }}</option>
                                                                        @endforeach
                                                                    </select>--}}
                                                                    <i class="fa fa-chevron-down input-right-icon"
                                                                       aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    </div>
                                                    @endfor

                                                    @if(!$loop->last) <hr> @endif
                                                @endforeach
                                            </div>
                                            <div class="form-separator-div">
                                                <div class="col-12 mt-4 right-then-center">
                                                    <button type="submit" class="site-btn red">CONTINUE</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-12 pl-0 pr-0">
                        <div id="accordion" class="site-accordion">
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <button class="btn-link" data-toggle="collapse"
                                            aria-expanded="false" aria-controls="collapseOne">
                                        <p class="accordion-heading">Flight Summary</p>
                                        <a href="{{ \URL::previous() }}" class="site-btn sm-btn red">Modify</a>
                                    </button>
                                </div>
                                <div id="collapse2" class="collapse show" aria-labelledby="headingTwo"
                                     data-parent="#accordion" style="">
                                    <div class="card-body p-0">
                                        <div class="upper-div">
                                            <div class="row ml-0 mr-0 mb-2">
                                                <div class="col-6 pl-0 pr-0">
                                                    <p class="site-text black-text">{{ __('TOTAL PASSENGER') }}</p>
                                                </div>
                                                <div class="col-6 pl-0 pr-0 text-right">
                                                    <p class="site-text black-text pr-3">{{ @$flightDetails['flyer'][0]['passenger_quantity'] }}</p>
                                                </div>
                                            </div>
                                            <div class="row ml-0 mr-0">
                                                <div class="col-6 pl-0 pr-0">
                                                    <p class="site-text black-text">{{ __('TOTAL AMOUNT') }}</p>
                                                </div>
                                                <div class="col-6 pl-0 pr-0 text-right">
                                                    <a class="site-text red-text" data-toggle="collapse"
                                                       href="#collapseEx2" role="button" aria-expanded="false"
                                                       aria-controls="collapseEx2">{{ '$'.@$flightDetails['fare']['total_fare_amount'] }}
                                                        <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="collapse show" id="collapseEx2">
                                            <div class="card card-body side-collapse-pd">
                                                <div class="row ml-0 mr-0 outbound-row">
                                                    <div class="col-6 pl-0 pr-0">
                                                        <p class="site-text space-nowrap popsb black-text">FLIGHTS</p>
                                                    </div>
                                                    <div class="col-6 pl-0 pr-0 text-right">
                                                        <img src="{{ asset('website/img/plane-takeoff.png') }}" class="plane img-fluid" alt="">
                                                    </div>
                                                </div>
{{--                                                @foreach($flightDetails['flights'] as $flight)--}}
                                                @foreach([$departure, $arrival] as $flight)
                                                    <div class="summary-div">
                                                        <div class="row ml-0 mr-0">
                                                            <div class="col-4 pl-0 pr-0">
                                                                <div class="summary-left">
                                                                    <p class="black-text sm-text popsb">{{ @$flight->departure_time_formated }}</p>
                                                                    <p class="site-text sm-text">{{ @$flight->departure_date_formated }}</p>

                                                                    <div class="flight-flag">
                                                                        <p class="black-text sm-text popsb mt-1">{{ @$flight->operating_airline.' '.@$flight->flight_number }}</p>
                                                                    </div>
                                                                    <p class="black-text sm-text popsb">{{ @$flight->arrival_time_formated }}</p>
                                                                    <p class="site-text sm-text">{{ @$flight->arrival_date_formated }}</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-8 pl-0 pr-0">
                                                                <ul class="site-ul flight-summary-ul">
                                                                    <li>
                                                                        <p class="black-text sm-text">{{ @$flight->airport_departure->city }} ({{ @$flight->airport_departure->iata }})</p>
                                                                        <p class="black-text sm-text">{{ @$flight->airport_departure->name }}</p>
                                                                        <p class="black-text sm-text">Terminal: {{ @$flight->departure_terminal }}</p>
                                                                        <p class="black-text sm-text">{{ @$flight->air_equip_name }}</p>
                                                                        <p class="black-text sm-text">Baggage: {{ @$flight->baggage_allowance->{"@attributes"}->Value }} {{ @$flight->baggage_allowance->{"@attributes"}->Unit }}</p>
                                                                        <p class="black-text sm-text" style="text-transform: capitalize;">{{ @$request['cabin'] }}</p>
                                                                    </li>
                                                                    <li>
                                                                        <p class="black-text sm-text">{{ @$flight->airport_arrival->city }} ({{ @$flight->airport_arrival->iata }})</p>
                                                                        <p class="black-text sm-text">{{ @$flight->airport_arrival->name }}</p>
                                                                        <p class="black-text sm-text">Arrival Terminal: {{ @$flight->arrival_terminal }}</p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

@push('js')

    <script>
        $(function(){
            $('form').on('change', '.removeMiddleName', function(e) {
                const $row = $(this).closest('.passenger-row').find('fieldset .row:first-child');

                $row.children('div:nth-child(1)').toggleClass('col-md-6 col-md-3');
                $row.children('div:nth-child(2)').toggleClass('col-md-6 col-md-3');
                $row.children('div:nth-child(3)').toggle();
                $row.children('div:nth-child(4)').toggle();

                // $(this).closest('.passenger-row').find('fieldset .row:first-child>div:nth-child(3)').toggle();
            });
        });
    </script>

@endpush
