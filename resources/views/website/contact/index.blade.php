@extends('website.layouts.app', ['Title' => 'Conatct Us'])

@push('js')
    <script>
        $(document).ready(function(){
            $('#cm-form-contact-us').on('submit', function(e){
                e.preventDefault()
                let $form = $(this)
                swal({
                    title: "Confirmation",
                    text: "Are you sure?",
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "No",
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: true,
                        },
                        confirm: {
                            text: "Yes",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(isConfirm => {
                    if (isConfirm) {
                        $.ajax({
                            url: $form.attr('action'),
                            dataType: 'JSON',
                            cache: false,
                            data: $form.serialize(),
                            type: 'post',
                            success: function (response) {
                                if (response.status == true) {
                                    swal("Success", response.message, "success");
                                    $form[0].reset();
                                } else {
                                    swal("Failed", response.message, "error");
                                }
                            },
                        });
                    }
                });

            })
        })
    </script>
@endpush

@section('content')
    <section class="light-blue-bg-section extra-pd">
        <div class="container">
            <div id="accordion" class="site-accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <button class="btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            <p class="accordion-heading">Contact Us</p>
                            <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">
                            <form action="{{ route('website.contact-us') }}" class="contact-form cm-form-validation" method="post" id="cm-form-contact-us">
                                @csrf
                                <div class="form-field">
                                    <label for="name" class="site-label">Your Name <span class="required">*</span></label>
                                    <input type="text" class="site-input" placeholder="Enter Name" name="name" id="name" data-validation="required">
                                </div>
                                <div class="form-field">
                                    <label for="email" class="site-label">Email Address <span class="required">*</span></label>
                                    <input type="email" class="site-input" placeholder="Enter Email Address" name="email" id="email" data-validation="required email">
                                </div>
                                <div class="form-field">
                                    <label for="subject" class="site-label">Subject <span class="required">*</span></label>
                                    <input type="text" class="site-input" placeholder="Enter Subject" name="subject" id="subject" data-validation="required">
                                </div>
                                <div class="form-field">
                                    <label for="message" class="site-label">Message <span class="required">*</span></label>
                                    <textarea class="site-input" id="" cols="30" rows="7" placeholder="Enter Message" name="message" data-validation="required"></textarea>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="site-btn blue">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
