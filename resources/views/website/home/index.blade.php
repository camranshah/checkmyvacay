@extends('website.layouts.app', ['Title' => 'Home'])
@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush
@push('js')

    <script>
        $(document).ready(function () {
            chaneRoundTripHandler();
            function templateSelect (payload) {
                if(payload.loading){
                    return $('<a class="dropdown-item text-center" href="#"><i class="fa fa-spin fa-spinner"></i></a>');
                }
                var $container = $(`
                    <a class="dropdown-item" href="#">
                        <p class="droplist-maintext"><span class="red-text">${payload.city} </span>(${payload.id}) </p>
                        <p class="droplist-subtext">${payload.text}</p>
                    </a>
                `);
                  return $container;
            }

            $('li.dropdown.mega-dropdown a').on('click', function (event) {
                $(this).parent().toggleClass('open');
            });

            const ajaxObj = {
                    url: '{{ url('web/airports?for=website') }}',
                    data: function (params) {
                        var query = {
                            keyword: params.term,
                            page: params.page || 1
                        }
                        return query;
                    }
                };

            const fromPicker = $('.cm-select-from-airports').select2({
                allowClear: true,
                escapeMarkup : function(markup) { return markup; }, // let our custom formatter work
                 placeholder: {
                    id: "",
                    text: '<i class="fas fa-plane-departure drop-icon"></i>Leaving From'
                },
                templateResult: templateSelect,
                ajax: ajaxObj
            });
            const toObject = {
                allowClear: true,
                escapeMarkup : function(markup) { return markup; }, // let our custom formatter work
                 placeholder: {
                    id: "",
                    text: '<i class="fas fa-plane-arrival drop-icon"></i>Going To'
                },
                templateResult: templateSelect,
                ajax: ajaxObj
            };

            $('.cm-select-to-airports').select2(toObject);


            // toPicker.on('select2:select', function(e){
            //     $('.cm-select-to-airports').select2('trigger', 'select', {data: e.params.data});
            // })

            // fromPicker.on('select2:select', function(e){
            //     $('.cm-select-from-airports').select2('trigger', 'select', {data: e.params.data});
            // })

            // $("#switcher").on('click', function(){
            //     const from = $('.cm-select-from-airports').val();
            //     const to = $('.cm-select-to-airports').val();
            //     console.log(fromPicker.select2('data'));
            //     // fromPicker.val(to).trigger("change");
            //     // toPicker.val(from).trigger("change");
            //
            // });


            $('.cm-departure-date').datepicker({
                format: 'dd/mm/yyyy',
                startDate: '+1d'
            });


            $('.cm-arrival-date').datepicker({
                format: 'dd/mm/yyyy',
                startDate: '+1d'
            });
            $('.repeater').repeater({
                show: function () {
                    $(this).slideDown();

                    $('.select2').remove();

                    $('.cm-departure-date').datepicker({
                        format: 'dd/mm/yyyy',
                        startDate: '+1d'
                    });

                    $('.cm-select-from-airports').select2({
                        allowClear: true,
                        escapeMarkup : function(markup) { return markup; }, // let our custom formatter work
                         placeholder: {
                            id: "",
                            text: '<i class="fas fa-plane-departure drop-icon"></i>Leaving From'
                        },
                        templateResult: templateSelect,
                        ajax: {
                            url: '{{ url('web/airports?for=website') }}',
                            data: function (params) {
                                var query = {
                                    keyword: params.term,
                                    page: params.page || 1
                                }
                                return query;
                            }
                        },

                    });

                    $('.cm-select-to-airports').select2({
                        allowClear: true,
                        escapeMarkup : function(markup) { return markup; }, // let our custom formatter work
                         placeholder: {
                            id: "",
                            text: '<i class="fas fa-plane-arrival drop-icon"></i>Going To'
                        },
                        templateResult: templateSelect,
                        ajax: {
                            url: '{{ url('web/airports?for=website') }}',
                            data: function (params) {
                                var query = {
                                    keyword: params.term,
                                    page: params.page || 1
                                }
                                return query;
                            }
                        }
                    });

                },
                hide: function () {

                    $(this).slideUp();

                }
            });
        })
        $( ".show-package-dropdown").click(function() {
          $('.package-popup').slideToggle('slow');
        });


        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10){ dd='0'+dd }
        if(mm<10){ mm='0'+mm }
        var today = dd+'/'+mm+'/'+yyyy;
        $(function() {
          $('input[name="daterange"]').daterangepicker({
              opens: 'right',
              locale: {
                  format: 'DD/MM/YYYY',
              },
              autoApply: true,
              minDate:today
          }, function(start, end, label) {
                $('#pills-round').find('.cm-arrival-date').val(start.format('DD/MM/YYYY'));
                $('#pills-round').find('.cm-departure-date').val(end.format('DD/MM/YYYY'));
                // console.log(start.format('DD/MM/YYYY'), end.format('DD/MM/YYYY'));
          });

          // $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
          //     console.log()
          //   $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
          // });
          //
          // $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
          //   $(this).val('');
          // });
        });

    </script>
    <script>
        $(function(){

            function getSelectedAirPortResult (keyword, selector) {
                $.ajax({
                    url: '{!! url('web/airports?for=website&page=1&keyword=') !!}' + keyword,
                    success: function (data) {
                        const result = data.results[0];
                        const fd = {
                            city: result.city,
                            disabled: false,
                            id: result.id,
                            selected: true,
                            sort: result.sort,
                            text: result.text,
                        };
                        $(selector).select2('trigger', 'select', { data: fd })
                    }
                });
            }

            @if(request('type') == 'one_way')

                getSelectedAirPortResult('{{strtolower(request('from.0'))}}', '.cm-select-from-airports:eq(1)')
                getSelectedAirPortResult('{{strtolower(request('to.0'))}}', '.cm-select-to-airports:eq(1)')

            @endif

            $('#pills-tab li').on('shown.bs.tab', function (e) {
                e.target // newly activated tab
                e.relatedTarget // previous active tab
                const relevantTabs = ['pills-one-tab', 'pills-round-tab'];
                const id = $(e.target).attr('id');
                if(!relevantTabs.includes(id)) return;

                if(id === relevantTabs[0]){
                    const dataFrom = $('.cm-select-from-airports:eq(0)').select2('data');
                    // console.log(dataFrom)
                    if(dataFrom[0])
                        $('.cm-select-from-airports:eq(1)').select2('trigger', 'select', { data: dataFrom[0] })

                    const data = $('.cm-select-to-airports:eq(0)').select2('data');
                    if(data[0])
                        $('.cm-select-to-airports:eq(1)').select2('trigger', 'select', { data: data[0] })
                }

                if(id === relevantTabs[1]){
                    const dataFrom = $('.cm-select-from-airports:eq(1)').select2('data');
                    if(dataFrom[0])
                        $('.cm-select-from-airports:eq(0)').select2('trigger', 'select', { data: dataFrom[0] })

                    const data = $('.cm-select-to-airports:eq(1)').select2('data');
                    if(data[0])
                        $('.cm-select-to-airports:eq(0)').select2('trigger', 'select', { data: data[0] })
                }
            })
        });
    </script>
@endpush

@section('content')
    @if ($messages = $errors->any())
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            @foreach($errors->all() as $message)
                <p><strong>{{ '*'.$message }}</strong></p>
            @endforeach
        </div>
    @endif

    <section class="homepage">
        <div class="container">

            @include('website.__partials.header')

            <div class="main-banner-div">
                <h1 class="banner-mainheading text-capitalize">Fly <span class="stroke-red">Your</span> <span class="stroke-blue">Way</span></h1>
                <!-- <h1 class="banner-mainheading text-capitalize">has never been so <span class="stroke-blue">easy!</span></h1> -->
                <h4 class="banner-subheading text-capitalize">Booking A Flight Has Never Been So Easy</h4>

                <div class="banner-search-div">

                    <ul class="nav nav-pills main-banner" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link {{ request('type', 'return') === 'return'? 'active': '' }}" id="pills-round-tab" data-toggle="pill" href="#pills-round"
                               role="tab" aria-controls="pills-round" aria-selected="true">Round Trip</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request('type', 'return') === 'one_way'? 'active': '' }}" id="pills-one-tab" data-toggle="pill" href="#pills-one" role="tab"
                               aria-controls="pills-one" aria-selected="false">One Way</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request('type', 'return') === 'multiple'? 'active': '' }}" id="pills-multi-tab" data-toggle="pill" href="#pills-multi"
                               role="tab" aria-controls="pills-multi" aria-selected="false">Multi Way</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane {{ request('type', 'return') === 'return'? 'fade show active': 'fade' }}" id="pills-round" role="tabpanel"
                             aria-labelledby="pills-round-tab">
                           @include('website.__partials.forms.search-round')
                        </div>
                        <div class="tab-pane {{ request('type', 'return') === 'one_way'? 'fade show active': 'fade' }}" id="pills-one" role="tabpanel" aria-labelledby="pills-one-tab">
                           @include('website.__partials.forms.search-one-way')
                        </div>
                        <div class="tab-pane {{ request('type', 'return') === 'multiple'? 'fade show active': 'fade' }}" id="pills-multi" role="tabpanel" aria-labelledby="pills-multi-tab">
                           @include('website.__partials.forms.search-multiple')
                        </div>
                    </div>
                    <!-- <a class="stopover-text">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add Stop
                    </a> -->
                    <!-- <p class="stopover-text">

                        <input type="checkbox" style="display:none;" id="stopover" name="radio-group">
                        <label class="stopover-text stopover " for="stopover"><i class="fas fa-plus" aria-hidden="true"></i> Add Stopover</label>
                    </p>
                    <div class="outbound-div" style="display:none;">
                        <div class="row m-0 mr-0">
                            <div class="col-lg-6 col-12 pl-0 pr-0 d-flex align-items-end outbound-col">
                                <div class="outbound-input-div">
                                    <label for="" class="stopover-label">Outbound Stopover</label>
                                    <div class="dropdown">
                                        <div class="banner-select dropdown-toggle" type="button"
                                             id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                             aria-expanded="false">
                                            <i class="fas fa-plane-departure drop-icon"></i>
                                            Stop Over Destination
                                        </div>
                                        <div class="dropdown-menu leaving-dropdown"
                                             aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">
                                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(SYD)
                                                </p>
                                                <p class="droplist-subtext">Kingsford Smith in Australia</p>
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(RSE)
                                                </p>
                                                <p class="droplist-subtext">Sydney Au-Rose B Smith Australia</p>
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(YQY)
                                                </p>
                                                <p class="droplist-subtext">Sydney (Canda) B Canada</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-field mb-0">
                                    <i class="fa fa-chevron-up drop-arrow-up" aria-hidden="true"></i>
                                    <select name="" id="" class="day-night-select">
                                        <option value="">Nights</option>
                                        <option value="">Days</option>
                                    </select>
                                    <i class="fa fa-chevron-down drop-arrow" aria-hidden="true"></i>
                                </div>

                            </div>
                            <div class="col-lg-6 col-12 pl-0 pr-0 d-flex align-items-end outbound-col">
                                <div class="outbound-input-div">
                                    <label for="" class="stopover-label">Return Stopover</label>
                                    <div class="dropdown">
                                        <div class="banner-select dropdown-toggle" type="button"
                                             id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                             aria-expanded="false">
                                            <i class="fas fa-plane-departure drop-icon"></i>
                                            Stop Over Destination
                                        </div>
                                        <div class="dropdown-menu leaving-dropdown"
                                             aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">
                                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(SYD)
                                                </p>
                                                <p class="droplist-subtext">Kingsford Smith in Australia</p>
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(RSE)
                                                </p>
                                                <p class="droplist-subtext">Sydney Au-Rose B Smith Australia</p>
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <p class="droplist-maintext"><span class="red-text">Sydney</span>(YQY)
                                                </p>
                                                <p class="droplist-subtext">Sydney (Canda) B Canada</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-field mb-0">
                                    <i class="fa fa-chevron-up drop-arrow-up" aria-hidden="true"></i>
                                    <select name="" id="" class="day-night-select">
                                        <option value="">Nights</option>
                                        <option value="">Days</option>
                                    </select>
                                    <i class="fa fa-chevron-down drop-arrow" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <section class="banner-below">
        <div class="container">
            <div class="row ml-0 mr-0">
                <div class="col-md-4 col-12">
                    <div class="travel-card">
                        <img src="{{ asset('website/img/city1.png') }}" class="travel-city img-fluid" alt="">
                        <div class="travel-card-content">
                            <p class="travelcard-heading">Introducing the new CMF App</p>
                            <p class="site-text">
                                Lorem Ipsum. Proin gravida nibh velit it’s a cold world out there.This is
                                Photoshop's version of Lorem Ipsum.
                            </p>
                            <div class="travel-arrow-div right-then-center">
                                <i class="fas fa-arrow-right travel-arrow red"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="travel-card">
                        <img src="{{ asset('website/img/city2.png') }}" class="travel-city img-fluid" alt="">
                        <div class="travel-card-content">
                            <p class="travelcard-heading">Introducing the new CMF App</p>
                            <p class="site-text">
                                Lorem Ipsum. Proin gravida nibh velit it’s a cold world out there.This is
                                Photoshop's version of Lorem Ipsum.
                            </p>
                            <div class="travel-arrow-div right-then-center">
                                <i class="fas fa-arrow-right travel-arrow blue"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="travel-card">
                        <img src="{{ asset('website/img/city3.png') }}" class="travel-city img-fluid" alt="">
                        <div class="travel-card-content">
                            <p class="travelcard-heading">Introducing the new CMF App</p>
                            <p class="site-text">
                                Lorem Ipsum. Proin gravida nibh velit it’s a cold world out there.This is
                                Photoshop's version of Lorem Ipsum.
                            </p>
                            <div class="travel-arrow-div right-then-center">
                                <i class="fas fa-arrow-right travel-arrow red"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--<section class="travel-insurance">
        <div class="container">
            <div class="row ml-0 mr-0">
                <div class="col-md-6 col-12">
                    <h1 class="banner-mainheading dark-blue-text text-capitalize mb-4">Travel <span
                            class="stroke-red">Insurance</span></h1>
                    <p class="site-text mb-2">
                        CheckMyFares.com is an Australian owned and operated online travel company, ABN 58 628 811
                        521.
                    </p>
                    <p class="site-text mb-2">
                        We utilise industry leading technology to offer customers a comprehensive and simple flight
                        booking tool, to access great value airfares. Our customers can compare and book flights
                        from all major airlines worldwide at a great price, with piece of mind.
                    </p>
                    <p class="site-text mb-2">
                        CheckMyFares is ATAS accredited (A15782), by the Australian Federation of Travel Agents
                        (AFTA) and is accredited by IATA to promote and sell international air passenger
                        transportation (HE 02365694).
                    </p>
                    <p class="site-text">
                        Customers have complete control of their booking process, whether it’s a simple domestic
                        flight or a complex international itinerary with stop overs, multiple destinations and even
                        dual one-way flights.
                    </p>
                    <div class="text-left mt-3">
                        <a href="" class="site-btn blue">read more</a>
                    </div>
                </div>
                <div class="col-md-6 col-12 text-center">
                    <img src="{{ asset('website/img/travel-man.png') }}" class="travel-man img-fluid" alt="">
                </div>
            </div>

        </div>
    </section>--}}
    <section class="check-vacay">
        <div class="container">
            <h1 class="banner-mainheading white-text text-capitalize mb-4">CheckMy<span
                    class="stroke-red">Vacay</span></h1>
            <div class="row ml-0 mr-0">
                <div class="col-md-4 col-12">
                    <div class="vacay-card">
                        <img src="{{ asset('website/img/service-img1.png') }}" class="vacay-img img-fluid" alt="">
                        <p class="travelcard-heading white-text">Competitive Pricing</p>
                        <p class="site-text">
                            Lorem Ipsum. Proin gravida nibh velit it’s a cold world out there.This is Photoshop's
                            version of Lorem Ipsum.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="vacay-card">
                        <img src="{{ asset('website/img/service-img2.png') }}" class="vacay-img img-fluid" alt="">
                        <p class="travelcard-heading white-text">Award-Winning Service</p>
                        <p class="site-text">
                            Lorem Ipsum. Proin gravida nibh velit it’s a cold world out there.This is Photoshop's
                            version of Lorem Ipsum.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="vacay-card">
                        <img src="{{ asset('website/img/service-img3.png') }}" class="vacay-img img-fluid" alt="">
                        <p class="travelcard-heading white-text">Worldwide Coverage</p>
                        <p class="site-text">
                            Lorem Ipsum. Proin gravida nibh velit it’s a cold world out there.This is Photoshop's
                            version of Lorem Ipsum.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="best-app">
        <div class="container">
            <div class="row ml-0 mr-0 align-items-center">
                <div class="col-md-6 col-12 center-then-left">
                    <img src="{{ asset('website/img/mobile-img.png') }}" class="mobile-img img-fluid" alt="">
                </div>
                <div class="col-md-6 col-12">
                    <h1 class="banner-mainheading text-capitalize">The best flight <span
                            class="stroke-blue">app</span></h1>
                    <h1 class="banner-mainheading text-capitalize">for your <span class="stroke-blue">mobile</span>
                        app.</h1>
                    <p class="site-text white-text mt-4">
                        Lorem Ipsum veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate.
                    </p>
                    <img src="{{ asset('website/img/stores-availabe.png')}}" class="store-img img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="popular-destination">
        <div class="container">
            <h1 class="banner-mainheading dark-blue-text text-capitalize"><span class="stroke-blue">Popular</span>
                destinations</h1>
            <div class="slider-nav homepage-slider">
                <div>
                    <div class="travel-card">
                        <img src="{{ asset('website/img/pop-destination1.png') }}" class="travel-city img-fluid" alt="">
                        <div class="travel-card-content">
                            <p class="travelcard-heading">Introducing the new CMF App</p>
                            <p class="travelcard-subheading">Lorem Ipsum. Proin gravida nibh</p>
                            <p class="site-text">
                                Lorem Ipsum. Proin gravida nibh velit it’s a cold world out there.This is
                                Photoshop's version of Lorem Ipsum.
                            </p>
                            <div class="travel-arrow-div right-then-center">
                                <i class="fas fa-arrow-right travel-arrow red"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="travel-card">
                        <img src="{{ asset('website/img/pop-destination2.png') }}" class="travel-city img-fluid" alt="">
                        <div class="travel-card-content">
                            <p class="travelcard-heading">Introducing the new CMF App</p>
                            <p class="travelcard-subheading">Lorem Ipsum. Proin gravida nibh</p>
                            <p class="site-text">
                                Lorem Ipsum. Proin gravida nibh velit it’s a cold world out there.This is
                                Photoshop's version of Lorem Ipsum.
                            </p>
                            <div class="travel-arrow-div right-then-center">
                                <i class="fas fa-arrow-right travel-arrow blue"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="travel-card">
                        <img src="{{ asset('website/img/pop-destination3.png') }}" class="travel-city img-fluid" alt="">
                        <div class="travel-card-content">
                            <p class="travelcard-heading">Introducing the new CMF App</p>
                            <p class="travelcard-subheading">Lorem Ipsum. Proin gravida nibh</p>
                            <p class="site-text">
                                Lorem Ipsum. Proin gravida nibh velit it’s a cold world out there.This is
                                Photoshop's version of Lorem Ipsum.
                            </p>
                            <div class="travel-arrow-div right-then-center">
                                <i class="fas fa-arrow-right travel-arrow red"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="travel-card">
                        <img src="{{ asset('website/img/pop-destination3.png') }}" class="travel-city img-fluid" alt="">
                        <div class="travel-card-content">
                            <p class="travelcard-heading">Introducing the new CMF App</p>
                            <p class="travelcard-subheading">Lorem Ipsum. Proin gravida nibh</p>
                            <p class="site-text">
                                Lorem Ipsum. Proin gravida nibh velit it’s a cold world out there.This is
                                Photoshop's version of Lorem Ipsum.
                            </p>
                            <div class="travel-arrow-div right-then-center">
                                <i class="fas fa-arrow-right travel-arrow red"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center mt-4">
                <a href="" class="site-btn blue">view more</a>
            </div>
        </div>
    </section>
@endsection
