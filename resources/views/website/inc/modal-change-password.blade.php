<div class="modal fade" id="cm-modal-change-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered site-modal" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="site-modalheading mb-3 popsb">Change Password</h5>
            <form action="{{ route('website.profile.change-password') }}" method="post" class="cm-form-validation" id="cm-form-change-password">
                @csrf
                <label for="" class="site-label">Current Password</label>
                <div class="form-field">
                    <input type="password" class="site-input icon current-input" placeholder="Enter Current Password" name="current_password" data-validation="required">
                    <i class="fa fa-eye-slash current-icon input-right-icon" aria-hidden="true"></i>
                </div>
                <label for="" class="site-label">New Password</label>
                <div class="form-field">
                    <input type="password" class="site-input icon enter-input" placeholder="Enter New Password" name="password" data-validation="required">
                    <i class="fa fa-eye-slash enter-icon input-right-icon" aria-hidden="true"></i>
                </div>
                <label for="" class="site-label">Confirm Password</label>
                <div class="form-field">
                    <input type="password" class="site-input icon confirm-input" placeholder="Confirm Password" name="password_confirmation" data-validation="required">
                    <i class="fa fa-eye-slash confirm-icon input-right-icon" aria-hidden="true"></i>
                </div>
                <div class="text-center">
                    <button type="submit" class="site-btn blue text-center text-uppercase mt-2">Update password</button>
                </div>
            </form>
        </div>
    </div>
</div>
