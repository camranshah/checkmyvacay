<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ @$Title.' - '.config('app.name') }}</title>
    <link rel="icon" href="{{ asset('website/img/favicon.png') }}" type="image/gif" sizes="30x30">
    <link rel="stylesheet" href="{{ asset('website/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/jquery.mCustomScrollbar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/fonts/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/ion.rangeSlider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('website/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/app-assets/vendors/css/forms/selects/select2.min.css') }}">
    <link href="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css" rel="stylesheet" type="text/css"/>

    @stack('css')
    <style>
        .select2-container--open .select2-dropdown--below {
            width: auto !important;
            margin-top: 5px;
        }
        .droplist-subtext {
            white-space: nowrap !important;
            overflow: unset !important;
            text-overflow: unset !important;
        }
        #pills-round .ul-flex-center li:nth-child(3) ,
        #pills-one .ul-flex-center li:nth-child(3) {
            width: 250px;
        }

        #pills-round .ul-flex-center li:nth-child(3) input ,
        #pills-one .ul-flex-center li:nth-child(3) input {
            max-width: 100%;
        }

        #pills-round .ul-flex-center li:nth-child(4) ,
        #pills-one .ul-flex-center li:nth-child(4) {
            flex: 2;
        }

        #pills-round .ul-flex-center li:nth-child(4) * ,
        #pills-one .ul-flex-center li:nth-child(4) * {
            width: 100%;
        }

        #pills-round .ul-flex-center li:nth-child(4) span ,
        #pills-one .ul-flex-center li:nth-child(4) span {
            text-align: left;
        }

        #pills-round .ul-flex-center li:nth-child(4) i.fa-user ,
        #pills-one .ul-flex-center li:nth-child(4) i.fa-user {
            width: 20px;
        }
        .daterangepicker td.disabled, .daterangepicker option.disabled {
            text-decoration: none;
        }
    </style>
    <script>
        window.base_url = '{{ url('/') }}'
    </script>
</head>
<body>
<div class="wrapper">
    <div id="content">

        @if(Request::segment(1) !== 'login')
            @include('website.__partials.header')
        @endif

        @yield('content')

        @include('website.__partials.footer')

    </div>
</div>
<div class="overlay"></div>
<script src="{{ asset('website/js/jquery.min.js') }}"></script>
<script src="{{ asset('website/js/slick.min.js') }}"></script>
<script src="{{ asset('website/js/popper.min.js') }}"></script>
<script src="{{ asset('website/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('website/js/datatable-basic.js') }}"></script>
<script src="{{ asset('website/js/datatables.min.js') }}"></script>
<script src="{{ asset('website/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('website/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('website/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('website/js/bootstrap-datetimepicker.min.js') }}"></script>
{{--    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>--}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script src="{{ asset('custom/app-assets/vendors/js/forms/select/select2.full.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>
<script src="{{ asset('website/js/custom.js') }}"></script>
<script>
    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.validate({
            modules: 'location, date, security, file',
            form: '.cm-form-validation'
        });

        $('.cm-input-mask-number').mask('000000');
        $('.cm-input-mask-percent').mask('00.00');
        $('.cm-input-mask-cvv').mask('000');
        $('.cm-input-mask-phone-us').mask('(000) 000-0000');
        $('.cm-input-mask-credit-card-expiry-date').mask('00/0000');
        $('.cm-input-mask-credit-card-number').mask('0000-0000-0000-0000');

    })
</script>
@stack('js')
</body>
</html>
