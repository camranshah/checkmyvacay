@extends('website.layouts.app', ['Title' => 'Manage Bookings'])

@section('content')
    <section class="light-blue-bg-section extra-pd">
        <div class="container">
            <div id="accordion" class="site-accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <button class="btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false"
                                aria-controls="collapseOne">
                            <p class="accordion-heading">Manage Booking</p>
                            <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion"
                         style="">
                        <div class="card-body">
                            <form action="{{ route('website.manage-booking') }}" class="contact-form cm-form-validation"
                                  method="POST">
                                @csrf
                                <div class="form-field">
                                    <label for="booking_no" class="site-label">Booking Number <span
                                            class="required">*</span></label>
                                    <input type="text" class="form-control site-input @error('booking_no') is-invalid @enderror" id="booking_no" placeholder="CMF-2019-12345"
                                           name="booking_no"
                                           value="{{ old('booking_no') }}" data-validation="required"
                                           data-validation-error-msg="Please enter booking no.">
                                    @error('booking_no')
                                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                                <div class="form-field">
                                    <label for="last_name" class="site-label">Last Name <span class="required">*</span></label>
                                    <input type="text" class="form-control site-input @error('last_name') is-invalid @enderror" id="last_name" placeholder="Enter Last Name"
                                           name="last_name"
                                           value="{{ old('last_name') }}" data-validation="required"
                                           data-validation-error-msg="Please enter last name">
                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                                <div class="text-center">
                                    <button class="site-btn red">Manage Booking</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
