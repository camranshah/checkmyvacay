@extends('website.layouts.app', ['Title' => 'Manage Bookings'])

@section('content')
    <section class="light-blue-bg-section extra-pd">
        <div class="container">
            <div class="col-12 pl-0 pr-0 left-col-spacing">
                <div id="" class="site-accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <button class="btn-link" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="false" aria-controls="collapseOne">
                                <p class="accordion-heading">Manage Booking</p>
                                <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                             data-parent="#accordion" style="">
                            <div class="card-body rounded-0">
                                <div class="row ml-0 mr-0 align-items-center">
                                    <div class="col-md-3 col-sm-6 col-12 pl-0 pr-0">
                                        <div class="d-flex align-items-center booking-first-div">
                                            <i class="fas fa-plane-departure drop-icon mr-4"></i>
                                            <div>
                                                <p class="site-text black-text popMed">{{ @$Booking->booking_id }}</p>
                                                <p class="site-text black-text">{{ @$Booking->flight_details['originDepartureLocationCode'] }}
                                                    > {{ @$Booking->flight_details['originArrivalLocationCode'] }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-12  pl-0 pr-0">
                                        <div class="booking-second-div">
                                            <p class="site-text black-text popMed">DEP: {{ date('M d, Y', strtotime($Booking->flight_details['originDepartureDateTime'])) }}</p>
                                            <p class="site-text black-text">{{ @getAirlineName($Booking->flight_details['originDepartureAirlineCode'])['name'] }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-12 pl-0 pr-0">
                                        <div class="booking-third-div">
                                            <p class="site-text black-text popMed">BOOKED </p>
                                            <p class="site-text black-text">{{ date('M d, Y', strtotime($Booking->created_at)) }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-12 pl-0 pr-0">
                                        <div class="booking-last-div">
                                            <a data-toggle="collapse" href="#collapseExample" role="button"
                                               aria-expanded="false" aria-controls="collapseExample">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <p class="site-text black-text popMed">Total Amount:</p>
                                                        <p class="site-text red-text popMed"><span
                                                                class="black-text popsb">$ </span>{{ @$Booking->flight_details['totalFlightAmount'] }}</p>
                                                    </div>

                                                    <i class="fa fa-chevron-down black-text down"
                                                       aria-hidden="true"></i>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="collapse show" id="collapseExample">
                                    <div class="card card-body manage-booking-body">
                                        <div class="d-sm-flex align-items-center justify-content-between">
                                            <div>
                                                <p class="site-text black-text popsb">GDS PNR: {{ @$Booking->gds_pnr }}</p>
                                                <p class="site-text black-text popsb">Airline PNR: {{ @$Booking->airline_pnr }}</p>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row ml-0 mr-0">
                                            <div class="col-md-4 col-12 pl-0 mb-2">
                                                <p class="site-text black-text popsb">{{ @$Booking->flight_details['originDepartureAirlineCode'] }} {{ @$Booking->flight_details['originDepartureFlightNo'] }}</p>
                                                <p class="site-text black-text popsb text-uppercase">{{ @getAirlineName($Booking->flight_details['originDepartureAirlineCode'])['name'] }}</p>
                                            </div>
                                            <div class="col-md-8 col-12 pl-0">
                                                <div class="row ml-0 mr-0 align-items-center">
                                                    <div class="col-5 pl-0 pr-0">
                                                        <p class="site-text black-text popsb">{{ @$Booking->flight_details['airportDepartureTop']->name.' ('.@$Booking->flight_details['airportDepartureTop']->iata.')' }}</p>
                                                        <p class="site-text black-text">{{ @$Booking->flight_details['airportDepartureTop']->country }}</p>
                                                        <p class="site-text black-text">{{ @date('M d, Y', strtotime($Booking->flight_details['originDepartureDateTime'])) }}</p>
                                                        <p class="site-text black-text">{{ @date('H:i', strtotime($Booking->flight_details['originDepartureDateTime'])) }}</p>
                                                    </div>
                                                    <div class="col-2 pl-0 pr-0">
                                                        <p class="site-text black-text popsb">To</p>
                                                    </div>
                                                    <div class="col-5 pl-0 pr-0">
                                                        <p class="site-text black-text popsb">{{ @$Booking->flight_details['airportArrivalTop']->name.' ('.@$Booking->flight_details['airportDepartureTop']->iata.')' }}</p>
                                                        <p class="site-text black-text">{{ @$Booking->flight_details['airportArrivalTop']->country }}</p>
                                                        <p class="site-text black-text">{{ @date('M d, Y', strtotime($Booking->flight_details['originArrivalDateTime'])) }}</p>
                                                        <p class="site-text black-text">{{ @date('H:i', strtotime($Booking->flight_details['originArrivalDateTime'])) }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div id="" class="site-accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <button class="btn-link rounded-0" data-toggle="collapse" data-target="#collapse2"
                                    aria-expanded="false" aria-controls="collapseOne">
                                <p class="accordion-heading">Passenger Information</p>
                                <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div id="collapse2" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion"
                             style="">
                            <div class="card-body rounded-0">
                                @foreach($Booking->flight_details['airTravelers'] as $traveller)
                                    <div class="d-md-flex align-items-center justify-content-between">
                                        <div class="mb-2">
                                            <p class="site-text black-text popsb">{{ $traveller['name'] }}</p>
                                        </div>
                                        <div class="mb-2">
                                            <p class="site-text black-text popsb">Ticket No.</p>
                                            <p class="site-text black-text">{{ $traveller['ticketDocumentNo'] }}</p>
                                        </div>
                                        <div class="mb-2">
                                            <p class="site-text black-text popsb">Date Of Issue:</p>
                                            <p class="site-text black-text">{{ @date('M d, Y', strtotime($traveller['ticketDocumentDateOfIssue'])) }}</p>
                                        </div>
                                        <div class="mb-2">
                                            <p class="site-text black-text popsb">Type:</p>
                                            <p class="site-text black-text">{{ $traveller['ticketDocumentType'] }}</p>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                <div id="" class="site-accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <button class="btn-link rounded-0" data-toggle="collapse" data-target="#collapse3"
                                    aria-expanded="false" aria-controls="collapseOne">
                                <p class="accordion-heading">Contact Details</p>
                                <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div id="collapse3" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion"
                             style="">
                            <div class="card-body rounded-0">
                                <p class="site-text black-text popsb mb-2">Mr. Yechen Peng</p>
                                <p class="site-text black-text popsb">alexpeng041@gmail.com</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="" class="site-accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <button class="btn-link rounded-0" data-toggle="collapse" data-target="#collapse4"
                                    aria-expanded="false" aria-controls="collapseOne">
                                <p class="accordion-heading">Fare Detail(s)</p>
                                <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div id="collapse4" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion"
                             style="">
                            <div class="card-body">
                                <div class="table-responsive fare-details">
                                    <div class="maain-tabble">
                                        <table class="table table-bordered zero-configuration fare-details">
                                            <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Product Name</th>
                                                <th>Quantity</th>
                                                <th>By</th>
                                                <th>Unit Price</th>
                                                <th>Product Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($Booking->flight_details['amountDetails'] as $fare)
                                            <tr>
                                                <td>
                                                    <p class="site-text popsb black-text">{{ $fare['type'] }}</p>
                                                </td>
                                                <td>
                                                    <p class="site-text popsb black-text">{{ $fare['qty'] }}</p>
                                                </td>
                                                <td>
                                                    <p class="site-text popsb black-text">
                                                        <i class="fas fa-times black-text"></i>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p class="site-text popsb red-text"><span
                                                            class="black-text popsb">$ </span>{{ $fare['baseFareAmount'] }}</p>
                                                </td>
                                                <td>
                                                    <p class="site-text popsb red-text"><span
                                                            class="black-text popsb">$ </span>{{ $fare['totalFareAmount'] }}</p>
                                                </td>
                                                <td>
                                                    <i class="fas fa-plus-circle fare-detail-plus"></i>
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
