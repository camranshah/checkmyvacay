@extends('website.layouts.app', ['Title' => 'Profile'])

@push('js')
    <script>
        $(document).ready(function () {
            $('#edit-details').click(function (e) {
                e.preventDefault();

                let $this = $(this)

                if ($(this).text() == 'Edit Details') {
                    $(".site-input").removeAttr("readonly");
                    $(".site-input.select").removeAttr("disabled");
                    $(this).text('Update');

                    return;
                }

                $('#cm-form-update-profile').submit();

            });
            $('#cm-form-update-profile').on('submit', function (e) {

                e.preventDefault()

                let $form = $(this)

                swal({
                    title: "Confirmation",
                    text: "Are you sure?",
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "No",
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: true,
                        },
                        confirm: {
                            text: "Yes",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(isConfirm => {
                    if (isConfirm) {
                        $.ajax({
                            url: $form.attr('action'),
                            dataType: 'JSON',
                            cache: false,
                            data: $form.serialize(),
                            type: 'post',
                            success: function (response) {
                                if (response.status == true) {
                                    swal("Success", response.message, "success");
                                } else {
                                    swal("Failed", response.message, "error");
                                }
                                $(".site-input").attr("readonly", true);
                                $(".site-input.select").attr("disabled", true);
                                $('#edit-details').text('Edit Details');
                            },
                        });
                    }
                });
            })
            $('#cm-form-change-password').on('submit', function(e){
                e.preventDefault()

                let $form = $(this)

                swal({
                    title: "Confirmation",
                    text: "Are you sure?",
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "No",
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: true,
                        },
                        confirm: {
                            text: "Yes",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(isConfirm => {
                    if (isConfirm) {
                        $.ajax({
                            url: $form.attr('action'),
                            dataType: 'JSON',
                            cache: false,
                            data: $form.serialize(),
                            type: 'post',
                            success: function (response) {
                                if (response.status == true) {
                                    swal("Success", response.message, "success");
                                    $('#cm-modal-change-password').modal('hide')
                                    $form[0].reset();
                                } else {
                                    swal("Failed", response.message, "error");
                                }
                            },
                        });
                    }
                });

            })
        })
    </script>
@endpush
@section('content')
    <section class="light-blue-bg-section extra-pd">
        <div class="container">
            <div id="accordion" class="site-accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <button class="btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false"
                                aria-controls="collapseOne">
                            <p class="accordion-heading">My Profile</p>
                            <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion"
                         style="">
                        <div class="card-body">
                            <form action="{{ route('website.profile') }}" class="register-form cm-form-validation"
                                  id="cm-form-update-profile">
                                <div class="row ml-0 mr-0">
                                    <div class="col-md-3 col-12">
                                        <label for="" class="site-label">Select Title <span
                                                class="required">*</span> </label>
                                        <div class="form-field">
                                            <select class="site-input icon select" name="title" id=""
                                                    data-validation="required" disabled>
                                                <option value="">Select</option>
                                                <option
                                                    value="Mr." {{ auth()->user()->profile->title == 'Mr.' ? 'selected' : '' }}>
                                                    Mr.
                                                </option>
                                                <option
                                                    value="Mrs." {{ auth()->user()->profile->title == 'Mrs.' ? 'selected' : '' }}>
                                                    Mrs.
                                                </option>
                                                <option
                                                    value="Ms." {{ auth()->user()->profile->title == 'Ms.' ? 'selected' : '' }}>
                                                    Ms.
                                                </option>
                                                <option
                                                    value="Dr." {{ auth()->user()->profile->title == 'Dr.' ? 'selected' : '' }}>
                                                    Dr.
                                                </option>
                                            </select>
                                            <i class="fa fa-chevron-down input-right-icon" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="form-field">
                                            <label for="" class="site-label">First Name <span class="required">*</span>
                                            </label>
                                            <input type="text" class="site-input" placeholder="Enter First Name"
                                                   name="first_name"
                                                   value="{{ old('first_name', auth()->user()->profile->first_name) }}"
                                                   data-validation="required" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="form-field">
                                            <label for="" class="site-label">Middle Name </label>
                                            <input type="text" class="site-input" placeholder="Enter Middle Name"
                                                   name="middle_name"
                                                   value="{{ old('middle_name', auth()->user()->profile->middle_name) }}"
                                                   readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="form-field">
                                            <label for="" class="site-label">Last Name <span class="required">*</span>
                                            </label>
                                            <input type="text" class="site-input" placeholder="Enter Last Name"
                                                   name="last_name"
                                                   value="{{ old('last_name', auth()->user()->profile->last_name) }}"
                                                   data-validation="required" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ml-0 mr-0 align-items-end">
                                    <div class="col-md-3 col-12">
                                        <div class="form-field">
                                            <label for="" class="site-label">Date of Birth <span
                                                    class="required">*</span> </label>
                                            <div class="input-group date form">
                                                <input type="text"
                                                       class="form-control date_1 datepicker site-input icon"
                                                       placeholder="DD/MM/YY" autocomplete="off" name="dob"
                                                       value="{{ auth()->user()->profile->dob }}"
                                                       data-validation="required" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-12">
                                        <label for="" class="site-label">Phone # <span class="required">*</span>
                                        </label>
                                        <div class="form-field">
                                            <select class="site-input icon select " name="phone_code" id=""
                                                    data-validation="required" required disabled>
                                                @foreach(\App\Country::all() as $country)
                                                    <option
                                                        value="{{ '+'.$country->phonecode }}" {{ '+'.$country->phonecode == auth()->user()->profile->phone_code ? 'selected' : '' }}>{{ $country->code.' (+'.$country->phonecode.')' }}</option>
                                                @endforeach
                                            </select>
                                            <i class="fa fa-chevron-down input-right-icon" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-4 col-12">
                                        <div class="form-field">
                                            <input type="text" class="site-input cm-input-mask-phone-us"
                                                   placeholder="Enter Your Phone Number" name="phone"
                                                   data-validation="required" id=""
                                                   value="{{ auth()->user()->profile->phone }}"
                                                   readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 left-then-center mt-3 profile-btn-div">
                                    <a class="site-btn red extra-pd mt-3 mr-3" id="edit-details">Edit Details</a>
                                    <a href="javascript:void(0)" class="site-btn blue extra-pd mt-3" data-toggle="modal"
                                       data-target="#cm-modal-change-password">Change Password</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('website.inc.modal-change-password')
@endsection
