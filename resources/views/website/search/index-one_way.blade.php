@extends('website.layouts.app', ['Title' => 'Search Results'])

<style>
.list-style-none{
    list-style: none;
}
.list-style-none label {
    display: block;
}

.list-style-none input[type=checkbox] {
    position: inherit;
}
</style>
@section('content')

{{--    @dump($data[0]['flights'])--}}
{{--    @dump(get_defined_vars()['__data'])--}}

    <section class="inner-page-pd" id="app">
        <div class="container">
            <ul class="site-ul ul-flex-center flight-overview-ul">

                <li>
                    <div class="flight-overview-single">
                        <i class="fas fa-plane-departure drop-icon"></i>
                        <div class="plane-side-text">
                            <p class="site-text black-text">{{ @$_GET['from'][0] }} to {{ @$_GET['to'][0] }}</p>
                            <p class="site-text">{{ @$_GET['departure_date'][0] }}</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flight-overview-single">
                        <i class="fas fa-user drop-icon"></i>
                        <div class="plane-side-text">
                            <p class="site-text black-text">Passenger (s)</p>
                            <p class="site-text">({{ @$_GET['adult'] }}) Adult ({{ @$_GET['child'] }}) Child
                                ({{ @$_GET['infant'] }}) Infant</p>
                        </div>
                    </div>
                </li>
                @if(isset($_GET['cabin']))
                    <li>
                        <div class="flight-overview-single">
                            <img src="{{ asset('website/img/cabin-icon.png') }}" class="drop-icon img-fluid" alt="">
                            <div class="plane-side-text">
                                <p class="site-text black-text">Cabin</p>
                                <p class="site-text">{{ @$_GET['cabin'] }}</p>
                            </div>
                        </div>
                    </li>
                @endif
                <li>
                    <a href="{{ route('website.home', $_GET) }}" class="site-btn red">MODIFY</a>
                </li>
            </ul>
        </div>
        <section class="light-blue-bg-section d-none" id="d-none">
            <one-way-listing-index
                inline-template
                requestFrom='{{ request('from.0') }}'
                :flights="{{ json_encode($x) }}"
            >

            <div class="container" v-if="airlineKeys.length > 0">
                <div class="row ml-0 mr-0">
                    <div class="col-12 pl-0 pr-0">
                        <div class="row ml-0 mr-0">
                            <div class="col-lg-3 col-12 pl-0 pr-0">

                            </div>
                            <div class="col-lg-9 col-12 pl-0 pr-0">
                                <p class="extra-text white-text"><i class="fas fa-plane-departure drop-icon"></i>433
                                    Flights Found</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-12 left-col-spacing pl-0 pr-0">
                        <div class="separate-div">
                            <p class="site-text text-center mb-3">
                                All Flights are displayed in Austrailian Dollars (AUD). in Local time (24hr Format)
                            </p>
                            <div class="d-flex align-items-center justify-content-center">
                                <i class="fa fa-filter filter" aria-hidden="true"></i>
                                <div>
                                    <p class="site-text red-text text-capitalize">Filter your result</p>
                                    <p class="site-text">0 Filter(s) Active</p>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs result-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab"
                                   href="#home" role="tab" aria-controls="home"
                                   aria-selected="true">{{ @$_GET['from'][0] }} &gt; {{ @$_GET['to'][0] }}</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="result-side-col">
                                    <div id="accordion" class="site-accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <button class="btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    <p class="accordion-heading">Cheapest time to fly</p>
                                                    <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                                <div class="card-body">
                                                    <p class="accordion-heading mb-2">Departure</p>
                                                    <histogramslider
                                                        v-if="departure_data.length > 1"
                                                        :width="220"
                                                        :data="departure_data"
                                                        :bar-gap="1"
                                                        :bar-width="6"
                                                        :bar-radius="0"
                                                        :grid-num="0"
                                                        :bar-height="50"
                                                        :prettify="prettify"
                                                        :handle-size="15"
                                                        :force-edges="true"
                                                        :colors="['#bac6f2', '#bac6f2']"
                                                        :min="0"
                                                        :max="24"
                                                        @finish="finishDepartureTime($event)"
                                                    ></histogramslider>
                                                    <div class="timeContainer">
                                                        <span>@{{ formatTime(selectedDepartureTime.from) }}</span>
                                                        <span>@{{ formatTime(selectedDepartureTime.to) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <button class="btn-link collapsed" data-toggle="collapse"
                                                        data-target="#collapse2" aria-expanded="false"
                                                        aria-controls="collapseOne">
                                                    <p class="accordion-heading">Airlines</p>
                                                    <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div id="collapse2" class="collapse" aria-labelledby="headingOne"
                                                 data-parent="#accordion" style="">
                                                <div class="card-body">
                                                    <ul class="p-0 m-0 list-style-none">
                                                        <li v-for="(airline,index) in Object.keys(airlines)">
                                                            <label :for="`airline${index}`">
                                                            <input type="checkbox" name="airline" :value="airline" v-model="selectedAirline">
                                                                @{{ airline }}
                                                                <span class="float-right">(@{{ airlines[airline] }})</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <button class="btn-link collapsed" data-toggle="collapse"
                                                        data-target="#collapse3" aria-expanded="false"
                                                        aria-controls="collapseOne">
                                                    <p class="accordion-heading">Stops</p>
                                                    <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div id="collapse3" class="collapse" aria-labelledby="headingOne"
                                                 data-parent="#accordion" style="">
                                                <div class="card-body">
                                                    <ul class="p-0 m-0 list-style-none">
                                                        <li v-for="(stop,index) in Object.keys(stops)">
                                                            <label :for="`stopfor${index}`">
                                                            <input type="checkbox" name="stops" :value="stop" v-model="selectedStops">
                                                                @{{ stop > 0? `${stop} - ${stop} ${stop > 1? 'Stops': 'Stop'}`: '0 - Direct'  }}
                                                                <span class="float-right">(@{{ stops[stop] }})</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-9 col-12 pl-0 pr-0">
                        <div class="overflow-div">

                            <table-header @onsort="sortChange"></table-header>

{{--                        @foreach($data as $key)--}}

{{--                            @foreach($key['flights'] as $flight)--}}

                                <form action="{{ route('website.booking') }}" method="post"
                                      v-for="(key,x) in airlineKeys" v-cloak
                                >
                                @csrf
                                    <input type="hidden" value="{{json_encode(request()->all())}}" name="request">
                                    <div class="ticket-div">
                                        <div class="ticket-info-div"
                                             v-for="(flight,flightIndex) in flights[key].ongoing" :key="flightIndex">
                                            <p class="stopover-text ticket-checkbox">
                                                <input type="radio" v-model="flight.selected" :id="`${key}${flightIndex}`" @change="selectFlight(flight, flightIndex, key)" name="selectedFlight">
                                                <label :for="`${key}${flightIndex}`" class="bordered"></label>
                                            </p>
                                            <div class="flight-dest-time">
                                                <div class="text-center">
                                                    <img :src="`https://images.kiwi.com/airlines/64x64/${flight['operating_airline']}.png`" class="airline-img img-fluid" alt="">
                                                    <small style="display: block;" v-text="flight['operating_airline_name']['name']"></small>
                                                </div>
                                                <div class="ml-3 time-info">
                                                    <p class="site-text black-text time" v-text="flight['departure_time_formated'] + ' - ' + flight['arrival_time_formated']"></p>
                                                    <p class="site-text country" v-cloak>
                                                        @{{ flight['airport_departure']['city'] }}
                                                        (@{{ flight['airport_departure']['iata'] }})
                                                        to @{{ flight['airport_arrival']['city'] }}
                                                        (@{{ flight['airport_arrival']['iata'] }})</p>
                                                </div>
                                            </div>
                                            <div class="flight-stop">
                                                <p class="site-text">@{{ flight.flightCount > 1 ? 'Connecting' : 'Direct' }}</p>
                                                <p class="site-text">@{{ flight['airport_departure']['iata'] }} - @{{ flight['airport_arrival']['iata'] }}</p>
                                            </div>
{{--                                            @if(is_array($flight['baggage_allowance']))--}}
                                            <template v-if="Array.isArray(flight.baggage_allowance)">
{{--                                                @foreach($flight['baggage_allowance'] as $baggage)--}}
                                                    <div class="baggage-detail" v-for="baggage in flight.baggage_allowance">
                                                        <i class="fa fa-briefcase baggage-icon"
                                                           aria-hidden="true"></i>
                                                        <div class="baggage-info">
                                                            <p class="site-text">@{{ baggage['@attributes']['Value'] + ' ' + baggage['@attributes']['Unit'] }}</p>
                                                        </div>
                                                    </div>
{{--                                                @endforeach--}}
                                            </template>
{{--                                            @else--}}
                                            <div class="baggage-detail" v-else>
                                                <i class="fa fa-briefcase baggage-icon" aria-hidden="true"></i>
                                                <div class="baggage-info">
                                                    <p class="site-text" v-if="flight['baggage_allowance']">@{{ flight['baggage_allowance']['@attributes']['Value'] + ' ' + flight['baggage_allowance']['@attributes']['Unit'] }}</p>
                                                </div>
                                            </div>
{{--                                            @endif--}}
                                            <div class="ticket-endpart">
                                                <!-- <div class="endpart-info"> -->
                                                    <p class="site-text">@{{ convertToHoursMins(flight['duration']) }}</p>
                                                    <p class="site-text">@{{ flight['air_equip_name'] }}</p>
                                                <!-- </div> -->
                                                <!-- <i class="fa fa-chevron-down" aria-hidden="true"></i> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ticket-div-total">
                                        <div class="total-flight-cost">
                                            <p class="price" v-if="selectedAirlineKey !== key">$0.00</p>
                                            <p class="price" v-if="selectedAirlineKey === key && selectedFlight">@{{ '$' + parseInt(selectedFlight['fare']['total_fare_amount']).toFixed(2) }}</p>
                                            <p class="site-text black-text">{{ __('Total Amount') }}</p>
                                        </div>
                                        <input type="hidden" name="flight_details" :value="JSON.stringify(selectedFlight)"/>
                                        <input type="hidden" name="departure" :value="JSON.stringify(selectedFlight)"/>
                                        <input type="hidden" name="arrival" :value="JSON.stringify(selectedFlight)"/>
                                        <input type="hidden" name="session_id" value="{{ $session_id }}">
                                        <button type="submit" class="site-btn red">{{ __('Select') }}</button>
                                    </div>
                                    <div class="ticket-div mt-4"></div>
                                </form>

{{--                            @endforeach--}}

{{--                        @endforeach--}}
                    </div>
                    {{--@if($details['total_pages'] > 1)
                        <div class="pagination-div">
                            <ul class="site-ul pagination-ul">
                                @for($i = 1; $i <= $details['total_pages']; $i++)
                                    <li class="pagination-li">
                                        <a class="{{ $details['current_page'] == $i ? 'active' : '' }}"
                                           href="{{ Request::fullUrl() }}&search_id={{ $details['search_session_id'] }}&page_no={{ $i }}">{{ $i }}</a>
                                    </li>
                                @endfor
                            </ul>
                        </div>
                    @endif--}}
                    </div>
                </div>
            </div>

            </one-way-listing-index>
        </section>
    </section>
@endsection
@push('css')
    <style>
        span.irs-grid-text { display: none; }
        span.irs-from, span.irs-to, span.irs-single {display: none;}
        .timeContainer { display: flex;justify-content: space-between;position: relative;top: -15px; }
        .timeContainer > span { font-size: 14px;color: #a3a3a3; }
    </style>
@endpush
@push('js')
    <script src="{{ asset('js/app.js') }}"></script>
@endpush
