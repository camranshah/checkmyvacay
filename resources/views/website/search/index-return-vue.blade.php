@extends('website.layouts.app', ['Title' => 'Search Results'])

@section('content')
    <div id="app">
    <router-view></router-view>

    </div>

@endsection
@push('css')
    <style>
        .ticket-div.plane-land::after {
            background: #e5e5e5 url({{ asset('website/img/plane-land.png') }}) no-repeat center;
        }

        .ticket-div.plane-takeoff::after {
            background: #e5e5e5 url({{ asset('website/img/plane-takeoff.png') }}) no-repeat center;
        }

        .list-style-none {
            list-style: none;
        }

        .list-style-none label {
            display: block;
        }

        .list-style-none input[type=checkbox] {
            position: inherit;
        }
        .vue-histogram-slider-wrapper {
            --handle-size: 15px !important;
        }

        span.irs-grid-text { display: none; }
        span.irs-from, span.irs-to, span.irs-single {display: none;}
        .timeContainer { display: flex;justify-content: space-between;position: relative;top: -15px; }
        .timeContainer > span { font-size: 14px;color: #a3a3a3; }
    </style>
@endpush
@push('js')
    <script src="{{ asset('js/app.js') }}"></script>
@endpush
