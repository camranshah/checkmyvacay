@extends('website.layouts.app', ['Title' => 'Search Results'])

@section('content')

{{--    @dump($data[0]['flights'])--}}
{{--    @dump(get_defined_vars()['__data'])--}}

    <section class="inner-page-pd">
        <div class="container">
            <ul class="site-ul ul-flex-center flight-overview-ul">

                <li>
                    <div class="flight-overview-single">
                        <i class="fas fa-plane-departure drop-icon"></i>
                        <div class="plane-side-text">
                            <p class="site-text black-text">{{ @$_GET['from'][0] }} to {{ @$_GET['to'][0] }}</p>
                            <p class="site-text">{{ @$_GET['departure_date'][0] }}</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flight-overview-single">
                        <i class="fas fa-user drop-icon"></i>
                        <div class="plane-side-text">
                            <p class="site-text black-text">Passenger (s)</p>
                            <p class="site-text">({{ @$_GET['adult'] }}) Adult ({{ @$_GET['child'] }}) Child
                                ({{ @$_GET['infant'] }}) Infant</p>
                        </div>
                    </div>
                </li>
                @if(isset($_GET['cabin']))
                    <li>
                        <div class="flight-overview-single">
                            <img src="{{ asset('website/img/cabin-icon.png') }}" class="drop-icon img-fluid" alt="">
                            <div class="plane-side-text">
                                <p class="site-text black-text">Cabin</p>
                                <p class="site-text">{{ @$_GET['cabin'] }}</p>
                            </div>
                        </div>
                    </li>
                @endif
                <li>
                    <a href="#" class="site-btn red">MODIFY</a>
                </li>
            </ul>
        </div>
        <section class="light-blue-bg-section">
            <div class="container">
                <div class="row ml-0 mr-0">
                    <div class="col-12 pl-0 pr-0">
                        <div class="row ml-0 mr-0">
                            <div class="col-lg-3 col-12 pl-0 pr-0">

                            </div>
                            <div class="col-lg-9 col-12 pl-0 pr-0">
                                <p class="extra-text white-text"><i class="fas fa-plane-departure drop-icon"></i>433
                                    Flights Found</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-12 left-col-spacing pl-0 pr-0">
                        <div class="separate-div">
                            <p class="site-text text-center mb-3">
                                All Flights are displayed in Austrailian Dollars (AUD). in Local time (24hr Format)
                            </p>
                            <div class="d-flex align-items-center justify-content-center">
                                <i class="fa fa-filter filter" aria-hidden="true"></i>
                                <div>
                                    <p class="site-text red-text text-capitalize">Filter your result</p>
                                    <p class="site-text">0 Filter(s) Active</p>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs result-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab"
                                   href="#home" role="tab" aria-controls="home"
                                   aria-selected="true">{{ @$_GET['from'][0] }} &gt; {{ @$_GET['to'][0] }}</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="result-side-col">
                                    <div id="accordion" class="site-accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <button class="btn-link collapsed" data-toggle="collapse"
                                                        data-target="#collapseOne"
                                                        aria-expanded="false" aria-controls="collapseOne">
                                                    <p class="accordion-heading">Cheapest time to fly</p>
                                                    <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                                 data-parent="#accordion" style="">
                                                <div class="card-body">
                                                    <p class="accordion-heading mb-2">Departure</p>
                                                    <div class="range-slider-div position-relative">
                                                        <div class="graph"></div>
                                                        <div id="demo_1" class="range-slider py-3"></div>
                                                    </div>
                                                    <p class="accordion-heading mt-3 mb-2 border-top pt-2">Return</p>
                                                    <div class="range-slider-div position-relative">
                                                        <div class="graph"></div>
                                                        <div id="demo_2" class="range-slider py-3"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <button class="btn-link collapsed" data-toggle="collapse"
                                                        data-target="#collapse2" aria-expanded="false"
                                                        aria-controls="collapseOne">
                                                    <p class="accordion-heading">Airports</p>
                                                    <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div id="collapse2" class="collapse" aria-labelledby="headingOne"
                                                 data-parent="#accordion" style="">
                                                <div class="card-body">
                                                    <p class="accordion-heading mb-2">Departure</p>
                                                    <div id="demo_1" class="range-slider py-3"></div>
                                                    <p class="accordion-heading mt-3 mb-2 border-top pt-2">Return</p>
                                                    <div id="demo_2" class="range-slider py-3"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <button class="btn-link collapsed" data-toggle="collapse"
                                                        data-target="#collapse3" aria-expanded="false"
                                                        aria-controls="collapseOne">
                                                    <p class="accordion-heading">Stops</p>
                                                    <i class="fa fa-chevron-up accordion-arrow" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div id="collapse3" class="collapse" aria-labelledby="headingOne"
                                                 data-parent="#accordion" style="">
                                                <div class="card-body">
                                                    <p class="accordion-heading mb-2">Departure</p>
                                                    <div id="demo_1" class="range-slider py-3"></div>
                                                    <p class="accordion-heading mt-3 mb-2 border-top pt-2">Return</p>
                                                    <div id="demo_2" class="range-slider py-3"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-9 col-12 pl-0 pr-0">
                        <div class="overflow-div">
                        <ul class="site-ul ul-flex-center filter-category-ul">
                            <li>
                                <div class="filter-category-single">
                                    <p class="site-text text-uppercase">airlines</p>
                                    <i class="fa fa-sort sort-icon" aria-hidden="true"></i>
                                </div>
                            </li>
                            <li>
                                <div class="filter-category-single">
                                    <p class="site-text text-uppercase">departure</p>
                                    <i class="fa fa-sort sort-icon" aria-hidden="true"></i>
                                </div>
                            </li>
                            <li>
                                <div class="filter-category-single">
                                    <p class="site-text text-uppercase">stops</p>
                                    <i class="fa fa-sort sort-icon" aria-hidden="true"></i>
                                </div>
                            </li>
                            <li>
                                <div class="filter-category-single">
                                    <p class="site-text text-uppercase">arrival</p>
                                    <i class="fa fa-sort sort-icon" aria-hidden="true"></i>
                                </div>
                            </li>
                            <li>
                                <div class="filter-category-single">
                                    <p class="site-text text-uppercase">price</p>
                                    <i class="fa fa-sort sort-icon" aria-hidden="true"></i>
                                </div>
                            </li>
                            <li>
                                <div class="filter-category-single">
                                    <p class="site-text text-uppercase">duration</p>
                                    <i class="fa fa-sort sort-icon" aria-hidden="true"></i>
                                </div>
                            </li>
                        </ul>
                        @foreach($data as $key)
                            <form action="{{ route('website.booking') }}" method="post">
                                @csrf
                                @foreach($key['flights'] as $flight)
                                    <div class="ticket-div">
                                        <div class="ticket-info-div">
                                            <!-- <p class="stopover-text ticket-checkbox">
                                                <input type="checkbox" id="aaa" name="radio-group">
                                                <label for="aaa" class="bordered"></label>
                                            </p> -->
                                            <div class="flight-dest-time">
                                                <div class="text-center">
                                                    <img src="https://images.kiwi.com/airlines/64x64/{{$flight['operating_airline']}}.png" class="airline-img img-fluid" alt="">
                                                    <small style="display: block;">{{$flight['operating_airline_name']['name']}}</small>
                                                </div>
                                                <div class="ml-3 time-info">
                                                    <p class="site-text black-text time">{{ $flight['departure_time_formated'] }}
                                                        - {{ $flight['arrival_time_formated'] }}</p>
                                                    <p class="site-text country">{{ @$flight['airport_departure']['city'] }}
                                                        ({{ @$flight['airport_departure']['iata'] }})
                                                        to {{ @$flight['airport_arrival']['city'] }}
                                                        ({{ @$flight['airport_arrival']['iata'] }})</p>
                                                </div>
                                            </div>
                                            <div class="flight-stop">
                                                <p class="site-text">{{ @count($key['flights']) > 1 ? 'Connecting' : 'Direct' }}</p>
                                                <p class="site-text">{{ @$flight['airport_departure']['iata'].' - '.@$flight['airport_arrival']['iata'] }}</p>
                                            </div>
                                            @if(is_array($flight['baggage_allowance']))
                                                @foreach($flight['baggage_allowance'] as $baggage)
                                                    <div class="baggage-detail">
                                                        <i class="fa fa-briefcase baggage-icon"
                                                           aria-hidden="true"></i>
                                                        <div class="baggage-info">
                                                            <p class="site-text">{{ @$baggage['@attributes']['PaxType'] }} {{ @$baggage['@attributes']['Value'].' '.@$baggage['@attributes']['Unit']  }}</p>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="baggage-detail">
                                                    <i class="fa fa-briefcase baggage-icon" aria-hidden="true"></i>
                                                    <div class="baggage-info">
                                                        <p class="site-text">{{ @$flight['baggage_allowance']['@attributes']['PaxType'] }} {{ @$flight['baggage_allowance']['@attributes']['Value'].' '.@$flight['baggage_allowance']['@attributes']['Unit']  }}</p>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="ticket-endpart">
                                                <!-- <div class="endpart-info"> -->
                                                    <p class="site-text">{{ @convertToHoursMins($flight['duration']) }}</p>
                                                    <p class="site-text">{{ $flight['operating_airline'].' '.$flight['flight_number'] }}</p>
                                                <!-- </div> -->
                                                <!-- <i class="fa fa-chevron-down" aria-hidden="true"></i> -->
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="ticket-div-total">
                                    <div class="total-flight-cost">
                                        <p class="price">{{ $key['fare']['base_fare_currency_code'].' '.$key['fare']['base_fare_amount'] }}</p>
                                        <p class="site-text black-text">{{ __('Total Amount') }}</p>
                                    </div>
                                    <input type="hidden" name="flight_details" value="{{ serialize($key) }}"/>
                                    <button type="submit" class="site-btn red">{{ __('Select') }}</button>
                                </div>
                                <div class="ticket-div mt-4"></div>
                            </form>
                        @endforeach
                        </div>
                    </div>
                    @if($details['total_pages'] > 1)
                        <div class="pagination-div">
                            <ul class="site-ul pagination-ul">
                                @for($i = 1; $i <= $details['total_pages']; $i++)
                                    <li class="pagination-li">
                                        <a class="{{ $details['current_page'] == $i ? 'active' : '' }}"
                                           href="{{ Request::fullUrl() }}&search_id={{ $details['search_session_id'] }}&page_no={{ $i }}">{{ $i }}</a>
                                    </li>
                                @endfor
                            </ul>
                        </div>
                    @endif
                </div>
            </div>

        </section>
    </section>
@endsection
