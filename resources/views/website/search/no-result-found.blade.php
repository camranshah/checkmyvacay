@extends('website.layouts.app', ['Title' => 'Search Results'])

@section('content')

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="jumbotron text-center">
                  <h5 class="text-center">No flight found as per the given criteria.</h5>
                  <hr class="my-4">
                  <a class="site-btn red btn-lg" href="{{ url('/') }}" role="button">Go Back</a>
                </div>
            </div>
        </div>
    </div>

@endsection
