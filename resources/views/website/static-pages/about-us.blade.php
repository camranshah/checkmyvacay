@extends('website.layouts.app', ['Title' => 'About Us'])

@section('content')
    <section class="travel-insurance about">
        <div class="container">
            <div class="row ml-0 mr-0">
                <div class="col-md-6 col-12 pl-0 pr-0">
                    <h1 class="banner-mainheading dark-blue-text text-capitalize mb-4">About Us</h1>
                    <p class="site-text mb-2">
                        CheckMyFares.com is an Australian owned and operated online travel company, ABN 58 628 811 521.
                    </p>
                    <p class="site-text mb-2">
                        We utilise industry leading technology to offer customers a comprehensive and simple flight booking tool, to access great value airfares. Our customers can compare and book flights from all major airlines worldwide at a great price, with piece of mind.
                    </p>
                    <p class="site-text mb-2">
                        CheckMyFares is ATAS accredited (A15782), by the Australian Federation of Travel Agents (AFTA) and is accredited by IATA to promote and sell international air passenger transportation (HE 02365694).
                    </p>
                    <p class="site-text">
                        Customers have complete control of their booking process, whether it’s a simple domestic flight or a complex international itinerary with stop overs, multiple destinations and even dual one-way flights.
                    </p>
                    <div class="text-left mt-3">
                        <a href="" class="site-btn blue">read more</a>
                    </div>
                </div>
                <div class="col-md-6 col-12 pl-0 pr-0 text-center">
                    <img src="{{ asset('website/img/travel-man.png') }}" class="travel-man img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="alternate-img">
        <div class="row ml-0 mr-0">
            <div class="col-md-6 col-12 plane-img1">

            </div>
            <div class="col-md-6 col-12 text-center side-text-col">
                <h3 class="about-heading text-center mb-3">what is check my fares?</h3>
                <p class="site-text black-text text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </div>
        </div>
        <div class="row ml-0 mr-0">
            <div class="col-md-6 col-12 side-text-col">
                <h3 class="about-heading text-center mb-3">our mission</h3>
                <p class="site-text black-text text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                <ul class="site-ul about-ul">
                    <li>
                        Lorem ipsum dolor sit amet, consectetur
                    </li>
                    <li>
                        Adipisicing elit, sed do eiusmod tempor
                    </li>
                    <li>
                        Incididunt ut labore et dolore magna
                    </li>
                    <li>
                        Adipisicing elit, sed do eiusmod tempor
                    </li>
                    <li>
                        Incididunt ut labore et dolore magna
                    </li>
                    <li>
                        Incididunt ut labore et dolore magna
                    </li>
                    <li>
                        Adipisicing elit, sed do eiusmod tempor
                    </li>
                    <li>
                        Adipisicing elit, sed do eiusmod tempor
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-12 order-first order-md-last plane-img2">

            </div>
        </div>
    </section>
    <section class="best-app about">
        <div class="container">
            <div class="row ml-0 mr-0 align-items-center">
                <div class="col-md-6 col-12 center-then-left">
                    <img src="{{ asset('website/img/mobile-img.png') }}" class="mobile-img img-fluid" alt="">
                </div>
                <div class="col-md-6 col-12">
                    <h1 class="banner-mainheading text-capitalize">The best flight <span class="stroke-blue">app</span></h1>
                    <h1 class="banner-mainheading text-capitalize">for your <span class="stroke-blue">mobile</span> app.</h1>
                    <p class="site-text white-text mt-4">
                        Lorem Ipsum veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.
                    </p>
                    <img src="{{ asset('website/img/stores-availabe.png') }}" class="store-img img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>
@endsection
