<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::namespace('api')->group(function () {
//
//});

Route::namespace('api')->group(function () {

    Route::post('/login', 'LoginController@login');
    Route::post('/register', 'LoginController@register');
    Route::post('/forget-password', 'LoginController@forgetPassword');
    Route::post('/verify-token', 'LoginController@verifyToken');
    Route::get('/signup/activate/{token}', 'LoginController@signupActivate');
    Route::post('/signup/activate/resend', 'LoginController@resendSignupActivateEmail');

    Route::post('/get-flight-details', 'FlightController@getFlightDetails');
    Route::post('/check-flight', 'FlightController@checkFlight');
    Route::get('/settings', 'SettingsController@index');

    Route::get('/cms', 'CMSController@index');
    Route::get('/flash-sale', 'FlashSaleController@index');
    Route::get('/airports', 'AirportController@index');
    Route::get('/filters', 'GeneralController@getFilters');

    Route::post('/contact', 'ContactController@store');

//    Route::post('/verify-account', 'LoginController@verifyAccount');
    Route::get('/email-test/{session_id}', function(){

        // \App\FlightSession::truncate();

        $session = \App\FlightSession::whereSessionId(request('session_id'))->with('booking')->first();
        
        // dd($session->booking->detail);

        \Mail::to('arif.iqbal@salsoft.net')
            ->send(new App\Mail\BookingEmail($session));

        return $session;
    });

    Route::middleware('auth:api')->group(function () {
        Route::post('/update-password', 'LoginController@updatePassword');
        //User routes start
//        Route::post('/account-payment', 'UserController@accountPayment');
//        Route::post('/frequent-flyer-detail', 'UserController@frequentFlyerDetail');
        Route::match(['get', 'post'], '/profile', 'UserController@profile');
        Route::post('/changepassword', 'UserController@changePassword');
        //User routes end

        //Book flight
        Route::post('/book-flight', 'FlightController@bookFlight');
        Route::post('/make-payment', 'FlightController@makePayment');

        Route::get('/booking-log', 'FlightController@bookingLog');
        Route::post('/booking/detail', 'FlightController@bookingDetail');
        Route::get('/payment-log', 'FlightController@paymentLog');

        Route::post('/logout', 'LoginController@logout');

        Route::post('/frequent-flyer', 'FrequentFlyerController@create');
        Route::put('/frequent-flyer', 'FrequentFlyerController@update');
        Route::delete('/frequent-flyer', 'FrequentFlyerController@delete');

        Route::get('/booking/list', 'BookingController@index');
        Route::get('/booking/{id}', 'BookingController@show');

        Route::post('/payment/token', 'UserPaymentTokenController@create');
        Route::put('/payment/token', 'UserPaymentTokenController@update');
        Route::delete('/payment/token', 'UserPaymentTokenController@delete');

        Route::post('/payment/subscription', 'PaymentController@subscription');
        Route::post('/payment/booking', 'PaymentController@booking');

        Route::get('/notifications', 'NotificationController@index');
        Route::put('/notifications', 'NotificationController@update');
        Route::delete('/notifications', 'NotificationController@destroy');

        Route::post('/promo-code','PromoCodeController@index');


    });

    Route::get('/bk-notif-test', function () {
        $user = \App\User::find(26);
        $booking = \App\Booking::find(122);
        $user->notify(new \App\Notifications\Booking($user, $booking));
    });

    Route::get('/dump-airport', function (Request $request) {

//        $fileName = 'http://dev68.onlinetestingserver.com/checkmyvacay/public/airport_list20.csv';
//        $csvData = file_get_contents($fileName);
//        $lines = explode(PHP_EOL, $csvData);
//        $array = array();
//        foreach ($lines as $line) {
//            $array[] = str_getcsv($line);
//        }
//        unset($array[0]);
//
//        $airports = [];
//
//        foreach ($array as $arr) {
//            $airports[] = ['iata' => @$arr[0], 'city' => @$arr[1], 'country' => @$arr[3], 'name' => @$arr[2], 'fly_now' => @$arr[4], 'sort' => @$arr[5], 'type' => @$arr[6], 'sub_type' => @$arr[7]];
//        }
//
//        unset($airports[count($airports) - 1]);
//
//        //return $airports;
//
//        foreach ($airports as $airport) {
//            \App\Airport::updateOrCreate(['iata' => $airport['iata']],
//                [
//                    'iata' => $airport['iata'],
//                    'fly_now' => $airport['fly_now'],
//                    'sort' => $airport['sort'],
//                    'city' => $airport['city'],
//                    'country' => $airport['country'],
//                    'name' => $airport['name'],
//                    'type' => $airport['type'],
//                    'sub_type' => $airport['sub_type'],
//                ]);
//        }
//        return $airports;

    });
});

