<?php

/*
|
| Web Routes
|
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/web/airports', \web\AirportController::class . '@index');

Route::group(['prefix' => 'admin', 'namespace' => 'web'], function () {

    Route::any('{any}', function(){
        header('Location: https://checkmyfares.com/admin/login');
        exit();
    })->where('any', '.*');

    Route::prefix('login')->name('login.')->group(function () {
        Route::get('/', function () {
            return view('auth.login-admin');
        });
    });

    Route::middleware(['auth', 'isAdmin'])->group(function () {

        Route::prefix('booking-stats')->name('booking-stats.')->group(function () {
            Route::get('/', BookingStatsController::class . '@index')->name('index');
        });

        Route::prefix('notification')->name('notification.')->group(function () {
            Route::get('/', NotificationController::class . '@index')->name('index');
            Route::post('/', NotificationController::class . '@update')->name('update');
            Route::get('/store', NotificationController::class . '@store')->name('store');
        });

        Route::prefix('broadcast-message')->name('broadcast-message.')->group(function () {
            Route::get('/', BroadcastMessageController::class . '@index')->name('index');
            Route::post('/', BroadcastMessageController::class . '@store')->name('store');
        });

        Route::prefix('setting')->name('setting.')->group(function () {
            Route::get('/', SettingController::class . '@index')->name('index');
            Route::post('/', SettingController::class . '@update')->name('update');
        });

        Route::prefix('home')->name('home.')->group(function () {
            Route::get('/', DashboardController::class . '@index')->name('index');
        });

        Route::prefix('dashboard')->name('dashboard.')->group(function () {
            Route::get('/', DashboardController::class . '@index')->name('index');
        });

        Route::get('/', function(){
            return redirect()->route('dashboard.index');
        });

        Route::prefix('profile')->name('profile.')->group(function () {
            Route::get('/', ProfileController::class . '@index')->name('index');
            Route::post('/', ProfileController::class . '@update')->name('update');
        });

        Route::prefix('user')->name('user.')->group(function () {
            Route::get('/', UserController::class . '@index')->name('index');
            Route::get('/{id}', UserController::class . '@show')->name('show');
            Route::post('/', UserController::class . '@update')->name('update');
            Route::post('/edit', UserController::class . '@edit')->name('edit');
            Route::get('/{id}/booking-log', BookingLogController::class . '@index')->name('booking-log.index');
            Route::get('/{id}/subscription-log', SubscriptionLogController::class . '@index')->name('subscription-log.index');
        });

        Route::prefix('flash-sale')->name('flash-sale.')->group(function () {
            Route::get('/', FlashSaleController::class . '@index')->name('index');
            Route::get('/create', FlashSaleController::class . '@create')->name('create');
            Route::get('/{id}', FlashSaleController::class . '@show')->name('show');
            Route::post('/', FlashSaleController::class . '@update')->name('update');
            Route::post('/store', FlashSaleController::class . '@store')->name('store');
        });

        Route::prefix('promo-code')->name('promo-code.')->group(function () {
            Route::get('/', PromoCodeController::class . '@index')->name('index');
            Route::get('/create', PromoCodeController::class . '@create')->name('create');
            Route::get('/{id}', PromoCodeController::class . '@show')->name('show');
            Route::get('/{code}/edit', PromoCodeController::class . '@edit')->name('edit');
            Route::put('/{code}', PromoCodeController::class . '@updatePromoCode')->name('updatePromoCode');
            Route::post('/', PromoCodeController::class . '@update')->name('update');
            Route::post('/store', PromoCodeController::class . '@store')->name('store');
        });

        Route::prefix('referral')->name('referral.')->group(function () {
            Route::get('/', ReferralController::class . '@index')->name('index');
            Route::get('/create', ReferralController::class . '@create')->name('create');
            Route::post('/', ReferralController::class . '@store')->name('store');
        });

        Route::prefix('feedback')->name('feedback.')->group(function () {
            Route::get('/', FeedbackController::class . '@index')->name('index');
            Route::get('/{id}', FeedbackController::class . '@show')->name('show');
        });

        Route::prefix('payment-log')->name('payment-log.')->group(function () {
            Route::get('/', PaymentLogController::class . '@index')->name('index');
            Route::get('/{id}', PaymentLogController::class . '@show')->name('show');
        });

        Route::prefix('booked-flight')->name('booked-flight.')->group(function () {
            Route::get('/', BookedFlightController::class . '@index')->name('index');
            Route::get('/{id}', BookedFlightController::class . '@show')->name('show');
        });

        Route::prefix('report')->name('report.')->group(function () {
            Route::get('/{type}', ReportController::class . '@index')->name('index');
        });

        Route::prefix('api-log')->name('api-log.')->group(function () {
            Route::get('/', ApiLogController::class . '@index')->name('index');
        });
    });
});


//Route::namespace('admin')->group(function () {
//    Route::get('/', 'LoginController@login')->name('home');
//    Route::post('/login', 'LoginController@login')->name('login');
//    Route::match(['get','post'],'/forget-password', 'LoginController@forgetPassword')->name('forget.password');
//    Route::match(['get','post'],'/recover-password/{token}', 'LoginController@recoverPassword')->name('recover.password');
//});
//
//Route::group(['namespace' => 'admin','middleware' => 'auth'],function(){
//    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
//    Route::match(['get','post'],'/changepassword', 'DashboardController@changePassword')->name('changepassword');
//    Route::match(['get','post'],'/editprofile', 'DashboardController@editProfile')->name('edit.profile');
//    Route::get('/logout', 'DashboardController@logout')->name('logout');
//
//    Route::group(['prefix' => '/user','as' => 'user.'],function(){
//        Route::get('/view/{id?}', 'UserController@view')->name('view');
//        Route::get('/change-status/{id}', 'UserController@changeStatus')->name('change-status');
//    });
//
//    Route::group(['prefix' => '/booking','as' => 'booking.'],function(){
//        Route::get('/view/{id?}', 'BookingController@view')->name('view');
//    });
//
//    Route::group(['prefix' => '/payment','as' => 'payment.'],function(){
//        Route::get('/account', 'PaymentController@accountPayments')->name('account');
//        Route::get('/flight', 'PaymentController@bookingPayments')->name('flight');
//    });
//
//    Route::match(['get','post'],'/commission', 'CommissionController@commission')->name('commission');
//
//    Route::group(['prefix' => '/exchange-rate','as' => 'exchange.rate.'],function(){
//        Route::get('/view', 'ExchangeRateController@view')->name('view');
//        Route::match(['get','post'],'/add', 'ExchangeRateController@add')->name('add');
//        Route::match(['get','post'],'/update/{id}', 'ExchangeRateController@update')->name('update');
//    });
//
////    Route::group(['prefix' => '/report','as' => 'report.'],function(){
////        Route::get('/account', 'ReportController@accountPayments')->name('account');
////        Route::get('/flight', 'ReportController@bookingPayments')->name('flight');
////    });
//});

Route::namespace('website')->name('website')->group(function () {

//    Route::get('/youtube', 'YoutubeController@index')->name('.youtube');
//    Route::post('/youtube', 'YoutubeController@store')->name('.youtube.store');
//    Route::get('/facebook', 'FacebookController@index')->name('.facebook');
//    Route::post('/facebook', 'FacebookController@store')->name('.facebook.store');

    Route::get('/about-us', 'StaticPagesController@aboutUs')->name('.about-us');
    Route::get('/customer-support', 'StaticPagesController@customerSupport')->name('.customer-support');
    Route::get('/faqs', 'StaticPagesController@faqs')->name('.faqs');
    Route::get('/terms-and-conditions', 'StaticPagesController@termsAndConditions')->name('.terms-and-conditions');
    Route::get('/privacy-policy', 'StaticPagesController@privacyPolicy')->name('.privacy-policy');
    Route::get('/refund-policy', 'StaticPagesController@refundPolicy')->name('.refund-policy');

    Route::prefix('/contact-us')->name('.contact-us')->group(function () {
        Route::get('/', 'ContactUsController@index');
        Route::post('/', 'ContactUsController@store');
    });

    Route::prefix('/manage-booking')->name('.manage-booking')->group(function () {
        Route::get('/', 'ManageBookingController@index');
        Route::post('/', 'ManageBookingController@show');
    });

    Route::prefix('/booking')->name('.booking')->group(function () {
        Route::post('/', 'BookingController@create');
        Route::post('/provision', 'BookingController@provisionBooking')->name('.provision');
        Route::post('/confirm', 'BookingController@confirmBooking')->name('.confirm');
    });

    Route::prefix('/search-flight')->name('.search-flight')->group(function () {
        Route::get('/', 'SearchFlightController@index');
    });

    Route::get('/test-booking',function(){
        $data['booking'] = \App\Booking::find(110);
        return view('website.booking.complete', $data);
    });

    foreach (['/', '/home'] as $prefix):
        Route::prefix($prefix)->name('.home')->group(function () {
            Route::get('/', 'HomeController@index');
        });
    endforeach;

    Route::group(['middleware' => ['auth', 'isUser']], function () {

        Route::prefix('/profile')->name('.profile')->group(function () {
            Route::get('/', 'ProfileController@index');
            Route::post('/', 'ProfileController@update');
            Route::post('/change-password', 'ProfileController@changePassword')->name('.change-password');
        });

    });

});


// point # 8,11,27,33,52,55
